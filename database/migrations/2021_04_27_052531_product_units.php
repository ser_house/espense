<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Ramsey\Uuid\Uuid;

class ProductUnits extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('unit', function (Blueprint $table) {
      $table->uuid('id')->primary();
      $table->string('title');

      $table->index('title');
    });

    $default_unit_id = Uuid::uuid4();
    DB::insert("INSERT INTO unit VALUES ('$default_unit_id', 'ед.')");
    $init_units = [
      'л.',
      'кг.',
      'т.',
      'мешок',
      'шт.',
      'м2',
      'м3',
    ];
    $insert = [];
    foreach ($init_units as $unit) {
      $insert[] = [
        'id' => Uuid::uuid4(),
        'title' => $unit,
      ];
    }
    DB::table('unit')->insert($insert);

    Schema::table('expense', function (Blueprint $table) {
      $table->uuid('unit_id')->nullable()->default(null);

      $table->foreign('unit_id')
        ->references('id')->on('unit')
        ->onDelete('cascade')
        ->onUpdate('restrict');
    });

    DB::table('expense')->update(['unit_id' => $default_unit_id]);
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('expense', function (Blueprint $table) {
      $table->dropColumn('unit_id');
    });
    Schema::dropIfExists('unit');
  }
}
