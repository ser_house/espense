<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Init extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		Schema::create('category', function (Blueprint $table) {
			$table->uuid('id')->primary();
			$table->string('title');

			$table->index('title');
		});

		Schema::create('product', function (Blueprint $table) {
			$table->uuid('id')->primary();
			$table->string('title');
			$table->uuid('category_id')->nullable();

			$table->index('title');

			$table->foreign('category_id')
				->references('id')->on('category')
				->onDelete('cascade')
				->onUpdate('restrict');
		});

		Schema::create('contractor', function (Blueprint $table) {
			$table->uuid('id')->primary();
			$table->string('title')->unique();
			$table->string('url')->nullable();
		});

		Schema::create('target', function(Blueprint $table) {
			$table->uuid('id')->primary();
			$table->string('title')->unique();
			$table->boolean('is_active')->default(true);
			$table->boolean('is_default')->default(false);
		});

		Schema::create('expense', function (Blueprint $table) {
			$table->uuid('id')->primary();
			$table->date('date');
			$table->uuid('target_id');
			$table->uuid('product_id');
			$table->decimal('quantity');
			$table->decimal('amount');
			$table->text('note')->nullable();
			$table->uuid('contractor_id')->nullable();
			$table->boolean('verified')->default(false);

			$table->foreign('target_id')
				->references('id')->on('target')
				->onDelete('cascade')
				->onUpdate('restrict');

			$table->foreign('product_id')
				->references('id')->on('product')
				->onDelete('cascade')
				->onUpdate('restrict');

			$table->foreign('contractor_id')
				->references('id')->on('contractor')
				->onDelete('cascade')
				->onUpdate('restrict');

		});

		DB::statement("ALTER TABLE expense ADD COLUMN created_at TIMESTAMP(3) NOT NULL DEFAULT now()");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		$tables = [
			'expense',
			'contractor',
			'product',
			'category',
		];

		foreach ($tables as $table) {
			Schema::dropIfExists($table);
		}
	}
}
