<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ToTracking extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('to_tracking_expense', function (Blueprint $table) {
      $table->uuid('expense_id')->primary();

      $table->foreign('expense_id')
        ->references('id')->on('expense')
        ->onDelete('cascade')
        ->onUpdate('restrict')
      ;
    });
    Schema::create('tracked_product', function (Blueprint $table) {
      $table->uuid('product_id')->primary();

      $table->foreign('product_id')
        ->references('id')->on('product')
        ->onDelete('cascade')
        ->onUpdate('restrict')
      ;
    });

    $expense_ids = DB::table('tracking')
      ->select('expense_id')
      ->distinct()
      ->pluck('expense_id')
      ->all()
    ;
    $to_insert = [];
    foreach ($expense_ids as $id) {
      $to_insert[] = ['expense_id' => $id];
    }
    DB::table('to_tracking_expense')->insert($to_insert);

    $product_ids = DB::table('expense')
      ->select('product_id')
      ->whereIn('id', $expense_ids)
      ->distinct()
      ->pluck('product_id')
      ->all()
    ;
    $to_insert = [];
    foreach ($product_ids as $id) {
      $to_insert[] = ['product_id' => $id];
    }
    DB::table('tracked_product')->insert($to_insert);
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    $tables = [
      'to_tracking_expense',
      'tracked_product',
    ];

    foreach ($tables as $table) {
      Schema::dropIfExists($table);
    }
  }
}
