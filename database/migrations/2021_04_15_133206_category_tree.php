<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CategoryTree extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('category', function (Blueprint $table) {
      $table->uuid('parent_id')->nullable()->default(null);

      $table->foreign('parent_id')
        ->references('id')->on('category')
        ->onDelete('cascade')
        ->onUpdate('restrict');
    });

    $target = DB::select("SELECT id, title FROM target LIMIT 1");
    if ($target) {
      $id = $target[0]->id;
      $title = $target[0]->title;
      DB::table('category')->insert(['id' => $id, 'title' => $title]);
      DB::table('category')->where('title', '!=', $title)->update(['parent_id' => $id]);
    }

    Schema::table('expense', function (Blueprint $table) {
      $table->dropColumn('target_id');
    });
    Schema::drop('target');
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('category', function (Blueprint $table) {
      $table->dropColumn('parent_id');
    });
  }
}
