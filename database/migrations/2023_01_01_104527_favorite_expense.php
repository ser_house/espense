<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FavoriteExpense extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('favorite', function (Blueprint $table) {
      $table->uuid('expense_id')->primary();
    });
    $ids = DB::table('expense')->select('id')->where('is_typical', true)->pluck('id')->all();
    $to_insert = [];
    foreach ($ids as $id) {
      $to_insert[] = ['expense_id' => $id];
    }
    DB::table('favorite')->insert($to_insert);

    Schema::table('expense', function (Blueprint $table) {
      $table->dropColumn('is_typical');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('expense', function (Blueprint $table) {
      $table->boolean('is_typical')->default(false);
    });

    $ids = DB::table('favorite')->select('expense_id')->pluck('id')->toArray();
    foreach ($ids as $id) {
      DB::table('expense')->where('id', $id)->update(['is_typical' => true]);
    }
    Schema::dropIfExists('favorite');
  }
}
