<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductTracking extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('tracking', function (Blueprint $table) {
      $table->uuid('id')->primary();
      $table->uuid('expense_id');
      $table->decimal('quantity');
      $table->date('started_date');
      $table->date('finished_date')->nullable()->default(null);
      $table->integer('days')->nullable()->default(null);

      $table->foreign('expense_id')
        ->references('id')->on('expense')
        ->onDelete('cascade')
        ->onUpdate('restrict')
      ;
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    $tables = [
      'tracking',
    ];

    foreach ($tables as $table) {
      Schema::dropIfExists($table);
    }
  }
}
