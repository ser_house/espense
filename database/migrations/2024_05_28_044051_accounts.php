<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Ramsey\Uuid\Uuid;

class Accounts extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('account', function (Blueprint $table) {
      $table->uuid('id')->primary();
      $table->string('title');
      $table->uuid('contractor_id')->nullable()->default(null);

      $table->foreign('contractor_id')
        ->references('id')->on('contractor')
        ->onDelete('cascade')
        ->onUpdate('restrict')
      ;

      $table->index('title');
    });

    $defaultSrcAccount = new \App\Core\Account\DefaultSrcAccount();
    DB::insert("INSERT INTO account VALUES ('$defaultSrcAccount->id', '$defaultSrcAccount->title')");

    Schema::table('expense', function (Blueprint $table) {
      $table->uuid('account_id')->nullable();

      $table->foreign('account_id')
        ->references('id')->on('account')
        ->onDelete('cascade')
        ->onUpdate('restrict')
      ;
    });

    DB::table('expense')->update(['account_id' => $defaultSrcAccount->id]);

    Schema::table('expense', function (Blueprint $table) {
      $table->uuid('account_id')->nullable(false)->change();
    });


    Schema::create('transaction', function (Blueprint $table) {
      $table->uuid('id')->primary();
      $table->date('date');
      $table->uuid('account_src_id');
      $table->uuid('account_target_id');
      $table->decimal('amount');
      $table->text('note')->nullable();

      $table->foreign('account_src_id')
        ->references('id')->on('account')
        ->onDelete('cascade')
        ->onUpdate('restrict')
      ;

      $table->foreign('account_target_id')
        ->references('id')->on('account')
        ->onDelete('cascade')
        ->onUpdate('restrict')
      ;
    });

    DB::statement("ALTER TABLE transaction ADD COLUMN created_at TIMESTAMP(3) NOT NULL DEFAULT now()");
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('expense', function (Blueprint $table) {
      $table->dropColumn('account_id');
    });
    Schema::dropIfExists('transaction');
    Schema::dropIfExists('account');
  }
}
