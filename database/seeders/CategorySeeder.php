<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.11.2019
 * Time: 6:03
 */
class CategorySeeder extends Seeder {

  public const CATEGORY_CATEGORY = ['id' => 'a0a40a52-cea8-4a66-99ca-5fede70352e3', 'title' => 'Категория'];
  public const CATEGORY_STRATEGY = ['id' => '53b254d0-becd-4a3c-8620-78aa9743dbdb', 'title' => 'Стратегия'];


  public const DATA = [
    self::CATEGORY_CATEGORY,
    self::CATEGORY_STRATEGY,
  ];

  public function run() {

    $categories = [];
    foreach (self::DATA as $category) {
      $categories[] = $category;
    }

    DB::table('category')->insert($categories);
  }
}
