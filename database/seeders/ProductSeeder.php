<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.11.2019
 * Time: 6:03
 */
class ProductSeeder extends Seeder {

  public const PRODUCT1 = [
    'id' => '69b6c30a-eafd-4c06-9398-64c55f505e1f',
    'title' => 'Молоко',
    'category' => CategorySeeder::CATEGORY_CATEGORY,
  ];

  public const PRODUCT2 = [
    'id' => '43c7f22f-f0ea-4d70-824a-9e192c6a596f',
    'title' => 'Мокороны',
    'category' => CategorySeeder::CATEGORY_CATEGORY,
  ];

  public const PRODUCT3 = [
    'id' => '96a3bde4-c54a-42b8-b67c-74c805305dab',
    'title' => 'Кузькина падь',
    'category' => CategorySeeder::CATEGORY_CATEGORY,
  ];

  public const PRODUCT4 = [
    'id' => '5ef6dedd-2836-4b33-86f3-082359836747',
    'title' => 'Руны таво',
    'category' => CategorySeeder::CATEGORY_CATEGORY,
  ];

  public const PRODUCT5 = [
    'id' => '226c879a-b03b-4d31-87d6-ce45aad1769b',
    'title' => 'Кина и копыта',
    'category' => CategorySeeder::CATEGORY_STRATEGY,
  ];

  public const PRODUCT6 = [
    'id' => 'a18720bb-1b48-454e-a9ff-ece816b689bd',
    'title' => 'Борога без копытов',
    'category' => CategorySeeder::CATEGORY_STRATEGY,
  ];

  public const PRODUCT7 = [
    'id' => '3e9517af-a86f-4c78-96a2-4f8aaaf330d2',
    'title' => 'Уникальной нейм',
    'category' => CategorySeeder::CATEGORY_STRATEGY,
  ];

  public const PRODUCT8 = [
    'id' => 'bda5bb52-8268-4d31-bdd2-5de01802bd16',
    'title' => 'Товар',
    'category' => CategorySeeder::CATEGORY_STRATEGY,
  ];

  public const DATA = [
    self::PRODUCT1,
    self::PRODUCT2,
    self::PRODUCT3,
    self::PRODUCT4,
    self::PRODUCT5,
    self::PRODUCT6,
    self::PRODUCT7,
    self::PRODUCT8,
  ];

  public function run() {

    $products = [];
    foreach (self::DATA as $datum) {
      $datum['category_id'] = $datum['category']['id'];
      unset($datum['category']);
      $products[] = $datum;
    }

    DB::table('product')->insert($products);
  }
}
