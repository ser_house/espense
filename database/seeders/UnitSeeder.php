<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.11.2019
 * Time: 6:03
 */
class UnitSeeder extends Seeder {

  public const UNIT1 = [
    'id' => '69b6c30a-eafd-4c06-9398-64c55f505e1f',
    'title' => 'Ед.изм.1',
  ];
  public const UNIT2 = [
    'id' => '43c7f22f-f0ea-4d70-824a-9e192c6a596f',
    'title' => 'Ед.изм.2',
  ];
  public const UNIT3 = [
    'id' => '96a3bde4-c54a-42b8-b67c-74c805305dab',
    'title' => 'Ед.изм.3',
  ];

  public const DATA = [
    self::UNIT1,
    self::UNIT2,
    self::UNIT3,
  ];

  public function run() {
    DB::table('unit')->insert(self::DATA);
  }
}
