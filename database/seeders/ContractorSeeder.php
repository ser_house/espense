<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.11.2019
 * Time: 6:03
 */
class ContractorSeeder extends Seeder {

  public const CONTRACTOR1 = [
    'id' => '69b6c30a-eafd-4c06-9398-64c55f505e1f',
    'title' => 'Молоко',
  ];
  public const CONTRACTOR2 = [
    'id' => '43c7f22f-f0ea-4d70-824a-9e192c6a596f',
    'title' => 'Мокороны',
  ];
  public const CONTRACTOR3 = [
    'id' => '96a3bde4-c54a-42b8-b67c-74c805305dab',
    'title' => 'Кузькина падь',
  ];
  public const CONTRACTOR4 = [
    'id' => '5ef6dedd-2836-4b33-86f3-082359836747',
    'title' => 'Руны таво',
  ];
  public const CONTRACTOR5 = [
    'id' => '226c879a-b03b-4d31-87d6-ce45aad1769b',
    'title' => 'Кина и копыта',
  ];
  public const CONTRACTOR6 = [
    'id' => 'a18720bb-1b48-454e-a9ff-ece816b689bd',
    'title' => 'Борога без копытов',
  ];
  public const CONTRACTOR7 = [
    'id' => '3e9517af-a86f-4c78-96a2-4f8aaaf330d2',
    'title' => 'Уникальной нейм',
  ];
  public const CONTRACTOR8 = [
    'id' => 'bda5bb52-8268-4d31-bdd2-5de01802bd16',
    'title' => 'Товар',
  ];

  public const DATA = [
    self::CONTRACTOR1,
    self::CONTRACTOR2,
    self::CONTRACTOR3,
    self::CONTRACTOR4,
    self::CONTRACTOR5,
    self::CONTRACTOR6,
    self::CONTRACTOR7,
    self::CONTRACTOR8,
  ];

  public function run() {
    DB::table('contractor')->insert(self::DATA);
  }
}
