<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.11.2019
 * Time: 6:03
 */
class ExpenseSeeder extends Seeder {

  public function run() {

    $expense_with_unit = [
      'id' => Uuid::uuid4(),
      'date' => '2017-10-10',
      'product_id' => ProductSeeder::PRODUCT5['id'],
      'quantity' => 1.00,
      'unit_id' => UnitSeeder::UNIT1['id'],
      'amount' => 100.00,
      'contractor_id' => ContractorSeeder::CONTRACTOR2['id'],
    ];
    DB::table('expense')->insert([$expense_with_unit]);

    $expenses = [];
    $expenses[] = [
      'id' => Uuid::uuid4(),
      'date' => '2017-10-01',
      'product_id' => ProductSeeder::PRODUCT1['id'],
      'quantity' => 1.00,
      'amount' => 100.00,
      'contractor_id' => ContractorSeeder::CONTRACTOR1['id'],
    ];
    $expenses[] = [
      'id' => Uuid::uuid4(),
      'date' => '2017-10-02',
      'product_id' => ProductSeeder::PRODUCT1['id'],
      'quantity' => 1.00,
      'amount' => 100.00,
      'contractor_id' => ContractorSeeder::CONTRACTOR1['id'],
    ];
    $expenses[] = [
      'id' => Uuid::uuid4(),
      'date' => '2017-10-03',
      'product_id' => ProductSeeder::PRODUCT2['id'],
      'quantity' => 1.00,
      'amount' => 100.00,
      'contractor_id' => ContractorSeeder::CONTRACTOR1['id'],
    ];
    $expenses[] = [
      'id' => Uuid::uuid4(),
      'date' => '2017-10-04',
      'product_id' => ProductSeeder::PRODUCT2['id'],
      'quantity' => 1.00,
      'amount' => 100.00,
      'contractor_id' => ContractorSeeder::CONTRACTOR1['id'],
    ];
    $expenses[] = [
      'id' => Uuid::uuid4(),
      'date' => '2017-10-05',
      'product_id' => ProductSeeder::PRODUCT3['id'],
      'quantity' => 1.00,
      'amount' => 100.00,
      'contractor_id' => ContractorSeeder::CONTRACTOR1['id'],
    ];
    $expenses[] = [
      'id' => Uuid::uuid4(),
      'date' => '2017-10-06',
      'product_id' => ProductSeeder::PRODUCT3['id'],
      'quantity' => 1.00,
      'amount' => 100.00,
      'contractor_id' => ContractorSeeder::CONTRACTOR2['id'],
    ];
    $expenses[] = [
      'id' => Uuid::uuid4(),
      'date' => '2017-10-07',
      'product_id' => ProductSeeder::PRODUCT4['id'],
      'quantity' => 1.00,
      'amount' => 100.00,
      'contractor_id' => ContractorSeeder::CONTRACTOR2['id'],
    ];
    $expenses[] = [
      'id' => Uuid::uuid4(),
      'date' => '2017-10-08',
      'product_id' => ProductSeeder::PRODUCT4['id'],
      'quantity' => 1.00,
      'amount' => 100.00,
      'contractor_id' => ContractorSeeder::CONTRACTOR2['id'],
    ];
    $expenses[] = [
      'id' => Uuid::uuid4(),
      'date' => '2017-10-09',
      'product_id' => ProductSeeder::PRODUCT5['id'],
      'quantity' => 1.00,
      'amount' => 100.00,
      'contractor_id' => ContractorSeeder::CONTRACTOR2['id'],
    ];
    $expenses[] = [
      'id' => Uuid::uuid4(),
      'date' => '2017-10-10',
      'product_id' => ProductSeeder::PRODUCT5['id'],
      'quantity' => 1.00,
      'amount' => 100.00,
      'contractor_id' => ContractorSeeder::CONTRACTOR2['id'],
    ];

    DB::table('expense')->insert($expenses);
  }
}
