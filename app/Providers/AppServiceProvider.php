<?php

namespace App\Providers;

use App\Application\Analytics\DataProvider as AnalyticsDataProvider;
use App\Application\Category\DataProvider as CategoryDataProvider;
use App\Application\Category\Repository as CategoryRepository;
use App\Application\Contractor\DataProvider as ContractorDataProvider;
use App\Application\Contractor\Repository as ContractorRepository;
use App\Application\EventDispatcher;
use App\Application\Expense\Converter as ExpenseConverter;
use App\Application\Expense\DataProvider as ExpenseDataProvider;
use App\Application\Expense\Repository as ExpenseRepository;
use App\Application\Product\DataProvider as ProductDataProvider;
use App\Application\Product\Repository as ProductRepository;
use App\Application\Transaction;
use App\Application\Unit\DataProvider as UnitDataProvider;
use App\Core\Analytics\IDataProvider as IAnalyticsDataProvider;
use App\Core\Category\IDataProvider as ICategoryDataProvider;
use App\Core\Category\IRepository as ICategoryRepository;
use App\Core\Contractor\IDataProvider as IContractorDataProvider;
use App\Core\Contractor\IRepository as IContractorRepository;
use App\Core\Expense\IConverter as IExpenseConverter;
use App\Core\Expense\IDataProvider as IExpenseDataProvider;
use App\Core\Expense\IRepository as IExpenseRepository;
use App\Core\IEventDispatcher;
use App\Core\ITransaction;
use App\Core\Product\IDataProvider as IProductDataProvider;
use App\Core\Product\IRepository as IProductRepository;
use App\Core\Service\ICacheFlusher;
use App\Core\Service\StaticCacheFlusher;
use App\Core\Unit\IDataProvider as IUnitDataProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use App\Core\Unit\IRepository as IUnitRepository;
use App\Application\Unit\Repository as UnitRepository;
use App\Core\Analytics\ByTime\IByTimeDataProviderFactory;
use App\Application\Analytics\ByTime\ByTimeDataProviderFactory;
use App\Core\Tracking\IRepository as ITrackRepository;
use App\Application\Tracking\Repository as TrackRepository;
use App\Core\Tracking\IDataProvider as ITrackingDataProvider;
use App\Application\Tracking\DataProvider as TrackingDataProvider;

use App\Core\Account\IDataProvider as IAccountDataProvider;
use App\Core\Account\IRepository as IAccountRepository;
use App\Application\Account\DataProvider as AccountDataProvider;
use App\Application\Account\Repository as AccountRepository;

use App\Core\Transaction\IConverter as ITransactionConverter;
use App\Core\Transaction\IDataProvider as ITransactionDataProvider;
use App\Core\Transaction\IRepository as ITransactionRepository;
use App\Application\Transaction\Converter as TransactionConverter;
use App\Application\Transaction\DataProvider as TransactionDataProvider;
use App\Application\Transaction\Repository as TransactionRepository;

class AppServiceProvider extends ServiceProvider {
  /**
   * Register any application services.
   *
   * @return void
   */
  public function register() {
    $this->app->bind(IContractorRepository::class, ContractorRepository::class);
    $this->app->bind(IContractorDataProvider::class, ContractorDataProvider::class);

    $this->app->bind(ICategoryRepository::class, CategoryRepository::class);
    $this->app->bind(ICategoryDataProvider::class, CategoryDataProvider::class);

    $this->app->bind(IProductRepository::class, ProductRepository::class);
    $this->app->bind(IProductDataProvider::class, ProductDataProvider::class);

    $this->app->bind(IUnitDataProvider::class, UnitDataProvider::class);
    $this->app->bind(IUnitRepository::class, UnitRepository::class);

    $this->app->bind(ITrackRepository::class, TrackRepository::class);
    $this->app->bind(ITrackingDataProvider::class, TrackingDataProvider::class);

    $this->app->bind(IExpenseRepository::class, ExpenseRepository::class);
    $this->app->bind(IExpenseDataProvider::class, ExpenseDataProvider::class);
    $this->app->bind(IExpenseConverter::class, ExpenseConverter::class);

    $this->app->bind(IAnalyticsDataProvider::class, AnalyticsDataProvider::class);
    $this->app->bind(IByTimeDataProviderFactory::class, ByTimeDataProviderFactory::class);

    $this->app->bind(ICacheFlusher::class, StaticCacheFlusher::class);
    $this->app->bind(IEventDispatcher::class, EventDispatcher::class);
    $this->app->bind(ITransaction::class, Transaction::class);

    $this->app->bind(IAccountRepository::class, AccountRepository::class);
    $this->app->bind(IAccountDataProvider::class, AccountDataProvider::class);

    $this->app->bind(ITransactionRepository::class, TransactionRepository::class);
    $this->app->bind(ITransactionDataProvider::class, TransactionDataProvider::class);
    $this->app->bind(ITransactionConverter::class, TransactionConverter::class);
  }

  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot() {
    Blade::directive('spaceless', function () {
      return '<?php ob_start() ?>';
    });

    Blade::directive('endspaceless', function () {
      return "<?php echo preg_replace('/>\\s+</', '><', ob_get_clean()); ?>";
    });

    Blade::directive('nl2br', function ($expression) {
      return "<?php echo str_replace(PHP_EOL, '<br/>', e($expression)); ?>";
    });
  }
}
