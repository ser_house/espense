<?php

namespace App\Providers;

use App\Application\Category\Event\MovedToCategory as AppCategoryMovedToCategoryEvent;
use App\Application\Category\Listener\MovedToCategory as CategoryMovedToCategoryListener;
use App\Application\Expense\Event\Added as AppExpenseAddedEvent;
use App\Application\Expense\Event\Removed as AppExpenseRemovedEvent;
use App\Application\Expense\Event\Updated as AppExpenseUpdatedEvent;
use App\Application\Expense\Listener\Added as ExpenseAddedListener;
use App\Application\Expense\Listener\Removed as ExpenseRemovedListener;
use App\Application\Expense\Listener\Updated as ExpenseUpdatedListener;
use App\Application\Product\Event\MovedToCategory as AppProductMovedToCategoryEvent;
use App\Application\Product\Listener\MovedToCategory as ProductMovedToCategoryListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider {
  /**
   * The event listener mappings for the application.
   *
   * @var array
   */
  protected $listen = [
    AppExpenseAddedEvent::class => [
      ExpenseAddedListener::class,
    ],
    AppExpenseUpdatedEvent::class => [
      ExpenseUpdatedListener::class,
    ],
    AppExpenseRemovedEvent::class => [
      ExpenseRemovedListener::class,
    ],
    AppProductMovedToCategoryEvent::class => [
      ProductMovedToCategoryListener::class,
    ],
    AppCategoryMovedToCategoryEvent::class => [
      CategoryMovedToCategoryListener::class,
    ],
  ];

  /**
   * Register any events for your application.
   *
   * @return void
   */
  public function boot() {
    parent::boot();
  }
}
