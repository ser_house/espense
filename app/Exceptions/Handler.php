<?php

namespace App\Exceptions;

use App\Core\Exception\BusyEntityException;
use App\Core\Exception\ExistsException;
use App\Core\Exception\FailedToSaveEntityException;
use App\Core\Exception\MissingRequiredDataException;
use App\Core\Exception\NegativeAmountException;
use App\Core\Exception\NotEqualsAmountsException;
use App\Core\Exception\NotFoundEntityException;
use App\Core\Generic\ApiResponse\Error;
use App\Core\Generic\ApiResponse\Exists;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Throwable;


class Handler extends ExceptionHandler {
  /**
   * A list of the exception types that are not reported.
   *
   * @var array
   */
  protected $dontReport = [
    //
  ];

  /**
   * A list of the inputs that are never flashed for validation exceptions.
   *
   * @var array
   */
  protected $dontFlash = [

  ];

  /**
   * @inheritDoc
   */
  public function report(Throwable $e) {
    if ($e instanceof MissingRequiredDataException) {
      return;
    }

    if ($e instanceof FailedToSaveEntityException) {
      return;
    }

    if ($e instanceof NotFoundEntityException) {
      return;
    }

    if ($e instanceof NotEqualsAmountsException) {
      return;
    }

    if ($e instanceof NegativeAmountException) {
      return;
    }

    if ($e instanceof ExistsException) {
      return;
    }

    if ($e instanceof BusyEntityException) {
      return;
    }

    parent::report($e);
  }

  /**
   * @inheritDoc
   */
  public function render($request, Throwable $e) {
    if ($request->is('api/*')) {
      return $this->renderApiException($request, $e);
    }

    return parent::render($request, $e);
  }

  /**
   * @param $request
   * @param Throwable $exception
   *
   * @return JsonResponse
   */
  private function renderApiException($request, Throwable $exception) {
    if ($exception instanceof MissingRequiredDataException) {
      return response()->json(new Error($exception->getMessage()), Response::HTTP_BAD_REQUEST);
    }

    if ($exception instanceof FailedToSaveEntityException) {
      return response()->json(new Error($exception->getMessage()), Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    if ($exception instanceof NotFoundEntityException) {
      return response()->json(new Error($exception->getMessage()), Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    if ($exception instanceof NotEqualsAmountsException) {
      return response()->json(new Error($exception->getMessage()), Response::HTTP_BAD_REQUEST);
    }

    if ($exception instanceof NegativeAmountException) {
      return response()->json(new Error($exception->getMessage()), Response::HTTP_BAD_REQUEST);
    }

    if ($exception instanceof ExistsException) {
      return response()->json(new Exists($exception->getMessage(), $exception->existsItem), Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    if ($exception instanceof BusyEntityException) {
      return response()->json(new Error($exception->getMessage()), Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return response()->json(new Error($exception->getMessage()), Response::HTTP_INTERNAL_SERVER_ERROR);
  }
}
