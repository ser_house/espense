<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;


class InfoCommand extends Command {

  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'info:info';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'This command shows info about current environment';


  /**
   * Execute the console command.
   *
   * @return void
   */
  public function handle() {
    $env = env('APP_ENV', 'Undefined');
    $app_name = env('APP_NAME', 'Undefined');
    $db_connection = env('DB_CONNECTION', 'Undefined');
    $db_name = env('DB_DATABASE', 'Undefined');

    $this->info(sprintf('Current environment: "%s"', $env));
    $this->info(sprintf('Application name: "%s"', $app_name));
    $this->info(sprintf('Database connection: "%s"', $db_connection));
    $this->info(sprintf('Database name: "%s"', $db_name));
  }
}
