<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.02.2020
 * Time: 11:21
 */


namespace App\Core;


interface ITransaction {
  public function start(): void;

  public function commit(): void;

  public function rollback(): void;
}
