<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 26.11.2019
 * Time: 11:02
 */


namespace App\Core\Category;

use App\Core\Generic\DTO\Item as DtoItem;

class Converter {

  /**
   * @param Category $entity
   *
   * @return DtoItem
   */
  public function domainToDto(Category $entity): DtoItem {
    return new DtoItem((string)$entity->id, (string)$entity->title);
  }
}
