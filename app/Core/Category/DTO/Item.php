<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 25.04.2021
 * Time: 7:06
 */


namespace App\Core\Category\DTO;

use App\Core\Generic\DTO\Item as GenericItem;

class Item extends GenericItem {

  /**
   * Item constructor.
   *
   * @param string $id
   * @param string $title
   * @param GenericItem|null $parent
   */
  public function __construct(string $id, string $title, public readonly ?GenericItem $parent = null) {
    parent::__construct($id, $title);
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize(): mixed {
    $array = parent::jsonSerialize();
    $array['parent'] = $this->parent ? [
      'id' => $this->parent->id,
      'title' => $this->parent->title,
    ] : null;

    return $array;
  }
}
