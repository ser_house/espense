<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 26.11.2019
 * Time: 10:58
 */


namespace App\Core\Category\DTO;

use App\Core\Generic\DTO\Item as GenericItem;
use App\Core\Product\DTO\Item as ProductItem;

class ManageItem extends GenericItem {
  /**
   * ManageItem constructor.
   *
   * @param string $id
   * @param string $title
   * @param ProductItem[] $products
   */
  public function __construct(string $id, string $title, public readonly array $products) {
    parent::__construct($id, $title);
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize(): mixed {
    $array = parent::jsonSerialize();
    $array['products'] = $this->products;

    return $array;
  }
}
