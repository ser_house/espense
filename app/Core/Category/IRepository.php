<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 10:45
 */


namespace App\Core\Category;

use App\Core\Title;

interface IRepository {
  /**
   *
   * @return CategoryId
   */
  public function nextId(): CategoryId;

  /**
   * @param Category $category
   */
  public function add(Category $category): void;

  /**
   * @param Category $category
   */
  public function update(Category $category): void;

  /**
   * @param CategoryId $categoryId
   */
  public function remove(CategoryId $categoryId): void;

  /**
   *
   * @return Category[]
   */
  public function findAll(): array;

  /**
   * @param CategoryId $categoryId
   *
   * @return Category|null
   */
  public function findById(CategoryId $categoryId): ?Category;

  /**
   * @param Title $title
   *
   * @return Category|null
   */
  public function findByTitle(Title $title): ?Category;

  /**
   * @param Title $parent_title
   * @param Title $title
   *
   * @return Category|null
   */
  public function findByParentAndTitle(Title $parent_title, Title $title): ?Category;
}
