<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:14
 */


namespace App\Core\Category\UseCase\MoveToCategory;

use App\Core\Category\Category;
use App\Core\Category\IRepository as CategoryRepository;
use App\Core\Exception\NotFoundEntityException;
use App\Core\IEventDispatcher;


class Action {

  public function __construct(
    private readonly CategoryRepository $categoryRepository,
    private readonly IEventDispatcher $eventDispatcher,
  ) {

  }

  /**
   * @param Input $input
   *
   * @return MovedToCategoryEvent
   */
  public function handle(Input $input): MovedToCategoryEvent {
    $id = $input->categoryId;

    $category = $this->categoryRepository->findById($id);
    if (null === $category) {
      throw new NotFoundEntityException('Перемещение невозможно: категория не найдена.');
    }

    $targetCategory = null;
    if ($input->targetCategoryId) {
      $targetCategory = $this->categoryRepository->findById($input->targetCategoryId);
      if (null === $targetCategory) {
        throw new NotFoundEntityException('Перемещение невозможно: категория назначения не найдена.');
      }
    }

    $oldCategory = null;
    if ($category->parentId) {
      $oldCategory = $this->categoryRepository->findById($category->parentId);
    }

    $category = new Category($id, $category->title, $input->targetCategoryId);
    $this->categoryRepository->update($category);

    $event = new MovedToCategoryEvent($category, $oldCategory, $targetCategory);
    $this->eventDispatcher->dispatch($event);
    return $event;
  }
}
