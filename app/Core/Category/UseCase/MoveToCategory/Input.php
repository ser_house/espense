<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:15
 */


namespace App\Core\Category\UseCase\MoveToCategory;


use App\Core\Category\CategoryId;
use App\Core\Exception\MissingRequiredDataException;
use Exception;


class Input {

  public readonly CategoryId $categoryId;
  public readonly ?CategoryId $targetCategoryId;

  /**
   * Input constructor.
   *
   * @param array $input = [
   *    'id' => (string), required
   *    'category_id' => (string), required
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    if (empty($input['category_id'])) {
      throw new MissingRequiredDataException(['Не указан id перемещаемой категории.']);
    }

    if (empty($input['target_category_id'])) {
      $this->targetCategoryId = null;
    }
    else {
      $this->targetCategoryId = new CategoryId($input['target_category_id']);
    }

    $this->categoryId = new CategoryId($input['category_id']);

  }
}
