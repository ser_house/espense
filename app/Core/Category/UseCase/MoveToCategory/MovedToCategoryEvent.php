<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.02.2020
 * Time: 19:51
 */


namespace App\Core\Category\UseCase\MoveToCategory;

use App\Core\Category\Category;
use App\Core\Category\UseCase\Add\AddedEvent;

/**
 * Class MovedToCategoryEvent
 *
 * @package App\Core\Category\UseCase\MoveToCategory
 */
class MovedToCategoryEvent extends AddedEvent {

  public function __construct(
    Category $category,
    public readonly ?Category $oldCategory,
    public readonly ?Category $newCategory,
  ) {
    parent::__construct($category);
  }
}
