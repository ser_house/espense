<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:14
 */


namespace App\Core\Category\UseCase\Show;

use App\Core\Category\IDataProvider;
use App\Core\Exception\NotFoundEntityException;
use App\Core\Generic\DTO\Item as CategoryItem;

class Action {

  public function __construct(private readonly IDataProvider $categoryDataProvider) {

  }


  /**
   * @param Input $input
   *
   * @return CategoryItem
   */
  public function handle(Input $input): CategoryItem {
    $item = $this->categoryDataProvider->findById($input->id);
    if (null === $item) {
      throw new NotFoundEntityException('Категория не найдена.');
    }

    return $item;
  }
}
