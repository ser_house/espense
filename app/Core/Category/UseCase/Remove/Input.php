<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:27
 */


namespace App\Core\Category\UseCase\Remove;


use App\Core\Category\CategoryId;
use App\Core\Exception\MissingRequiredDataException;
use Exception;

class Input {

  public readonly CategoryId $id;

  /**
   * Input constructor.
   *
   * @param array $input = [
   *    'id' => (string), required
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    if (empty($input['id'])) {
      throw new MissingRequiredDataException(['Не указан id категории.']);
    }
    $this->id = new CategoryId($input['id']);
  }
}
