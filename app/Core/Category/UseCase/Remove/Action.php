<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:29
 */


namespace App\Core\Category\UseCase\Remove;

use App\Core\Category\IRepository as CategoryRepository;
use App\Core\Exception\BusyEntityException;
use App\Core\Exception\NotFoundEntityException;
use App\Core\Product\IDataProvider;


class Action {

  public function __construct(
    private readonly CategoryRepository $categoryRepository,
    private readonly IDataProvider $productDataProvider,
  ) {

  }

  /**
   * @param Input $input
   *
   * @return RemovedEvent
   * @throws NotFoundEntityException
   * @throws BusyEntityException
   */
  public function handle(Input $input): RemovedEvent {
    $category = $this->categoryRepository->findById($input->id);
    if (null === $category) {
      throw new NotFoundEntityException('Удаление невозможно: категория не найдена.');
    }

    // @todo: пожалуй, логичнее было бы удалять и товары, если они не используются в расходах
    // (или если и используются, всё целиком, но после запроса пользователя).
    $count_products = $this->productDataProvider->countByCategoryId($input->id);
    if ($count_products) {
      throw new BusyEntityException('Удаление невозможно: у категории есть товары.');
    }

    $this->categoryRepository->remove($input->id);

    return new RemovedEvent($category);
  }
}
