<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.02.2020
 * Time: 13:33
 */


namespace App\Core\Category\UseCase\Remove;


use App\Core\Category\Category;

class RemovedEvent {

  public function __construct(public readonly Category $category) {

  }
}
