<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.02.2020
 * Time: 13:20
 */


namespace App\Core\Category\UseCase\Add;


use App\Core\Category\Category;
use App\Core\IEvent;

class AddedEvent implements IEvent {

  public function __construct(public readonly Category $category) {

  }
}
