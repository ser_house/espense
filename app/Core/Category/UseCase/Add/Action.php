<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:04
 */


namespace App\Core\Category\UseCase\Add;


use App\Core\Category\Category as DomainCategory;
use App\Core\Category\Converter;
use App\Core\Category\IRepository as CategoryRepository;
use App\Core\Exception\ExistsException;

class Action {

  public function __construct(
    private readonly CategoryRepository $categoryRepository,
    private readonly Converter $categoryConverter
  ) {

  }

  /**
   * @param Input $input
   *
   * @return AddedEvent
   */
  public function handle(Input $input): AddedEvent {
    $title = $input->title;

    $exists = $this->categoryRepository->findByTitle($title);
    if ($exists && (
        ($exists->parentId && $exists->parentId->equals($input->parentId))
        ||
        (!$exists->parentId && !$input->parentId)
      )) {
      $item = $this->categoryConverter->domainToDto($exists);
      throw new ExistsException("Добавление невозможно: категория '$title' существует.", $item);
    }

    $id = $this->categoryRepository->nextId();

    $category = new DomainCategory($id, $title, $input->parentId);
    $this->categoryRepository->add($category);

    return new AddedEvent($category);
  }
}
