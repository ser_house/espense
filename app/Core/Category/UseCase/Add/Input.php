<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 16:37
 */


namespace App\Core\Category\UseCase\Add;

use App\Core\Category\CategoryId;
use App\Core\Exception\MissingRequiredDataException;
use App\Core\Title;
use Exception;

class Input {

  public readonly ?CategoryId $parentId;
  public readonly Title $title;

  /**
   * Input constructor.
   *
   * @param array $input = [
   *    'title' => (string), required
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    if (empty($input['title'])) {
      throw new MissingRequiredDataException(['Не указано название категории.']);
    }

    $this->parentId = empty($input['parent_id']) ? null : new CategoryId($input['parent_id']);
    $this->title = new Title($input['title']);
  }
}
