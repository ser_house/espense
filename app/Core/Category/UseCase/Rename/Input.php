<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:15
 */


namespace App\Core\Category\UseCase\Rename;


use App\Core\Category\CategoryId;
use App\Core\Exception\MissingRequiredDataException;
use App\Core\Title;
use Exception;

class Input {

  public readonly CategoryId $id;
  public readonly Title $newTitle;

  /**
   * Input constructor.
   *
   * @param array $input = [
   *    'id' => (string), required
   *    'title' => (string), required
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    if (empty($input['id'])) {
      throw new MissingRequiredDataException(['Не указан id категории.']);
    }
    if (empty($input['title'])) {
      throw new MissingRequiredDataException(['Не указано название категории.']);
    }

    $this->id = new CategoryId($input['id']);
    $this->newTitle = new Title($input['title']);
  }
}
