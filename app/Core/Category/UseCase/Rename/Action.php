<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:14
 */


namespace App\Core\Category\UseCase\Rename;

use App\Core\Category\IRepository as CategoryRepository;
use App\Core\Exception\NotFoundEntityException;

class Action {

  public function __construct(private readonly CategoryRepository $categoryRepository) {

  }

  /**
   * @param Input $input
   *
   * @return RenamedEvent
   */
  public function handle(Input $input): RenamedEvent {
    $currentCategory = $this->categoryRepository->findById($input->id);
    if (null === $currentCategory) {
      throw new NotFoundEntityException('Переименование невозможно: категория не найдена.');
    }

    $renamedCategory = $currentCategory->rename($input->newTitle);
    $this->categoryRepository->update($renamedCategory);

    return new RenamedEvent($renamedCategory, $currentCategory->title);
  }
}
