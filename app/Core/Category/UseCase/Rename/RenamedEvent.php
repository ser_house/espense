<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.02.2020
 * Time: 13:29
 */


namespace App\Core\Category\UseCase\Rename;


use App\Core\Category\Category;
use App\Core\Title;

class RenamedEvent {

  public function __construct(
    public readonly Category $category,
    public readonly Title $oldTitle,
  ) {

  }
}
