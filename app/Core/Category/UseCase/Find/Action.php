<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:14
 */


namespace App\Core\Category\UseCase\Find;

use App\Core\Category\IDataProvider;
use App\Core\Generic\DTO\Item;
use App\Core\Generic\UseCase\Find\Input;


class Action {

  public function __construct(private readonly IDataProvider $categoryDataProvider) {

  }


  /**
   * @param Input $input
   *
   * @return Item[]
   */
  public function handle(Input $input): array {
    if ($input->like) {
      return $this->categoryDataProvider->findLikeTitle($input->like);
    }

    return $this->categoryDataProvider->findByTitle($input->title);
  }
}
