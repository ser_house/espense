<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.12.2019
 * Time: 9:28
 */


namespace App\Core\Category;

use App\Core\Generic\DTO\Item;


interface IDataProvider {

  /**
   * @param string $like
   *
   * @return Item[]
   */
  public function findLikeTitle(string $like): array;

  /**
   * @param string $title
   *
   * @return Item[]
   */
  public function findByTitle(string $title): array;

  /**
   * @param CategoryId $categoryId
   *
   * @return Item|null
   */
  public function findById(CategoryId $categoryId): ?Item;

  /**
   *
   * @return array
   */
  public function allAsTree(): array;

  /**
   * @param string $delimiter
   *
   * @return array
   */
  public function listMaterializedItems(string $delimiter = '/'): array;

  /**
   * Категории, содержащие непосредственно товары.
   *
   * @return array[] with id and title
   */
  public function categoriesWithProduct(): array;

  /**
   * @return array[] with id and title
   */
  public function categories(): array;
}
