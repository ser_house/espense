<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 0:01
 */


namespace App\Core\Category;

use App\Core\Title;
use JsonSerializable;


final class Category implements JsonSerializable {

  public function __construct(
    public readonly CategoryId $id,
    public readonly Title $title,
    public readonly ?CategoryId $parentId = null,
  ) {

  }

  /**
   * @param Title $title
   *
   * @return Category
   */
  public function rename(Title $title): self {
    return new self($this->id, $title, $this->parentId);
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize(): mixed {
    return [
      'id' => (string)$this->id,
      'title' => (string)$this->title,
      'parent_id' => $this->parentId ? (string)$this->parentId : null,
    ];
  }
}
