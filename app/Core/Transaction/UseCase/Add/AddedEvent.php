<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 25.10.2019
 * Time: 19:16
 */


namespace App\Core\Transaction\UseCase\Add;


use App\Core\Transaction\Transaction;
use App\Core\IEvent;

/**
 * Class AddedEvent
 *
 */
class AddedEvent implements IEvent {

  public function __construct(
    public readonly Transaction $transaction,
  ) {

  }
}
