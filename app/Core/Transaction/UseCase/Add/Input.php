<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.02.2020
 * Time: 10:28
 */


namespace App\Core\Transaction\UseCase\Add;


use App\Core\Account\AccountId;
use App\Core\Account\DefaultSrcAccount;
use App\Core\Amount;
use App\Core\Exception\MissingRequiredDataException;
use App\Core\Generic\Note;
use DateTimeImmutable;

class Input {

  public readonly DateTimeImmutable $date;
  public readonly AccountId $accountSrcId;
  public readonly AccountId $accountTargetId;
  public readonly Amount $amount;
  public readonly ?Note $note;

  /**
   * Input constructor.
   *
   * @param array $input = [
   *    'date' => (string), required
   *    'account_src_id' => (string), required
   *    'account_target_id' => (string), required
   *    'amount' => (string), required
   *    'note' => (string), optional
   * ]
   *
   * @throws MissingRequiredDataException
   */
  public function __construct(array $input) {
    if (empty($input['date'])) {
      $missing_data[] = 'Дата не указана.';
    }

    if (empty($input['account_src_id'])) {
      $missing_data[] = 'Счёт-источник не указан.';
    }

    if (empty($input['account_target_id'])) {
      $missing_data[] = 'Счёт назначения не указан.';
    }

    if (!empty($input['account_src_id']) && !empty($input['account_target_id']) && $input['account_src_id'] === $input['account_target_id']) {
      $missing_data[] = 'Совпадают счета.';
    }

    if (empty($input['amount'])) {
      $missing_data[] = 'Сумма не указана.';
    }

    if (!empty($missing_data)) {
      throw new MissingRequiredDataException($missing_data);
    }

    $this->date = new DateTimeImmutable($input['date']);
    $this->accountSrcId = new AccountId($input['account_src_id']);
    $this->accountTargetId = new AccountId($input['account_target_id']);
    $this->amount = new Amount($input['amount']);

    if (!empty($input['note'])) {
      $this->note = new Note($input['note']);
    }
    else {
      $this->note = null;
    }
  }
}
