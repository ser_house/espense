<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.02.2020
 * Time: 10:35
 */


namespace App\Core\Transaction\UseCase\Add;


use App\Core\Account\IDataProvider as AccountsDataProvider;
use App\Core\Exception\FailedToSaveEntityException;
use App\Core\Transaction\Transaction as DomainTransaction;
use App\Core\Transaction\IRepository as TransactionRepository;
use App\Core\IEventDispatcher;
use App\Core\ITransaction as Transaction;
use App\Core\Transaction\UseCase\NegativeAccountRemainException;
use Throwable;

class Action {

  public function __construct(
    private readonly TransactionRepository $transactionRepository,
    private readonly IEventDispatcher $eventDispatcher,
    private readonly Transaction $transaction,
    private readonly AccountsDataProvider $accountsDataProvider,
  ) {

  }

  /**
   * @param Input $input
   *
   * @return AddedEvent
   * @throws FailedToSaveEntityException
   */
  public function handle(Input $input): AddedEvent {
    $id = $this->transactionRepository->nextId();
    $transaction = new DomainTransaction(
      $id,
      $input->date,
      $input->accountSrcId,
      $input->accountTargetId,
      $input->amount,
      $input->note
    );

    $this->transaction->start();
    try {
      $this->transactionRepository->add($transaction);
      $src_remain = $this->accountsDataProvider->accountRemain($transaction->accountSrcId);
      if ($src_remain < 0.0) {
        throw new NegativeAccountRemainException($src_remain);
      }
      $this->transaction->commit();
    }
    catch (Throwable $e) {
      $this->transaction->rollback();
      throw $e;
    }

    $event = new AddedEvent($transaction);
    $this->eventDispatcher->dispatch($event);

    return $event;
  }
}
