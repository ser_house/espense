<?php

namespace App\Core\Transaction\UseCase;

use DomainException;

final class NegativeAccountRemainException extends DomainException {
  /**
   * @inheritDoc
   */
  public function __construct(float $value, int $code = 0, ?\Throwable $previous = null) {
    parent::__construct("Отрицательный остаток по счету: $value", $code, $previous);
  }

}
