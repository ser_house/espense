<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:15
 */


namespace App\Core\Transaction\UseCase\Find;

use App\Core\Account\Account;
use App\Core\Account\AccountId;
use App\Core\Account\IRepository as AccountRepository;
use App\Core\Title;
use DateTimeImmutable;
use InvalidArgumentException;


class Input {

  public const ANY_KEY = 'any';
  public const ANY_LABEL = 'Любой';

  public const SORT_ASC = 'asc';
  public const SORT_DESC = 'desc';

  public readonly ?Account $accountSrc;
  public readonly ?Account $accountTarget;
  protected array $months;
  protected array $years;
  protected ?DateTimeImmutable $date;
  public readonly string $sort;


  public function __construct(
    array $input,
    private readonly AccountRepository $accountRepository,
  ) {

    $this->accountSrc = $this->findAccountSrc($input);
    $this->accountTarget = $this->findAccountTarget($input);

    $this->initDates($input);

    $this->sort = $this->getSort($input);
  }

  /**
   * @return array
   */
  public function months(): array {
    return $this->months;
  }

  /**
   * @return array
   */
  public function years(): array {
    return $this->years;
  }

  /**
   * @return DateTimeImmutable|null
   */
  public function date(): ?DateTimeImmutable {
    return $this->date;
  }

  /**
   * @return string|null
   */
  public function accountSrcTitle(): ?string {
    return $this->accountSrc ? $this->accountSrc->title : null;
  }

  /**
   * @return string|null
   */
  public function accountTargetTitle(): ?string {
    return $this->accountTarget ? $this->accountTarget->title : null;
  }

  /**
   * @return string[]
   */
  public function anyItemOption(): array {
    return [self::ANY_KEY => self::ANY_LABEL];
  }

  /**
   * @return bool
   */
  public function isEmpty(): bool {
    return null === $this->date
      && empty($this->months)
      && empty($this->years)
      && empty($this->accountSrc)
      && empty($this->accountTarget);
  }

  /**
   * @param array $input
   *
   * @return $this
   * @throws \Exception
   */
  protected function initDates(array $input): static {
    if (!empty($input['date'])) {
      $this->date = new DateTimeImmutable($input['date']);
      $this->months = [];
      $this->years = [];
    }
    else {
      $this->date = null;

      $input['months'] = array_filter($input['months'] ?? [], static function ($month) {
        return self::ANY_KEY !== $month;
      });
      $input['years'] = array_filter($input['years'] ?? [], static function ($year) {
        return self::ANY_KEY !== $year;
      });

      if (!empty($input['months'])) {
        $this->months = $input['months'];
      }
      else {
        $this->months = [];
      }

      if (!empty($input['years'])) {
        $this->years = $input['years'];
      }
      else {
        $this->years = [];
      }
    }

    return $this;
  }

  /**
   * @param array $input
   *
   * @return Account|null
   */
  private function findAccountSrc(array $input): ?Account {
    if (!empty($input['account_src'])) {
      return $this->accountRepository->findByTitle(new Title($input['account_src']));
    }
    if (!empty($input['account_src_id'])) {
      return $this->accountRepository->findById(new AccountId($input['account_src_id']));
    }

    return null;
  }

  /**
   * @param array $input
   *
   * @return Account|null
   */
  private function findAccountTarget(array $input): ?Account {
    if (!empty($input['account_target'])) {
      return $this->accountRepository->findByTitle(new Title($input['account_target']));
    }
    if (!empty($input['account_target_id'])) {
      return $this->accountRepository->findById(new AccountId($input['account_target_id']));
    }

    return null;
  }

  /**
   * @param array $input
   *
   * @return string
   */
  private function getSort(array $input): string {
    if (!empty($input['sort'])) {
      $lower_sort = mb_strtolower($input['sort']);
      if (self::SORT_ASC !== $lower_sort && self::SORT_DESC !== $lower_sort) {
        throw new InvalidArgumentException('Некорректное значение сортировки.');
      }

      return $lower_sort;
    }

    return self::SORT_DESC;
  }
}
