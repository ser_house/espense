<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:14
 */


namespace App\Core\Transaction\UseCase\Find;

use App\Core\Account\IRepository as AccountRepository;
use App\Core\Transaction\DTO\Item as TransactionItem;
use App\Core\Transaction\IDataProvider as TransactionDataProvider;

class Action {

  public function __construct(
    private readonly TransactionDataProvider $transactionDataProvider,
    private readonly AccountRepository $accountRepository,
  ) {

  }

  /**
   * @param array $raw_input
   *
   * @return Input
   */
  public function buildInput(array $raw_input): Input {
    return new Input($raw_input, $this->accountRepository);
  }

  /**
   * @param Input $input
   *
   * @return TransactionItem[]
   */
  public function handle(Input $input): array {
    if ($input->isEmpty()) {
      return [];
    }
    return $this->transactionDataProvider->find($input);
  }
}
