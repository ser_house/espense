<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.02.2020
 * Time: 15:32
 */


namespace App\Core\Transaction\UseCase\Remove;


use App\Core\Transaction\Transaction;
use App\Core\IEvent;

class RemovedEvent implements IEvent {

  public function __construct(public readonly Transaction $transaction) {

  }
}
