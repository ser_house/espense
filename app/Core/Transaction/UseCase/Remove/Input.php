<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.02.2020
 * Time: 10:57
 */


namespace App\Core\Transaction\UseCase\Remove;

use App\Core\Exception\MissingRequiredDataException;
use App\Core\Transaction\TransactionId;
use Exception;

class Input {

  public readonly TransactionId $id;

  /**
   * Input constructor.
   *
   * @param array $input = [
   *    'id' => (string), required
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    if (empty($input['id'])) {
      throw new MissingRequiredDataException(['Не указан id перевода.']);
    }
    $this->id = new TransactionId($input['id']);
  }
}
