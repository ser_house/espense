<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.02.2020
 * Time: 10:35
 */


namespace App\Core\Transaction\UseCase\Update;

use App\Core\Exception\FailedToSaveEntityException;
use App\Core\IEventDispatcher;
use App\Core\Transaction\IRepository as TransactionRepository;


class Action {

  public function __construct(
    private readonly TransactionRepository $transactionRepository,
    private readonly IEventDispatcher $eventDispatcher,
  ) {

  }


  /**
   * @param Input $input
   *
   * @return UpdatedEvent
   * @throws FailedToSaveEntityException
   */
  public function handle(Input $input): UpdatedEvent {
    $originalTransaction = $this->transactionRepository->get($input->id);

    $updatedTransaction = $originalTransaction->buildNewTransaction(
      $input->amount ?: $originalTransaction->amount,
      $input->note ?: $originalTransaction->note
    );
    $this->transactionRepository->update($updatedTransaction);

    $event = new UpdatedEvent($originalTransaction, $updatedTransaction);
    $this->eventDispatcher->dispatch($event);

    return $event;
  }
}
