<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 20.11.2019
 * Time: 18:12
 */


namespace App\Core\Transaction\UseCase\Update;


use App\Core\Transaction\Transaction;
use App\Core\IEvent;

class UpdatedEvent implements IEvent {

  public function __construct(public readonly Transaction $originalTransaction, public readonly Transaction $updatedTransaction) {

  }
}
