<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.02.2020
 * Time: 10:47
 */


namespace App\Core\Transaction\UseCase\Update;

use App\Core\Amount;
use App\Core\Exception\MissingRequiredDataException;
use App\Core\Generic\Note;
use App\Core\Transaction\TransactionId;
use DateTimeImmutable;
use Exception;

class Input {

  public readonly TransactionId $id;
  public readonly ?DateTimeImmutable $date;
  public readonly ?Amount $amount;
  public readonly ?Note $note;

  /**
   * Input constructor.
   *
   * Как минимум значение одного поля должно быть указано, иначе нечего обновлять.
   *
   * @param array $input = [
   *    'id' => (string), required
   *    'date' => (string), optional
   *    'amount' => (string), optional
   *    'note' => (string), optional
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    if (empty($input['id'])) {
      throw new MissingRequiredDataException(['id перевода не указан.']);
    }

    $this->id = new TransactionId($input['id']);

    unset($input['id']);
    $input = array_filter($input);

    if (empty($input)) {
      $fields = [
        'date' => 'Дата не указана',
        'amount' => 'Сумма не указана',
        'note' => 'Примечание не указано',
      ];
      $missing_data = array_values($fields);
      throw new MissingRequiredDataException($missing_data);
    }

    if (!empty($input['date'])) {
      $this->date = new DateTimeImmutable($input['date']);
    }
    else {
      $this->date = null;
    }

    if (!empty($input['amount'])) {
      $this->amount = new Amount($input['amount']);
    }
    else {
      $this->amount = null;
    }

    if (!empty($input['note'])) {
      $this->note = new Note($input['note']);
    }
    else {
      $this->note = null;
    }
  }
}
