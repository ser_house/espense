<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:14
 */


namespace App\Core\Transaction\UseCase\Show;

use App\Core\Exception\NotFoundEntityException;
use App\Core\Transaction\DTO\Item as TransactionItem;
use App\Core\Transaction\IConverter as TransactionConverter;
use App\Core\Transaction\IRepository as TransactionRepository;

class Action {

  public function __construct(
    private readonly TransactionRepository $transactionRepository,
    private readonly TransactionConverter $transactionConverter,
  ) {

  }


  /**
   * @param Input $input
   *
   * @return TransactionItem
   */
  public function handle(Input $input): TransactionItem {
    $transaction = $this->transactionRepository->findById($input->id);
    if (null === $transaction) {
      throw new NotFoundEntityException('Перевод не найден.');
    }

    return $this->transactionConverter->domainToDto($transaction);
  }
}
