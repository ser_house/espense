<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 0:13
 */


namespace App\Core\Transaction;

use App\Core\EntityId;

/**
 * Class TransactionId
 *
 * @package App\Core\Transaction
 */
final class TransactionId extends EntityId {

}
