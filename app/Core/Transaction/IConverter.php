<?php

namespace App\Core\Transaction;

use App\Core\Transaction\DTO\Item as TransactionItem;
use App\Core\Transaction\Transaction as Entity;
use App\Models\Transaction as Model;
use Exception;
use stdClass;

interface IConverter {
  /**
   * @param Entity $entity
   *
   * @return Model
   */
  public function domainToModel(Entity $entity): Model;

  /**
   * @param Model $model
   *
   * @return Entity
   * @throws Exception
   */
  public function modelToDomain(Model $model): Entity;

  /**
   * @param iterable $entities
   *
   * @return Model[]
   */
  public function domainsToModels(iterable $entities): array;

  /**
   * @param iterable $models
   *
   * @return Entity[]
   * @throws Exception
   */
  public function modelsToDomains(iterable $models): array;

  /**
   * @param stdClass $dbItem
   *
   * @return TransactionItem
   * @throws Exception
   */
  public function dbItemToDto(stdClass $dbItem): TransactionItem;

  /**
   * @param iterable $db_items
   *
   * @return TransactionItem[]
   * @throws Exception
   */
  public function dbItemsToDto(iterable $db_items): array;


  /**
   * @param Entity $transaction
   *
   * @return TransactionItem
   */
  public function domainToDto(Entity $transaction): TransactionItem;

  /**
   * @inheritDoc
   */
  public function domainsToDto(iterable $transactions): array;
}
