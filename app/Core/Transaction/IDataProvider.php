<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.09.2019
 * Time: 16:13
 */


namespace App\Core\Transaction;

use App\Core\Exception\NotFoundEntityException;
use App\Core\Transaction\DTO\Item as TransactionItem;
use App\Core\Transaction\UseCase\Find\Input as FindInput;

interface IDataProvider {

  /**
   * @return TransactionItem[]
   */
  public function all(): array;

  /**
   * Если запись не найдена, то выбрасывается исключение.
   *
   * @param TransactionId $transactionId
   *
   * @return TransactionItem
   * @throws NotFoundEntityException
   */
  public function itemById(TransactionId $transactionId): TransactionItem;

  /**
   *
   * @return TransactionItem|null
   */
  public function lastItem(): ?TransactionItem;

  /**
   *
   * @return TransactionItem[]
   */
  public function todayItems(): array;

  /**
   * @param FindInput $input
   *
   * @return array
   */
  public function find(FindInput $input): array;
}
