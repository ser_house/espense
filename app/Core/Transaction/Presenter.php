<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 2:04
 */


namespace App\Core\Transaction;

use App\Core\Account\DefaultSrcAccount;
use App\Core\Formatter;
use App\Core\Generic\DTO\Item as GenericItem;
use App\Core\Transaction\DTO\Item as TransactionItem;

class Presenter {

  /**
   * @param TransactionItem $transactionItem
   * @param string $date_format
   *
   * @return View
   */
  public function view(TransactionItem $transactionItem, string $date_format = 'd.m.Y'): View {
    $view = new View();
    $view->id = $transactionItem->id;
    $view->date = $transactionItem->date->format($date_format);

    $view->accountSrc = $transactionItem->accountSrc;
    $view->accountTarget = $transactionItem->accountTarget;

    $view->amount = Formatter::formatFloat($transactionItem->amount);
    $view->note = $transactionItem->note;

    return $view;
  }

  /**
   * @param TransactionItem[] $transactionItems
   * @param string $date_format
   *
   * @return View[]
   */
  public function views(array $transactionItems, string $date_format = 'd.m.Y'): array {
    $views = [];

    /** @var TransactionItem $transactionItem */
    foreach ($transactionItems as $transactionItem) {
      $views[] = $this->view($transactionItem, $date_format);
    }

    return $views;
  }

  /**
   * @param string $date_format
   *
   * @return View
   */
  public function viewNew(string $date_format = 'Y-m-d'): View {
    $view = new View();
    $view->id = '';
    $view->date = date($date_format);

    $defaultAccount = new DefaultSrcAccount();
    $view->accountSrc = new GenericItem((string)$defaultAccount->id, (string)$defaultAccount->title);

    $view->accountTarget = null;

    $view->amount = '';
    $view->note = '';

    return $view;
  }
}
