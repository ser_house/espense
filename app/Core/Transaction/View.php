<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 2:03
 */


namespace App\Core\Transaction;

use App\Core\Generic\DTO\Item;

class View {
  public string $id;
  public string $date;
  public Item $accountSrc;
  public ?Item $accountTarget;
  public string $amount;
  public string $note;
}
