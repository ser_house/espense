<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 2:03
 */


namespace App\Core\Transaction\DTO;

use App\Core\Generic\DTO\Item as DtoItem;
use DateTimeImmutable;

/**
 * Class Item
 *
 * @package App\Core\Transaction\DTO
 */
class Item {
  public string $id;
  public DateTimeImmutable $date;
  public DtoItem $accountSrc;
  public ?DtoItem $accountTarget;
  public float $amount;
  public string $note;
}
