<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 10:45
 */


namespace App\Core\Transaction;

use App\Core\Exception\FailedToSaveEntityException;
use App\Core\Exception\NotFoundEntityException;

interface IRepository {

  /**
   *
   * @return TransactionId
   */
  public function nextId(): TransactionId;

  /**
   * @param TransactionId $entityId
   *
   * @return Transaction
   * @throws NotFoundEntityException
   */
  public function get(TransactionId $entityId): Transaction;

  /**
   * @param TransactionId $entityId
   *
   * @return Transaction|null
   */
  public function findById(TransactionId $entityId): ?Transaction;

  /**
   * @param Transaction $transaction
   *
   * @throws FailedToSaveEntityException
   */
  public function add(Transaction $transaction): void;

  /**
   * @param Transaction $transaction
   *
   * @throws FailedToSaveEntityException
   */
  public function update(Transaction $transaction): void;

  /**
   * @param TransactionId $transactionId
   *
   * @return mixed
   */
  public function remove(TransactionId $transactionId);
}
