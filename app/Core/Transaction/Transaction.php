<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 0:45
 */


namespace App\Core\Transaction;

use App\Core\Account\AccountId;
use App\Core\Amount;
use App\Core\Generic\Note;
use DateTimeImmutable;


final class Transaction {

  public function __construct(
    public readonly TransactionId $id,
    public readonly DateTimeImmutable $date,
    public readonly AccountId $accountSrcId,
    public readonly AccountId $accountTargetId,
    public readonly Amount $amount,
    public readonly ?Note $note = null,
  ) {

  }

  /**
   * Создаёт новый перевод на основе текущего (дата, счета копируются из текущего).
   *
   * @param Amount $amount
   * @param Note|null $note
   *
   * @return Transaction
   */
  public function buildNewTransaction(Amount $amount, Note $note = null): Transaction {
    return new Transaction(
      new TransactionId(),
      $this->date,
      $this->accountSrcId,
      $this->accountTargetId,
      $amount,
      $note
    );
  }
}
