<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 0:07
 */

namespace App\Core;


use Exception;
use Ramsey\Uuid\Uuid;

/**
 * Class EntityId
 *
 * @package App\Core
 */
class EntityId {
  protected string $id;

  /**
   * EntityId constructor.
   *
   * @param string|null $id
   *
   * @throws Exception
   */
  public function __construct(string $id = null) {
    $this->id = $id ?? (string)Uuid::uuid4();
  }

  /**
   * @return string
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * @param EntityId $id
   *
   * @return bool
   */
  public function equals(EntityId $id): bool {
    return $id->getId() === $this->getId();
  }

  /**
   * @inheritDoc
   */
  public function __toString() {
    return $this->getId();
  }
}
