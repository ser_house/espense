<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 0:07
 */


namespace App\Core;

use App\Core\Exception\NegativeAmountException;
use Exception;

/**
 * Class Amount
 *
 * @package App\Core\Expense\Entity
 */
final class Amount {

  protected float $value;

  /**
   * Amount constructor.
   *
   * @param string $value
   *
   * @throws Exception
   */
  public function __construct(string $value) {
    $float_value = (float)str_replace([',', ' '], ['.', ''], $value);
    if ($float_value < 0.0) {
      throw new NegativeAmountException('Отрицательная сумма не разрешена.');
    }
    $this->value = $float_value;
  }

  /**
   * @return float
   */
  public function value(): float {
    return $this->value;
  }

  /**
   * @param Amount $amount
   *
   * @return bool
   */
  public function equals(Amount $amount): bool {
    return $amount->value() === $this->value();
  }

  /**
   * @inheritDoc
   */
  public function __toString() {
    return (string)$this->value();
  }

  /**
   *
   * @return bool
   */
  public function isEmpty(): bool {
    return empty($this->amount);
  }
}
