<?php

namespace App\Core;

class Formatter {
  public static function formatAmount(Amount $amount): string {
    return self::formatFloat($amount->value());
  }

  public static function formatFloat(float $value): string {
    return number_format($value, 2, ',', ' ');
  }
}
