<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.09.2019
 * Time: 6:42
 */


namespace App\Core;


/**
 * Class StaticCache
 *
 * @package App\Core
 */
class StaticCache {

  private static array $cache = [];

  /**
   * @param string $key
   * @param $data
   */
  public static function set(string $key, $data): void {
    static::$cache[$key] = $data;
  }

  /**
   * @param string $key
   *
   * @return mixed
   */
  public static function get(string $key) {
    if (static::has($key)) {
      return static::$cache[$key];
    }

    return null;
  }

  /**
   * @param string $key
   *
   * @return bool
   */
  public static function has(string $key): bool {
    if (empty(static::$cache)) {
      return false;
    }

    return array_key_exists($key, static::$cache);
  }

  /**
   * @param string $key
   */
  public static function flush(string $key): void {
    unset(static::$cache[$key]);
  }

  public static function clear(): void {
    static::$cache = [];
  }
}
