<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 10:45
 */


namespace App\Core\Contractor;

use App\Core\Title;

interface IRepository {

  /**
   *
   * @return ContractorId
   */
  public function nextId(): ContractorId;

  /**
   * @param Contractor $contractor
   *
   * @return mixed
   */
  public function add(Contractor $contractor);

  /**
   * @param Contractor $contractor
   */
  public function update(Contractor $contractor): void;

  /**
   * @param ContractorId $contractorId
   */
  public function remove(ContractorId $contractorId): void;

  /**
   *
   * @return Contractor[]
   */
  public function findAll(): array;

  /**
   * @param ContractorId $contractorId
   *
   * @return Contractor|null
   */
  public function findById(ContractorId $contractorId): ?Contractor;

  /**
   * @param ContractorId $contractorId
   *
   * @return Contractor
   */
  public function get(ContractorId $contractorId): Contractor;

  /**
   * @param Title $title
   *
   * @return Contractor|null
   */
  public function findByTitle(Title $title): ?Contractor;
}
