<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.09.2019
 * Time: 19:05
 */


namespace App\Core\Contractor;


use App\Core\Generic\DTO\Item;

/**
 * Class Converter
 *
 * @package App\Core\Contractor
 */
class Converter {

  /**
   * @param Contractor $contractor
   *
   * @return Item
   */
  public function entityToDTO(Contractor $contractor): Item {
    $id = (string)$contractor->id;
    $title = (string)$contractor->title;

    return new Item($id, $title);
  }
}
