<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 0:01
 */


namespace App\Core\Contractor;

use App\Core\Generic\Note;
use App\Core\Title;

/**
 * Class Contractor
 *
 * @package App\Core
 */
final class Contractor {

  /**
   * Contractor constructor.
   *
   * @param ContractorId $id
   * @param Title $title
   * @param Url|null $url
   * @param Note|null $note
   */
  public function __construct(
    public readonly ContractorId $id,
    public readonly Title $title,
    public readonly ?Url $url = null,
    public readonly ?Note $note = null,
  ) {

  }

  /**
   * @param Title $newTitle
   *
   * @return $this
   */
  public function rename(Title $newTitle): self {
    return new self($this->id, $newTitle, $this->url, $this->note);
  }

  /**
   * @param Url|null $newUrl
   *
   * @return $this
   */
  public function changeUrl(?Url $newUrl): self {
    return new self($this->id, $this->title, $newUrl, $this->note);
  }

  /**
   * @param Note|null $newNote
   *
   * @return $this
   */
  public function changeNote(?Note $newNote): self {
    return new self($this->id, $this->title, $this->url, $newNote);
  }

  /**
   * @param Url|null $url
   *
   * @return bool
   */
  public function isEqualsUrl(?Url $url): bool {
    // Может быть и null, поэтому не через Url::equals.
    return (string)$this->url === (string)$url;
  }

  /**
   * @param Note|null $note
   *
   * @return bool
   */
  public function isEqualsNote(?Note $note): bool {
    // Может быть и null, поэтому не через Note::equals.
    return (string)$this->note === (string)$note;
  }

  /**
   * @inheritDoc
   */
  public function __toString() {
    return (string)$this->title;
  }
}
