<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.12.2019
 * Time: 9:28
 */


namespace App\Core\Contractor;


use App\Core\Contractor\DTO\ManageItem;
use App\Core\Contractor\UseCase\Show\Item;


interface IDataProvider {

  /**
   * @param ContractorId $contractorId
   *
   * @return Item|null
   */
  public function findById(ContractorId $contractorId): ?Item;

  /**
   *
   * @return DTO\ManageItem[]
   */
  public function manageItems(): array;

  /**
   * @param ContractorId $contractorId
   *
   * @return ManageItem
   */
  public function manageItemById(ContractorId $contractorId): ManageItem;

  /**
   * @param string $query
   *
   * @return Item[]
   */
  public function searchLikeTitle(string $query): array;
}
