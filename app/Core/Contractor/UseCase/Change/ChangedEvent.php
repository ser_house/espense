<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.02.2020
 * Time: 13:53
 */


namespace App\Core\Contractor\UseCase\Change;


use App\Core\Contractor\Contractor;

class ChangedEvent {

  public function __construct(public readonly Contractor $unchangedContractor, public readonly Contractor $changedContractor) {

  }

  /**
   *
   * @return bool
   */
  public function isTitleChanged(): bool {
    return !$this->changedContractor->title->equals($this->unchangedContractor->title);
  }

  /**
   *
   * @return bool
   */
  public function isUrlChanged(): bool {
    return !$this->changedContractor->isEqualsUrl($this->unchangedContractor->url);
  }

  /**
   *
   * @return bool
   */
  public function isNoteChanged(): bool {
    return !$this->changedContractor->isEqualsNote($this->unchangedContractor->note);
  }
}
