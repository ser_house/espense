<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:14
 */


namespace App\Core\Contractor\UseCase\Change;

use App\Core\Contractor\IRepository as ContractorRepository;
use App\Core\Exception\NotFoundEntityException;

class Action {

  public function __construct(private readonly ContractorRepository $contractorRepository) {

  }

  /**
   * @param Input $input
   *
   * @return ChangedEvent
   */
  public function handle(Input $input): ChangedEvent {
    $unchangedContractor = $this->contractorRepository->findById($input->id);
    if (null === $unchangedContractor) {
      throw new NotFoundEntityException('Изменение невозможно: контрагент не найден.');
    }

    $changedContractor = clone $unchangedContractor;
    if (!$unchangedContractor->title->equals($input->newTitle)) {
      $changedContractor = $changedContractor->rename($input->newTitle);
    }

    if (!$unchangedContractor->isEqualsUrl($input->newUrl)) {
      $changedContractor = $changedContractor->changeUrl($input->newUrl);
    }

    if (!$unchangedContractor->isEqualsNote($input->newNote)) {
      $changedContractor = $changedContractor->changeNote($input->newNote);
    }

    $this->contractorRepository->update($changedContractor);

    return new ChangedEvent($unchangedContractor, $changedContractor);
  }
}
