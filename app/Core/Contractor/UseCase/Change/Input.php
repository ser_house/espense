<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:15
 */


namespace App\Core\Contractor\UseCase\Change;


use App\Core\Contractor\ContractorId;
use App\Core\Contractor\Url;
use App\Core\Exception\MissingRequiredDataException;
use App\Core\Generic\Note;
use App\Core\Title;
use Exception;

class Input {

  public readonly ContractorId $id;
  public readonly Title $newTitle;
  public readonly Url $newUrl;
  public readonly Note $newNote;

  /**
   * Input constructor.
   *
   * @param array $input = [
   *    'id' => (string), required
   *    'title' => (string), required
   *    'url' => (string), optional
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    if (empty($input['id'])) {
      throw new MissingRequiredDataException(['Не указан id контрагента.']);
    }
    if (empty($input['title'])) {
      throw new MissingRequiredDataException(['Не указано название контрагента.']);
    }

    $this->id = new ContractorId($input['id']);
    $this->newTitle = new Title($input['title']);
    $this->newUrl = new Url($input['url'] ?? '');
    $this->newNote = new Note($input['note'] ?? '');
  }
}
