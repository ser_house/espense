<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:14
 */


namespace App\Core\Contractor\UseCase\Show;

use App\Core\Contractor\IDataProvider;
use App\Core\Exception\NotFoundEntityException;

class Action {

  public function __construct(private readonly IDataProvider $contractorDataProvider) {

  }


  /**
   * @param Input $input
   *
   * @return Item
   */
  public function handle(Input $input): Item {
    $item = $this->contractorDataProvider->findById($input->id);
    if (null === $item) {
      throw new NotFoundEntityException('Контрагент не найден.');
    }

    return $item;
  }
}
