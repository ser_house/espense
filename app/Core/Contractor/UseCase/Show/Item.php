<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 20.11.2019
 * Time: 4:14
 */


namespace App\Core\Contractor\UseCase\Show;

class Item {

  public function __construct(
    public readonly string $id,
    public readonly string $title,
    public readonly string $url = '',
    public readonly string $note = '',
  ) {

  }
}
