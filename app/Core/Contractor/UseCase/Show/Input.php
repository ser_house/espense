<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:15
 */


namespace App\Core\Contractor\UseCase\Show;

use App\Core\Contractor\ContractorId;
use App\Core\Exception\MissingRequiredDataException;
use Exception;


class Input {

  public readonly ContractorId $id;

  /**
   * Input constructor.
   *
   * @param array $input = [
   *    'id' => (string), required
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    if (empty($input['id'])) {
      throw new MissingRequiredDataException(['Не указан id контрагента.']);
    }

    $this->id = new ContractorId($input['id']);
  }
}
