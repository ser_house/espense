<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.02.2020
 * Time: 13:55
 */


namespace App\Core\Contractor\UseCase\Remove;


use App\Core\Contractor\Contractor;

class RemovedEvent {

  public function __construct(public readonly Contractor $contractor) {

  }
}
