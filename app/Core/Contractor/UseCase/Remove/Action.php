<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:29
 */


namespace App\Core\Contractor\UseCase\Remove;

use App\Core\Contractor\IRepository as ContractorRepository;
use App\Core\Exception\BusyEntityException;
use App\Core\Exception\NotFoundEntityException;
use App\Core\Expense\IDataProvider as ExpenseDataProvider;

class Action {

  public function __construct(
    private readonly ContractorRepository $contractorRepository,
    private readonly ExpenseDataProvider $expenseDataProvider,
  ) {

  }


  /**
   * @param Input $input
   *
   * @return RemovedEvent
   */
  public function handle(Input $input): RemovedEvent {
    $contractor = $this->contractorRepository->findById($input->id);
    if (null === $contractor) {
      throw new NotFoundEntityException('Удаление невозможно: контрагент не найден.');
    }

    $count_expenses = $this->expenseDataProvider->countByContractorId($input->id);
    if ($count_expenses) {
      throw new BusyEntityException('Удаление невозможно: контрагент используется в расходах.');
    }

    $this->contractorRepository->remove($input->id);

    return new RemovedEvent($contractor);
  }
}
