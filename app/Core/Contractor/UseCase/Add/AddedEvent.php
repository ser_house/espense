<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.02.2020
 * Time: 13:49
 */


namespace App\Core\Contractor\UseCase\Add;


use App\Core\Contractor\Contractor;

class AddedEvent {

  public function __construct(public readonly Contractor $contractor) {

  }
}
