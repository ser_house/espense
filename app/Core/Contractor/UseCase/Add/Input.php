<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 16:37
 */


namespace App\Core\Contractor\UseCase\Add;


use App\Core\Contractor\Url;
use App\Core\Exception\MissingRequiredDataException;
use App\Core\Generic\Note;
use App\Core\Title;
use Exception;

class Input {

  public readonly Title $title;
  public readonly ?Url $url;
  public readonly ?Note $note;

  /**
   * Input constructor.
   *
   * @param array $input = [
   *    'title' => (string), required
   *    'url' => (string), optional
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    if (empty($input['title'])) {
      throw new MissingRequiredDataException(['Не указано название контрагента.']);
    }

    $this->title = new Title($input['title']);
    $this->url = !empty($input['url']) ? new Url($input['url']) : null;
    $this->note = !empty($input['note']) ? new Note($input['note']) : null;
  }
}
