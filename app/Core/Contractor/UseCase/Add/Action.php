<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:04
 */


namespace App\Core\Contractor\UseCase\Add;


use App\Core\Contractor\Contractor as DomainContractor;
use App\Core\Contractor\Converter;
use App\Core\Contractor\IRepository as ContractorRepository;
use App\Core\Exception\ExistsException;

class Action {

  public function __construct(
    private readonly ContractorRepository $contractorRepository,
    private readonly Converter $contractorConverter
  ) {

  }


  /**
   * @param Input $input
   *
   * @return AddedEvent
   */
  public function handle(Input $input): AddedEvent {
    $title = $input->title;

    $exists = $this->contractorRepository->findByTitle($title);
    if ($exists) {
      $item = $this->contractorConverter->entityToDTO($exists);
      throw new ExistsException("Добавление невозможно: контрагент '$title' существует.", $item);
    }

    $id = $this->contractorRepository->nextId();

    $contractor = new DomainContractor($id, $title, $input->url, $input->note);
    $this->contractorRepository->add($contractor);

    return new AddedEvent($contractor);
  }
}
