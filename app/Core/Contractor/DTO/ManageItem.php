<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.12.2019
 * Time: 9:37
 */


namespace App\Core\Contractor\DTO;


use App\Core\Generic\DTO\Item as DtoItem;

class ManageItem extends DtoItem {

  public function __construct(
    string $id,
    string $title,
    public readonly ?string $url,
    public readonly ?string $note,
    public readonly int $expenses_count = 0,
  ) {
    parent::__construct($id, $title);

  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize(): mixed {
    $json = parent::jsonSerialize();
    $json['url'] = $this->url;
    $json['note'] = $this->note;
    $json['expenses_count'] = $this->expenses_count;

    return $json;
  }
}
