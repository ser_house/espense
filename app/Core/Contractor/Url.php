<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 0:01
 */


namespace App\Core\Contractor;

use Exception;

/**
 * Class Url
 *
 * @package App\Core
 */
final class Url {

  private string $value;

  /**
   * Url constructor.
   *
   * @param string $value
   *
   * @throws Exception
   */
  public function __construct(string $value) {
    $this->value = $value;
  }

  /**
   * @return string
   */
  public function value(): string {
    return $this->value;
  }

  /**
   * @param Url $url
   *
   * @return bool
   */
  public function equals(Url $url): bool {
    return $url->value() === $this->value();
  }

  /**
   * @inheritDoc
   */
  public function __toString() {
    return $this->value();
  }
}
