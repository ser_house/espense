<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 01.12.2018
 * Time: 02:29
 */

namespace App\Core\Service;

use DateTimeImmutable;
use Exception;

/**
 * Class DateGenerator
 *
 * @package App\Core\Service
 */
class DateGenerator {

  /**
   * Карта месяцев для получения их первого и последнего дней.
   */
  public const MONTHS = [
    1 => 'January',
    2 => 'February',
    3 => 'March',
    4 => 'April',
    5 => 'May',
    6 => 'June',
    7 => 'July',
    8 => 'August',
    9 => 'September',
    10 => 'October',
    11 => 'November',
    12 => 'December',
  ];

  public const SHORT_MONTH_NAMES_RU = [
    'Янв',
    'Фев',
    'Март',
    'Апр',
    'Май',
    'Июнь',
    'Июль',
    'Авг',
    'Сент',
    'Окт',
    'Ноя',
    'Дек',
  ];


  private int $year;
  private $month_name;
  private DateTimeImmutable $date;

  /**
   * DateGenerator constructor.
   *
   * @param int $year
   * @param int $month
   *
   * @throws Exception
   */
  public function __construct(int $year, int $month) {
    $this->year = $year;
    $this->month_name = self::MONTHS[$month];

    $this->date = new DateTimeImmutable("$year-$month-01 00:00:00");
  }

  /**
   *
   * @return DateTimeImmutable
   * @throws Exception
   */
  public function getRandomDate(): DateTimeImmutable {
    $firstDay = $this->getFirstDateOfMonth();
    $lastDay = $this->getLastDateOfMonth();

    $timestamp = random_int($firstDay->getTimestamp(), $lastDay->getTimestamp());

    return new DateTimeImmutable("@$timestamp");
  }

  /**
   *
   * @return DateTimeImmutable
   */
  public function getFirstDateOfMonth(): DateTimeImmutable {
    return $this->date->modify("first day of $this->month_name $this->year");
  }

  /**
   *
   * @return DateTimeImmutable
   */
  public function getLastDateOfMonth(): DateTimeImmutable {
    return $this->date->modify("last day of $this->month_name $this->year");
  }
}
