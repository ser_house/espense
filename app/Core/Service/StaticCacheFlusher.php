<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.11.2019
 * Time: 12:18
 */


namespace App\Core\Service;


use App\Core\StaticCache;

class StaticCacheFlusher implements ICacheFlusher {
  /**
   * @inheritDoc
   */
  public function flush(string $key): void {
    StaticCache::flush($key);
  }

  /**
   * @inheritDoc
   */
  public function clear(): void {
    StaticCache::clear();
  }

}
