<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.11.2019
 * Time: 12:17
 */


namespace App\Core\Service;


interface ICacheFlusher {

  /**
   * @param string $key
   */
  public function flush(string $key): void;

  /**
   *
   */
  public function clear(): void;
}
