<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 0:26
 */


namespace App\Core\Product;

use App\Core\Category\CategoryId;
use App\Core\Title;

/**
 * Class Product
 *
 * @package App\Core
 */
final class Product {

  public function __construct(
    public readonly ProductId $id,
    public readonly Title $title,
    public readonly CategoryId $categoryId,
  ) {

  }
}
