<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.02.2020
 * Time: 19:51
 */


namespace App\Core\Product\UseCase\Rename;

use App\Core\Product\Product;

/**
 * Class RenamedEvent
 *
 * @package App\Core\Product\UseCase\Rename
 */
class RenamedEvent {

  public function __construct(
    public readonly Product $originalProduct,
    public readonly Product $updatedProduct,
  ) {

  }
}
