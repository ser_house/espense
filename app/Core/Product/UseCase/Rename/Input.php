<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:15
 */


namespace App\Core\Product\UseCase\Rename;

use App\Core\Exception\MissingRequiredDataException;
use App\Core\Product\ProductId;
use App\Core\Title;
use Exception;

class Input {

  public readonly ProductId $id;
  public readonly Title $newTitle;


  /**
   * Input constructor.
   *
   * @param array $input = [
   *    'id' => (string), required
   *    'title' => (string), required
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    if (empty($input['id'])) {
      throw new MissingRequiredDataException(['Не указан id товара.']);
    }
    if (empty($input['title'])) {
      throw new MissingRequiredDataException(['Не указано название товара.']);
    }

    $this->id = new ProductId($input['id']);
    $this->newTitle = new Title($input['title']);
  }
}
