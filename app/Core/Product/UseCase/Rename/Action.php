<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:14
 */


namespace App\Core\Product\UseCase\Rename;

use App\Core\Exception\NotFoundEntityException;
use App\Core\Product\IRepository as ProductRepository;
use App\Core\Product\Product as DomainProduct;

class Action {

  public function __construct(private readonly ProductRepository $productRepository) {

  }

  /**
   * @param Input $input
   *
   * @return RenamedEvent
   */
  public function handle(Input $input): RenamedEvent {
    $product = $this->productRepository->findById($input->id);
    if (null === $product) {
      throw new NotFoundEntityException('Переименование невозможно: товар не найден.');
    }

    $updatedProduct = new DomainProduct($input->id, $input->newTitle, $product->categoryId);
    $this->productRepository->update($updatedProduct);

    return new RenamedEvent($product, $updatedProduct);
  }
}
