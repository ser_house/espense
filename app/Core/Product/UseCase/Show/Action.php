<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:14
 */


namespace App\Core\Product\UseCase\Show;

use App\Core\Exception\NotFoundEntityException;
use App\Core\Product\DTO\Item as ProductItem;
use App\Core\Product\IDataProvider as ProductDataProvider;

class Action {

  public function __construct(private readonly ProductDataProvider $productDataProvider) {

  }

  /**
   * @param Input $input
   *
   * @return ProductItem
   */
  public function handle(Input $input): ProductItem {
    $productItem = $this->productDataProvider->findById($input->id);
    if (null === $productItem) {
      throw new NotFoundEntityException('Товар не найден.');
    }

    return $productItem;
  }
}
