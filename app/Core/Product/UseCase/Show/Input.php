<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:15
 */


namespace App\Core\Product\UseCase\Show;

use App\Core\Exception\MissingRequiredDataException;
use App\Core\Product\ProductId;
use Exception;


class Input {

  public readonly ProductId $id;

  /**
   * Input constructor.
   *
   * @param array $input = [
   *    'id' => (string), required
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    if (empty($input['id'])) {
      throw new MissingRequiredDataException(['Не указан id товара.']);
    }

    $this->id = new ProductId($input['id']);
  }
}
