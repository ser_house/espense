<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 16:37
 */


namespace App\Core\Product\UseCase\Add;

use App\Core\Category\CategoryId;
use App\Core\Exception\MissingRequiredDataException;
use App\Core\Title;
use Exception;

class Input {

  public readonly Title $title;
  public readonly CategoryId $categoryId;

  /**
   * Input constructor.
   *
   * Категория товара должна быть указана либо как id, либо как title.
   *
   * @param array $input = [
   *    'title' => (string), required
   *    'category_id' => (string), required
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    if (empty($input['title'])) {
      throw new MissingRequiredDataException(['Не указано название товара.']);
    }

    if (empty($input['category_id'])) {
      throw new MissingRequiredDataException(['Не указана категория.']);
    }

    $this->title = new Title($input['title']);
    $this->categoryId = new CategoryId($input['category_id']);
  }
}
