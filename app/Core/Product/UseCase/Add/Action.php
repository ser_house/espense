<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:04
 */


namespace App\Core\Product\UseCase\Add;


use App\Core\Category\IRepository as CategoryRepository;
use App\Core\Exception\ExistsException;
use App\Core\Exception\NotFoundEntityException;
use App\Core\Product\Converter;
use App\Core\Product\IRepository as ProductRepository;
use App\Core\Product\Product as DomainProduct;

class Action {

  public function __construct(
    private readonly ProductRepository $productRepository,
    private readonly CategoryRepository $categoryRepository,
    private readonly Converter $productConverter,
  ) {

  }


  /**
   * @param Input $input
   *
   * @return AddedEvent
   */
  public function handle(Input $input): AddedEvent {
    $category = $this->categoryRepository->findById($input->categoryId);

    if (null === $category) {
      throw new NotFoundEntityException('Добавление товара невозможно: категория не найдена.');
    }

    $exists = $this->productRepository->findByTitleAndCategory($input->title, $category->id);
    if ($exists) {
      $category_title = (string)$category->title;
      $item = $this->productConverter->domainToDto($exists);
      throw new ExistsException("Добавление невозможно: в категории '$category_title' уже есть товар '$input->title'.", $item);
    }

    $id = $this->productRepository->nextId();

    $product = new DomainProduct($id, $input->title, $category->id);
    $this->productRepository->add($product);

    return new AddedEvent($product, $category);
  }
}
