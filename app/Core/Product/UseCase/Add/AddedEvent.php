<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.02.2020
 * Time: 14:51
 */


namespace App\Core\Product\UseCase\Add;


use App\Core\Category\Category;
use App\Core\IEvent;
use App\Core\Product\Product;

class AddedEvent implements IEvent {

  public function __construct(public readonly Product $product, public readonly Category $category) {

  }
}
