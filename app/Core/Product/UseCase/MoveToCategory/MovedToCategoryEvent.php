<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.02.2020
 * Time: 19:51
 */


namespace App\Core\Product\UseCase\MoveToCategory;

use App\Core\Category\Category;
use App\Core\Product\Product;
use App\Core\Product\UseCase\Add\AddedEvent;


class MovedToCategoryEvent extends AddedEvent {

  public function __construct(Product $product, Category $category, public readonly Category $oldCategory) {
    parent::__construct($product, $category);
  }
}
