<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:15
 */


namespace App\Core\Product\UseCase\MoveToCategory;


use App\Core\Category\CategoryId;
use App\Core\Exception\MissingRequiredDataException;
use App\Core\Product\ProductId;
use Exception;


class Input {

  public readonly ProductId $productId;
  public readonly CategoryId $categoryId;

  /**
   * Input constructor.
   *
   * @param array $input = [
   *    'id' => (string), required
   *    'category_id' => (string), required
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    if (empty($input['id'])) {
      throw new MissingRequiredDataException(['Не указан id товара.']);
    }

    if (empty($input['category_id'])) {
      throw new MissingRequiredDataException(['Не указаны данные категории.']);
    }

    $this->productId = new ProductId($input['id']);
    $this->categoryId = new CategoryId($input['category_id']);
  }
}
