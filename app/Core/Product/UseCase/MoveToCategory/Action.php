<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:14
 */


namespace App\Core\Product\UseCase\MoveToCategory;

use App\Core\Category\IRepository as CategoryRepository;
use App\Core\Exception\NotFoundEntityException;
use App\Core\IEventDispatcher;
use App\Core\Product\IRepository as ProductRepository;


class Action {

  public function __construct(
    private readonly ProductRepository $productRepository,
    private readonly CategoryRepository $categoryRepository,
    private readonly IEventDispatcher $eventDispatcher,
  ) {

  }


  /**
   * @param Input $input
   *
   * @return MovedToCategoryEvent
   */
  public function handle(Input $input): MovedToCategoryEvent {
    $product = $this->productRepository->findById($input->productId);
    if (null === $product) {
      throw new NotFoundEntityException('Перемещение невозможно: товар не найден.');
    }

    $category = $this->categoryRepository->findById($input->categoryId);
    if (null === $category) {
      throw new NotFoundEntityException('Перемещение невозможно: категория не найдена.');
    }

    $oldCategory = $this->categoryRepository->findById($product->categoryId);

    $this->productRepository->updateCategory($input->productId, $input->categoryId);

    $event = new MovedToCategoryEvent($product, $category, $oldCategory);
    $this->eventDispatcher->dispatch($event);
    return $event;
  }
}
