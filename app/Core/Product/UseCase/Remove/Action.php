<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:29
 */


namespace App\Core\Product\UseCase\Remove;

use App\Core\Exception\BusyEntityException;
use App\Core\Exception\NotFoundEntityException;
use App\Core\Expense\IDataProvider as ExpenseDataProvider;
use App\Core\Product\IRepository as ProductRepository;

class Action {

  public function __construct(
    private readonly ProductRepository $productRepository,
    private readonly ExpenseDataProvider $expenseDataProvider,
  ) {

  }


  /**
   * @param Input $input
   *
   * @return RemovedEvent
   */
  public function handle(Input $input): RemovedEvent {
    $product = $this->productRepository->findById($input->id);
    if (null === $product) {
      throw new NotFoundEntityException('Удаление невозможно: товар не найден.');
    }

    $count_expenses = $this->expenseDataProvider->countByProductId($input->id);
    if ($count_expenses) {
      throw new BusyEntityException('Удаление невозможно: товар используется в расходах.');
    }

    $this->productRepository->remove($input->id);

    return new RemovedEvent($product);
  }
}
