<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.02.2020
 * Time: 15:02
 */


namespace App\Core\Product\UseCase\Remove;


use App\Core\Product\Product;

class RemovedEvent {

  public function __construct(public readonly Product $product) {

  }
}
