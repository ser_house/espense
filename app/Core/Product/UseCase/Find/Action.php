<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:14
 */


namespace App\Core\Product\UseCase\Find;

use App\Core\Generic\DTO\Item;
use App\Core\Product\IDataProvider;


class Action {

  public function __construct(private readonly IDataProvider $productDataProvider) {

  }

  /**
   * @param Input $input
   *
   * @return Item[]
   */
  public function handle(Input $input): array {
    return $this->productDataProvider->search($input);
  }
}
