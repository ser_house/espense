<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.11.2019
 * Time: 9:05
 */


namespace App\Core\Product\UseCase\Find;


use App\Core\Exception\MissingRequiredDataException;
use App\Core\Title;

class Input {
  public readonly ?string $like;
  public readonly ?Title $title;
  public readonly ?Title $categoryTitle;


  /**
   * Input constructor.
   *
   * @param array $input
   */
  public function __construct(array $input) {
    if (empty($input['like']) && empty($input['title']) && empty($input['category_title'])) {
      throw new MissingRequiredDataException(['Не указана строка для поиска ("like" - для поиска по частичному совпадению в названии; "title" - для поиска по точному совпадению названия; "category_title" - для поиска по частичному совпадению в названии категории).']);
    }

    if (!empty($input['like'])) {
      $this->like = trim($input['like']);
    }
    else {
      $this->like = null;
    }

    if (!empty($input['title'])) {
      $this->title = new Title($input['title']);
    }
    else {
      $this->title = null;
    }

    if (!empty($input['category_title'])) {
      $this->categoryTitle = new Title($input['category_title']);
    }
    else {
      $this->categoryTitle = null;
    }
  }
}
