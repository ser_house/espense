<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 20.11.2019
 * Time: 3:54
 */


namespace App\Core\Product;

use App\Core\Category\CategoryId;
use App\Core\Product\DTO\Item as ProductItem;
use App\Core\Product\DTO\ManageItem as ManageProductItem;
use App\Core\Product\Exceptions\ProductNotFoundException;
use App\Core\Product\UseCase\Find\Input;

interface IDataProvider {

  /**
   * @param ProductId $productId
   *
   * @return ProductItem|null
   */
  public function findById(ProductId $productId): ?ProductItem;

  /**
   * @param ProductId $productId
   *
   * @return ProductItem
   * @throws ProductNotFoundException
   */
  public function getById(ProductId $productId): ProductItem;

  /**
   * @param Input $input
   *
   * @return ProductItem[]
   */
  public function search(Input $input): array;

  /**
   * @param CategoryId $categoryId
   *
   * @return int
   */
  public function countByCategoryId(CategoryId $categoryId): int;

  /**
   *
   * @return ManageProductItem[]
   */
  public function allManageItems(): array;

  /**
   * @param CategoryId $categoryId
   *
   * @return string[]
   */
  public function findTitlesByCategoryId(CategoryId $categoryId): array;
}
