<?php

namespace App\Core\Product\DTO;

use App\Core\Category\DTO\Item as Category;

class ProductItem {
  public function __construct(
    public readonly string $id,
    public readonly string $title,
    public readonly Category $category,
  ) {

  }
}
