<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 20.11.2019
 * Time: 4:14
 */


namespace App\Core\Product\DTO;

use App\Core\Category\DTO\Item as Category;

class Item {

  public function __construct(
    public readonly string $id,
    public readonly string $title,
    public readonly Category $category,
  ) {

  }
}
