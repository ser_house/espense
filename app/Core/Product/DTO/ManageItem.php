<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 26.11.2019
 * Time: 10:58
 */


namespace App\Core\Product\DTO;

use App\Core\Generic\DTO\Item as GenericItem;

class ManageItem extends GenericItem {

  public function __construct(
    string $id,
    string $title,
    public readonly int $expenses_count,
    public readonly string $category_id,
  ) {
    parent::__construct($id, $title);
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize(): mixed {
    $array = parent::jsonSerialize();
    $array['category_id'] = $this->category_id;
    $array['expenses_count'] = $this->expenses_count;

    return $array;
  }
}
