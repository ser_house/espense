<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 10:45
 */


namespace App\Core\Product;

use App\Core\Category\CategoryId;
use App\Core\Exception\NotFoundEntityException;
use App\Core\Title;

interface IRepository {

  /**
   *
   * @return ProductId
   */
  public function nextId(): ProductId;

  /**
   * @param Product $product
   */
  public function add(Product $product): void;

  /**
   * @param Product $product
   */
  public function update(Product $product): void;

  /**
   * @param ProductId $productId
   * @param CategoryId $newCategoryId
   */
  public function updateCategory(ProductId $productId, CategoryId $newCategoryId): void;

  /**
   * @param ProductId $productId
   */
  public function remove(ProductId $productId);

  /**
   *
   * @return Product[]
   */
  public function findAll(): array;

  /**
   * @param ProductId $productId
   *
   * @return Product
   * @throws NotFoundEntityException
   */
  public function get(ProductId $productId): Product;

  /**
   * @param ProductId $productId
   *
   * @return Product|null
   */
  public function findById(ProductId $productId): ?Product;

  /**
   * @param Title $title
   *
   * @return Product|null
   */
  public function findByTitle(Title $title): ?Product;

  /**
   * @param Title $title
   * @param CategoryId $categoryId
   *
   * @return Product|null
   */
  public function findByTitleAndCategory(Title $title, CategoryId $categoryId): ?Product;
}
