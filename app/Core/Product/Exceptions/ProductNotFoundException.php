<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 17.05.2021
 * Time: 23:20
 */


namespace App\Core\Product\Exceptions;


use DomainException;

class ProductNotFoundException extends DomainException {

}
