<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.03.2020
 * Time: 10:01
 */


namespace App\Core\Product;


use App\Core\Generic\DTO\Item;

class Converter {
  /**
   * @param Product $product
   *
   * @return Item
   */
  public function domainToDto(Product $product): Item {
    return new Item((string)$product->id, (string)$product->title);
  }
}
