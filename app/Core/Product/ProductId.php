<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 0:13
 */


namespace App\Core\Product;

use App\Core\EntityId;

/**
 * Class ProductId
 *
 * @package App\Core
 */
final class ProductId extends EntityId {

}
