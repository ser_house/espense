<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 25.10.2019
 * Time: 19:11
 */


namespace App\Core\Exception;


use App\Core\EntityId;
use Exception;
use Throwable;

class FailedToSaveEntityException extends Exception {

  /**
   * @inheritDoc
   */
  public function __construct(public readonly EntityId $entityId, Throwable $previous = null) {
    parent::__construct('Сохранить не удалось.', 0, $previous);
  }
}
