<?php

namespace App\Core\Exception;

use DomainException;
use Throwable;

class ValueNotInExceptedException extends DomainException {

  public function __construct(string $value, array $excepted, Throwable $previous = null) {
    $excepted_str = implode(', ', $excepted);
    $msg = "Значение '$value' недопустимо. Допустимые значения: $excepted_str";
    parent::__construct($msg, 0, $previous);
  }
}
