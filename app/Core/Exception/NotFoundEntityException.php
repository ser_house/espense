<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.11.2019
 * Time: 8:52
 */


namespace App\Core\Exception;


use DomainException;

class NotFoundEntityException extends DomainException {

}
