<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:34
 */


namespace App\Core\Exception;


use DomainException;

class BusyEntityException extends DomainException {

}
