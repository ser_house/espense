<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.02.2020
 * Time: 20:18
 */


namespace App\Core\Exception;


use DomainException;

class NotEqualsAmountsException extends DomainException {

}
