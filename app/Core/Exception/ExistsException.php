<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:06
 */


namespace App\Core\Exception;

use App\Core\Generic\DTO\Item;
use DomainException;
use Throwable;

class ExistsException extends DomainException {

  public function __construct(string $message, public readonly Item $existsItem, Throwable $previous = null) {
    parent::__construct($message, 0, $previous);
  }
}
