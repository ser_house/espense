<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.11.2019
 * Time: 8:42
 */


namespace App\Core\Exception;


use InvalidArgumentException;

class MissingRequiredDataException extends InvalidArgumentException {
  /**
   * @inheritDoc
   */
  public function __construct(public readonly array $missing_data) {
    parent::__construct(implode(PHP_EOL, $this->missing_data));
  }
}
