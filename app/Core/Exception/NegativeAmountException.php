<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.02.2020
 * Time: 10:13
 */


namespace App\Core\Exception;


use DomainException;

class NegativeAmountException extends DomainException {

}
