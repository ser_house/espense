<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.02.2020
 * Time: 19:51
 */


namespace App\Core\Tracking\UseCase\UpdateQty;

use App\Core\Expense\Entity\Quantity;
use App\Core\Product\DTO\ProductItem;
use App\Core\Tracking\Track;


class UpdatedTrackingQtyEvent {

  public function __construct(
    public readonly Track $track,
    public readonly ProductItem $productItem,
    public readonly Quantity $oldQty,
  ) {

  }
}
