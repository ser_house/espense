<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:15
 */


namespace App\Core\Tracking\UseCase\UpdateQty;

use App\Core\Exception\MissingRequiredDataException;
use App\Core\Expense\Entity\Quantity;
use App\Core\Tracking\TrackId;
use Exception;

class Input {

  public readonly TrackId $trackId;
  public readonly Quantity $qty;

  /**
   * Input constructor.
   *
   * @param array $input = [
   *    'id' => (string), required
   *    'new_qty' => (int), required
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    $missing_data = [];
    if (empty($input['id'])) {
      $missing_data[] = 'Не указан id записи учета срока использования.';
    }
    if (empty($input['new_qty'])) {
      $missing_data[] = 'Не указано новое кол-во.';
    }

    if (!empty($missing_data)) {
      throw new MissingRequiredDataException($missing_data);
    }

    $this->trackId = new TrackId($input['id']);
    $this->qty = new Quantity($input['new_qty']);
  }
}
