<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:14
 */


namespace App\Core\Tracking\UseCase\UpdateQty;

use App\Core\Exception\NotFoundEntityException;
use App\Core\Tracking\IRepository as TrackRepository;
use App\Core\Expense\IDataProvider as ExpenseDataProvider;

class Action {

  public function __construct(
    private readonly TrackRepository $trackRepository,
    private readonly ExpenseDataProvider $expenseDataProvider,
  ) {

  }

  /**
   * @param Input $input
   *
   * @return UpdatedTrackingQtyEvent
   */
  public function handle(Input $input): UpdatedTrackingQtyEvent {

    $track = $this->trackRepository->get($input->trackId);
    if (null === $track) {
      throw new NotFoundEntityException('Завершение невозможно: отслеживание товара не найдено.');
    }

    $oldQty = $track->quantity;

    $track->quantity = $input->qty;
    $this->trackRepository->setQty($track->id, $track->quantity);

    $expense = $this->expenseDataProvider->itemById($track->expenseId);
    return new UpdatedTrackingQtyEvent($track, $expense->product, $oldQty);
  }
}
