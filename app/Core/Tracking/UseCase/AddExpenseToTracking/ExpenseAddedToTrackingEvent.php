<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.02.2020
 * Time: 19:51
 */


namespace App\Core\Tracking\UseCase\AddExpenseToTracking;

use App\Core\Expense\Entity\Expense;

class ExpenseAddedToTrackingEvent {

  public function __construct(public readonly Expense $expense) {

  }
}
