<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:14
 */


namespace App\Core\Tracking\UseCase\AddExpenseToTracking;

use App\Core\Exception\NotFoundEntityException;
use App\Core\Expense\IRepository as ExpenseRepository;
use App\Core\Tracking\IRepository as TrackRepository;

class Action {

  public function __construct(
    private readonly ExpenseRepository $expenseRepository,
    private readonly TrackRepository $trackRepository,
  ) {

  }

  /**
   * @param Input $input
   *
   * @return ExpenseAddedToTrackingEvent
   */
  public function handle(Input $input): ExpenseAddedToTrackingEvent {

    $expense = $this->expenseRepository->findById($input->expenseId);
    if (null === $expense) {
      throw new NotFoundEntityException('Добавление расхода невозможно: расход не найден.');
    }
    $this->trackRepository->addExpenseToTracking($expense);

    return new ExpenseAddedToTrackingEvent($expense);
  }
}
