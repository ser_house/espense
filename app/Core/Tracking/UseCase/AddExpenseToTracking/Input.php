<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:15
 */


namespace App\Core\Tracking\UseCase\AddExpenseToTracking;

use App\Core\Exception\MissingRequiredDataException;
use App\Core\Expense\Entity\ExpenseId;
use Exception;

class Input {

  public readonly ExpenseId $expenseId;

  /**
   * Input constructor.
   *
   * @param array $input = [
   *    'id' => (string), required
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    $missing_data = [];
    if (empty($input['id'])) {
      $missing_data[] = 'Не указан id расхода.';
    }
    if (!empty($missing_data)) {
      throw new MissingRequiredDataException($missing_data);
    }

    $this->expenseId = new ExpenseId($input['id']);
  }
}
