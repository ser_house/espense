<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.02.2020
 * Time: 19:51
 */


namespace App\Core\Tracking\UseCase\Add;

use App\Core\Tracking\Track;


class AddedTrackingEvent {

  public function __construct(public readonly Track $track) {

  }
}
