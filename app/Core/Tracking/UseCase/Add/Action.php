<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:14
 */


namespace App\Core\Tracking\UseCase\Add;

use App\Core\Exception\NotFoundEntityException;
use App\Core\Expense\IRepository as ExpenseRepository;
use App\Core\Tracking\IRepository as TrackRepository;
use App\Core\Tracking\Track;

class Action {

  public function __construct(
    private readonly ExpenseRepository $expenseRepository,
    private readonly TrackRepository $trackRepository,
  ) {

  }

  /**
   * @param Input $input
   *
   * @return AddedTrackingEvent
   */
  public function handle(Input $input): AddedTrackingEvent {

    $expense = $this->expenseRepository->findById($input->expenseId);
    if (null === $expense) {
      throw new NotFoundEntityException('Изменение невозможно: расход не найден.');
    }

    $track = new Track($expense->id, $input->startedDate, $input->quantity);
    $this->trackRepository->add($track);

    return new AddedTrackingEvent($track);
  }
}
