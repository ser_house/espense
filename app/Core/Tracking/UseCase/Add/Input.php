<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:15
 */


namespace App\Core\Tracking\UseCase\Add;

use App\Core\Exception\MissingRequiredDataException;
use App\Core\Expense\Entity\ExpenseId;
use App\Core\Expense\Entity\Quantity;
use DateTimeImmutable;
use Exception;

class Input {

  public readonly ExpenseId $expenseId;
  public readonly Quantity $quantity;
  public readonly DateTimeImmutable $startedDate;

  /**
   * Input constructor.
   *
   * @param array $input = [
   *    'expense_id' => (string), required
   *    'start_date' => (string), required
   *    'quantity' => (string), required
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    $missing_data = [];
    if (empty($input['expense_id'])) {
      $missing_data[] = 'Не указан id расхода.';
    }
    if (empty($input['quantity'])) {
      $missing_data[] = 'Кол-во не указано.';
    }
    if (empty($input['start_date'])) {
      $missing_data[] = 'Дата начала не указана.';
    }

    if (!empty($missing_data)) {
      throw new MissingRequiredDataException($missing_data);
    }

    $this->expenseId = new ExpenseId($input['expense_id']);
    $this->quantity = new Quantity($input['quantity']);
    $this->startedDate = new DateTimeImmutable($input['start_date']);
  }
}
