<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:14
 */


namespace App\Core\Tracking\UseCase\Finish;

use App\Core\Exception\NotFoundEntityException;
use App\Core\Tracking\IRepository as TrackRepository;
use App\Core\Expense\IDataProvider as ExpenseDataProvider;

class Action {

  public function __construct(
    private readonly TrackRepository $trackRepository,
    private readonly ExpenseDataProvider $expenseDataProvider,
  ) {

  }

  /**
   * @param Input $input
   *
   * @return FinishedTrackingEvent
   */
  public function handle(Input $input): FinishedTrackingEvent {

    $track = $this->trackRepository->get($input->trackId);
    if (null === $track) {
      throw new NotFoundEntityException('Завершение невозможно: отслеживание товара не найдено.');
    }

    $track->setFinishedDate($input->finishedDate);
    $this->trackRepository->setFinishedDate($track->id, $track->finishedDate(), $track->days());

    $expense = $this->expenseDataProvider->itemById($track->expenseId);
    return new FinishedTrackingEvent($track, $expense->product);
  }
}
