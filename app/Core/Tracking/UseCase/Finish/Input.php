<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:15
 */


namespace App\Core\Tracking\UseCase\Finish;

use App\Core\Exception\MissingRequiredDataException;
use App\Core\Tracking\TrackId;
use DateTimeImmutable;
use Exception;

class Input {

  public readonly TrackId $trackId;
  public readonly DateTimeImmutable $finishedDate;

  /**
   * Input constructor.
   *
   * @param array $input = [
   *    'id' => (string), required
   *    'finished_date' => (string), required
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    $missing_data = [];
    if (empty($input['id'])) {
      $missing_data[] = 'Не указан id записи учета срока использования.';
    }
    if (empty($input['finished_date'])) {
      $missing_data[] = 'Не указана дата завершения.';
    }

    if (!empty($missing_data)) {
      throw new MissingRequiredDataException($missing_data);
    }

    $this->trackId = new TrackId($input['id']);
    $this->finishedDate = new DateTimeImmutable($input['finished_date']);
  }
}
