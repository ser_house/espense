<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:14
 */


namespace App\Core\Tracking\UseCase\RemoveExpenseFromToTracking;

use App\Core\Exception\NotFoundEntityException;
use App\Core\Expense\IRepository as ExpenseRepository;
use App\Core\Tracking\IRepository as TrackRepository;

class Action {

  public function __construct(
    private readonly ExpenseRepository $expenseRepository,
    private readonly TrackRepository $trackRepository,
  ) {

  }

  /**
   * @param Input $input
   *
   * @return ExpenseRemovedFromToTrackingEvent
   */
  public function handle(Input $input): ExpenseRemovedFromToTrackingEvent {

    $expense = $this->expenseRepository->findById($input->expenseId);
    if (null === $expense) {
      throw new NotFoundEntityException('Удаление расхода из списка невозможно: расход не найден.');
    }
    $this->trackRepository->removeExpenseFromToTracking($expense);

    return new ExpenseRemovedFromToTrackingEvent($expense);
  }
}
