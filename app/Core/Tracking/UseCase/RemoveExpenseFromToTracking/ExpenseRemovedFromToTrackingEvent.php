<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.02.2020
 * Time: 19:51
 */


namespace App\Core\Tracking\UseCase\RemoveExpenseFromToTracking;

use App\Core\Expense\Entity\Expense;

class ExpenseRemovedFromToTrackingEvent {

  public function __construct(public readonly Expense $expense) {

  }
}
