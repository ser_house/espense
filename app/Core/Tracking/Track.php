<?php

namespace App\Core\Tracking;

use App\Core\Expense\Entity\ExpenseId;
use App\Core\Expense\Entity\Quantity;
use DateTimeImmutable;

class Track {
  public readonly TrackId $id;
  private ?DateTimeImmutable $finishedDate = null;
  private ?int $days = null;

  public function __construct(
    public readonly ExpenseId $expenseId,
    public readonly DateTimeImmutable $startedDate,
    public Quantity $quantity
  ) {
    $this->id = new TrackId();
  }

  /**
   * @param DateTimeImmutable $finishedDate
   *
   * @return int кол-во дней
   */
  public function setFinishedDate(DateTimeImmutable $finishedDate): int {
    $this->finishedDate = $finishedDate;
    $diff = $this->finishedDate->diff($this->startedDate);
    $this->days = $diff->days;
    return $this->days;
  }

  public function finishedDate(): ?DateTimeImmutable {
    return $this->finishedDate ?? null;
  }

  public function days(): int {
    return $this->days ?? 0;
  }
}
