<?php

namespace App\Core\Tracking\DTO;

use App\Core\Expense\DTO\Item as ExpenseItem;
use DateTimeImmutable;

class CurrentItem {

  public readonly int $days;

  public function __construct(
    public readonly string $id,
    public readonly ExpenseItem $expense,
    public readonly float $quantity,
    public readonly DateTimeImmutable $startedDate,
  ) {
    $today = new DateTimeImmutable();
    $diff = $today->diff($this->startedDate);
    $this->days = $diff->days;
  }
}
