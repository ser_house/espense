<?php

namespace App\Core\Tracking\DTO;

readonly class FinishedTrackingProduct {

  public function __construct(
    public string $title,
    public float $qty,
    public string $unit,
    public int $days,
    public float $in_day,
  ) {
  }
}
