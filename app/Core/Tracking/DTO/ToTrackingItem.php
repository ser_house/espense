<?php

namespace App\Core\Tracking\DTO;

use App\Core\Expense\DTO\Item as ExpenseItem;

class ToTrackingItem {

  public function __construct(public readonly ExpenseItem $expenseItem, public readonly bool $is_tracking) {
  }
}
