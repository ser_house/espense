<?php

namespace App\Core\Tracking\DTO;

use App\Core\Expense\DTO\Item as ExpenseItem;
use DateTimeImmutable;

class ArchiveItem {

  public function __construct(
    public readonly string $id,
    public readonly ExpenseItem $expense,
    public readonly float $quantity,
    public readonly DateTimeImmutable $startedDate,
    public readonly DateTimeImmutable $finishedDate,
    public readonly int $days,
  ) {

  }
}
