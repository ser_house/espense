<?php

namespace App\Core\Tracking\Views;

use App\Core\Expense\Main\View as ExpenseView;

class ViewCurrent {
  public string $id;
  public ExpenseView $expense;
  public float $quantity;
  public string $started_date;
  public int $days;
}
