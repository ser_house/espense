<?php

namespace App\Core\Tracking\Views;

use App\Core\Tracking\DTO\ArchiveItem;
use App\Core\Tracking\DTO\CurrentItem;
use App\Core\Tracking\DTO\ToTrackingItem;
use App\Core\Expense\Main\Presenter as ExpensePresenter;


class Presenter {

  public function __construct(
    private readonly ExpensePresenter $expensePresenter,
  ) {
  }

  /**
   * @param CurrentItem $item
   *
   * @return ViewCurrent
   */
  public function viewCurrentItem(CurrentItem $item): ViewCurrent {
    $view = new ViewCurrent();
    $view->id = $item->id;
    $view->expense = $this->expensePresenter->view($item->expense);
    $view->quantity = $item->quantity;
    $view->started_date = $item->startedDate->format('d.m.Y');
    $view->days = $item->days;

    return $view;
  }

  /**
   * @param CurrentItem[] $items
   *
   * @return ViewCurrent[]
   */
  public function viewsCurrentItems(array $items): array {
    $views = [];
    foreach ($items as $item) {
      $views[] = $this->viewCurrentItem($item);
    }
    return $views;
  }

  /**
   * @param ArchiveItem $item
   *
   * @return ViewArchive
   */
  public function viewArchiveItem(ArchiveItem $item): ViewArchive {
    $view = new ViewArchive();
    $view->id = $item->id;
    $view->expense = $this->expensePresenter->view($item->expense);
    $view->quantity = $item->quantity;
    $view->started_date = $item->startedDate->format('d.m.Y');
    $view->finished_date = $item->finishedDate->format('d.m.Y');
    $view->days = $item->days;

    return $view;
  }

  /**
   * @param ArchiveItem[] $items
   *
   * @return ViewArchive[]
   */
  public function viewsArchiveItems(array $items): array {
    $views = [];
    foreach ($items as $item) {
      $views[] = $this->viewArchiveItem($item);
    }
    return $views;
  }

  /**
   * @param ToTrackingItem $item
   *
   * @return ViewToTrackingItem
   */
  public function viewToTrackingItem(ToTrackingItem $item): ViewToTrackingItem {
    $view = new ViewToTrackingItem();
    $view->expense = $this->expensePresenter->view($item->expenseItem);
    $view->is_tracking = $item->is_tracking;
    return $view;
  }

  /**
   * @param ToTrackingItem[] $items
   *
   * @return ViewToTrackingItem[]
   */
  public function viewsToTrackingItems(array $items): array {
    $views = [];
    foreach ($items as $item) {
      $views[] = $this->viewToTrackingItem($item);
    }
    return $views;
  }
}
