<?php

namespace App\Core\Tracking\Views;

use App\Core\Expense\Main\View as ExpenseView;

class ViewToTrackingItem {
  public ExpenseView $expense;
  public bool $is_tracking;
}
