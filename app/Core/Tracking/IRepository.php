<?php

namespace App\Core\Tracking;

use App\Core\Exception\NotFoundEntityException;
use App\Core\Expense\Entity\Expense;
use App\Core\Expense\Entity\Quantity;
use DateTimeImmutable;

interface IRepository {

  /**
   * @param Track $track
   *
   * @return void
   */
  public function add(Track $track): void;

  /**
   * @param TrackId $trackId
   *
   * @return Track
   * @throws NotFoundEntityException
   */
  public function get(TrackId $trackId): Track;

  /**
   * @param TrackId $trackId
   * @param DateTimeImmutable $finishedDate
   * @param int $days
   *
   * @return void
   */
  public function setFinishedDate(TrackId $trackId, DateTimeImmutable $finishedDate, int $days): void;

  /**
   * @param TrackId $trackId
   * @param Quantity $quantity
   *
   * @return void
   */
  public function setQty(TrackId $trackId, Quantity $quantity): void;

  /**
   * @param Expense $expense
   *
   * @return void
   */
  public function addExpenseToTracking(Expense $expense): void;

  /**
   * @param Expense $expense
   *
   * @return void
   */
  public function removeExpenseFromToTracking(Expense $expense): void;
}
