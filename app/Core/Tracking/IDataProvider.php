<?php

namespace App\Core\Tracking;

use App\Core\Tracking\DTO\ArchiveItem;
use App\Core\Tracking\DTO\CurrentItem;
use App\Core\Tracking\DTO\FinishedTrackingProduct;
use App\Core\Tracking\DTO\ToTrackingItem;

interface IDataProvider {

  /**
   * @return CurrentItem[]
   */
  public function currentTrackedItems(): array;

  /**
   * @return ArchiveItem[]
   */
  public function finishedTracks(): array;

  /**
   * @return ToTrackingItem[]
   */
  public function toTrackingExpenses(): array;

  /**
   * @return FinishedTrackingProduct[]
   */
  public function finishedTotals(): array;
}
