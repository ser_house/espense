<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.09.2019
 * Time: 16:13
 */


namespace App\Core\Expense;

use App\Core\Category\CategoryId;
use App\Core\Contractor\ContractorId;
use App\Core\Expense\DTO\Item as ExpenseItem;
use App\Core\Expense\Entity\ExpenseId;
use App\Core\Expense\UseCase\Find\Input as FindInput;
use App\Core\Product\ProductId;
use App\Core\Unit\UnitId;
use Exception;

interface IDataProvider {

  /**
   * Если запись не найдена, то выбрасывается исключение.
   *
   * @param ExpenseId $expenseId
   *
   * @return ExpenseItem
   * @throws Exception
   */
  public function itemById(ExpenseId $expenseId): ExpenseItem;

  /**
   *
   * @return ExpenseItem|null
   */
  public function lastItem(): ?ExpenseItem;

  /**
   *
   * @return ExpenseItem[]
   */
  public function todayItems(): array;

  /**
   * @param FindInput $input
   *
   * @return array
   */
  public function find(FindInput $input): array;

  /**
   * @param ProductId $productId
   *
   * @return int
   */
  public function countByProductId(ProductId $productId): int;

  /**
   * @param ContractorId $contractorId
   *
   * @return int
   */
  public function countByContractorId(ContractorId $contractorId): int;

  /**
   * @param UnitId $unitId
   *
   * @return int
   */
  public function countByUnitId(UnitId $unitId): int;

  /**
   * @param UnitId $unitId
   * @param ExpenseId|null $expenseId
   * @param CategoryId|null $categoryId
   * @param ProductId|null $productId
   *
   * @return int
   */
  public function setUnitByParams(UnitId $unitId, ?ExpenseId $expenseId, ?CategoryId $categoryId, ?ProductId $productId): int;

  /**
   *
   * @return array
   */
  public function getFavoriteItems(): array;

  /**
   * @param ProductId $productId
   *
   * @return ExpenseId|null
   */
  public function getFavoriteExpenseId(ProductId $productId): ?ExpenseId;

  /**
   * @param ExpenseId $expenseId
   */
  public function addToFavorites(ExpenseId $expenseId): void;

  /**
   * @param ExpenseId $expenseId
   */
  public function removeFromFavorites(ExpenseId $expenseId): void;
}
