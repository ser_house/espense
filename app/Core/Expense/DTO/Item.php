<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 2:03
 */


namespace App\Core\Expense\DTO;

use App\Core\Generic\DTO\Item as DtoItem;
use App\Core\Product\DTO\ProductItem as ProductItem;
use DateTimeImmutable;

/**
 * Class Item
 *
 * @package App\Core\Expense\DTO
 */
class Item {
  public string $id;
  public DateTimeImmutable $date;
  public DtoItem $account;
  public ProductItem $product;
  public float $quantity;
  public ?DtoItem $unit = null;
  public float $amount;
  public ?DtoItem $contractor = null;
  public string $note;
  public bool $is_favorite;
  public bool $is_tracking;
}
