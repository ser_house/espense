<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 0:07
 */


namespace App\Core\Expense\Entity;

use Exception;

/**
 * Class Quantity
 *
 * @package App\Core\Expense\Entity
 */
final class Quantity {

  protected float $value;

  /**
   * Quantity constructor.
   *
   * @param string $value
   *
   * @throws Exception
   */
  public function __construct(string $value) {
    $value = str_replace(',', '.', $value);
    $this->value = (float)$value;
  }

  /**
   * @return float
   */
  public function value(): float {
    return $this->value;
  }

  /**
   * @param Quantity $quantity
   *
   * @return bool
   */
  public function equals(Quantity $quantity): bool {
    return $quantity->value() === $this->value();
  }

  /**
   * @inheritDoc
   */
  public function __toString() {
    return (string)$this->value;
  }
}
