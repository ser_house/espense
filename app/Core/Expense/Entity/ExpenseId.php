<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 0:13
 */


namespace App\Core\Expense\Entity;

use App\Core\EntityId;

/**
 * Class ExpenseId
 *
 * @package App\Core\Expense\Entity
 */
final class ExpenseId extends EntityId {

}
