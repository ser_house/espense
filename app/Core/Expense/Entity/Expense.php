<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 0:45
 */


namespace App\Core\Expense\Entity;

use App\Core\Account\AccountId;
use App\Core\Amount;
use App\Core\Contractor\ContractorId;
use App\Core\Generic\Note;
use App\Core\Product\ProductId;
use App\Core\Unit\UnitId;
use DateTimeImmutable;


final class Expense {

  public function __construct(
    public readonly ExpenseId $id,
    public readonly DateTimeImmutable $date,
    public readonly AccountId $accountId,
    public readonly ProductId $productId,
    public readonly UnitId $unitId,
    public readonly Quantity $quantity,
    public readonly Amount $amount,
    public readonly ?ContractorId $contractorId = null,
    public readonly ?Note $note = null,
  ) {

  }

  /**
   * Создаёт новый расход на основе текущего (дата, ед.изм., контрагент копируются из текущего).
   *
   * @param ProductId $productId
   * @param Quantity $quantity
   * @param Amount $amount
   * @param Note|null $note
   *
   * @return Expense
   */
  public function buildNewExpense(ProductId $productId, Quantity $quantity, Amount $amount, Note $note = null): Expense {
    return new Expense(
      new ExpenseId(),
      $this->date,
      $this->accountId,
      $productId,
      $this->unitId,
      $quantity,
      $amount,
      $this->contractorId,
      $note
    );
  }
}
