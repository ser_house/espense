<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 20.11.2019
 * Time: 18:12
 */


namespace App\Core\Expense\UseCase\Update;


use App\Core\Expense\Entity\Expense;
use App\Core\IEvent;

class UpdatedEvent implements IEvent {

  public function __construct(public readonly Expense $originalExpense, public readonly Expense $updatedExpense) {

  }
}
