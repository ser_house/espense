<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.02.2020
 * Time: 10:35
 */


namespace App\Core\Expense\UseCase\Update;

use App\Core\Exception\FailedToSaveEntityException;
use App\Core\Exception\NotFoundEntityException;
use App\Core\Expense\Entity\Expense;
use App\Core\Expense\IRepository as ExpenseRepository;
use App\Core\IEventDispatcher;


class Action {

  public function __construct(
    private readonly ExpenseRepository $expenseRepository,
    private readonly IEventDispatcher $eventDispatcher,
  ) {

  }


  /**
   * @param Input $input
   *
   * @return UpdatedEvent
   * @throws FailedToSaveEntityException
   */
  public function handle(Input $input): UpdatedEvent {
    $originalExpense = $this->expenseRepository->findById($input->id);
    if (null === $originalExpense) {
      throw new NotFoundEntityException('Расход не найден.');
    }

    $updatedExpense = new Expense(
      $originalExpense->id,
      $input->date ?: $originalExpense->date,
      $originalExpense->accountId,
      $input->productId ?: $originalExpense->productId,
      $input->unitId ?: $originalExpense->unitId,
      $input->quantity ?: $originalExpense->quantity,
      $input->amount ?: $originalExpense->amount,
      $input->contractorId ?: $originalExpense->contractorId,
      $input->note ?: $originalExpense->note
    );
    $this->expenseRepository->update($updatedExpense);

    $event = new UpdatedEvent($originalExpense, $updatedExpense);
    $this->eventDispatcher->dispatch($event);

    return $event;
  }
}
