<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.02.2020
 * Time: 10:47
 */


namespace App\Core\Expense\UseCase\Update;

use App\Core\Amount;
use App\Core\Contractor\ContractorId;
use App\Core\Exception\MissingRequiredDataException;
use App\Core\Expense\Entity\ExpenseId;
use App\Core\Expense\Entity\Quantity;
use App\Core\Generic\Note;
use App\Core\Product\ProductId;
use App\Core\Unit\UnitId;
use DateTimeImmutable;
use Exception;

class Input {

  public readonly ExpenseId $id;
  public readonly ?DateTimeImmutable $date;
  public readonly ?ProductId $productId;
  public readonly ?UnitId $unitId;
  public readonly ?Quantity $quantity;
  public readonly ?Amount $amount;
  public readonly ?ContractorId $contractorId;
  public readonly ?Note $note;

  /**
   * Input constructor.
   *
   * Как минимум значение одного поля должно быть указано, иначе нечего обновлять.
   *
   * @param array $input = [
   *    'id' => (string), required
   *    'date' => (string), optional
   *    'product_id' => (string), optional
   *    'quantity' => (string), optional
   *    'amount' => (string), optional
   *    'contractor_id' => (string), optional
   *    'note' => (string), optional
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    if (empty($input['id'])) {
      throw new MissingRequiredDataException(['id расхода не указан.']);
    }

    $this->id = new ExpenseId($input['id']);

    unset($input['id']);
    $input = array_filter($input);

    if (empty($input)) {
      $fields = [
        'date' => 'Дата не указана',
        'product_id' => 'Товар не указан',
        'unit_id' => 'Ед. измерения не указана',
        'quantity' => 'Кол-во не указано',
        'amount' => 'Сумма не указана',
        'contractor_id' => 'Контрагент не указан',
        'note' => 'Примечание не указано',
      ];
      $missing_data = array_values($fields);
      throw new MissingRequiredDataException($missing_data);
    }

    if (!empty($input['date'])) {
      $this->date = new DateTimeImmutable($input['date']);
    }
    else {
      $this->date = null;
    }

    if (!empty($input['product_id'])) {
      $this->productId = new ProductId($input['product_id']);
    }
    else {
      $this->productId = null;
    }

    if (!empty($input['unit_id'])) {
      $this->unitId = new UnitId($input['unit_id']);
    }
    else {
      $this->unitId = null;
    }

    if (!empty($input['quantity'])) {
      $this->quantity = new Quantity($input['quantity']);
    }
    else {
      $this->quantity = null;
    }

    if (!empty($input['amount'])) {
      $this->amount = new Amount($input['amount']);
    }
    else {
      $this->amount = null;
    }

    if (!empty($input['contractor_id'])) {
      $this->contractorId = new ContractorId($input['contractor_id']);
    }
    else {
      $this->contractorId = null;
    }

    if (!empty($input['note'])) {
      $this->note = new Note($input['note']);
    }
    else {
      $this->note = null;
    }
  }
}
