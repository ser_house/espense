<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.02.2020
 * Time: 10:28
 */


namespace App\Core\Expense\UseCase\Add;


use App\Core\Account\AccountId;
use App\Core\Amount;
use App\Core\Contractor\ContractorId;
use App\Core\Exception\MissingRequiredDataException;
use App\Core\Expense\Entity\Quantity;
use App\Core\Generic\Note;
use App\Core\Product\ProductId;
use App\Core\Unit\UnitId;
use DateTimeImmutable;
use Exception;

class Input {

  public readonly DateTimeImmutable $date;
  public readonly AccountId $accountId;
  public readonly ProductId $productId;
  public readonly UnitId $unitId;
  public readonly Quantity $quantity;
  public readonly Amount $amount;
  public readonly ?ContractorId $contractorId;
  public readonly ?Note $note;

  /**
   * Input constructor.
   *
   * @param array $input = [
   *    'date' => (string), required
   *    'account_id' => (string), required
   *    'product_id' => (string), required
   *    'quantity' => (string), required
   *    'amount' => (string), required
   *    'contractor_id' => (string), optional
   *    'note' => (string), optional
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    if (empty($input['date'])) {
      $missing_data[] = 'Дата не указана.';
    }

    if (empty($input['account_id'])) {
      $missing_data[] = 'Счёт не указан.';
    }

    if (empty($input['product_id'])) {
      $missing_data[] = 'Товар не указан.';
    }

    if (empty($input['unit_id'])) {
      $missing_data[] = 'Ед. измерения не указана.';
    }

    if (empty($input['quantity'])) {
      $missing_data[] = 'Кол-во не указано.';
    }

    if (empty($input['amount'])) {
      $missing_data[] = 'Сумма не указана.';
    }

    if (!empty($missing_data)) {
      throw new MissingRequiredDataException($missing_data);
    }

    $this->date = new DateTimeImmutable($input['date']);
    $this->productId = new ProductId($input['product_id']);
    $this->accountId = new AccountId($input['account_id']);
    $this->unitId = new UnitId($input['unit_id']);
    $this->quantity = new Quantity($input['quantity']);
    $this->amount = new Amount($input['amount']);

    if (!empty($input['contractor_id'])) {
      $this->contractorId = new ContractorId($input['contractor_id']);
    }
    else {
      $this->contractorId = null;
    }

    if (!empty($input['note'])) {
      $this->note = new Note($input['note']);
    }
    else {
      $this->note = null;
    }
  }
}
