<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.02.2020
 * Time: 10:35
 */


namespace App\Core\Expense\UseCase\Add;


use App\Core\Exception\FailedToSaveEntityException;
use App\Core\Expense\Entity\Expense as DomainExpense;
use App\Core\Expense\IDataProvider as ExpenseDataProvider;
use App\Core\Expense\IRepository as ExpenseRepository;
use App\Core\IEventDispatcher;
use App\Core\ITransaction as Transaction;
use Throwable;

class Action {

  public function __construct(
    private readonly ExpenseRepository $expenseRepository,
    private readonly ExpenseDataProvider $expenseDataProvider,
    private readonly IEventDispatcher $eventDispatcher,
    private readonly Transaction $transaction,
  ) {

  }

  /**
   * @param Input $input
   *
   * @return AddedEvent
   * @throws FailedToSaveEntityException
   */
  public function handle(Input $input): AddedEvent {
    $id = $this->expenseRepository->nextId();
    $expense = new DomainExpense(
      $id,
      $input->date,
      $input->accountId,
      $input->productId,
      $input->unitId,
      $input->quantity,
      $input->amount,
      $input->contractorId,
      $input->note
    );
    $prevFavoriteExpenseId = $this->expenseDataProvider->getFavoriteExpenseId($expense->productId);
    $this->transaction->start();
    try {
      $this->expenseRepository->add($expense);
      if ($prevFavoriteExpenseId) {
        $this->expenseDataProvider->addToFavorites($id);
        $this->expenseDataProvider->removeFromFavorites($prevFavoriteExpenseId);
      }
      $this->transaction->commit();
    }
    catch (Throwable $e) {
      $this->transaction->rollback();
      throw $e;
    }

    $event = new AddedEvent($expense, $prevFavoriteExpenseId);
    $this->eventDispatcher->dispatch($event);

    return $event;
  }
}
