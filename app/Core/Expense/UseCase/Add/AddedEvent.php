<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 25.10.2019
 * Time: 19:16
 */


namespace App\Core\Expense\UseCase\Add;


use App\Core\Expense\Entity\Expense;
use App\Core\Expense\Entity\ExpenseId;
use App\Core\IEvent;

/**
 * Class AddedEvent
 *
 */
class AddedEvent implements IEvent {

  public function __construct(
    public readonly Expense $expense,
    public readonly ?ExpenseId $prevFavoriteExpenseId = null,
  ) {

  }
}
