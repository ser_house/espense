<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.06.2021
 * Time: 19:16
 */


namespace App\Core\Expense\UseCase\RemoveFromFavorites;


use App\Core\Expense\Entity\ExpenseId;
use App\Core\Expense\IDataProvider as ExpenseDataProvider;
use App\Core\IEventDispatcher;


class Action {

  public function __construct(
    private readonly ExpenseDataProvider $expenseDataProvider,
    private readonly IEventDispatcher $eventDispatcher,
  ) {

  }


  /**
   * @param ExpenseId $expenseId
   *
   * @return RemovedFromFavoritesEvent
   */
  public function handle(ExpenseId $expenseId): RemovedFromFavoritesEvent {
    $this->expenseDataProvider->removeFromFavorites($expenseId);

    $event = new RemovedFromFavoritesEvent($expenseId);
    $this->eventDispatcher->dispatch($event);

    return $event;
  }
}
