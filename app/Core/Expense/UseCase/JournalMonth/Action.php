<?php

namespace App\Core\Expense\UseCase\JournalMonth;

use App\Core\Category\IRepository as CategoryRepository;
use App\Core\Contractor\IRepository as ContractorRepository;
use App\Core\Expense\IDataProvider as ExpenseDataProvider;
use App\Core\Expense\UseCase\Find\Action as FindUseCase;
use App\Core\Product\IDataProvider as ProductDataProvider;
use App\Core\Product\IRepository as ProductRepository;

class Action {

  public function __construct(
    private readonly CategoryRepository $categoryRepository,
    private readonly ProductRepository $productRepository,
    private readonly ContractorRepository $contractorRepository,
    private readonly ProductDataProvider $productDataProvider,
    private readonly FindUseCase $findUseCase,
  ) {

  }

  /**
   * @param array $raw_input
   *
   * @return Input
   */
  public function buildInput(array $raw_input): Input {
    return new Input(
      $raw_input,
      $this->categoryRepository,
      $this->productRepository,
      $this->contractorRepository,
      $this->productDataProvider,
    );
  }

  public function handle(Input $input): array {
    return $this->findUseCase->handle($input);
  }
}
