<?php

namespace App\Core\Expense\UseCase\JournalMonth;

use DateTimeImmutable;

class Input extends \App\Core\Expense\UseCase\Find\Input {

  public readonly int $year;
  public readonly int $month;

  protected function initDates(array $input): static {
    $this->date = null;

    if (empty($input)) {
      $this->year = (int)date('Y');
      $this->month = (int)date('m');
      $this->years = [$this->year];
      $this->months = [$this->month];
    }
    else {
      if (!empty($input['date'])) {
        $this->date = new DateTimeImmutable($input['date']);
      }
      [$this->year, $this->month] = explode('-', $input['month']);
      $this->years = [$this->year];
      $this->months = [$this->month];
    }

    return $this;
  }
}
