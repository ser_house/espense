<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:29
 */


namespace App\Core\Expense\UseCase\Remove;

use App\Core\Exception\NotFoundEntityException;
use App\Core\Expense\IRepository as ExpenseRepository;
use App\Core\IEventDispatcher;

class Action {

  public function __construct(
    private readonly ExpenseRepository $expenseRepository,
    private readonly IEventDispatcher $eventDispatcher,
  ) {

  }


  /**
   * @param Input $input
   *
   * @return RemovedEvent
   */
  public function handle(Input $input): RemovedEvent {
    $expense = $this->expenseRepository->findById($input->id);
    if (null === $expense) {
      throw new NotFoundEntityException('Удаление невозможно: расход не найден.');
    }

    $this->expenseRepository->remove($input->id);

    $event = new RemovedEvent($expense);
    $this->eventDispatcher->dispatch($event);

    return $event;
  }
}
