<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 20.11.2019
 * Time: 18:12
 */


namespace App\Core\Expense\UseCase\SetUnit;


use App\Core\IEvent;
use App\Core\Unit\UnitId;

class SetUnitEvent implements IEvent {

  public function __construct(public readonly UnitId $unitId, public readonly int $affected_num) {

  }
}
