<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.02.2020
 * Time: 10:47
 */


namespace App\Core\Expense\UseCase\SetUnit;

use App\Core\Category\CategoryId;
use App\Core\Exception\MissingRequiredDataException;
use App\Core\Expense\Entity\ExpenseId;
use App\Core\Product\ProductId;
use App\Core\Unit\UnitId;
use Exception;

class Input {

  public readonly UnitId $unitId;
  public readonly ?ExpenseId $expenseId;
  public readonly ?CategoryId $categoryId;
  public readonly ?ProductId $productId;

  /**
   * Input constructor.
   *
   * @param array $input = [
   *    'unit_id' => (string), required
   *    'expense_id' => (string), optional
   *    'product_id' => (string), optional
   *    'date' => (string), optional
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    if (empty($input['unit_id'])) {
      throw new MissingRequiredDataException(['id единицы измерения не указан.']);
    }

    $this->unitId = new UnitId($input['unit_id']);

    if (!empty($input['expense_id'])) {
      $this->expenseId = new ExpenseId($input['expense_id']);
    }
    else {
      $this->expenseId = null;
    }

    if (!empty($input['category_id'])) {
      $this->categoryId = new CategoryId($input['category_id']);
    }
    else {
      $this->categoryId = null;
    }

    if (!empty($input['product_id'])) {
      $this->productId = new ProductId($input['product_id']);
    }
    else {
      $this->productId = null;
    }
  }
}
