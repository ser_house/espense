<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.02.2020
 * Time: 10:35
 */


namespace App\Core\Expense\UseCase\SetUnit;

use App\Core\Expense\IDataProvider as ExpenseDataProvider;
use App\Core\IEventDispatcher;
use App\Core\ITransaction;
use Throwable;


class Action {

  public function __construct(
    private readonly ExpenseDataProvider $expenseDataProvider,
    private readonly IEventDispatcher $eventDispatcher,
    private readonly ITransaction $transaction,
  ) {

  }


  /**
   * @param Input $input
   *
   * @return SetUnitEvent
   * @throws Throwable
   */
  public function handle(Input $input): SetUnitEvent {
    $unitId = $input->unitId;
    $this->transaction->start();
    try {
      $affected_num = $this->expenseDataProvider->setUnitByParams($unitId, $input->expenseId, $input->categoryId, $input->productId);
      $this->transaction->commit();
    }
    catch (Throwable $e) {
      $this->transaction->rollback();
      throw $e;
    }

    $event = new SetUnitEvent($unitId, $affected_num);
    $this->eventDispatcher->dispatch($event);

    return $event;
  }
}
