<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:14
 */


namespace App\Core\Expense\UseCase\Show;

use App\Core\Exception\NotFoundEntityException;
use App\Core\Expense\DTO\Item as ExpenseItem;
use App\Core\Expense\IConverter as ExpenseConverter;
use App\Core\Expense\IRepository as ExpenseRepository;

class Action {

  public function __construct(
    private readonly ExpenseRepository $expenseRepository,
    private readonly ExpenseConverter $expenseConverter,
  ) {

  }


  /**
   * @param Input $input
   *
   * @return ExpenseItem
   */
  public function handle(Input $input): ExpenseItem {
    $expense = $this->expenseRepository->findById($input->id);
    if (null === $expense) {
      throw new NotFoundEntityException('Расход не найден.');
    }

    return $this->expenseConverter->domainToDto($expense);
  }
}
