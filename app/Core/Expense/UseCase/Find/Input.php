<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:15
 */


namespace App\Core\Expense\UseCase\Find;

use App\Core\Category\Category;
use App\Core\Category\IRepository as CategoryRepository;
use App\Core\Contractor\Contractor;
use App\Core\Contractor\IRepository as ContractorRepository;
use App\Core\Product\DTO\Item;
use App\Core\Product\IDataProvider as ProductDataProvider;
use App\Core\Product\IRepository as ProductRepository;
use App\Core\Product\Product;
use App\Core\Title;
use DateTimeImmutable;
use InvalidArgumentException;


class Input {

  public const ANY_KEY = 'any';
  public const ANY_LABEL = 'Любой';

  public const SORT_ASC = 'asc';
  public const SORT_DESC = 'desc';

  public readonly ?Category $category;
  public readonly ?Product $product;
  public readonly ?Contractor $contractor;
  public readonly bool $is_favorite;
  protected array $months;
  protected array $years;
  protected ?DateTimeImmutable $date;
  public readonly string $sort;


  public function __construct(
    array $input,
    private readonly CategoryRepository $categoryRepository,
    private readonly ProductRepository $productRepository,
    private readonly ContractorRepository $contractorRepository,
    private readonly ProductDataProvider $productDataProvider,
  ) {

    $this->category = $this->getCategory($input);
    $this->product = $this->getProduct($input);

    $this->contractor = $this->getContractor($input);

    $this->is_favorite = !empty($input['is_favorite']);

    $this->initDates($input);

    $this->sort = $this->getSort($input);
  }

  /**
   * @return array
   */
  public function months(): array {
    return $this->months;
  }

  /**
   * @return array
   */
  public function years(): array {
    return $this->years;
  }

  /**
   * @return DateTimeImmutable|null
   */
  public function date(): ?DateTimeImmutable {
    return $this->date;
  }

  /**
   * @return string|null
   */
  public function categoryTitle(): ?string {
    return $this->category ? $this->category->title : null;
  }

  /**
   * @return Item|null
   */
  public function productItem(): ?Item {
    if (!$this->product) {
      return null;
    }
    return $this->productDataProvider->getById($this->product->id);
  }

  /**
   * @return string|null
   */
  public function contractorTitle(): ?string {
    return $this->contractor ? $this->contractor->title : null;
  }

  /**
   * @return string[]
   */
  public function anyItemOption(): array {
    return [self::ANY_KEY => self::ANY_LABEL];
  }

  /**
   * @return bool
   */
  public function isEmpty(): bool {
    return null === $this->date
      && empty($this->months)
      && empty($this->years)
      && empty($this->category)
      && empty($this->product)
      && empty($this->contractor)
      && !$this->is_favorite;
  }

  /**
   * @param array $input
   *
   * @return $this
   * @throws \Exception
   */
  protected function initDates(array $input): static {
    if (!empty($input['date'])) {
      $this->date = new DateTimeImmutable($input['date']);
      $this->months = [];
      $this->years = [];
    }
    else {
      $this->date = null;

      $input['months'] = array_filter($input['months'] ?? [], static function ($month) {
        return self::ANY_KEY !== $month;
      });
      $input['years'] = array_filter($input['years'] ?? [], static function ($year) {
        return self::ANY_KEY !== $year;
      });

      if (!empty($input['months'])) {
        $this->months = $input['months'];
      }
      else {
        $this->months = [];
      }

      if (!empty($input['years'])) {
        $this->years = $input['years'];
      }
      else {
        $this->years = [];
      }
    }

    return $this;
  }

  /**
   * @param array $input
   *
   * @return Category|null
   */
  private function getCategory(array $input): ?Category {
    if (!empty($input['category'])) {
      if (str_contains($input['category'], ' / ')) {
        [$parent_title, $title] = explode(' / ', $input['category']);
        return $this->categoryRepository->findByParentAndTitle(new Title($parent_title), new Title($title));
      }

      return $this->categoryRepository->findByTitle(new Title($input['category']));
    }

    return null;
  }

  /**
   * @param array $input
   *
   * @return Product|null
   */
  private function getProduct(array $input): ?Product {
    if (!empty($input['product'])) {
      $parts = explode('/', $input['product']);
      $category_parent_title = trim($parts[0]);
      $categoryParent = $this->categoryRepository->findByTitle(new Title($category_parent_title));
      if (null === $categoryParent) {
        throw new InvalidArgumentException("Родительская категория '$category_parent_title' не найдена.");
      }

      // Категория / Продукт
      if (count($parts) === 2) {
        return $this->productRepository->findByTitleAndCategory(new Title(trim($parts[1])), $categoryParent->id);
      }
      // Категория / Подкатегория / Продукт
      elseif (count($parts) === 3) {
        $category_title = trim($parts[1]);
        $category = $this->categoryRepository->findByTitle(new Title($category_title));
        if (null === $category) {
          return $this->productRepository->findByTitleAndCategory(new Title(trim($parts[1])), $categoryParent->id);
//        throw new InvalidArgumentException("Категория '$category_title' не найдена.");
        }
        return $this->productRepository->findByTitleAndCategory(new Title(trim($parts[2])), $category->id);
      }
    }
    return null;
  }

  /**
   * @param array $input
   *
   * @return Contractor|null
   */
  private function getContractor(array $input): ?Contractor {
    if (!empty($input['contractor'])) {
      return $this->contractorRepository->findByTitle(new Title($input['contractor']));
    }
    return null;
  }

  /**
   * @param array $input
   *
   * @return string
   */
  private function getSort(array $input): string {
    if (!empty($input['sort'])) {
      $lower_sort = mb_strtolower($input['sort']);
      if (self::SORT_ASC !== $lower_sort && self::SORT_DESC !== $lower_sort) {
        throw new InvalidArgumentException('Некорректное значение сортировки.');
      }

      return $lower_sort;
    }

    return self::SORT_DESC;
  }

}
