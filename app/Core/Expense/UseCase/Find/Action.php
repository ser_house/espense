<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:14
 */


namespace App\Core\Expense\UseCase\Find;

use App\Core\Category\IRepository as CategoryRepository;
use App\Core\Contractor\IRepository as ContractorRepository;
use App\Core\Expense\DTO\Item as ExpenseItem;
use App\Core\Expense\IDataProvider as ExpenseDataProvider;
use App\Core\Product\IDataProvider as ProductDataProvider;
use App\Core\Product\IRepository as ProductRepository;

class Action {

  public function __construct(
    private readonly ExpenseDataProvider $expenseDataProvider,
    private readonly CategoryRepository $categoryRepository,
    private readonly ProductRepository $productRepository,
    private readonly ContractorRepository $contractorRepository,
    private readonly ProductDataProvider $productDataProvider,
  ) {

  }

  /**
   * @param array $raw_input
   *
   * @return Input
   */
  public function buildInput(array $raw_input): Input {
    return new Input(
      $raw_input,
      $this->categoryRepository,
      $this->productRepository,
      $this->contractorRepository,
      $this->productDataProvider,
    );
  }

  /**
   * @param Input $input
   *
   * @return ExpenseItem[]
   */
  public function handle(Input $input): array {
    if ($input->isEmpty()) {
      return [];
    }
    return $this->expenseDataProvider->find($input);
  }
}
