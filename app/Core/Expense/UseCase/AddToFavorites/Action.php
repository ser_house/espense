<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.06.2021
 * Time: 19:16
 */


namespace App\Core\Expense\UseCase\AddToFavorites;


use App\Core\Exception\NotFoundEntityException;
use App\Core\Expense\Entity\ExpenseId;
use App\Core\Expense\IDataProvider as ExpenseDataProvider;
use App\Core\Expense\IRepository as ExpenseRepository;
use App\Core\IEventDispatcher;
use App\Core\ITransaction as Transaction;
use Throwable;

class Action {

  public function __construct(
    private readonly ExpenseRepository $expenseRepository,
    private readonly ExpenseDataProvider $expenseDataProvider,
    private readonly IEventDispatcher $eventDispatcher,
    private readonly Transaction $transaction,
  ) {
  }


  /**
   * @param ExpenseId $expenseId
   *
   * @return AddedToFavoritesEvent
   */
  public function handle(ExpenseId $expenseId): AddedToFavoritesEvent {
    $expense = $this->expenseRepository->findById($expenseId);
    if (null === $expense) {
      throw new NotFoundEntityException();
    }

    $prevFavoriteExpenseId = $this->expenseDataProvider->getFavoriteExpenseId($expense->productId);

    $this->transaction->start();
    try {
      $this->expenseDataProvider->addToFavorites($expenseId);
      if ($prevFavoriteExpenseId) {
        $this->expenseDataProvider->removeFromFavorites($prevFavoriteExpenseId);
      }
      $this->transaction->commit();
    }
    catch (Throwable $e) {
      $this->transaction->rollback();
      throw $e;
    }

    $event = new AddedToFavoritesEvent($expenseId, $prevFavoriteExpenseId);
    $this->eventDispatcher->dispatch($event);

    return $event;
  }
}
