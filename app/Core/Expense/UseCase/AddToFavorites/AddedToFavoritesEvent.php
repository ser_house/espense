<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.06.2021
 * Time: 19:17
 */


namespace App\Core\Expense\UseCase\AddToFavorites;


use App\Core\Expense\Entity\ExpenseId;
use App\Core\IEvent;

class AddedToFavoritesEvent implements IEvent {

  public function __construct(
    public readonly ExpenseId $expenseId,
    public readonly ?ExpenseId $prevFavoriteExpenseId = null,
  ) {

  }
}
