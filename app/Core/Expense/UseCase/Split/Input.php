<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.02.2020
 * Time: 11:04
 */


namespace App\Core\Expense\UseCase\Split;


use App\Core\Exception\MissingRequiredDataException;
use App\Core\Expense\Entity\ExpenseId;
use InvalidArgumentException;

class Input {

  public readonly ExpenseId $id;
  /** @var SubExpense[] */
  public readonly array $sub_expenses;


  public function __construct(array $input) {
    if (empty($input['id'])) {
      throw new MissingRequiredDataException(['id расхода не указан.']);
    }

    if (empty($input['expenses'])) {
      throw new MissingRequiredDataException(['Данные по новым расходам не указаны.']);
    }

    if (!is_array($input['expenses'])) {
      throw new InvalidArgumentException('Неверный формат данных по новым расходам.');
    }

    $this->id = new ExpenseId($input['id']);

    $sub_expenses = [];
    foreach ($input['expenses'] as $sub_expense_input) {
      $sub_expenses[] = new SubExpense($sub_expense_input);
    }
    $this->sub_expenses = $sub_expenses;
  }
}
