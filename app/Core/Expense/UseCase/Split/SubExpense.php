<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.02.2020
 * Time: 11:06
 */


namespace App\Core\Expense\UseCase\Split;


use App\Core\Amount;
use App\Core\Exception\MissingRequiredDataException;
use App\Core\Expense\Entity\Quantity;
use App\Core\Generic\Note;
use App\Core\Product\ProductId;
use Exception;

class SubExpense {

  public readonly ProductId $productId;
  public readonly Quantity $quantity;
  public readonly Amount $amount;
  public readonly ?Note $note;


  /**
   * SubExpense constructor.
   *
   * @param array $input = [
   *    'product_id' => (string), required
   *    'quantity' => (string), required
   *    'amount' => (string), required
   *    'note' => (string), optional
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    if (empty($input['product_id'])) {
      $missing_data[] = 'Товар не указан.';
    }

    if (empty($input['quantity'])) {
      $missing_data[] = 'Кол-во не указано.';
    }

    if (empty($input['amount'])) {
      $missing_data[] = 'Сумма не указана.';
    }

    if (!empty($missing_data)) {
      throw new MissingRequiredDataException($missing_data);
    }

    $this->productId = new ProductId($input['product_id']);
    $this->quantity = new Quantity($input['quantity']);
    $this->amount = new Amount($input['amount']);
    if (!empty($input['note'])) {
      $this->note = new Note($input['note']);
    }
    else {
      $this->note = null;
    }
  }
}
