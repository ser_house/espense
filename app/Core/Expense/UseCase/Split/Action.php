<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.02.2020
 * Time: 11:04
 */


namespace App\Core\Expense\UseCase\Split;

use App\Core\Exception\NotEqualsAmountsException;
use App\Core\Exception\NotFoundEntityException;
use App\Core\Expense\Entity\Expense as DomainExpense;
use App\Core\Expense\IRepository as ExpenseRepository;
use App\Core\ITransaction as Transaction;
use Throwable;

class Action {

  public function __construct(
    private readonly ExpenseRepository $expenseRepository,
    private readonly Transaction $transaction,
  ) {

  }

  /**
   * @param Input $input
   *
   * @return DomainExpense[] новые расходы
   * @throws NotFoundEntityException
   * @throws NotEqualsAmountsException
   * @throws Throwable
   */
  public function handle(Input $input): array {
    $expense = $this->expenseRepository->findById($input->id);
    if (null === $expense) {
      throw new NotFoundEntityException('Оригинальный расход не найден.');
    }

    $total = $expense->amount->value();
    $new_total = 0.0;

    $new_expenses = [];

    // Подготовительные работы и логика - вне транзакции.
    foreach ($input->sub_expenses as $subExpense) {
      $amount = $subExpense->amount;
      $newExpense = $expense->buildNewExpense(
        $subExpense->productId,
        $subExpense->quantity,
        $amount,
        $subExpense->note
      );
      $new_total += $amount->value();
      $new_expenses[] = $newExpense;
    }

    if ($new_total != $total) {
      throw new NotEqualsAmountsException('Итого новых расходов не равно сумме исходного.');
    }

    $this->transaction->start();

    try {
      foreach ($new_expenses as $newExpense) {
        $this->expenseRepository->add($newExpense);
      }
      $this->expenseRepository->remove($expense->id);
    }
    catch (Throwable $e) {
      $this->transaction->rollback();
      throw $e;
    }

    $this->transaction->commit();

    return $new_expenses;
  }
}
