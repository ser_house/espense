<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 2:04
 */


namespace App\Core\Expense\Main;

use App\Core\Account\DefaultSrcAccount;
use App\Core\Expense\DTO\Item as ExpenseItem;
use App\Core\Generic\DTO\Item as GenericItem;

class Presenter {

  /**
   * @param array $units
   * @param string $date_format
   *
   * @return View
   */
  public function viewNew(array $units, string $date_format = 'Y-m-d'): View {
    $view = new View();

    $view->id = '';
    $view->date = date($date_format);

    $defaultAccount = new DefaultSrcAccount();
    $view->account = new GenericItem((string)$defaultAccount->id, (string)$defaultAccount->title);
    $view->category = '';
    $view->product = null;
    $view->unit = new GenericItem($units[0]->id, $units[0]->title);
    $view->quantity = 1;
    $view->amount = '';
    $view->contractor = null;
    $view->note = '';
    $view->is_favorite = false;

    return $view;
  }

  /**
   * @param View $expenseItem
   *
   * @return string
   */
  public function categoryPath(View $expenseItemView): string {
    $category = $expenseItemView->product->category;
    if ($category->parent) {
      return $category->parent->title . ' / ' . $category->title;
    }
    return $category->title;
  }

  /**
   * @param ExpenseItem $expenseItem
   * @param string $date_format
   *
   * @return View
   */
  public function view(ExpenseItem $expenseItem, string $date_format = 'd.m.Y'): View {
    $view = new View();
    $view->id = $expenseItem->id;
    $view->date = $expenseItem->date->format($date_format);
    $view->account = $expenseItem->account;

    $view->product = $expenseItem->product;

    $view->quantity = $expenseItem->quantity;
    $view->unit = $expenseItem->unit ? new GenericItem($expenseItem->unit->id, $expenseItem->unit->title) : null;
    $view->amount = $expenseItem->amount;
    $view->contractor = $expenseItem->contractor ?: null;
    $view->note = $expenseItem->note;
    $view->is_favorite = $expenseItem->is_favorite;
    $view->is_tracking = $expenseItem->is_tracking;

    return $view;
  }

  /**
   * @param ExpenseItem[] $expenseItems
   * @param string $date_format
   *
   * @return View[]
   */
  public function views(array $expenseItems, string $date_format = 'd.m.Y'): array {
    $views = [];

    /** @var ExpenseItem $expenseItem */
    foreach ($expenseItems as $expenseItem) {
      $views[] = $this->view($expenseItem, $date_format);
    }

    return $views;
  }
}
