<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 2:03
 */


namespace App\Core\Expense\Main;

use App\Core\Generic\DTO\Item;
use App\Core\Product\DTO\ProductItem;

/**
 * Class View
 *
 * @package App\Core\Expense\Main
 */
class View {
  public string $id;
  public string $date;
  public Item $account;
  public string $category;
  public ?ProductItem $product = null;
  public string $quantity;
  public ?Item $unit;
  public string $amount;
  public ?Item $contractor = null;
  public string $note;
  public bool $is_favorite;
  public bool $is_tracking;
}
