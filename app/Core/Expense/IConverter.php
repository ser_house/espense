<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.02.2020
 * Time: 11:54
 */


namespace App\Core\Expense;

use App\Core\Expense\DTO\Item as ExpenseItem;
use App\Core\Expense\Entity\Expense;


interface IConverter {
  /**
   * @param Expense $expense
   *
   * @return ExpenseItem
   */
  public function domainToDto(Expense $expense): ExpenseItem;

  /**
   * @param iterable $expenses
   *
   * @return array
   */
  public function domainsToDto(iterable $expenses): array;
}
