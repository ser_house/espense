<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 10:45
 */


namespace App\Core\Expense;

use App\Core\Exception\FailedToSaveEntityException;
use App\Core\Exception\NotFoundEntityException;
use App\Core\Expense\Entity\Expense;
use App\Core\Expense\Entity\ExpenseId;

interface IRepository {

  /**
   *
   * @return ExpenseId
   */
  public function nextId(): ExpenseId;

  /**
   * @param ExpenseId $entityId
   *
   * @return Expense
   * @throws NotFoundEntityException
   */
  public function get(ExpenseId $entityId): Expense;

  /**
   * @param ExpenseId $entityId
   *
   * @return Expense|null
   */
  public function findById(ExpenseId $entityId): ?Expense;

  /**
   * @param Expense $expense
   *
   * @throws FailedToSaveEntityException
   */
  public function add(Expense $expense): void;

  /**
   * @param Expense $expense
   *
   * @throws FailedToSaveEntityException
   */
  public function update(Expense $expense): void;

  /**
   * @param ExpenseId $expenseId
   *
   * @return mixed
   */
  public function remove(ExpenseId $expenseId);
}
