<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.10.2019
 * Time: 10:39
 */


namespace App\Core\Generic;


/**
 * Class Flag
 *
 * @package App\Core\Generic
 */
class Flag {

  protected bool $value;

  /**
   * Flag constructor.
   *
   * @param bool $value
   */
  public function __construct(bool $value) {
    $this->value = $value;
  }

  /**
   *
   * @return bool
   */
  public function toggle(): bool {
    $this->value = !$this->value;

    return $this->value;
  }

  /**
   * Сброс флага (false).
   *
   * @return $this
   */
  public function reset(): self {
    $this->value = false;

    return $this;
  }

  /**
   * Установка флага (true).
   *
   * @return $this
   */
  public function set(): self {
    $this->value = true;

    return $this;
  }
}
