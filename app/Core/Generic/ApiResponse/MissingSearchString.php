<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 21.11.2019
 * Time: 9:32
 */


namespace App\Core\Generic\ApiResponse;


class MissingSearchString extends Generic {
  /**
   * MissingSearchString constructor.
   */
  public function __construct() {
    parent::__construct('error', 'Не указана искомая строка.');
  }
}
