<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 24.09.2019
 * Time: 17:36
 */


namespace App\Core\Generic\ApiResponse;


use JsonSerializable;

class Generic implements JsonSerializable {

  public function __construct(protected readonly string $type, protected readonly string $msg) {

  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize(): mixed {
    return [
      'type' => $this->type,
      'msg' => $this->msg,
    ];
  }
}
