<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.09.2019
 * Time: 11:27
 */


namespace App\Core\Generic\ApiResponse;


use App\Core\Generic\DTO\Item;

class Exists extends Generic {

  public function __construct(string $message, private readonly Item $existsItem) {
    parent::__construct('exists', $message);
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize(): mixed {
    $data = parent::jsonSerialize();
    $data['item'] = $this->existsItem;

    return $data;
  }
}
