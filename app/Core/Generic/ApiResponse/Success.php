<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 24.09.2019
 * Time: 17:37
 */


namespace App\Core\Generic\ApiResponse;


class Success extends Generic {

  /**
   * Success constructor.
   *
   * @param string $msg
   */
  public function __construct(string $msg) {
    parent::__construct('success', $msg);
  }
}
