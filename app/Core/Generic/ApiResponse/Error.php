<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 24.09.2019
 * Time: 17:37
 */


namespace App\Core\Generic\ApiResponse;


class Error extends Generic {

  /**
   * Error constructor.
   *
   * @param string $msg
   * @param array $errors
   */
  public function __construct(string $msg, private readonly array $errors = []) {
    parent::__construct('error', $msg);
  }


  /**
   * @inheritDoc
   */
  public function jsonSerialize(): mixed {
    $result = parent::jsonSerialize();
    $result['errors'] = $this->errors;

    return $result;
  }
}
