<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:15
 */


namespace App\Core\Generic\UseCase\Find;

use App\Core\Exception\MissingRequiredDataException;
use App\Core\Title;


class Input {

  public readonly ?string $like;
  public readonly ?Title $title;


  public function __construct(array $input) {
    if (empty($input['like']) && empty($input['title'])) {
      throw new MissingRequiredDataException(['Не указана строка для поиска ("like" - для поиска по частичному совпадению в названии; "title" - для поиска по точному совпадению названия).']);
    }

    if (!empty($input['like'])) {
      $this->like = trim($input['like']);
    }
    else {
      $this->like = null;
    }

    if (!empty($input['title'])) {
      $this->title = new Title($input['title']);
    }
    else {
      $this->title = null;
    }
  }
}
