<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.04.2021
 * Time: 9:37
 */


namespace App\Core\Generic\Formatter;


class Text {
  /**
   * Склоняем словоформу
   *
   * @param int $value
   * @param array $words ['день', 'дня', 'дней']
   *
   * @return string
   */
  public function morph(int $value, $words): string {
    $num = $value % 100;
    if ($num > 19) {
      $num = $num % 10;
    }

    $out = $value . ' ';

    switch ($num) {
      case 1:
        $out .= $words[0];
        break;

      case 2:
      case 3:
      case 4:
        $out .= $words[1];
        break;

      default:
        $out .= $words[2];
        break;
    }

    return $out;
  }
}
