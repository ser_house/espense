<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.09.2019
 * Time: 18:18
 */


namespace App\Core\Generic\DTO;


use JsonSerializable;

class Item implements JsonSerializable {

  /**
   * Item constructor.
   *
   * @param string $id
   * @param string $title
   */
  public function __construct(public readonly string $id, public readonly string $title) {

  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize(): mixed {
    return [
      'id' => $this->id,
      'title' => $this->title,
    ];
  }
}
