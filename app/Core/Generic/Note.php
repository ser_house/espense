<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 0:47
 */


namespace App\Core\Generic;

use Exception;

/**
 * Class Note
 *
 * @package App\Core\Expense\Entity
 */
final class Note {

  protected string $value;

  /**
   * Note constructor.
   *
   * @param string $value
   *
   * @throws Exception
   */
  public function __construct(string $value) {
    $this->value = trim($value);
  }

  /**
   * @return string
   */
  public function value(): string {
    return $this->value;
  }

  /**
   * @param Note $note
   *
   * @return bool
   */
  public function equals(Note $note): bool {
    return $note->value() === $this->value();
  }

  /**
   * @inheritDoc
   */
  public function __toString() {
    return $this->value();
  }
}
