<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 14:13
 */


namespace App\Core\Generic\View;


class Message {

  public readonly string $page_title;

  /**
   * Message constructor.
   *
   * @param string $title
   * @param string $type
   * @param string $text
   * @param string|null $page_title
   */
  public function __construct(
    public readonly string $title,
    public readonly string $type,
    public readonly string $text,
    string $page_title = null,
  ) {
    $this->page_title = $page_title ?? $this->title;
  }
}
