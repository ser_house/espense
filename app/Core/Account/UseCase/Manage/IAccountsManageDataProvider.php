<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.12.2019
 * Time: 9:28
 */


namespace App\Core\Account\UseCase\Manage;

use App\Core\Account\AccountId;
use App\Core\Exception\NotFoundEntityException;
use App\Core\Generic\DTO\Item;

interface IAccountsManageDataProvider {
  /**
   * Выбрать все счета, с контагентами.
   * Добавить новый счет с контрагентом.
   * Удалить счет.
   * Установить контрагента для счета.
   */
  /**
   * @param string $like
   *
   * @return Item[]
   */
  public function findLikeTitle(string $like): array;

  /**
   * @param string $title
   *
   * @return Item[]
   */
  public function findByTitle(string $title): array;

  /**
   * @param AccountId $accountId
   *
   * @return Item|null
   */
  public function findById(AccountId $accountId): ?Item;

  /**
   * @param AccountId $accountId
   *
   * @return Item
   * @throws NotFoundEntityException
   */
  public function get(AccountId $accountId): Item;

  /**
   * @return Item[]
   */
  public function items(): array;

  /**
   * @return ManageItem[]
   */
  public function manageItems(): array;

  /**
   * @param AccountId $accountId
   *
   * @return ManageItem
   */
  public function manageItemById(AccountId $accountId): ManageItem;
}
