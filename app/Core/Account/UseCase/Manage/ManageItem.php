<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.12.2019
 * Time: 9:37
 */


namespace App\Core\Account\UseCase\Manage;


use App\Core\Generic\DTO\Item;

class ManageItem extends Item {

  public function __construct(
    string $id,
    string $title,
    public readonly ?Item $contractor = null,
    public readonly int $ops_count = 0,
  ) {
    parent::__construct($id, $title);
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize(): mixed {
    $json = parent::jsonSerialize();
    $json['ops_count'] = $this->ops_count;
    $json['contractor'] = $this->contractor ? $this->contractor->jsonSerialize() : null;

    return $json;
  }
}
