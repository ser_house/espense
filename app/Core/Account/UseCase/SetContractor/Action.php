<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:14
 */


namespace App\Core\Account\UseCase\SetContractor;

use App\Core\Account\IRepository as AccountRepository;
use App\Core\Contractor\IRepository as ContractorRepository;
use App\Core\Exception\NotFoundEntityException;

class Action {

  public function __construct(
    private readonly AccountRepository $accountRepository,
    private readonly ContractorRepository $contractorRepository,
  ) {

  }

  /**
   * @param Input $input
   *
   * @return SetContractorEvent
   */
  public function handle(Input $input): SetContractorEvent {
    $account = $this->accountRepository->findById($input->id);
    if (null === $account) {
      throw new NotFoundEntityException('Счёт не найден.');
    }

    $this->accountRepository->setContractorId($account->id, $input->contractorId);

    $prevContractor = null;
    if ($account->contractorId) {
      $prevContractor = $this->contractorRepository->get($account->contractorId);
    }
    $currentContractor = $this->contractorRepository->get($input->contractorId);

    return new SetContractorEvent($account, $prevContractor, $currentContractor);
  }
}
