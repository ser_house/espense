<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:15
 */


namespace App\Core\Account\UseCase\SetContractor;


use App\Core\Account\AccountId;
use App\Core\Contractor\ContractorId;
use App\Core\Exception\MissingRequiredDataException;
use Exception;

class Input {

  public readonly AccountId $id;
  public readonly ContractorId $contractorId;

  /**
   * Input constructor.
   *
   * @param array $input = [
   *    'id' => (string), required
   *    'title' => (string), required
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    if (empty($input['id'])) {
      throw new MissingRequiredDataException(['Не указан id счета.']);
    }
    if (empty($input['contractor_id'])) {
      throw new MissingRequiredDataException(['Не указан id контрагентa.']);
    }

    $this->id = new AccountId($input['id']);
    $this->contractorId = new ContractorId($input['contractor_id']);
  }
}
