<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.02.2020
 * Time: 13:29
 */


namespace App\Core\Account\UseCase\Rename;


use App\Core\Account\Account;
use App\Core\Title;

class RenamedEvent {

  public function __construct(
    public readonly Account $account,
    public readonly Title $oldTitle,
  ) {

  }
}
