<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:14
 */


namespace App\Core\Account\UseCase\Rename;

use App\Core\Account\IRepository as AccountRepository;
use App\Core\Exception\NotFoundEntityException;

class Action {

  public function __construct(private readonly AccountRepository $accountRepository) {

  }

  /**
   * @param Input $input
   *
   * @return RenamedEvent
   */
  public function handle(Input $input): RenamedEvent {
    $currentAccount = $this->accountRepository->findById($input->id);
    if (null === $currentAccount) {
      throw new NotFoundEntityException('Переименование невозможно: счет не найден.');
    }

    $renamedAccount = $currentAccount->rename($input->newTitle);
    $this->accountRepository->update($renamedAccount);

    return new RenamedEvent($renamedAccount, $currentAccount->title);
  }
}
