<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:15
 */


namespace App\Core\Account\UseCase\Rename;


use App\Core\Account\AccountId;
use App\Core\Exception\MissingRequiredDataException;
use App\Core\Title;
use Exception;

class Input {

  public readonly AccountId $id;
  public readonly Title $newTitle;

  /**
   * Input constructor.
   *
   * @param array $input = [
   *    'id' => (string), required
   *    'title' => (string), required
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    if (empty($input['id'])) {
      throw new MissingRequiredDataException(['Не указан id счета.']);
    }
    if (empty($input['title'])) {
      throw new MissingRequiredDataException(['Не указано название счета.']);
    }

    $this->id = new AccountId($input['id']);
    $this->newTitle = new Title($input['title']);
  }
}
