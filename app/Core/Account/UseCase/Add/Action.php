<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:04
 */


namespace App\Core\Account\UseCase\Add;


use App\Core\Account\Account as DomainAccount;
use App\Core\Account\IRepository as AccountRepository;
use App\Core\Exception\ExistsException;
use App\Core\Generic\DTO\Item;

class Action {

  public function __construct(
    private readonly AccountRepository $accountRepository,
  ) {

  }

  /**
   * @param Input $input
   *
   * @return AddedEvent
   */
  public function handle(Input $input): AddedEvent {
    $title = $input->title;

    $exists = $this->accountRepository->findByTitle($title);
    if ($exists) {
      throw new ExistsException("Добавление невозможно: счет '$title' существует.", new Item((string)$exists->id, (string)$exists->title));
    }

    $id = $this->accountRepository->nextId();

    $account = new DomainAccount($id, $title, $input->contractorId);
    $this->accountRepository->add($account);

    return new AddedEvent($account);
  }
}
