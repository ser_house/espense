<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 16:37
 */


namespace App\Core\Account\UseCase\Add;

use App\Core\Contractor\ContractorId;
use App\Core\Exception\MissingRequiredDataException;
use App\Core\Title;
use Exception;

class Input {

  public readonly ?ContractorId $contractorId;
  public readonly Title $title;

  /**
   * Input constructor.
   *
   * @param array $input = [
   *    'title' => (string), required
   *    'contractor_id' => (string), optional
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    if (empty($input['title'])) {
      throw new MissingRequiredDataException(['Не указано название счета.']);
    }

    $this->contractorId = empty($input['contractor_id']) ? null : new ContractorId($input['contractor_id']);
    $this->title = new Title($input['title']);
  }
}
