<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:14
 */


namespace App\Core\Account\UseCase\Show;

use App\Core\Account\IDataProvider;
use App\Core\Exception\NotFoundEntityException;
use App\Core\Generic\DTO\Item as AccountItem;

class Action {

  public function __construct(private readonly IDataProvider $accountDataProvider) {

  }


  /**
   * @param Input $input
   *
   * @return AccountItem
   */
  public function handle(Input $input): AccountItem {
    $item = $this->accountDataProvider->findById($input->id);
    if (null === $item) {
      throw new NotFoundEntityException('Счет не найден.');
    }

    return $item;
  }
}
