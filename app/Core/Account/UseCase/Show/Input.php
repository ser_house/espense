<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:15
 */


namespace App\Core\Account\UseCase\Show;

use App\Core\Account\AccountId;
use App\Core\Exception\MissingRequiredDataException;
use Exception;


class Input {

  public readonly AccountId $id;

  /**
   * Input constructor.
   *
   * @param array $input = [
   *    'id' => (string), required
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    if (empty($input['id'])) {
      throw new MissingRequiredDataException(['Не указан id счета.']);
    }

    $this->id = new AccountId($input['id']);
  }
}
