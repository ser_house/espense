<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.02.2020
 * Time: 13:29
 */


namespace App\Core\Account\UseCase\ClearContractor;


use App\Core\Account\Account;
use App\Core\Contractor\Contractor;

class ClearedContractorEvent {

  public function __construct(
    public readonly Account $account,
    public readonly ?Contractor $prevContractor,
  ) {

  }
}
