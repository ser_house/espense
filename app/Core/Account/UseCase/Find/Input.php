<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:15
 */


namespace App\Core\Account\UseCase\Find;

use App\Core\Account\Account;
use App\Core\Account\AccountId;
use App\Core\Account\IRepository as AccountRepository;
use App\Core\Title;

class Input {
  public readonly ?Account $account;
  public readonly bool $with_remain_only;

  public function __construct(
    array $input,
    private readonly AccountRepository $accountRepository,
  ) {
    $this->account = $this->findAccount($input);
    $this->with_remain_only = !empty($input['with_remain_only']);
  }


  /**
   * @return string|null
   */
  public function accountTitle(): ?string {
    return $this->account ? $this->account->title : null;
  }

  /**
   * @return bool
   */
  public function isEmpty(): bool {
    return null === $this->account;
  }

  /**
   * @param array $input
   *
   * @return Account|null
   */
  private function findAccount(array $input): ?Account {
    if (!empty($input['account'])) {
      return $this->accountRepository->findByTitle(new Title($input['account']));
    }
    if (!empty($input['account_id'])) {
      return $this->accountRepository->findById(new AccountId($input['account_id']));
    }

    return null;
  }
}
