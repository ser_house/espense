<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:14
 */


namespace App\Core\Account\UseCase\Find;

use App\Core\Account\IDataProvider;
use App\Core\Generic\DTO\Item;

class Action {

  public function __construct(private readonly IDataProvider $accountDataProvider) {

  }


  /**
   * @param Input $input
   *
   * @return Item[]
   */
  public function handle(Input $input): array {
    if ($input->account) {
      return [$this->accountDataProvider->findById($input->account->id)];
    }

    return $this->accountDataProvider->items();
  }
}
