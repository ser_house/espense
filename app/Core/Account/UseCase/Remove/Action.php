<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:29
 */


namespace App\Core\Account\UseCase\Remove;

use App\Core\Account\IRepository as AccountRepository;
use App\Core\Exception\NotFoundEntityException;


class Action {

  public function __construct(
    private readonly AccountRepository $accountRepository,
  ) {

  }

  /**
   * @param Input $input
   *
   * @return RemovedEvent
   * @throws NotFoundEntityException
   */
  public function handle(Input $input): RemovedEvent {
    $account = $this->accountRepository->findById($input->id);
    if (null === $account) {
      throw new NotFoundEntityException('Удаление невозможно: счет не найден.');
    }

    $this->accountRepository->remove($input->id);

    return new RemovedEvent($account);
  }
}
