<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.02.2020
 * Time: 13:33
 */


namespace App\Core\Account\UseCase\Remove;


use App\Core\Account\Account;

class RemovedEvent {

  public function __construct(public readonly Account $account) {

  }
}
