<?php

namespace App\Core\Account;

enum OpType: int {
  case Incoming = 0;
  case Outcoming = 1;
  case Expense = 2;

  public function label(): string {
    return match ($this) {
      self::Incoming => 'Вх. перевод',
      self::Outcoming => 'Исх. перевод',
      self::Expense => 'Расход',
    };
  }
}
