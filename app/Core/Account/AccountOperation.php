<?php

namespace App\Core\Account;

use App\Core\Amount;
use App\Core\Title;
use DateTimeImmutable;

class AccountOperation {

  public function __construct(
    public readonly DateTimeImmutable $date,
    public readonly Title $title,
    public readonly Amount $amount,
    public readonly OpType $type,
    public readonly string $note,
  ) {
  }
}
