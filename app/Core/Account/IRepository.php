<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 10:45
 */


namespace App\Core\Account;

use App\Core\Contractor\ContractorId;
use App\Core\Exception\NotFoundEntityException;
use App\Core\Title;

interface IRepository {
  /**
   *
   * @return AccountId
   */
  public function nextId(): AccountId;

  /**
   * @param Account $account
   */
  public function add(Account $account): void;

  /**
   * @param AccountId $accountId
   *
   * @return Account
   * @throws NotFoundEntityException
   */
  public function get(AccountId $accountId): Account;

  /**
   * @param Account $account
   */
  public function update(Account $account): void;

  /**
   * @param AccountId $accountId
   * @param ContractorId $contractorId
   *
   * @return void
   */
  public function setContractorId(AccountId $accountId, ContractorId $contractorId): void;

  /**
   * @param AccountId $accountId
   *
   * @return void
   */
  public function clearContractorId(AccountId $accountId): void;

  /**
   * @param AccountId $accountId
   */
  public function remove(AccountId $accountId): void;

  /**
   *
   * @return Account[]
   */
  public function all(): array;

  /**
   * @param AccountId $accountId
   *
   * @return Account|null
   */
  public function findById(AccountId $accountId): ?Account;

  /**
   * @param Title $title
   *
   * @return Account|null
   */
  public function findByTitle(Title $title): ?Account;
}
