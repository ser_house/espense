<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.12.2019
 * Time: 9:28
 */


namespace App\Core\Account;

use App\Core\Account\UseCase\Manage\ManageItem;
use App\Core\Exception\NotFoundEntityException;
use App\Core\Generic\DTO\Item;
use App\Core\Account\UseCase\Find\Input as FindInput;

interface IDataProvider {

  /**
   * @param string $like
   *
   * @return Item[]
   */
  public function findLikeTitle(string $like): array;

  /**
   * @param string $title
   *
   * @return Item[]
   */
  public function findByTitle(string $title): array;

  /**
   * @param AccountId $accountId
   *
   * @return Item|null
   */
  public function findById(AccountId $accountId): ?Item;

  /**
   * @param AccountId $accountId
   *
   * @return Item
   * @throws NotFoundEntityException
   */
  public function get(AccountId $accountId): Item;

  /**
   * @return Item[]
   */
  public function items(): array;

  /**
   * Остаток добавляется к названию.
   *
   * @return AccountItem[]
   */
  public function itemsWithRemain(): array;

  /**
   * @param FindInput $input
   *
   * @return AccountItem[]
   */
  public function itemsWithoutDefault(FindInput $input): array;

  /**
   * @param AccountId $accountId
   *
   * @return float
   */
  public function accountRemain(AccountId $accountId): float;

  /**
   * @return ManageItem[]
   */
  public function manageItems(): array;

  /**
   * @param AccountId $accountId
   *
   * @return ManageItem
   */
  public function manageItemById(AccountId $accountId): ManageItem;

  /**
   * @param AccountId $accountId
   * @param bool $sort_date_asc
   *
   * @return AccountOperation[]
   */
  public function operations(AccountId $accountId, bool $sort_date_asc = true): array;
}
