<?php

namespace App\Core\Account;

use App\Core\Amount;
use App\Core\Generic\DTO\Item;

class AccountItem extends Item {

  /**
   * @param string $id
   * @param string $title
   * @param Amount|null $remain
   * @param Item|null $contractor
   */
  public function __construct(
    string $id,
    string $title,
    public readonly ?Amount $remain,
    public readonly ?Item $contractor
  ) {
    parent::__construct($id, $title);
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize(): mixed {
    $json = parent::jsonSerialize();
    $json['remain'] = $this->remain?->value();
    $json['contractor'] = $this->contractor ? $this->contractor->jsonSerialize() : null;

    return $json;
  }
}
