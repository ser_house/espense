<?php

namespace App\Core\Account;

use App\Core\Contractor\ContractorId;
use App\Core\Title;

class Account {
  public function __construct(
    public readonly AccountId $id,
    public readonly Title $title,
    public readonly ?ContractorId $contractorId = null,
  ) {

  }

  /**
   * @param Title $title
   *
   * @return self
   */
  public function rename(Title $title): self {
    return new self($this->id, $title);
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize(): mixed {
    return [
      'id' => (string)$this->id,
      'title' => (string)$this->title,
      'contractor_id' => (string)$this->contractorId,
    ];
  }
}
