<?php

namespace App\Core\Account;

use App\Core\Title;

class DefaultSrcAccount extends Account {
  private const ID = '0c25c958-f6ba-414e-86c7-f2e724b9d19e';

  public function __construct() {
    parent::__construct(new AccountId(self::ID), new Title('По умолчанию'));
  }
}
