<?php

namespace App\Core\Analytics\View;

class CategoryItem extends ExpenseItem {
  public string $type = 'category';
}