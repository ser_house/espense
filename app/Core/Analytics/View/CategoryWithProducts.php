<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.09.2019
 * Time: 15:27
 */


namespace App\Core\Analytics\View;

/**
 * Class TotalRootItem.
 *
 * DTO для передачи структуры расходов из названия, подпунктов расходов ({@see ExpenseItem}) и общего итого.
 *
 * @package App\Core\Analytics\View
 */
class CategoryWithProducts extends ExpenseItem {

  /**
   * TotalRootItem constructor.
   *
   * @param string $title
   * @param ExpenseItem[] $items
   * @param float $total
   */
  public function __construct(string $title, public readonly array $items = [], float $total = 0.0) {
    parent::__construct('', $title, $total);

  }
}
