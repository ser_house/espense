<?php

namespace App\Core\Analytics\View;

class ProductItem extends ExpenseItem {
  public string $type = 'product';
}