<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.09.2019
 * Time: 0:25
 */


namespace App\Core\Analytics\View;

/**
 * Class ExpenseItem.
 *
 * View модель, описывающая некий расход (метку и сумму) для представления.
 *
 * @package App\Core\Analytics\View
 */
class ExpenseItem {

  /**
   * @param string $id
   * @param string $title
   * @param float $amount
   */
  public function __construct(
    public readonly string $id,
    public string $title,
    public readonly float $amount,
  ) {
  }
}
