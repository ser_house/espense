<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.09.2019
 * Time: 0:23
 */


namespace App\Core\Analytics\Calendar\View;

use App\Core\Analytics\View\ExpenseItem;
use Exception;

/**
 * Class Day
 *
 * @package App\Core\Analytics\Calendar\View
 */
class Day {

  /**
   * Day constructor.
   *
   * @param int $number
   * @param string $date
   * @param ExpenseItem[] $expenses
   *
   * @throws Exception
   */
  public function __construct(
    public readonly int $number,
    public readonly string $date,
    public array $expenses = []) {

  }

  /**
   * @param ExpenseItem $expense
   */
  public function addExpense(ExpenseItem $expense): void {
    $this->expenses[] = $expense;
  }
}
