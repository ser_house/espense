<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.10.2019
 * Time: 16:22
 */


namespace App\Core\Analytics\Calendar\View;

use App\Core\Analytics\View\ExpenseItem;

/**
 * Class Month
 *
 * @package App\Core\Analytics\Calendar\View
 */
class Month {

  /**
   * Month constructor.
   *
   * @param string $title
   * @param ExpenseItem[] $expenses
   */
  public function __construct(
    public readonly string $title,
    public array $expenses = []) {

  }

  /**
   * @param ExpenseItem $expense
   */
  public function addExpense(ExpenseItem $expense): void {
    $this->expenses[] = $expense;
  }
}
