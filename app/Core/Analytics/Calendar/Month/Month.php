<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.09.2019
 * Time: 14:32
 */


namespace App\Core\Analytics\Calendar\Month;

use DateInterval;
use DatePeriod;
use DateTimeImmutable;
use Exception;
use InvalidArgumentException;

/**
 * Class Month
 *
 * @package App\Core\Analytics\Calendar\Month
 */
final class Month {

  private DateTimeImmutable $startDate;
  private DateTimeImmutable $endDate;

  /**
   * Month constructor.
   *
   * @param int $year
   * @param int $month
   *
   * @throws Exception
   */
  public function __construct(public readonly int $year, public readonly int $month) {
    if ($month < 1 || $month > 12) {
      throw new InvalidArgumentException("Неверный номер месяца (год: '$year', номер месяца: '$month')");
    }

    $this->startDate = $this->startDate();
    $this->endDate = $this->endDate();
  }

  /**
   *
   * @return DateTimeImmutable
   * @throws Exception
   */
  public function startDate(): DateTimeImmutable {
    $monthFormatter = new Formatter();
    $month_num_str = $monthFormatter->normalizedMonthNumber($this->month);

    return new DateTimeImmutable("$this->year-$month_num_str-01");
  }

  /**
   *
   * @return DateTimeImmutable
   * @throws Exception
   */
  public function endDate(): DateTimeImmutable {
    return new DateTimeImmutable($this->startDate->format('Y-m-t'));
  }

  /**
   *
   * @return $this
   * @throws Exception
   */
  public function addMonth(): self {
    $monthInterval = new DateInterval('P1M');

    $nextStartDate = $this->startDate->add($monthInterval);

    return new self($nextStartDate->format('Y'), $nextStartDate->format('m'));
  }

  /**
   *
   * @return $this
   * @throws Exception
   */
  public function subMonth(): self {
    $monthInterval = new DateInterval('P1M');

    $prevStartDate = $this->startDate->sub($monthInterval);

    return new self($prevStartDate->format('Y'), $prevStartDate->format('m'));
  }

  /**
   *
   * @return DatePeriod
   * @throws Exception
   */
  public function dailyPeriod(): DatePeriod {
    $dayInterval = new DateInterval('P1D');

    return new DatePeriod($this->startDate, $dayInterval, $this->endDate->add($dayInterval));
  }

  /**
   *
   * @return bool
   */
  public function canGoToPrev(): bool {
    return true;
  }

  /**
   *
   * @return bool
   */
  public function canGoToNext(): bool {
    $current_year = (int)date('Y');
    $current_month = (int)date('m');

    if ($current_year > $this->year) {
      return true;
    }

    return $current_month > $this->month;
  }

  /**
   * @return $this
   * @throws Exception
   */
  public function prevMonth(): self {
    $prev_year = $this->year;
    $prev_month = $this->month - 1;
    if (0 === $prev_month) {
      $prev_month = 12;
      $prev_year--;
    }

    return new self($prev_year, $prev_month);
  }

  /**
   * @return $this
   * @throws Exception
   */
  public function nextMonth(): self {
    $next_year = $this->year;
    $next_month = $this->month + 1;
    if (13 === $next_month) {
      $next_month = 1;
      $next_year++;
    }

    return new self($next_year, $next_month);
  }

  /**
   *
   * @return string
   */
  public function monthTitle(): string {
    $months = $this->months();

    return $months[$this->month];
  }

  /**
   *
   * @return array
   */
  public function months(): array {
    return [
      1 => 'Январь',
      2 => 'Февраль',
      3 => 'Март',
      4 => 'Апрель',
      5 => 'Май',
      6 => 'Июнь',
      7 => 'Июль',
      8 => 'Август',
      9 => 'Сентябрь',
      10 => 'Октябрь',
      11 => 'Ноябрь',
      12 => 'Декабрь',
    ];
  }

  /**
   *
   * @return array
   */
  public function weekDayNames(): array {
    return [
      'пн',
      'вт',
      'ср',
      'чт',
      'пт',
      'сб',
      'вс',
    ];
  }
}
