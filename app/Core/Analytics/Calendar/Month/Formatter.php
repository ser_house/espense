<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 24.10.2019
 * Time: 9:47
 */


namespace App\Core\Analytics\Calendar\Month;


class Formatter {
  /**
   * @param Month $month
   *
   * @return string
   */
  public function formattedForSql(Month $month): string {
    $year_str = $month->year;
    $month_num_str = $this->normalizedMonthNumber($month->month);

    return "$year_str-$month_num_str";
  }

  /**
   * @param int $year
   * @param int $month
   *
   * @return string
   */
  public function formattedYearMonthForSql(int $year, int $month): string {
    $month_num_str = $this->normalizedMonthNumber($month);

    return "$year-$month_num_str";
  }

  /**
   * @param int $month
   *
   * @return string
   */
  public function normalizedMonthNumber(int $month): string {
    $month_str = (string)$month;
    if ($month < 10) {
      return str_pad($month_str, 2, '0', STR_PAD_LEFT);
    }

    return $month_str;
  }
}
