<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.09.2019
 * Time: 0:23
 */


namespace App\Core\Analytics\Calendar\Month;

use App\Core\Analytics\Calendar\View\Day;
use App\Core\Analytics\IDataProvider as AnalyticsDataProvider;
use DateTimeImmutable;
use Exception;

/**
 * Class Builder
 *
 * @package App\Core\Analytics\Calendar\Month
 */
class Builder {

  public function __construct(private readonly AnalyticsDataProvider $analyticsDataProvider) {

  }

  /**
   * @param int $year
   * @param int $month_num
   *
   * @return Item
   * @throws Exception
   */
  public function build(int $year, int $month_num): Item {
    $month = new Month($year, $month_num);

    $item = new Item();
    $item->title = $month->monthTitle();
    $item->year = $month->year;
    $item->month = $month->month;

    $days = [];

    $period = $month->dailyPeriod();

    $week = 1;
    /** @var DateTimeImmutable $date */
    foreach ($period as $index => $date) {
      $key = $date->format('Y-m-d');
      $week_day = $date->format('N');

      if ($index > 0 && 1 == $week_day) {
        $week++;
      }
      if (!isset($item->weeks[$week])) {
        $item->weeks[$week] = array_fill(1, 7, null);
      }

      // Полностью заполняем месяц пустыми (без расходов) объектами Day.
      $item->weeks[$week][$week_day] = new Day($date->format('d'), $date->format('d.m.Y'));
      // Сохраняем ссылку на объект дня и дальше обратимся по ней прямо к дню в неделе.
      $days[$key] = &$item->weeks[$week][$week_day];
    }

    // Получаем расходы по дням месяца
    $data = $this->analyticsDataProvider->totalByDatesAndCategories($year, $month_num);

    // и раскладываем их по соответствующим дням.
    foreach ($data as $date_str => $date_items) {
      foreach ($date_items as $date_item) {
        $days[$date_str]->addExpense($date_item);
      }
    }

    return $item;
  }
}
