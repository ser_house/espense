<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.10.2019
 * Time: 12:14
 */


namespace App\Core\Analytics\Calendar\Month;


class Item {
  public string $title;
  public array $weeks;
  public int $year;
  public int $month;
}
