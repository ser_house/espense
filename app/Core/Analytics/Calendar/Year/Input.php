<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 24.11.2019
 * Time: 14:04
 */


namespace App\Core\Analytics\Calendar\Year;


use App\Core\Category\Category;
use App\Core\Contractor\Contractor;
use App\Core\Product\Product;

class Input {

  public function __construct(
    public readonly int $year,
    public readonly ?Category $category = null,
    public readonly ?Product $product = null,
    public readonly ?Contractor $contractor = null,
  ) {

  }
}
