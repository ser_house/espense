<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.10.2019
 * Time: 16:09
 */


namespace App\Core\Analytics\Calendar\Year;


use Exception;
use InvalidArgumentException;

final class Year {

  private const MIN_YEAR = 2000;

  /**
   * Year constructor.
   *
   * @param int $year
   *
   * @throws Exception
   */
  public function __construct(public readonly int $year) {
    if ($year < self::MIN_YEAR) {
      throw new InvalidArgumentException('Год не может более ранним, чем ' . self::MIN_YEAR . ", получен '$year'");
    }
  }

  /**
   *
   * @return bool
   */
  public function canGoToPrev(): bool {
    return $this->year > self::MIN_YEAR;
  }

  /**
   *
   * @return bool
   */
  public function canGoToNext(): bool {
    $current_year = date('Y');

    return $current_year > $this->year;
  }

  public function prevYear(): self {
    return new self($this->year - 1);
  }

  public function nextYear(): self {
    return new self($this->year + 1);
  }

  /**
   *
   * @return array [month_num => month_title]
   */
  public static function months(): array {
    return [
      1 => 'Январь',
      2 => 'Февраль',
      3 => 'Март',
      4 => 'Апрель',
      5 => 'Май',
      6 => 'Июнь',
      7 => 'Июль',
      8 => 'Август',
      9 => 'Сентябрь',
      10 => 'Октябрь',
      11 => 'Ноябрь',
      12 => 'Декабрь',
    ];
  }
}
