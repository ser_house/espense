<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.09.2019
 * Time: 0:23
 */


namespace App\Core\Analytics\Calendar\Year;

use App\Core\Analytics\Calendar\View\Month;
use App\Core\Analytics\IDataProvider as AnalyticsDataProvider;
use App\Core\Category\IRepository as ICategoryRepository;
use App\Core\Contractor\IRepository as IContractorRepository;
use App\Core\Product\IRepository as IProductRepository;
use App\Core\Title;
use Exception;


/**
 * Class Builder.
 *
 * Раскладывает месяца года по сезонам.
 *
 * @package App\Core\Analytics\Calendar\Year
 */
class Builder {

  public function __construct(
    private readonly AnalyticsDataProvider $analyticsDataProvider,
    private readonly ICategoryRepository $categoryRepository,
    private readonly IProductRepository $productRepository,
    private readonly IContractorRepository $contractorRepository
  ) {

  }

  /**
   * @param array $input
   *
   * @return Input
   */
  public function yearInputFromRaw(array $input): Input {
    if (empty($input['year'])) {
      $input['year'] = date('Y');
    }

    $category = null;
    if (!empty($input['category'])) {
      $category = $this->categoryRepository->findByTitle(new Title($input['category']));
    }

    $product = null;
    if (!empty($input['product'])) {
      $product = $this->productRepository->findByTitle(new Title($input['product']));
    }

    $contractor = null;
    if (!empty($input['contractor'])) {
      $contractor = $this->contractorRepository->findByTitle(new Title($input['contractor']));
    }

    $year = $input['year'];

    return new Input($year, $category, $product, $contractor);
  }

  /**
   * @param Input $input
   *
   * @return Item
   * @throws Exception
   */
  public function buildYearData(Input $input): Item {
    $year = new Year($input->year);

    $item = new Item();
    $item->year = $year->year;

    // Раскладываем месяца по сезонам так,
    // чтобы в одном ряду были только месяцы одного сезона.
    $item->months = $this->monthsBySeasonsMatrix(Year::months());

    $groupedByMonths = $this->analyticsDataProvider->totalByMonthsAndCategories($input);

    foreach ($groupedByMonths as $month_num => $month_items) {
      $line_num = $this->getSeasonNumberByMonthNumber($month_num);
      foreach ($month_items as $month_item) {
        $item->months[$line_num][$month_num]->addExpense($month_item);
      }
    }

    return $item;
  }

  /**
   * @param array $month_names
   *
   * @return array
   */
  private function monthsBySeasonsMatrix(array $month_names): array {
    return [
      1 => [
        0 => new Month(''),
        1 => new Month($month_names[1]),
        2 => new Month($month_names[2]),
      ],
      2 => [
        3 => new Month($month_names[3]),
        4 => new Month($month_names[4]),
        5 => new Month($month_names[5]),
      ],
      3 => [
        6 => new Month($month_names[6]),
        7 => new Month($month_names[7]),
        8 => new Month($month_names[8]),
      ],
      4 => [
        9 => new Month($month_names[9]),
        10 => new Month($month_names[10]),
        11 => new Month($month_names[11]),
      ],
      5 => [
        12 => new Month($month_names[12]),
        13 => new Month(''),
        14 => new Month(''),
      ],
    ];
  }

  /**
   * @param int $month_num
   *
   * @return int
   */
  private function getSeasonNumberByMonthNumber(int $month_num): int {
    $lines = [
      1 => 1,
      2 => 1,
      3 => 2,
      4 => 2,
      5 => 2,
      6 => 3,
      7 => 3,
      8 => 3,
      9 => 4,
      10 => 4,
      11 => 4,
      12 => 5,
    ];

    return $lines[$month_num];
  }
}
