<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.10.2019
 * Time: 12:14
 */


namespace App\Core\Analytics\Calendar\Year;


class Item {
  public int $year;
  public array $months;
}
