<?php

namespace App\Core\Analytics\ByTime;

interface IByTimeDataProviderFactory {
  /**
   * @param Params $params
   *
   * @return IByTimeDataProvider
   */
  public function get(Params $params): IByTimeDataProvider;
}
