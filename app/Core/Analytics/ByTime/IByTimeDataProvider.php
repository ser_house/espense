<?php

namespace App\Core\Analytics\ByTime;

interface IByTimeDataProvider {
  /**
   * @return Result
   */
  public function get(): Result;
}
