<?php

namespace App\Core\Analytics\ByTime;

class Result {
  /**
   * @param array $series
   * @param array $categories
   */
  public function __construct(public readonly array $series, public readonly array $categories) {

  }
}
