<?php

namespace App\Core\Analytics\ByTime;

use App\Core\Category\CategoryId;
use App\Core\Exception\MissingRequiredDataException;
use App\Core\Exception\ValueNotInExceptedException;
use App\Core\Product\ProductId;
use Throwable;

final class Params {
  private const GROUP_BY_YEARS = 'group_by_years';
  private const GROUP_BY_MONTHS = 'group_by_months';

  private const SUM_BY_PERIODS = 'sum_by_periods';
  private const SUM_BY_CHILDREN = 'sum_by_children';

  private string $group_by = self::GROUP_BY_YEARS;
  private string $sum_by = self::SUM_BY_CHILDREN;

  private ?CategoryId $categoryId = null;
  private ?ProductId $productId = null;
  private array $years = [];

  /**
   * @param array $input
   *
   * @throws Throwable
   */
  public function __construct(array $input) {
    if (isset($input['group_by'])) {
      $excepted = [self::GROUP_BY_YEARS, self::GROUP_BY_MONTHS];
      if (!in_array($input['group_by'], $excepted)) {
        throw new ValueNotInExceptedException($input['group_by'], $excepted);
      }
      $this->group_by = $input['group_by'];
      if (!$this->isGroupByYear()) {
        if (empty($input['years'])) {
          throw new MissingRequiredDataException(['Не указаны годы.']);
        }
        $this->years = $input['years'];
      }
    }

    if (empty($input['category_id']) && empty($input['product_id'])) {
      if (empty($input['category_id'])) {
        throw new MissingRequiredDataException(['Не указана категория.']);
      }
      if (empty($input['product_id'])) {
        throw new MissingRequiredDataException(['Не указан товар.']);
      }
    }
    if (!empty($input['category_id'])) {
      $this->categoryId = new CategoryId($input['category_id']);
    }


    if (!empty($input['product_id'])) {
      $this->productId = new ProductId($input['product_id']);
    }

    if (isset($input['sum_by'])) {
      $excepted = [self::SUM_BY_CHILDREN, self::SUM_BY_PERIODS];
      if (!in_array($input['sum_by'], $excepted)) {
        throw new MissingRequiredDataException(['Не указано по чему группировать.']);
      }
      $this->sum_by = $input['sum_by'];
    }
  }

  /**
   * @return string[]
   */
  public static function groupByOptions(): array {
    return [
      self::GROUP_BY_YEARS => 'по годам',
      self::GROUP_BY_MONTHS => 'по месяцам',
    ];
  }

  /**
   * @return string[]
   */
  public static function sumByOptions(): array {
    return [
      self::SUM_BY_PERIODS => 'по периодам',
      self::SUM_BY_CHILDREN => 'по позициям',
    ];
  }

  /**
   * @return bool
   */
  public function isGroupByYear(): bool {
    return self::GROUP_BY_YEARS === $this->group_by;
  }

  /**
   * @return bool
   */
  public function isSumByPeriods(): bool {
    return self::SUM_BY_PERIODS === $this->sum_by;
  }

  /**
   * @return CategoryId
   */
  public function categoryId(): CategoryId {
    return $this->categoryId;
  }

  /**
   * @return ProductId|null
   */
  public function productId(): ?ProductId {
    return $this->productId;
  }


  /**
   * @return array
   */
  public function years(): array {
    if ($this->isGroupByYear()) {
      // Конечно, это финт ушами. Но зато простейшее решение
      // (альтернатива - нагородить набор типов и фабрику для их
      // построения и это будет неоправданно в данном случае, хоть и красиво)
      throw new \Exception("Не имеет смысла запрашивать значение года для группировки по годам.");
    }
    return $this->years;
  }
}
