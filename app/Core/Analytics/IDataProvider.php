<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.09.2019
 * Time: 0:39
 */


namespace App\Core\Analytics;

use App\Core\Analytics\Calendar\Year\Input;
use App\Core\Analytics\View\CategoryItem;
use App\Core\Analytics\View\ExpenseItem;
use App\Core\Analytics\View\ProductItem;
use App\Core\Category\CategoryId;


interface IDataProvider {

  public const TOTALS = 'totals';

  /**
   *
   * @return array
   *   $year => $year
   */
  public function journalYears(): array;

  /**
   * @param int $year
   * @param int $month_num
   *
   * @return ExpenseItem[]
   *  даты в качестве ключей
   */
  public function totalByDatesAndCategories(int $year, int $month_num): array;

  /**
   * @param int $year
   * @param int $month_num
   *
   * @return ExpenseItem[]
   *  даты в качестве ключей
   */
  public function productsByDatesAndCategories(int $year, int $month_num): array;

  /**
   * @param Input $input
   *
   * @return array
   */
  public function totalByMonthsAndCategories(Input $input): array;

  /**
   * @param int $year
   * @param CategoryId $categoryId
   *
   * @return array
   */
  public function productsByYearAndCategory(int $year, CategoryId $categoryId): array;

  /**
   * @param int $year
   * @param int $month_num
   *
   * @return ExpenseItem[]
   */
  public function totalByCategoriesForMonth(int $year, int $month_num): array;

  /**
   * @param int $year
   *
   * @return ExpenseItem[]
   */
  public function totalByCategoriesForYear(int $year): array;

  /**
   * @param Input $input
   *
   * @return ExpenseItem[]
   */
  public function totalByMonths(Input $input): array;

  /**
   * @return CategoryItem[]
   */
  public function totalByCategories(): array;

  /**
   * @param CategoryId $categoryId
   *
   * @return float|null
   */
  public function totalByCategory(CategoryId $categoryId): ?float;

  /**
   * @param CategoryId $categoryId
   *
   * @return CategoryItem[]|ProductItem[]
   */
  public function totalByCategoryChildren(CategoryId $categoryId): array;

  /**
   *
   * @return ExpenseItem[]
   */
  public function totalsByRoot(): array;
}
