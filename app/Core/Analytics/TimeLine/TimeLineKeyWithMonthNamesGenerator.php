<?php

namespace App\Core\Analytics\TimeLine;

use App\Core\Service\DateGenerator;
use stdClass;

class TimeLineKeyWithMonthNamesGenerator {

  /**
   * @param int $year
   * @param int $month
   *
   * @return string
   */
  public function generateByYearMonth(int $year, int $month): string {
    $month_names = DateGenerator::SHORT_MONTH_NAMES_RU;
    $month_name = $month_names[$month - 1];
    return "$year-$month_name";
  }

  /**
   * @param TimeLineMonth $timeLineMonth
   *
   * @return string
   */
  public function generateByTimeLineMonth(TimeLineMonth $timeLineMonth): string {
    return $this->generateByYearMonth($timeLineMonth->year, $timeLineMonth->month);
  }

  /**
   * @param stdClass $object
   *
   * @return string
   */
  public function generateByObject(stdClass $object): string {
    return $this->generateByYearMonth($object->year, $object->month);
  }
}
