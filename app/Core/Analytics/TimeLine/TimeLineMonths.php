<?php

namespace App\Core\Analytics\TimeLine;

class TimeLineMonths {

  /** @var TimeLineMonth[] */
  private array $items;

  /**
   * @param TimeLineKeyWithMonthNamesGenerator $timeLineKeyBuilder
   */
  public function __construct(private readonly TimeLineKeyWithMonthNamesGenerator $timeLineKeyBuilder) {
  }

  /**
   * @param TimeLineMonth $item
   *
   * @return $this
   */
  public function add(TimeLineMonth $item): self {
    $this->items[] = $item;
    return $this;
  }

  /**
   * @return TimeLineMonth
   */
  public function first(): TimeLineMonth {
    return $this->items[0];
  }

  /**
   * @return TimeLineMonth
   */
  public function last(): TimeLineMonth {
    $last_key = array_key_last($this->items);
    return $this->items[$last_key];
  }

  /**
   * @return TimeLineMonth[]
   */
  public function items(): array {
    return $this->items;
  }

  /**
   * @return array
   */
  public function initialTimeLine(): array {
    $data = [];
    foreach ($this->items as $item) {
      $key = $this->timeLineKeyBuilder->generateByTimeLineMonth($item);
      $data[$key] = null;
    }
    return $data;
  }
}
