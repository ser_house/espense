<?php

namespace App\Core\Analytics\TimeLine;

class TimeLineMonth {

  public function __construct(public readonly int $year, public readonly int $month) {
  }
}
