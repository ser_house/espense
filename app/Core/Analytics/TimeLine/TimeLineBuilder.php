<?php

namespace App\Core\Analytics\TimeLine;

use DateTimeImmutable;

class TimeLineBuilder {
  public function __construct(private readonly TimeLineKeyWithMonthNamesGenerator $timeLineKeyBuilder) {
  }

  /**
   * @param int $first_year
   * @param int $first_month
   * @param int|null $last_year
   * @param int|null $last_month
   *
   * @return TimeLineMonths
   */
  public function buildMonths(int $first_year, int $first_month, int $last_year = null, int $last_month = null): TimeLineMonths {
    if (!$last_year || !$last_month) {
      $today = new DateTimeImmutable();

      if (!$last_year) {
        $last_year = (int)$today->format('Y');
      }

      if (!$last_month) {
        $last_month = (int)$today->format('m');
      }
    }

    $result = new TimeLineMonths($this->timeLineKeyBuilder);

    for ($year = $first_year; $year <= $last_year; $year++) {
      if ($first_year === $year) {
        for ($month = $first_month; $month <= 12; $month++) {
          $result->add(new TimeLineMonth($year, $month));
        }
      }
      elseif ($last_year === $year) {
        for ($month = 1; $month <= $last_month; $month++) {
          $result->add(new TimeLineMonth($year, $month));
        }
      }
      else {
        for ($month = 1; $month <= 12; $month++) {
          $result->add(new TimeLineMonth($year, $month));
        }
      }
    }

    return $result;
  }

  /**
   * @param array $objects
   *
   * @return TimeLineMonths
   */
  public function buildMonthsFromStdObjectsList(array $objects): TimeLineMonths {
    $first_year = (int)$objects[0]->year;
    $first_month = (int)$objects[0]->month;
    $last_key = array_key_last($objects);
    $last_year = (int)$objects[$last_key]->year;
    $last_month = (int)$objects[$last_key]->month;

    return $this->buildMonths($first_year, $first_month, $last_year, $last_month);
  }
}
