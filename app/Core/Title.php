<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 0:01
 */


namespace App\Core;

use InvalidArgumentException;

/**
 * Class Title
 *
 * @package App\Core
 */
class Title {

  protected string $value;

  /**
   * Title constructor.
   *
   * @param string $value
   *
   * @throws InvalidArgumentException
   */
  public function __construct(string $value) {
    $value = trim($value);
    if (mb_strlen($value) < 2) {
      throw new InvalidArgumentException("Недостаточная длина названия: '$value'");
    }
    $this->value = $this->ucfirst($value);
  }

  /**
   * @return string
   */
  public function value(): string {
    return $this->value;
  }

  /**
   * @param Title $title
   *
   * @return bool
   */
  public function equals(Title $title): bool {
    return $title->value() === $this->value();
  }

  /**
   * @inheritDoc
   */
  public function __toString() {
    return $this->value();
  }

  private function ucfirst(string $string, string $encoding = 'UTF-8'): string {
    $strlen = mb_strlen($string, $encoding);
    $firstChar = mb_substr($string, 0, 1, $encoding);
    $then = mb_substr($string, 1, $strlen - 1, $encoding);

    return mb_strtoupper($firstChar, $encoding) . $then;
  }
}
