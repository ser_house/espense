<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:15
 */


namespace App\Core\Unit\UseCase\Rename;


use App\Core\Exception\MissingRequiredDataException;
use App\Core\Unit\Title;
use App\Core\Unit\UnitId;
use Exception;

class Input {

  public readonly UnitId $id;
  public readonly Title $newTitle;

  /**
   * Input constructor.
   *
   * @param array $input = [
   *    'id' => (string), required
   *    'title' => (string), required
   *    'url' => (string), optional
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    if (empty($input['id'])) {
      throw new MissingRequiredDataException(['Не указан id единицы измерения.']);
    }
    if (empty($input['title'])) {
      throw new MissingRequiredDataException(['Не указано название единицы измерения.']);
    }

    $this->id = new UnitId($input['id']);
    $this->newTitle = new Title($input['title']);
  }
}
