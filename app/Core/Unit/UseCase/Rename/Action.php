<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:14
 */


namespace App\Core\Unit\UseCase\Rename;

use App\Core\Exception\NotFoundEntityException;
use App\Core\Unit\IRepository as UnitRepository;

class Action {

  public function __construct(private readonly UnitRepository $unitRepository) {

  }

  /**
   * @param Input $input
   *
   * @return RenameEvent
   */
  public function handle(Input $input): RenameEvent {
    $originalUnit = $this->unitRepository->findById($input->id);
    if (null === $originalUnit) {
      throw new NotFoundEntityException('Изменение невозможно: единица измерения не найдена.');
    }

    $renamedUnit = clone $originalUnit;
    if (!$originalUnit->title->equals($input->newTitle)) {
      $renamedUnit = $renamedUnit->rename($input->newTitle);
    }

    $this->unitRepository->update($renamedUnit);

    return new RenameEvent($originalUnit, $renamedUnit);
  }
}
