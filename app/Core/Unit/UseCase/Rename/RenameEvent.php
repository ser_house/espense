<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.02.2020
 * Time: 13:53
 */


namespace App\Core\Unit\UseCase\Rename;


use App\Core\Unit\Unit;

class RenameEvent {

  public function __construct(
    public readonly Unit $unchangedUnit,
    public readonly Unit $changedUnit,
  ) {

  }
}
