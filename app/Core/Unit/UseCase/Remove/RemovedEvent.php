<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.02.2020
 * Time: 13:55
 */


namespace App\Core\Unit\UseCase\Remove;


use App\Core\Unit\Unit;

class RemovedEvent {

  public function __construct(public readonly Unit $unit) {

  }
}
