<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:29
 */


namespace App\Core\Unit\UseCase\Remove;

use App\Core\Unit\IRepository as UnitRepository;
use App\Core\Exception\BusyEntityException;
use App\Core\Exception\NotFoundEntityException;
use App\Core\Expense\IDataProvider as ExpenseDataProvider;

class Action {

  public function __construct(
    private readonly UnitRepository $unitRepository,
    private readonly ExpenseDataProvider $expenseDataProvider,
  ) {

  }

  /**
   * @param Input $input
   *
   * @return RemovedEvent
   */
  public function handle(Input $input): RemovedEvent {
    $unit = $this->unitRepository->findById($input->id);
    if (null === $unit) {
      throw new NotFoundEntityException('Удаление невозможно: единица измерения не найдена.');
    }

    $count_expenses = $this->expenseDataProvider->countByUnitId($input->id);
    if ($count_expenses) {
      throw new BusyEntityException('Удаление невозможно: единица измерения используется в расходах.');
    }

    $this->unitRepository->remove($input->id);

    return new RemovedEvent($unit);
  }
}
