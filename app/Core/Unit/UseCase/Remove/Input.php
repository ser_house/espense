<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:27
 */


namespace App\Core\Unit\UseCase\Remove;


use App\Core\Exception\MissingRequiredDataException;
use App\Core\Unit\UnitId;
use Exception;

class Input {

  public readonly UnitId $id;

  /**
   * Input constructor.
   *
   * @param array $input = [
   *    'id' => (string), required
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    if (empty($input['id'])) {
      throw new MissingRequiredDataException(['Не указан id единицы измерения.']);
    }
    $this->id = new UnitId($input['id']);
  }
}
