<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 16:37
 */


namespace App\Core\Unit\UseCase\Add;


use App\Core\Exception\MissingRequiredDataException;
use App\Core\Unit\Title;
use Exception;

class Input {

  public readonly Title $title;

  /**
   * Input constructor.
   *
   * @param array $input = [
   *    'title' => (string), required
   *    'url' => (string), optional
   * ]
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    if (empty($input['title'])) {
      throw new MissingRequiredDataException(['Не указано название единицы измерения.']);
    }

    $this->title = new Title($input['title']);
  }
}
