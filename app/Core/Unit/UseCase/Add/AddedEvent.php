<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.02.2020
 * Time: 13:49
 */


namespace App\Core\Unit\UseCase\Add;


use App\Core\Unit\Unit;

class AddedEvent {

  public function __construct(public readonly Unit $unit) {

  }
}
