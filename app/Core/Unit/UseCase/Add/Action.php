<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:04
 */


namespace App\Core\Unit\UseCase\Add;

use App\Core\Unit\Unit;
use App\Core\Unit\Converter;
use App\Core\Unit\IRepository as UnitRepository;
use App\Core\Exception\ExistsException;

class Action {

  public function __construct(
    private readonly UnitRepository $unitRepository,
    private readonly Converter $unitConverter,
  ) {

  }

  /**
   * @param Input $input
   *
   * @return AddedEvent
   */
  public function handle(Input $input): AddedEvent {
    $exists = $this->unitRepository->findByTitle($input->title);
    if ($exists) {
      $item = $this->unitConverter->entityToDTO($exists);
      throw new ExistsException("Добавление невозможно: единица измерения '$input->title' существует.", $item);
    }

    $id = $this->unitRepository->nextId();

    $unit = new Unit($id, $input->title);
    $this->unitRepository->add($unit);

    return new AddedEvent($unit);
  }
}
