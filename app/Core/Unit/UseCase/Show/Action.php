<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.02.2020
 * Time: 17:14
 */


namespace App\Core\Unit\UseCase\Show;

use App\Core\Generic\DTO\Item;
use App\Core\Unit\IDataProvider;

class Action {

  public function __construct(private readonly IDataProvider $unitDataProvider) {

  }


  /**
   * @param Input $input
   *
   * @return Item
   */
  public function handle(Input $input): Item {
    return $this->unitDataProvider->getById($input->id);
  }
}
