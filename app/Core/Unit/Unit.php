<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 29.04.2021
 * Time: 8:29
 */


namespace App\Core\Unit;


class Unit {

  public function __construct(public readonly UnitId $id, public readonly Title $title) {

  }

  /**
   * @param Title $newTitle
   *
   * @return $this
   */
  public function rename(Title $newTitle): self {
    return new self($this->id, $newTitle);
  }
}
