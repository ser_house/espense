<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.09.2019
 * Time: 19:05
 */


namespace App\Core\Unit;


use App\Core\Generic\DTO\Item;

/**
 * Class Converter
 *
 * @package App\Core\Unit
 */
class Converter {

  /**
   * @param Unit $unit
   *
   * @return Item
   */
  public function entityToDTO(Unit $unit): Item {
    $id = (string)$unit->id;
    $title = (string)$unit->title;

    return new Item($id, $title);
  }
}
