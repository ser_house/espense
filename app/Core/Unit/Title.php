<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 0:01
 */


namespace App\Core\Unit;

use InvalidArgumentException;

/**
 * Class Title
 *
 * @package App\Core\Unit
 */
class Title {

  protected string $value;

  /**
   * Title constructor.
   *
   * @param string $value
   *
   * @throws InvalidArgumentException
   */
  public function __construct(string $value) {
    $value = trim($value);
    if (empty($value)) {
      throw new InvalidArgumentException('Название не может быть пустым.');
    }
    $this->value = $value;
  }

  /**
   * @return string
   */
  public function value(): string {
    return $this->value;
  }

  /**
   * @param Title $title
   *
   * @return bool
   */
  public function equals(Title $title): bool {
    return $title->value() === $this->value();
  }

  /**
   * @inheritDoc
   */
  public function __toString() {
    return $this->value();
  }
}
