<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.04.2021
 * Time: 8:48
 */


namespace App\Core\Unit;


use App\Core\Exception\NotFoundEntityException;
use App\Core\Generic\DTO\Item;
use App\Core\Unit\DTO\ManageItem;

interface IDataProvider {
  /**
   *
   * @return Item[]
   */
  public function all(): array;

  /**
   * @return array
   */
  public function allAsOptions(): array;

  /**
   *
   * @return ManageItem[]
   */
  public function manageItems(): array;

  /**
   * @param UnitId $unitId
   *
   * @return Item
   * @throws NotFoundEntityException
   */
  public function getById(UnitId $unitId): Item;
}
