<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 10:45
 */


namespace App\Core\Unit;

interface IRepository {

  /**
   *
   * @return UnitId
   */
  public function nextId(): UnitId;

  /**
   * @param Unit $unit
   *
   * @return mixed
   */
  public function add(Unit $unit);

  /**
   * @param Unit $unit
   */
  public function update(Unit $unit): void;

  /**
   * @param UnitId $unitId
   */
  public function remove(UnitId $unitId): void;

  /**
   *
   * @return Unit[]
   */
  public function findAll(): array;

  /**
   * @param UnitId $unitId
   *
   * @return Unit|null
   */
  public function findById(UnitId $unitId): ?Unit;

  /**
   * @param Title $title
   *
   * @return Unit|null
   */
  public function findByTitle(Title $title): ?Unit;
}
