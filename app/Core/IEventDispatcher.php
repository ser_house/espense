<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.11.2019
 * Time: 12:40
 */


namespace App\Core;

/**
 * Interface IEventDispatcher.
 * Отвязывает реализацию диспетчера событий домена от фреймворка.
 *
 * @package App\Core
 */
interface IEventDispatcher {
  /**
   * @param IEvent $event
   */
  public function dispatch(IEvent $event): void;
}
