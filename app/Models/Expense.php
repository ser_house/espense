<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.09.2019
 * Time: 12:02
 */


namespace App\Models;


use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Expense
 *
 * @property string $id
 * @property string $date
 * @property string $account_id
 * @property string $product_id
 * @property string $unit_id
 * @property float $quantity
 * @property float $amount
 * @property string|null $note
 * @property string|null $contractor_id
 * @method static Builder|Expense newModelQuery()
 * @method static Builder|Expense newQuery()
 * @method static Builder|Expense query()
 * @method static Builder|Expense whereAmount($value)
 * @method static Builder|Expense whereContractorId($value)
 * @method static Builder|Expense whereDate($value)
 * @method static Builder|Expense whereAccountId($value)
 * @method static Builder|Expense whereId($value)
 * @method static Builder|Expense whereNote($value)
 * @method static Builder|Expense whereProductId($value)
 * @method static Builder|Expense whereQuantity($value)
 * @mixin Eloquent
 * @property string $created_at
 * @method static Builder|Expense whereCreatedAt($value)
 * @method static Builder|Expense whereUnitId($value)
 */
class Expense extends Model {
  protected $table = 'expense';
  public $timestamps = false;
  public $incrementing = false;
  protected $keyType = 'string';

  protected $fillable = ['id', 'date', 'account_id', 'product_id', 'unit_id', 'quantity', 'amount', 'note', 'contractor_id'];
}
