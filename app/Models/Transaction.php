<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.09.2019
 * Time: 12:02
 */


namespace App\Models;


use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Transaction
 *
 * @property string $id
 * @property string $date
 * @property string $account_src_id
 * @property string $account_target_id
 * @property float $amount
 * @property string|null $note
 *
 * @method static Builder|Transaction newModelQuery()
 * @method static Builder|Transaction newQuery()
 * @method static Builder|Transaction query()
 * @method static Builder|Transaction whereAmount($value)
 * @method static Builder|Transaction whereDate($value)
 * @method static Builder|Transaction whereId($value)
 * @method static Builder|Transaction whereNote($value)
 * @method static Builder|Transaction whereAccountSrcId($value)
 * @method static Builder|Transaction whereAccountTargetId($value)
 * @mixin Eloquent
 * @property string $created_at
 * @method static Builder|Transaction whereCreatedAt($value)
 */
class Transaction extends Model {
  protected $table = 'transaction';
  public $timestamps = false;
  public $incrementing = false;
  protected $keyType = 'string';

  protected $fillable = ['id', 'date', 'account_src_id', 'account_target_id', 'amount', 'note'];
}
