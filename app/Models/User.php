<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * App\Models\User
 *
 * @property string $id
 * @property string $title
 * @property bool $is_active
 * @property bool $is_default
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User query()
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereIsActive($value)
 * @method static Builder|User whereIsDefault($value)
 * @method static Builder|User whereTitle($value)
 * @mixin Eloquent
 * @property string $timezone
 * @method static Builder|User whereTimezone($value)
 */
class User extends Model {
  protected $table = 'user';
  public $timestamps = false;
  public $incrementing = false;
  protected $keyType = 'string';

  protected $fillable = ['timezone'];

  protected static function boot() {
    parent::boot();
    static::creating(function ($user) {
      $user->{$user->getKeyName()} = (string)Str::uuid();
    });
  }
}
