<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Contractor
 *
 * @method static Builder|Contractor newModelQuery()
 * @method static Builder|Contractor newQuery()
 * @method static Builder|Contractor query()
 * @mixin Eloquent
 * @property string $id
 * @property string $title
 * @method static Builder|Contractor whereId($value)
 * @method static Builder|Contractor whereTitle($value)
 * @property string $url
 * @property string $note
 * @method static Builder|Contractor whereUrl($value)
 * @method static Builder|Contractor whereNote($value)
 */
class Contractor extends Model {
  protected $table = 'contractor';
  public $timestamps = false;
  protected $fillable = ['id', 'title', 'url', 'note'];
  public $incrementing = false;
  protected $keyType = 'string';
}
