<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Product
 *
 * @property string $id
 * @property string $title
 * @property string $category_id
 * @method static Builder|Product newModelQuery()
 * @method static Builder|Product newQuery()
 * @method static Builder|Product query()
 * @method static Builder|Product whereCategoryId($value)
 * @method static Builder|Product whereId($value)
 * @method static Builder|Product whereTitle($value)
 * @mixin Eloquent
 */
class Product extends Model {
  protected $table = 'product';
  public $timestamps = false;
  public $incrementing = false;
  protected $keyType = 'string';

  protected $fillable = ['id', 'category_id', 'title'];
}
