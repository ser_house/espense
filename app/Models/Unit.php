<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Unit
 *
 * @method static Builder|Unit newModelQuery()
 * @method static Builder|Unit newQuery()
 * @method static Builder|Unit query()
 * @mixin Eloquent
 * @property string $id
 * @property string $title
 * @method static Builder|Unit whereId($value)
 * @method static Builder|Unit whereTitle($value)
 */
class Unit extends Model {
  protected $table = 'unit';
  public $timestamps = false;
  protected $fillable = ['id', 'title'];
  public $incrementing = false;
  protected $keyType = 'string';
}
