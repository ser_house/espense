<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Account
 *
 * @method static Builder|Account newModelQuery()
 * @method static Builder|Account newQuery()
 * @method static Builder|Account query()
 * @mixin Eloquent
 * @property string $id
 * @property string $title
 * @property string|null $contractor_id
 *
 * @method static Builder|Account whereId($value)
 * @method static Builder|Account whereTitle($value)
 * @method static Builder|Account whereContractorId($value)
 */
class Account extends Model {
  protected $table = 'account';
  public $timestamps = false;
  protected $fillable = ['id', 'title', 'contractor_id'];
  public $incrementing = false;
  protected $keyType = 'string';
}
