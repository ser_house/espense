<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Category
 *
 * @method static Builder|Category newModelQuery()
 * @method static Builder|Category newQuery()
 * @method static Builder|Category query()
 * @mixin Eloquent
 * @property string $id
 * @property string $title
 * @property string $parent_id
 * @method static Builder|Category whereId($value)
 * @method static Builder|Category whereTitle($value)
 * @method static Builder|Category whereParentId($value)
 */
class Category extends Model {
  protected $table = 'category';
  public $timestamps = false;
  protected $fillable = ['id', 'title', 'parent_id'];
  public $incrementing = false;
  protected $keyType = 'string';
}
