<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 1:24
 */


namespace App\Http\Controllers\Transactions;

use App\Core\Account\IRepository as AccountRepository;
use App\Core\Transaction\Presenter;
use App\Core\Transaction\IDataProvider as TransactionDataProvider;
use App\Core\Transaction\UseCase\Find\Input as FindInput;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Illuminate\Http\Request;


class Main extends Controller {

  public function __construct(
    private readonly TransactionDataProvider $transactionDataProvider,
    private readonly AccountRepository $accountRepository,
  ) {

  }

  /**
   *
   * @return Factory|View
   */
  public function __invoke(Request $request) {
    $params = $request->all();
    if ($params) {
      $input = new FindInput($params, $this->accountRepository);
      $items = $this->transactionDataProvider->find($input);
    }
    else {
      $items = $this->transactionDataProvider->all();
    }


    $presenter = new Presenter();


    $item_views = $presenter->views($items);

    return view('transactions.main', [
      'items' => $item_views,
    ]);
  }
}
