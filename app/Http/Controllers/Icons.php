<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 1:24
 */


namespace App\Http\Controllers;

use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;


/**
 * Class Main
 *
 * @package App\Http\Controllers
 */
class Icons extends Controller {

  /**
   *
   * @return Factory|View
   * @throws Exception
   */
  public function __invoke() {
    $icon_classes = $this->getIconsClasses();

    return view('icons', [
      'icon_classes' => $icon_classes,
    ]);
  }

  private function getIconsClasses(): array {
    $path = resource_path() . '/fonts/icons/variables.scss';

    $content = file_get_contents($path);
    $classes = [];
    preg_match_all('/\$(icon-.*):/', $content, $classes);

    return $classes[1];
  }
}
