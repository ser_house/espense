<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 1:24
 */


namespace App\Http\Controllers\Accounts;

use App\Core\Account\AccountId;
use App\Core\Account\IDataProvider as AccountsDataProvider;
use App\Core\Account\IRepository as AccountsRepository;
use App\Core\Account\UseCase\Find\Input as FindInput;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Illuminate\Http\Request;


class Main extends Controller {

  public function __construct(
    private readonly AccountsRepository $accountsRepository,
    private readonly AccountsDataProvider $accountsDataProvider,
  ) {

  }

  /**
   *
   * @return Factory|View
   */
  public function __invoke(Request $request) {
    $params = $request->all();
    $input = new FindInput($params, $this->accountsRepository);
    $items = $this->accountsDataProvider->itemsWithoutDefault($input);

    $ops = [];
    foreach ($items as $item) {
      $id = new AccountId($item->id);
      $account_ops = $this->accountsDataProvider->operations($id);
      $ops[$item->id] = $account_ops;
    }
    // Для каждого счета надо вывести:
    //  остаток
    //  поступления
    //  переводы (если есть)
    //  расходы (если есть)

    return view('accounts.main', [
      'items' => $items,
      'ops' => $ops,
    ]);
  }
}
