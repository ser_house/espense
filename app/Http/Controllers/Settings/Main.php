<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 1:24
 */


namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\User;
use Config;
use DateTimeImmutable;
use DateTimeZone;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;


/**
 * Class Main
 *
 * @package App\Http\Controllers\Settings
 */
class Main extends Controller {

  /**
   *
   * @return Factory|View
   * @throws Exception
   */
  public function page() {
    $user = User::first();
    $current_tz = $user ? $user->timezone : Config::get('app.timezone');

    return view('settings.main', [
      'timezones' => $this->getTimezones(),
      'current_tz' => $current_tz,
    ]);
  }

  /**
   * @param Request $request
   *
   * @return Factory|View
   * @throws Exception
   */
  public function save(Request $request) {
    $tz = $request->get('tz');

    $user = User::first();
    if (!$user) {
      $user = new User();
    }

    $user->timezone = $tz;
    $user->save();

    $request->session()->flash('success', 'Сохранено.');

    return view('settings.main', [
      'timezones' => $this->getTimezones(),
      'current_tz' => $tz,
    ]);
  }

  /**
   *
   * @return array
   * @throws Exception
   */
  private function getTimezones(): array {
    $timezones = [];

    foreach (DateTimeZone::listIdentifiers(DateTimeZone::PER_COUNTRY, 'RU') as $timezone) {
      $dateTimeZone = new DateTimeZone($timezone);
      $dateTime = new DateTimeImmutable('now', $dateTimeZone);

      $offset = $dateTime->format('P');
      $current = $dateTime->format('H:i');

      $parts = explode('/', $timezone);

      $name = __("cities.$parts[1]");
      $tz = "$current ($name, $offset)";

      $timezones[$timezone] = $tz;
    }

    asort($timezones);

    return $timezones;
  }
}
