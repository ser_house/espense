<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.12.2019
 * Time: 9:26
 */


namespace App\Http\Controllers\Manage;

use App\Core\Account\IDataProvider as IAccountDataProvider;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class Accounts extends Controller {

  public function __construct(private readonly IAccountDataProvider $accountDataProvider) {

  }

  /**
   * @return Application|Factory|View
   */
  public function page() {
    $items = $this->accountDataProvider->manageItems();

    return view('manage.accounts', [
      'items' => $items,
    ]);
  }
}
