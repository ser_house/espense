<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 1:24
 */


namespace App\Http\Controllers\Manage;

use App\Core\Category\IDataProvider as CategoryDataProvider;
use App\Core\Product\IDataProvider as IProductDataProvider;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class Products extends Controller {

  public function __construct(
    private readonly CategoryDataProvider $categoryDataProvider,
    private readonly IProductDataProvider $productDataProvider,
  ) {

  }

  /**
   * @return Application|Factory|View
   */
  public function page() {
    $product_items = $this->productDataProvider->allManageItems();
    $products_by_category_id = [];
    foreach ($product_items as $product_item) {
      $category_id = $product_item->category_id;
      $product_id = $product_item->id;
      $products_by_category_id[$category_id][$product_id] = $product_item;
    }


    $categories = $this->categoryDataProvider->allAsTree();
    foreach ($categories as $category) {
      $this->fillCategory($category, $products_by_category_id);
    }

    return view('manage.products', [
      'category_items' => $categories,
    ]);
  }

  /**
   * @param $category
   * @param $products_by_category
   *
   * @return void
   */
  private function fillCategory($category, $products_by_category): void {
    $products = $products_by_category[$category->id] ?? [];
    $category->products = $products;
    foreach ($category->children as $child) {
      $this->fillCategory($child, $products_by_category);
    }
  }
}
