<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 1:24
 */


namespace App\Http\Controllers\Manage;

use App\Core\Category\IDataProvider as CategoryDataProvider;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class CategoriesList extends Controller {

  public function __construct(private readonly CategoryDataProvider $categoryDataProvider) {

  }

  /**
   * @return Application|Factory|View
   */
  public function page() {
    $list = $this->categoryDataProvider->listMaterializedItems();

    return view('manage.categories_list', [
      'categories' => $list,
    ]);
  }
}
