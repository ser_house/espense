<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.12.2019
 * Time: 9:26
 */


namespace App\Http\Controllers\Manage;

use App\Core\Unit\IDataProvider as IUnitDataProvider;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class Units extends Controller {

  public function __construct(private readonly IUnitDataProvider $unitDataProvider) {

  }

  /**
   * @return Application|Factory|View
   */
  public function page() {
    $items = $this->unitDataProvider->manageItems();

    return view('manage.units', [
      'items' => $items,
    ]);
  }
}
