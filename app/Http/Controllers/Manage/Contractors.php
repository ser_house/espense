<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.12.2019
 * Time: 9:26
 */


namespace App\Http\Controllers\Manage;

use App\Core\Contractor\IDataProvider as IContractorDataProvider;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class Contractors extends Controller {

  public function __construct(private readonly IContractorDataProvider $contractorDataProvider) {

  }

  /**
   * @return Application|Factory|View
   */
  public function page() {
    $items = $this->contractorDataProvider->manageItems();

    return view('manage.contractors', [
      'items' => $items,
    ]);
  }
}
