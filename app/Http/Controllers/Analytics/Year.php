<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.09.2019
 * Time: 14:05
 */


namespace App\Http\Controllers\Analytics;

use App\Application\YearRoutes;
use App\Core\Analytics\Calendar\Year\Builder as YearBuilder;
use App\Core\Analytics\Calendar\Year\Year as CalendarYear;
use App\Core\Analytics\IDataProvider as IAnalyticsDataProvider;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Throwable;

/**
 * Class Year
 *
 * @package App\Http\Controllers\Analytics
 */
class Year extends Controller {

  public function __construct(
    private readonly IAnalyticsDataProvider $analyticsDataProvider,
    private readonly YearBuilder $yearBuilder,
  ) {

  }

  /**
   * @inheritDoc
   */
  public function __invoke(Request $request) {
    $input = $request->all();

    try {
      $filter = $this->yearBuilder->yearInputFromRaw($input);
      $year_num = $filter->year;
      $yearData = $this->yearBuilder->buildYearData($filter);

      $category = $filter->category;

      if (null === $category) {
        $grouped_main_items = $this->analyticsDataProvider->totalByCategoriesForYear($year_num);
        $grouped_main_title = 'По категориям';
      }
      else {
        $grouped_main_items = $this->analyticsDataProvider->productsByYearAndCategory($year_num, $category->id);
        $grouped_main_title = 'По товарам';
      }

      $by_months = $this->analyticsDataProvider->totalByMonths($filter);

      $months = CalendarYear::months();
      foreach ($by_months as &$item) {
        $item->title = $months[$item->title];
      }
      unset($item);
      $years = $this->analyticsDataProvider->journalYears();

      $yearRoutes = new YearRoutes('analytics.year', $year_num);

      return view('analytics.year', [
        'yearData' => $yearData,
        'grouped_main' => [
          'items' => $grouped_main_items,
          'title' => $grouped_main_title,
        ],
        'by_months' => $by_months,
        'month_names' => array_values(CalendarYear::months()),
        'prev_route' => $yearRoutes->prev,
        'next_route' => $yearRoutes->next,
        'category' => $input['category'] ?? null,
        'product' => $input['product'] ?? null,
        'year' => $year_num,
        'years' => $years,
      ]);
    }
    catch (Throwable $e) {
      return redirect('analytics')->with('error', $e->getMessage());
    }
  }
}
