<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.09.2019
 * Time: 14:05
 */

namespace App\Http\Controllers\Analytics;

use App\Core\Analytics\ByTime\Params;
use App\Core\Analytics\IDataProvider as AnalyticsDataProvider;
use App\Core\Category\IDataProvider as CategoryDataProvider;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Class ByTime
 *
 * @package App\Http\Controllers\Analytics
 */
class ByTime extends Controller {

  public function __construct(
    private readonly AnalyticsDataProvider $analyticsDataProvider,
    private readonly CategoryDataProvider $categoryDataProvider,

  ) {

  }

  /**
   * @inheritDoc
   */
  public function __invoke(Request $request) {
    $years = $this->analyticsDataProvider->journalYears();

    $categories = $this->categoryDataProvider->categories();

    return view('analytics.by_time', [
      'categories' => $categories,
      'years' => $years,
      'group_by_options' => Params::groupByOptions(),
      'sum_by_options' => Params::sumByOptions(),
    ]);
  }
}
