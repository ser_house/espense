<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.09.2019
 * Time: 14:05
 */


namespace App\Http\Controllers\Analytics;

use App\Application\MonthRoutes;
use App\Core\Analytics\Calendar\Month\Builder as MonthBuilder;
use App\Core\Analytics\Calendar\Month\Month as CalendarMonth;
use App\Core\Analytics\Calendar\Year\Year as CalendarYear;
use App\Core\Analytics\IDataProvider as IAnalyticsDataProvider;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Throwable;


/**
 * Class Month
 *
 * @package App\Http\Controllers\Analytics
 */
class Month extends Controller {

  public function __construct(private readonly IAnalyticsDataProvider $analyticsDataProvider) {

  }

  /**
   * @inheritDoc
   */
  public function __invoke(Request $request,) {
    $month = $request->get('month');
    if (empty($month)) {
      $year = date('Y');
      $month_num = date('m');
    }
    else {
      [$year, $month_num] = explode('-', $month);
    }

    try {
      $monthRoutes = new MonthRoutes('analytics.month', $year, $month_num);
      $calendarMonth = new CalendarMonth($year, $month_num);

      $monthCategories = $this->analyticsDataProvider->totalByCategoriesForMonth($year, $month_num);

      $builder = new MonthBuilder($this->analyticsDataProvider);
      $data = $builder->build($year, $month_num);

      $months = CalendarYear::months();
      foreach ($months as $num => &$item) {
        $item = [
          'title' => $item,
          'url' => route('analytics.month', ['month' => "$year-$num"]),
        ];
      }
      unset($item);

      return view('analytics.month', [
        'month' => $data,
        'by_categories' => $monthCategories,
        'weekday_names' => $calendarMonth->weekDayNames(),
        'months' => $months,
        'prev_route' => $monthRoutes->prev,
        'next_route' => $monthRoutes->next,
      ]);
    }
    catch (Throwable $e) {
      return redirect('analytics')->with('error', $e->getMessage());
    }
  }
}
