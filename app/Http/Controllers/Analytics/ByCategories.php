<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.09.2019
 * Time: 14:05
 */
namespace App\Http\Controllers\Analytics;

use App\Core\Analytics\IDataProvider;
use App\Http\Controllers\Controller;

/**
 * Class ByCategories
 *
 * @package App\Http\Controllers\Analytics
 */
class ByCategories extends Controller {

  public function __construct(private readonly IDataProvider $dataProvider) {

  }

  /**
   * @inheritDoc
   */
  public function __invoke() {
    return view('analytics.by_categories', [
      'items' => $this->dataProvider->totalByCategories(),
    ]);
  }
}
