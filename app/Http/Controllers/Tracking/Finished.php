<?php

namespace App\Http\Controllers\Tracking;

use App\Core\Tracking\IDataProvider;
use App\Core\Tracking\Views\Presenter;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class Finished extends Controller {
  /**
   * @param Request $request
   *
   * @return Factory|View
   */
  public function __invoke(Request $request, IDataProvider $dataProvider, Presenter $presenter) {
    $items = $dataProvider->finishedTracks();
    $finished_products = $dataProvider->finishedTotals();
    return view('tracking.finished', [
      'items' => $presenter->viewsArchiveItems($items),
      'finished_products' => $finished_products,
    ]);
  }
}
