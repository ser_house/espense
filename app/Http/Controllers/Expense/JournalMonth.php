<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.09.2019
 * Time: 15:57
 */


namespace App\Http\Controllers\Expense;

use App\Application\MonthRoutes;
use App\Core\Analytics\Calendar\Year\Year as CalendarYear;
use App\Core\Expense\Main\Presenter;
use App\Core\Expense\UseCase\JournalMonth\Action as JournalMonthAction;
use App\Core\Expense\UseCase\JournalMonth\Input;
use App\Core\Unit\IDataProvider as UnitDataProvider;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class JournalMonth extends Controller {

  public function __construct(
    private readonly UnitDataProvider $unitDataProvider,
  ) {

  }

  /**
   * @param Request $request
   * @param JournalMonthAction $action
   *
   * @return Factory|View
   */
  public function __invoke(Request $request, JournalMonthAction $action) {
    $raw_input = $request->all();

    $input = $action->buildInput($raw_input);
    $items = $action->handle($input);

    $presenter = new Presenter();
    $item_views = $presenter->views($items);

    $units = $this->unitDataProvider->allAsOptions();

    $monthRoutes = new MonthRoutes('expense.journal_month', $input->year, $input->month);

    $months = CalendarYear::months();
    return view('expense.journal_month', [
      'category' => $input->categoryTitle(),
      'product' => $input->productItem(),
      'contractor' => $input->contractorTitle(),
      'sort_mode' => $raw_input['sort'] ?? 'asc',
      'month' => $input->month,
      'month_title' => $months[$input->month],
      'year' => $input->year,
      'any_item' => $input->anyItemOption(),
      'items' => $item_views,
      'units' => $units,
      'prev_route' => $monthRoutes->prev,
      'next_route' => $monthRoutes->next,
    ]);
  }
}
