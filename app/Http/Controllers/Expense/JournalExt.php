<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.09.2019
 * Time: 15:57
 */


namespace App\Http\Controllers\Expense;

use App\Core\Analytics\Calendar\Year\Year;
use App\Core\Analytics\IDataProvider as AnalyticsDataProvider;
use App\Core\Expense\Main\Presenter;
use App\Core\Expense\UseCase\Find\Action as FindUseCase;
use App\Core\Expense\UseCase\Find\Input as FindInput;
use App\Core\Unit\IDataProvider as UnitDataProvider;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class JournalExt extends Controller {

  public function __construct(
    private readonly AnalyticsDataProvider $analyticsDataProvider,
    private readonly UnitDataProvider $unitDataProvider,
  ) {

  }

  /**
   * @param Request $request
   * @param FindUseCase $useCase
   *
   * @return Factory|View
   */
  public function __invoke(Request $request, FindUseCase $useCase) {
    $raw_input = $request->all();

    if (empty($raw_input)) {
      $raw_input['years'] = [FindInput::ANY_KEY];
      $raw_input['months'] = [FindInput::ANY_KEY];
    }

    $findInput = $useCase->buildInput($raw_input);
    $items = $useCase->handle($findInput);

    $presenter = new Presenter();
    $item_views = $presenter->views($items);

    $years = $this->analyticsDataProvider->journalYears();
    $current_year = date('Y');
    if (!isset($years[$current_year])) {
      $years[$current_year] = $current_year;
    }

    $units = $this->unitDataProvider->allAsOptions();

    return view('expense.journal_ext', [
      'category' => $raw_input['category'] ?? null,
      'selected_product' => $findInput->productItem(),
      'contractor' => $findInput->contractorTitle(),
      'is_favorite' => $findInput->is_favorite,
      'selected_sort_mode' => $raw_input['sort'] ?? $findInput->sort,
      'selected_years' => $raw_input['years'] ?? $findInput->years(),
      'years' => $years,
      'selected_months' => $raw_input['months'] ?? $findInput->months(),
      'months' => Year::months(),
      'date' => $raw_input['date'] ?? $findInput->date(),
      'any_item' => $findInput->anyItemOption(),
      'items' => $item_views,
      'units' => $units,
    ]);
  }
}
