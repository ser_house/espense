<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 1:24
 */


namespace App\Http\Controllers\Expense;

use App\Core\Expense\IDataProvider as ExpenseDataProvider;
use App\Core\Expense\Main\Presenter as ExpensePresenter;
use App\Core\Transaction\Presenter as TransactionPresenter;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use App\Core\Unit\IDataProvider as UnitDataProvider;
use App\Core\Account\IDataProvider as AccountDataProvider;


/**
 * Class Main
 *
 * @package App\Http\Controllers\Expense
 */
class Main extends Controller {

  public function __construct(
    private readonly ExpenseDataProvider $expenseDataProvider,
    private readonly UnitDataProvider $unitDataProvider,
    private readonly AccountDataProvider $accountDataProvider,
    private readonly ExpensePresenter $expensePresenter,
    private readonly TransactionPresenter $transactionPresenter,
  ) {

  }

  /**
   *
   * @return Factory|View
   */
  public function page() {
    $items = $this->expenseDataProvider->todayItems();
    $item_views = $this->expensePresenter->views($items);
    $last_expense_item_view = $item_views ? last($item_views) : null;
    if (null === $last_expense_item_view) {
      $last_expense_item = $this->expenseDataProvider->lastItem();
      if ($last_expense_item) {
        $last_expense_item_view = $this->expensePresenter->view($last_expense_item);
      }
    }

    $favorite_items = $this->expenseDataProvider->getFavoriteItems();
    $favorite_items_views = $this->expensePresenter->views($favorite_items);

    $favorites_grouped_by_category = [];
    foreach ($favorite_items_views as $itemView) {
      $category_path = $this->expensePresenter->categoryPath($itemView);
      if (!isset($favorites_grouped_by_category[$category_path])) {
        $favorites_grouped_by_category[$category_path] = [];
      }
      $favorites_grouped_by_category[$category_path][] = $itemView;
    }
    ksort($favorites_grouped_by_category);

    $units_data = $this->unitDataProvider->all();
    $units = [];
    foreach ($units_data as $unit) {
      $units[$unit->id] = $unit->title;
    }

    $accounts = $this->accountDataProvider->itemsWithRemain();
    return view('expense.main', [
      'expense' => $this->expensePresenter->viewNew($units_data),
      'transaction' => $this->transactionPresenter->viewNew(),
      'accounts' => $accounts,
      'units' => $units,
      'items' => $item_views,
      'favorite_items' => $favorites_grouped_by_category,
      'last_expense_item' => $last_expense_item_view,
    ]);
  }
}
