<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.09.2019
 * Time: 12:21
 */


namespace App\Http\Api;

use App\Core\Analytics\IDataProvider as AnalyticsDataProvider;
use App\Core\Category\UseCase\Add\Action as AddAction;
use App\Core\Category\UseCase\Add\Input as AddInput;
use App\Core\Category\UseCase\Find\Action as FindAction;
use App\Core\Category\UseCase\MoveToCategory\Action as MoveToCategoryAction;
use App\Core\Category\UseCase\MoveToCategory\Input as MoveToCategoryInput;
use App\Core\Category\UseCase\Remove\Action as RemoveAction;
use App\Core\Category\UseCase\Remove\Input as RemoveInput;
use App\Core\Category\UseCase\Rename\Action as RenameAction;
use App\Core\Category\UseCase\Rename\Input as RenameInput;
use App\Core\Category\UseCase\Show\Action as ShowAction;
use App\Core\Category\UseCase\Show\Input as ShowInput;
use App\Core\Generic\UseCase\Find\Input as FindInput;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class Category
 *
 * @package App\Http\Api
 */
class Category extends Controller {

  /**
   * @param Request $request
   * @param AddAction $useCase
   *
   * @return JsonResponse|null
   * @throws Exception
   */
  public function add(Request $request, AddAction $useCase): ?JsonResponse {
    $input = new AddInput($request->all());
    $addedEvent = $useCase->handle($input);

    $category = $addedEvent->category;

    $title = (string)$category->title;

    return response()->json([
      'type' => 'success',
      'msg' => "Категория '$title' добавлена.",
      'category' => $category,
    ], Response::HTTP_CREATED);
  }

  /**
   * @param ShowAction $useCase
   * @param string $id
   *
   * @return JsonResponse
   * @throws Exception
   */
  public function show(ShowAction $useCase, string $id): JsonResponse {
    $input = new ShowInput(['id' => $id]);
    $item = $useCase->handle($input);

    return response()->json([
      'type' => 'success',
      'item' => $item,
    ]);
  }

  /**
   * @param Request $request
   * @param FindAction $useCase
   *
   * @return JsonResponse
   */
  public function find(Request $request, FindAction $useCase): JsonResponse {
    $input = new FindInput($request->all());
    $found_items = $useCase->handle($input);

    return response()->json($found_items);
  }

  /**
   * @param Request $request
   * @param RenameAction $useCase
   * @param string $id
   *
   * @return JsonResponse
   * @throws Exception
   */
  public function rename(Request $request, RenameAction $useCase, string $id): JsonResponse {
    $inputs = $request->all();
    $inputs['id'] = $id;
    $input = new RenameInput($inputs);
    $renamedEvent = $useCase->handle($input);

    $category = $renamedEvent->category;
    $old_title = (string)$renamedEvent->oldTitle;
    $new_title = (string)$category->title;

    return response()->json([
      'type' => 'success',
      'msg' => "Категория '$old_title' переименована в '$new_title'.",
      'category' => $category,
    ]);
  }

  /**
   * @param Request $request
   * @param MoveToCategoryAction $useCase
   * @param AnalyticsDataProvider $analyticsDataProvider
   * @param string $id
   *
   * @return JsonResponse
   * @throws Exception
   */
  public function moveToCategory(Request $request, MoveToCategoryAction $useCase, AnalyticsDataProvider $analyticsDataProvider, string $id): JsonResponse {
    $inputs = $request->all();
    $inputs['category_id'] = $id;
    $input = new MoveToCategoryInput($inputs);
    $movedToCategoryEvent = $useCase->handle($input);

    $category = $movedToCategoryEvent->category;
    $oldCategory = $movedToCategoryEvent->oldCategory;
    $newCategory = $movedToCategoryEvent->newCategory;

    $title = (string)$category->title;
    $old_title = $oldCategory ? (string)$oldCategory->title : 'корень';
    $new_title = $newCategory ? (string)$newCategory->title : 'корень';

    $totals = $analyticsDataProvider->totalsByRoot();

    return response()->json([
      'type' => 'success',
      'msg' => "Категория '$title' перемещена из '$old_title' в '$new_title'.",
      'category' => $category,
      'totals' => $totals,
    ]);
  }


  /**
   * @param RemoveAction $useCase
   * @param string $id
   *
   * @return JsonResponse
   * @throws Exception
   */
  public function remove(RemoveAction $useCase, string $id): JsonResponse {
    $input = new RemoveInput(['id' => $id]);
    $removedEvent = $useCase->handle($input);

    $title = (string)$removedEvent->category->title;

    return response()->json([
      'type' => 'success',
      'msg' => "Категория '$title' удалена.",
    ]);
  }
}
