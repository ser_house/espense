<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.09.2019
 * Time: 12:21
 */


namespace App\Http\Api;

use App\Core\Analytics\IDataProvider as AnalyticsDataProvider;
use App\Core\Generic\ApiResponse\Success;
use App\Core\Generic\DTO\Item;
use App\Core\Product\IDataProvider as ProductDataProvider;
use App\Core\Product\UseCase\Add\Action as AddAction;
use App\Core\Product\UseCase\Add\Input as AddInput;
use App\Core\Product\UseCase\Find\Action as FindAction;
use App\Core\Product\UseCase\Find\Input as FindInput;
use App\Core\Product\UseCase\MoveToCategory\Action as MoveToCategoryAction;
use App\Core\Product\UseCase\MoveToCategory\Input as MoveToCategoryInput;
use App\Core\Product\UseCase\Remove\Action as RemoveAction;
use App\Core\Product\UseCase\Remove\Input as RemoveInput;
use App\Core\Product\UseCase\Rename\Action as RenameAction;
use App\Core\Product\UseCase\Rename\Input as RenameInput;
use App\Core\Product\UseCase\Show\Action as ShowAction;
use App\Core\Product\UseCase\Show\Input as ShowInput;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class Product
 *
 * @package App\Http\Api
 */
class Product extends Controller {

  /**
   * @param Request $request
   * @param AddAction $useCase
   * @param ProductDataProvider $productDataProvider
   *
   * @return JsonResponse
   * @throws Exception
   */
  public function add(Request $request, AddAction $useCase, ProductDataProvider $productDataProvider): JsonResponse {
    $input = new AddInput($request->all());
    $addedEvent = $useCase->handle($input);

    $product = $productDataProvider->getById($addedEvent->product->id);
    $title = $product->title;

    $result = [
      'type' => 'success',
      'msg' => "Товар '$title' добавлен.",
      'product' => $product,
    ];

    return response()->json($result, Response::HTTP_CREATED);
  }

  /**
   * @param ShowAction $useCase
   * @param string $id
   *
   * @return JsonResponse
   * @throws Exception
   */
  public function show(ShowAction $useCase, string $id): JsonResponse {
    $input = new ShowInput(['id' => $id]);
    $item = $useCase->handle($input);

    return response()->json([
      'type' => 'success',
      'item' => $item,
    ]);
  }

  /**
   * @param Request $request
   * @param FindAction $useCase
   *
   * @return JsonResponse
   */
  public function find(Request $request, FindAction $useCase): JsonResponse {
    $input = new FindInput($request->all());
    $items = $useCase->handle($input);

    return response()->json($items);
  }

  /**
   * @param Request $request
   * @param RenameAction $useCase
   * @param string $id
   *
   * @return JsonResponse
   * @throws Exception
   */
  public function rename(Request $request, RenameAction $useCase, string $id): JsonResponse {
    $inputs = $request->all();
    $inputs['id'] = $id;
    $input = new RenameInput($inputs);
    $renamedEvent = $useCase->handle($input);

    $original_title = (string)$renamedEvent->originalProduct->title;
    $new_title = (string)$renamedEvent->updatedProduct->title;

    $id = (string)$renamedEvent->originalProduct->id;

    return response()->json([
      'type' => 'success',
      'msg' => "Товар '$original_title' переименован в '$new_title'.",
      'item' => new Item($id, $new_title),
    ]);
  }

  /**
   * @param Request $request
   * @param MoveToCategoryAction $useCase
   * @param AnalyticsDataProvider $analyticsDataProvider
   * @param ProductDataProvider $productDataProvider
   * @param string $id
   *
   * @return JsonResponse
   * @throws Exception
   */
  public function moveToCategory(Request $request, MoveToCategoryAction $useCase, AnalyticsDataProvider $analyticsDataProvider, ProductDataProvider $productDataProvider, string $id): JsonResponse {
    $inputs = $request->all();
    $inputs['id'] = $id;
    $input = new MoveToCategoryInput($inputs);
    $movedEvent = $useCase->handle($input);

    $product = $productDataProvider->getById($movedEvent->product->id);
    $category = $movedEvent->category;
    $oldCategory = $movedEvent->oldCategory;

    $title = $product->title;

    $old_category_title = (string)$oldCategory->title;
    $new_category_title = (string)$category->title;

    $totals = $analyticsDataProvider->totalsByRoot();

    return response()->json([
      'type' => 'success',
      'msg' => "Товар '$title' перемещен из категории '$old_category_title' в категорию '$new_category_title'.",
      'product' => $product,
      'totals' => $totals,
    ]);
  }

  /**
   * @param RemoveAction $useCase
   * @param string $id
   *
   * @return JsonResponse
   * @throws Exception
   */
  public function remove(RemoveAction $useCase, string $id): JsonResponse {
    $input = new RemoveInput(['id' => $id]);
    $removedEvent = $useCase->handle($input);
    $product = $removedEvent->product;

    $title = (string)$product->title;

    return response()->json(new Success("Товар '$title' удалён."));
  }
}
