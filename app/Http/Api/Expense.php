<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.09.2019
 * Time: 12:21
 */


namespace App\Http\Api;

use App\Application\Expense\Converter;
use App\Core\Analytics\IDataProvider as AnalyticsDataProvider;
use App\Core\Exception\FailedToSaveEntityException;
use App\Core\Expense\Entity\ExpenseId;
use App\Core\Expense\IDataProvider as ExpenseDataProvider;
use App\Core\Expense\Main\Presenter;
use App\Core\Expense\UseCase\Add\Action as AddAction;
use App\Core\Expense\UseCase\Add\Input as AddInput;
use App\Core\Expense\UseCase\Find\Action as FindAction;
use App\Core\Expense\UseCase\Remove\Action as RemoveAction;
use App\Core\Expense\UseCase\Remove\Input as RemoveInput;
use App\Core\Expense\UseCase\SetUnit\Action as SetUnitAction;
use App\Core\Expense\UseCase\SetUnit\Input as SetUnitInput;
use App\Core\Expense\UseCase\Show\Action as ShowAction;
use App\Core\Expense\UseCase\Show\Input as ShowInput;
use App\Core\Expense\UseCase\Split\Action as SplitAction;
use App\Core\Expense\UseCase\Split\Input as SplitInput;
use App\Core\Expense\UseCase\Update\Action as UpdateAction;

use App\Core\Expense\UseCase\AddToFavorites\Action as AddToFavoritesAction;
use App\Core\Expense\UseCase\RemoveFromFavorites\Action as RemoveFromFavoritesAction;

use App\Core\Expense\UseCase\Update\Input as UpdateInput;
use App\Core\Generic\Formatter\Text as TextFormatter;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Throwable;

/**
 * Class Expense
 *
 * @package App\Http\Api
 */
class Expense extends Controller {

  public function __construct(
    private readonly ExpenseDataProvider $expenseDataProvider,
    private readonly AnalyticsDataProvider $analyticsDataProvider,
    private readonly Converter $converter) {

  }

  /**
   * @param Request $request
   * @param AddAction $useCase
   *
   * @return JsonResponse|null
   * @throws FailedToSaveEntityException
   */
  public function add(Request $request, AddAction $useCase): ?JsonResponse {
    $input = new AddInput($request->all());
    $addedEvent = $useCase->handle($input);

    $expenseItem = $this->expenseDataProvider->itemById($addedEvent->expense->id);

    $presenter = new Presenter();
    $item = $presenter->view($expenseItem, 'Y-m-d');

    $totals = $this->analyticsDataProvider->totalsByRoot();

    return response()->json([
      'type' => 'success',
      'msg' => 'Расход добавлен.',
      'expense' => $item,
      'prev_favorite_id' => (string)$addedEvent->prevFavoriteExpenseId,
      'totals' => $totals,
    ], Response::HTTP_CREATED);
  }

  /**
   * @param ShowAction $useCase
   * @param string $id
   *
   * @return JsonResponse
   * @throws Exception
   */
  public function show(ShowAction $useCase, string $id): JsonResponse {
    $input = new ShowInput(['id' => $id]);
    $item = $useCase->handle($input);

    return response()->json([
      'type' => 'success',
      'item' => $item,
    ]);
  }

  /**
   * @param Request $request
   * @param FindAction $useCase
   *
   * @return JsonResponse|null
   * @throws Exception
   */
  public function find(Request $request, FindAction $useCase): ?JsonResponse {
    $presenter = new Presenter();

    if ($request->has('last')) {
      $item = $this->expenseDataProvider->lastItem();

      return response()->json([
        'type' => 'success',
        'item' => $item ? $presenter->view($item) : null,
      ]);
    }

    $raw_input = $request->all();
    $input = $useCase->buildInput($raw_input);
    $items = $useCase->handle($input);

    $item_views = $presenter->views($items);

    return response()->json([
      'type' => 'success',
      'items' => $item_views,
    ]);
  }

  /**
   * @param Request $request
   * @param UpdateAction $useCase
   * @param string $id
   *
   * @return JsonResponse|null
   * @throws FailedToSaveEntityException
   */
  public function update(Request $request, UpdateAction $useCase, string $id): ?JsonResponse {
    $inputs = $request->all();
    $inputs['id'] = $id;
    $input = new UpdateInput($inputs);
    $updatedEvent = $useCase->handle($input);
    $updatedExpense = $updatedEvent->updatedExpense;
    $originalExpense = $updatedEvent->originalExpense;

    $presenter = new Presenter();
    $expenseItem = $this->expenseDataProvider->itemById($updatedExpense->id);
    $item = $presenter->view($expenseItem);

    $totals = null;
    if (!$updatedExpense->amount->equals($originalExpense->amount)) {
      $totals = $this->analyticsDataProvider->totalsByRoot();
    }

    return response()->json([
      'type' => 'success',
      'msg' => 'Расход обновлен.',
      'expense' => $item,
      'totals' => $totals,
    ]);
  }

  /**
   * @param RemoveAction $useCase
   * @param string $id
   *
   * @return JsonResponse|null
   * @throws Exception
   */
  public function remove(RemoveAction $useCase, string $id): ?JsonResponse {
    $input = new RemoveInput(['id' => $id]);
    $useCase->handle($input);

    $totals = $this->analyticsDataProvider->totalsByRoot();

    return response()->json([
      'type' => 'success',
      'msg' => 'Расход удален.',
      'totals' => $totals,
    ]);
  }

  /**
   * @param Request $request
   * @param SplitAction $useCase
   * @param string $id
   *
   * @return JsonResponse
   * @throws Throwable
   */
  public function split(Request $request, SplitAction $useCase, string $id): JsonResponse {
    $inputs = $request->all();
    $inputs['id'] = $id;
    $input = new SplitInput($inputs);
    $expenses = $useCase->handle($input);

    $new_expense_items = $this->converter->domainsToDto($expenses);

    $presenter = new Presenter();

    return response()->json([
      'type' => 'success',
      'msg' => 'Расход разбит.',
      'expenses' => $presenter->views($new_expense_items),
    ]);
  }

  /**
   * @param Request $request
   * @param TextFormatter $textFormatter
   * @param SetUnitAction $useCase
   * @param string $unit_id
   *
   * @return JsonResponse
   * @throws Throwable
   */
  public function setUnit(Request $request, TextFormatter $textFormatter, SetUnitAction $useCase, string $unit_id): JsonResponse {
    $inputs = $request->all();
    $inputs['unit_id'] = $unit_id;
    $input = new SetUnitInput($inputs);
    $event = $useCase->handle($input);
    $affected_num = $event->affected_num;
    $records_title = $textFormatter->morph($affected_num, ['записи', 'записей', 'записей']);

    return response()->json([
      'type' => 'success',
      'msg' => "Единицы измерения установлены для $affected_num $records_title",
    ]);
  }

  /**
   * @param string $id
   * @param AddToFavoritesAction $action
   *
   * @return JsonResponse
   * @throws Exception
   */
  public function addToFavorites(string $id, AddToFavoritesAction $action): JsonResponse {
    $event = $action->handle(new ExpenseId($id));
    return response()->json([
      'type' => 'success',
      'msg' => 'Расход добавлен в избранное.',
      'prev_favorite_id' => (string)$event->prevFavoriteExpenseId,
    ]);
  }

  /**
   * @param string $id
   * @param RemoveFromFavoritesAction $action
   *
   * @return JsonResponse
   * @throws Exception
   */
  public function removeFromFavorites(string $id, RemoveFromFavoritesAction $action): JsonResponse {
    $action->handle(new ExpenseId($id));
    return response()->json([
      'type' => 'success',
      'msg' => 'Расход удален из избранного.',
    ]);
  }
}
