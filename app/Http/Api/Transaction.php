<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.09.2019
 * Time: 12:21
 */


namespace App\Http\Api;

use App\Core\Exception\FailedToSaveEntityException;
use App\Core\Transaction\IDataProvider as TransactionDataProvider;
use App\Core\Transaction\UseCase\Add\Action as AddAction;
use App\Core\Transaction\UseCase\Add\Input as AddInput;
use App\Core\Transaction\UseCase\Find\Action as FindAction;
use App\Core\Transaction\UseCase\Remove\Action as RemoveAction;
use App\Core\Transaction\UseCase\Remove\Input as RemoveInput;
use App\Core\Transaction\UseCase\Show\Action as ShowAction;
use App\Core\Transaction\UseCase\Show\Input as ShowInput;
use App\Core\Transaction\UseCase\Update\Action as UpdateAction;
use App\Core\Transaction\UseCase\Update\Input as UpdateInput;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class Transaction
 *
 * @package App\Http\Api
 */
class Transaction extends Controller {

  public function __construct(
    private readonly TransactionDataProvider $transactionDataProvider
  ) {

  }

  /**
   * @param Request $request
   * @param AddAction $useCase
   *
   * @return JsonResponse|null
   */
  public function add(Request $request, AddAction $useCase): ?JsonResponse {
    $input = new AddInput($request->all());
    $addedEvent = $useCase->handle($input);

    $transactionItem = $this->transactionDataProvider->itemById($addedEvent->transaction->id);

    return response()->json([
      'type' => 'success',
      'msg' => 'Перевод добавлен.',
      'transaction' => $transactionItem,
    ], Response::HTTP_CREATED);
  }

  /**
   * @param ShowAction $useCase
   * @param string $id
   *
   * @return JsonResponse
   * @throws Exception
   */
  public function show(ShowAction $useCase, string $id): JsonResponse {
    $input = new ShowInput(['id' => $id]);
    $item = $useCase->handle($input);

    return response()->json([
      'type' => 'success',
      'item' => $item,
    ]);
  }

  /**
   * @param Request $request
   * @param FindAction $useCase
   *
   * @return JsonResponse|null
   * @throws Exception
   */
  public function find(Request $request, FindAction $useCase): ?JsonResponse {
    $raw_input = $request->all();
    $input = $useCase->buildInput($raw_input);
    $items = $useCase->handle($input);

    return response()->json([
      'type' => 'success',
      'items' => $items,
    ]);
  }

  /**
   * @param Request $request
   * @param UpdateAction $useCase
   * @param string $id
   *
   * @return JsonResponse|null
   * @throws FailedToSaveEntityException
   */
  public function update(Request $request, UpdateAction $useCase, string $id): ?JsonResponse {
    $inputs = $request->all();
    $inputs['id'] = $id;
    $input = new UpdateInput($inputs);
    $updatedEvent = $useCase->handle($input);
    $updatedTransaction = $updatedEvent->updatedTransaction;

    $transactionItem = $this->transactionDataProvider->itemById($updatedTransaction->id);

    return response()->json([
      'type' => 'success',
      'msg' => 'Перевод обновлен.',
      'transaction' => $transactionItem,
    ]);
  }

  /**
   * @param RemoveAction $useCase
   * @param string $id
   *
   * @return JsonResponse|null
   * @throws Exception
   */
  public function remove(RemoveAction $useCase, string $id): ?JsonResponse {
    $input = new RemoveInput(['id' => $id]);
    $useCase->handle($input);

    return response()->json([
      'type' => 'success',
      'msg' => 'Перевод удален.',
    ]);
  }
}
