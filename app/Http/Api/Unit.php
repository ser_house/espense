<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.09.2019
 * Time: 12:21
 */


namespace App\Http\Api;


use App\Core\Generic\ApiResponse\Success;
use App\Core\Generic\DTO\Item;
use App\Core\Unit\UseCase\Add\Action as AddAction;
use App\Core\Unit\UseCase\Add\Input as AddInput;
use App\Core\Unit\UseCase\Remove\Action as RemoveAction;
use App\Core\Unit\UseCase\Remove\Input as RemoveInput;
use App\Core\Unit\UseCase\Rename\Action as RenameAction;
use App\Core\Unit\UseCase\Rename\Input as RenameInput;
use App\Core\Unit\UseCase\Show\Action as ShowAction;
use App\Core\Unit\UseCase\Show\Input as ShowInput;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class Unit
 *
 * @package App\Http\Api
 */
class Unit extends Controller {

  /**
   * @param Request $request
   * @param AddAction $useCase
   *
   * @return JsonResponse|null
   * @throws \Exception
   */
  public function add(Request $request, AddAction $useCase): ?JsonResponse {
    $input = new AddInput($request->all());
    $addedEvent = $useCase->handle($input);

    $unit = $addedEvent->unit;
    $id = (string)$unit->id;
    $title = (string)$unit->title;

    return response()->json([
      'type' => 'success',
      'msg' => "Единица измерения '$title' добавлена.",
      'item' => new Item($id, $title),
    ], Response::HTTP_CREATED);
  }

  /**
   * @param ShowAction $useCase
   * @param string $id
   *
   * @return JsonResponse
   * @throws \Exception
   */
  public function show(ShowAction $useCase, string $id): JsonResponse {
    $input = new ShowInput(['id' => $id]);
    $item = $useCase->handle($input);

    return response()->json([
      'type' => 'success',
      'item' => $item,
    ]);
  }

  /**
   * @param Request $request
   * @param RenameAction $useCase
   * @param string $id
   *
   * @return JsonResponse
   * @throws \Exception
   */
  public function rename(Request $request, RenameAction $useCase, string $id): JsonResponse {
    $inputs = $request->all();
    $inputs['id'] = $id;
    $input = new RenameInput($inputs);
    $renamedEvent = $useCase->handle($input);

    $renamedUnit = $renamedEvent->changedUnit;
    $unchanged_title = (string)$renamedEvent->unchangedUnit->title;
    $change_title = (string)$renamedUnit->title;

    $msg = "Единица измерения '$unchanged_title' переименована в '$change_title'";

    return response()->json([
      'type' => 'success',
      'msg' => $msg,
      'unit' => $renamedUnit,
    ]);
  }


  /**
   * @param RemoveAction $useCase
   * @param string $id
   *
   * @return JsonResponse
   * @throws \Exception
   */
  public function remove(RemoveAction $useCase, string $id): JsonResponse {
    $input = new RemoveInput(['id' => $id]);
    $removedEvent = $useCase->handle($input);
    $unit = $removedEvent->unit;
    $title = (string)$unit->title;

    return response()->json(new Success("Единица измерения '$title' удалёна."));
  }
}
