<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.09.2019
 * Time: 12:21
 */


namespace App\Http\Api;

use App\Core\Tracking\UseCase\Add\Action as AddAction;
use App\Core\Tracking\UseCase\Add\Input as AddInput;
use App\Core\Tracking\UseCase\Finish\Action as FinishAction;
use App\Core\Tracking\UseCase\Finish\Input as FinishInput;
use App\Core\Tracking\UseCase\UpdateQty\Action as UpdateQtyAction;
use App\Core\Tracking\UseCase\UpdateQty\Input as UpdateQtyInput;
use App\Core\Tracking\UseCase\AddExpenseToTracking\Action as AddExpenseToTrackingAction;
use App\Core\Tracking\UseCase\AddExpenseToTracking\Input as AddExpenseToTrackingInput;
use App\Core\Tracking\UseCase\RemoveExpenseFromToTracking\Action as RemoveExpenseFromToTrackingAction;
use App\Core\Tracking\UseCase\RemoveExpenseFromToTracking\Input as RemoveExpenseFromToTrackingInput;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class Tracking extends Controller {

  /**
   * @param Request $request
   * @param AddAction $useCase
   *
   * @return JsonResponse
   * @throws Exception
   */
  public function add(Request $request, AddAction $useCase): JsonResponse {
    $input = new AddInput($request->all());
    $useCase->handle($input);

    $result = [
      'type' => 'success',
      'msg' => "Добавлен учет расходования.",
    ];

    return response()->json($result, Response::HTTP_CREATED);
  }

  /**
   * @param string $id
   * @param Request $request
   * @param FinishAction $useCase
   *
   * @return JsonResponse
   * @throws Exception
   */
  public function finish(string $id, Request $request, FinishAction $useCase): JsonResponse {
    $data = $request->all();
    $data['id'] = $id;
    $input = new FinishInput($data);
    $event = $useCase->handle($input);
    $product_title = $event->productItem->title;
    $result = [
      'type' => 'success',
      'msg' => "Учет расходования товара \"$product_title\" завершен.",
    ];

    return response()->json($result, Response::HTTP_OK);
  }

  /**
   * @param string $id
   * @param Request $request
   * @param UpdateQtyAction $useCase
   *
   * @return JsonResponse
   * @throws Exception
   */
  public function updateQty(string $id, Request $request, UpdateQtyAction $useCase): JsonResponse {
    $data = $request->all();
    $data['id'] = $id;
    $input = new UpdateQtyInput($data);
    $event = $useCase->handle($input);
    $product_title = $event->productItem->title;
    $old_qty = $event->oldQty->value();
    $new_qty = $event->track->quantity->value();
    $result = [
      'type' => 'success',
      'msg' => "Кол-во товара \"$product_title\" изменено с $old_qty на $new_qty.",
    ];

    return response()->json($result, Response::HTTP_OK);
  }

  /**
   * @param string $id
   * @param AddExpenseToTrackingAction $useCase
   *
   * @return JsonResponse
   * @throws Exception
   */
  public function addExpenseToTracking(string $id, AddExpenseToTrackingAction $useCase): JsonResponse {
    $input = new AddExpenseToTrackingInput(['id' => $id]);
    $useCase->handle($input);
    $result = [
      'type' => 'success',
      'msg' => 'Расход добавлен в расходы для отслеживания.',
    ];

    return response()->json($result, Response::HTTP_OK);
  }

  /**
   * @param string $id
   * @param RemoveExpenseFromToTrackingAction $useCase
   *
   * @return JsonResponse
   * @throws Exception
   */
  public function removeExpenseFromToTracking(string $id, RemoveExpenseFromToTrackingAction $useCase): JsonResponse {
    $input = new RemoveExpenseFromToTrackingInput(['id' => $id]);
    $useCase->handle($input);
    $result = [
      'type' => 'success',
      'msg' => 'Расход удален из расходов для отслеживания.',
    ];

    return response()->json($result, Response::HTTP_OK);
  }
}
