<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.09.2019
 * Time: 12:21
 */


namespace App\Http\Api;

use App\Core\Account\UseCase\ClearContractor\Action as ClearContractorAction;
use App\Core\Account\UseCase\ClearContractor\Input as ClearContractorInput;
use App\Core\Exception\FailedToSaveEntityException;
use App\Core\Account\IDataProvider as AccountDataProvider;
use App\Core\Account\IRepository as AccountRepository;
use App\Core\Account\UseCase\Add\Action as AddAction;
use App\Core\Account\UseCase\Add\Input as AddInput;
use App\Core\Account\UseCase\Find\Action as FindAction;
use App\Core\Account\UseCase\Remove\Action as RemoveAction;
use App\Core\Account\UseCase\Remove\Input as RemoveInput;
use App\Core\Account\UseCase\Show\Action as ShowAction;
use App\Core\Account\UseCase\Show\Input as ShowInput;
use App\Core\Account\UseCase\Rename\Action as RenameAction;
use App\Core\Account\UseCase\Rename\Input as RenameInput;
use App\Core\Account\UseCase\SetContractor\Action as SetContractorAction;
use App\Core\Account\UseCase\SetContractor\Input as SetContractorInput;
use App\Core\Account\UseCase\Find\Input as FindInput;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class Account
 *
 * @package App\Http\Api
 */
class Account extends Controller {

  public function __construct(
    private readonly AccountDataProvider $accountDataProvider
  ) {

  }

  /**
   * @param Request $request
   * @param AddAction $useCase
   *
   * @return JsonResponse|null
   * @throws FailedToSaveEntityException
   */
  public function add(Request $request, AddAction $useCase): ?JsonResponse {
    $input = new AddInput($request->all());
    $addedEvent = $useCase->handle($input);

    $accountItem = $this->accountDataProvider->manageItemById($addedEvent->account->id);

    return response()->json([
      'type' => 'success',
      'msg' => "Счет {$addedEvent->account->title} добавлен.",
      'item' => $accountItem,
    ], Response::HTTP_CREATED);
  }

  /**
   * @param ShowAction $useCase
   * @param string $id
   *
   * @return JsonResponse
   * @throws Exception
   */
  public function show(ShowAction $useCase, string $id): JsonResponse {
    $input = new ShowInput(['id' => $id]);
    $item = $useCase->handle($input);

    return response()->json([
      'type' => 'success',
      'item' => $item,
    ]);
  }

  /**
   * @param Request $request
   * @param AccountRepository $accountRepository
   * @param FindAction $useCase
   *
   * @return JsonResponse|null
   */
  public function find(Request $request, AccountRepository $accountRepository, FindAction $useCase): ?JsonResponse {
    $input = new FindInput($request->all(), $accountRepository);
    $items = $useCase->handle($input);

    return response()->json($items);
  }

  /**
   * @param Request $request
   * @param RenameAction $useCase
   * @param string $id
   *
   * @return JsonResponse
   * @throws FailedToSaveEntityException
   */
  public function rename(Request $request, RenameAction $useCase, string $id): JsonResponse {
    $inputs = $request->all();
    $inputs['id'] = $id;
    $input = new RenameInput($inputs);
    $renamedEvent = $useCase->handle($input);
    $renamedAccount = $renamedEvent->account;

    $accountItem = $this->accountDataProvider->get($renamedAccount->id);

    return response()->json([
      'type' => 'success',
      'msg' => 'Счет переименован.',
      'account' => $accountItem,
    ]);
  }

  public function setContractor(Request $request, SetContractorAction $useCase, string $id): ?JsonResponse {
    $inputs = $request->all();
    $inputs['id'] = $id;
    $input = new SetContractorInput($inputs);
    $event = $useCase->handle($input);
    $account = $event->account;

    $accountItem = $this->accountDataProvider->get($account->id);
    $currentContractor = $event->currentContractor;
    return response()->json([
      'type' => 'success',
      'msg' => "К счету \"$account->title\" привязан контрагент \"$currentContractor->title\".",
      'account' => $accountItem,
    ]);
  }

  public function clearContractor(Request $request, ClearContractorAction $useCase, string $id): ?JsonResponse {
    $inputs = ['id' => $id];
    $input = new ClearContractorInput($inputs);
    $event = $useCase->handle($input);
    $account = $event->account;

    $accountItem = $this->accountDataProvider->get($account->id);
    $prevContractor = $event->prevContractor;
    return response()->json([
      'type' => 'success',
      'msg' => "К счету \"$account->title\" больше не привязан контрагент \"$prevContractor->title\".",
      'account' => $accountItem,
    ]);
  }

  /**
   * @param RemoveAction $useCase
   * @param string $id
   *
   * @return JsonResponse|null
   * @throws Exception
   */
  public function remove(RemoveAction $useCase, string $id): ?JsonResponse {
    $input = new RemoveInput(['id' => $id]);
    $useCase->handle($input);

    return response()->json([
      'type' => 'success',
      'msg' => 'Счет удален.',
    ]);
  }

//  public function operations(string $id): JsonResponse {
//
//  }
}
