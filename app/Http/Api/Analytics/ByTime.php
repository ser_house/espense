<?php

namespace App\Http\Api\Analytics;

use App\Core\Analytics\ByTime\IByTimeDataProviderFactory;
use App\Core\Analytics\ByTime\Params;
use App\Core\Expense\UseCase\Find\Input as FindInput;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ByTime extends Controller {

  public function __construct(private readonly IByTimeDataProviderFactory $byTimeDataProviderFactory) {

  }

  /**
   * @inheritDoc
   */
  public function __invoke(Request $request): JsonResponse {
    $input = $request->all();
    if (empty($input) || empty($input['years'])) {
      $input['years'] = [FindInput::ANY_KEY];
    }
    $params = new Params($input);
    $dataProvider = $this->byTimeDataProviderFactory->get($params);
    $result = $dataProvider->get();

    return response()->json([
      'type' => 'success',
      'data' => [
        'series' => $result->series,
        'categories' => $result->categories,
      ],
    ]);
  }
}
