<?php

namespace App\Http\Api\Analytics;

use App\Core\Analytics\IDataProvider;
use App\Core\Category\CategoryId;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class ByCategory extends Controller {

  public function __construct(private readonly IDataProvider $dataProvider) {

  }

  /**
   * @inheritDoc
   */
  public function __invoke(string $category_id): JsonResponse {
    $data = $this->dataProvider->totalByCategoryChildren(new CategoryId($category_id));

    return response()->json([
      'type' => 'success',
      'data' => $data,
    ]);
  }
}
