<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.09.2019
 * Time: 12:21
 */


namespace App\Http\Api;


use App\Core\Contractor\IDataProvider as IContractorDataProvider;
use App\Core\Contractor\UseCase\Add\Action as AddAction;
use App\Core\Contractor\UseCase\Add\Input as AddInput;
use App\Core\Contractor\UseCase\Change\Action as ChangeAction;
use App\Core\Contractor\UseCase\Change\ChangedEvent;
use App\Core\Contractor\UseCase\Change\Input as ChangeInput;
use App\Core\Contractor\UseCase\Remove\Action as RemoveAction;
use App\Core\Contractor\UseCase\Remove\Input as RemoveInput;
use App\Core\Contractor\UseCase\Show\Action as ShowAction;
use App\Core\Contractor\UseCase\Show\Input as ShowInput;
use App\Core\Generic\ApiResponse\MissingSearchString;
use App\Core\Generic\ApiResponse\Success;
use App\Core\Generic\DTO\Item;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class Contractor
 *
 * @package App\Http\Api
 */
class Contractor extends Controller {

  public function __construct(private readonly IContractorDataProvider $contractorDataProvider) {

  }


  /**
   * @param Request $request
   * @param AddAction $useCase
   *
   * @return JsonResponse|null
   * @throws \Exception
   */
  public function add(Request $request, AddAction $useCase): ?JsonResponse {
    $input = new AddInput($request->all());
    $addedEvent = $useCase->handle($input);

    $contractor = $addedEvent->contractor;
    $id = (string)$contractor->id;
    $title = (string)$contractor->title;

    return response()->json([
      'type' => 'success',
      'msg' => "Контрагент '$title' добавлен.",
      'item' => new Item($id, $title),
    ], Response::HTTP_CREATED);
  }


  /**
   * @param ShowAction $useCase
   * @param string $id
   *
   * @return JsonResponse
   * @throws \Exception
   */
  public function show(ShowAction $useCase, string $id): JsonResponse {
    $input = new ShowInput(['id' => $id]);
    $item = $useCase->handle($input);

    return response()->json([
      'type' => 'success',
      'item' => $item,
    ]);
  }

  /**
   * @param Request $request
   *
   * @return JsonResponse
   */
  public function find(Request $request): JsonResponse {
    $query = $request->get('like');
    if (empty($query)) {
      return response()->json(new MissingSearchString(), Response::HTTP_BAD_REQUEST);
    }
    $found_items = $this->contractorDataProvider->searchLikeTitle($query);

    return response()->json($found_items);
  }


  /**
   * @param Request $request
   * @param ChangeAction $useCase
   * @param string $id
   *
   * @return JsonResponse
   * @throws \Exception
   */
  public function change(Request $request, ChangeAction $useCase, string $id): JsonResponse {
    $inputs = $request->all();
    $inputs['id'] = $id;
    $input = new ChangeInput($inputs);
    $changedEvent = $useCase->handle($input);

    return response()->json([
      'type' => 'success',
      'msg' => $this->changedContractorMsgToUser($changedEvent, '<br>'),
      'contractor' => $this->contractorDataProvider->manageItemById($changedEvent->changedContractor->id),
    ]);
  }


  /**
   * @param RemoveAction $useCase
   * @param string $id
   *
   * @return JsonResponse
   * @throws \Exception
   */
  public function remove(RemoveAction $useCase, string $id): JsonResponse {
    $input = new RemoveInput(['id' => $id]);
    $removedEvent = $useCase->handle($input);
    $contractor = $removedEvent->contractor;
    $title = (string)$contractor->title;

    return response()->json(new Success("Контрагент '$title' удалён."));
  }

  /**
   * @param ChangedEvent $changedEvent
   * @param string $glue
   *
   * @return string
   */
  private function changedContractorMsgToUser(ChangedEvent $changedEvent, string $glue = PHP_EOL): string {
    $msg_arr = ['Контрагент изменен:'];

    if ($changedEvent->isTitleChanged()) {
      $unchanged_title = (string)$changedEvent->unchangedContractor->title;
      $change_title = (string)$changedEvent->changedContractor->title;

      $msg_arr[] = "название изменено с '$unchanged_title' на '$change_title'";
    }

    if ($changedEvent->isUrlChanged()) {
      $unchanged_url = (string)$changedEvent->unchangedContractor->url;
      $changed_url = (string)$changedEvent->changedContractor->url;

      if (empty($unchanged_url)) {
        $msg_arr[] = "добавлена ссылка '$changed_url'";
      }
      elseif (empty($changed_url)) {
        $msg_arr[] = 'удалена ссылка';
      }
      else {
        $msg_arr[] = "ссылка изменена с '$unchanged_url' на '$changed_url'";
      }
    }

    if ($changedEvent->isNoteChanged()) {
      $unchanged_note = (string)$changedEvent->unchangedContractor->note;
      $changed_note = (string)$changedEvent->changedContractor->note;

      if (empty($unchanged_note)) {
        $msg_arr[] = "добавлено примечание '$changed_note'";
      }
      elseif (empty($changed_note)) {
        $msg_arr[] = 'удалено примечание';
      }
      else {
        $msg_arr[] = "примечание изменено с '$unchanged_note' на '$changed_note'";
      }
    }

    return implode($glue, $msg_arr);
  }
}
