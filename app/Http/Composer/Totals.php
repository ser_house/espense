<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.10.2019
 * Time: 19:33
 */


namespace App\Http\Composer;

use App\Core\Analytics\IDataProvider as AnalyticsDataProvider;
use App\Core\Account\IDataProvider as AccountsDataProvider;
use App\Core\Account\UseCase\Find\Input as FindInput;
use App\Core\Account\IRepository as AccountsRepository;
use Illuminate\View\View;

class Totals {

  public function __construct(
    private readonly AnalyticsDataProvider $analyticsDataProvider,
    private readonly AccountsDataProvider $accountsDataProvider,
    private readonly AccountsRepository $accountsRepository,
  ) {

  }

  /**
   * Bind data to the view.
   *
   * @param View $view
   *
   * @return void
   */
  public function compose(View $view): void {
    $input = new FindInput(['with_remain_only' => true], $this->accountsRepository);
    $items = $this->accountsDataProvider->itemsWithoutDefault($input);
    $view->with('accounts', $items);
  }
}
