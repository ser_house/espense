<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.08.2019
 * Time: 15:38
 */


namespace App\Application\Unit;


use App\Core\Unit\Title;
use App\Core\Unit\Unit as Entity;
use App\Core\Unit\UnitId as EntityId;
use App\Models\Unit as Model;
use Exception;

/**
 * Class Converter
 *
 * @package App\Application\Unit
 */
class Converter {

  /**
   * @param Entity $entity
   *
   * @return Model
   */
  public function domainToModel(Entity $entity): Model {
    $model = new Model();
    $model->exists = true;
    $model->id = (string)$entity->id;
    $model->title = (string)$entity->title;

    return $model;
  }

  /**
   * @param Model $model
   *
   * @return Entity
   * @throws Exception
   */
  public function modelToDomain(Model $model): Entity {
    $id = new EntityId($model->id);
    $title = new Title($model->title);

    return new Entity($id, $title);
  }

  /**
   * @param iterable $entities
   *
   * @return Model[]
   */
  public function domainsToModels(iterable $entities): array {
    $models = [];

    /** @var Entity $entity */
    foreach ($entities as $entity) {
      $id = (string)$entity->id;
      $models[$id] = $this->domainToModel($entity);
    }

    return $models;
  }

  /**
   * @param iterable $models
   *
   * @return Entity[]
   * @throws Exception
   */
  public function modelsToDomains(iterable $models): array {
    $entities = [];

    foreach ($models as $model) {
      $entities[$model->id] = $this->modelToDomain($model);
    }

    return $entities;
  }
}
