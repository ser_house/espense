<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.04.2021
 * Time: 8:49
 */


namespace App\Application\Unit;


use App\Core\Exception\NotFoundEntityException;
use App\Core\Generic\DTO\Item;
use App\Core\Unit\DTO\ManageItem;
use App\Core\Unit\IDataProvider;
use App\Core\Unit\UnitId;
use DB;

class DataProvider implements IDataProvider {
  /**
   * @inheritDoc
   */
  public function all(): array {
    $data = DB::select('SELECT * FROM unit ORDER BY title');
    $items = [];
    foreach ($data as $datum) {
      $items[] = new Item($datum->id, $datum->title);
    }

    return $items;
  }

  /**
   * @inheritDoc
   */
  public function allAsOptions(): array {
    $units_data = $this->all();
    $units = [];
    foreach ($units_data as $unit) {
      $units[$unit->id] = $unit->title;
    }

    return $units;
  }


  /**
   * @inheritDoc
   */
  public function manageItems(): array {
    $sql = 'SELECT 
				u.*,
				COUNT(e.id) AS expenses_count
			FROM unit AS u
			LEFT JOIN expense AS e ON e.unit_id = u.id
			GROUP BY u.id
			ORDER BY u.title';

    $data = DB::select($sql);

    $items = [];
    foreach ($data as $datum) {
      $items[] = new ManageItem($datum->id, $datum->title, $datum->expenses_count);
    }

    return $items;
  }

  /**
   * @inheritDoc
   */
  public function getById(UnitId $unitId): Item {
    $unit_id_str = (string)$unitId;
    $data = DB::select("SELECT * FROM unit WHERE id = '$unit_id_str'");
    if (empty($data)) {
      throw new NotFoundEntityException('Ед. изм. не найдена.');
    }

    return new Item($data[0]->id, $data[0]->title);
  }
}
