<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 16.09.2019
 * Time: 23:31
 */


namespace App\Application\Unit;


use App\Core\Unit\Unit;
use App\Core\Unit\UnitId;
use App\Core\Unit\IRepository;
use App\Core\Unit\Title;
use App\Models\Unit as Model;

class Repository implements IRepository {

  public function __construct(private readonly Converter $converter) {

  }

  /**
   * @inheritDoc
   */
  public function nextId(): UnitId {
    return new UnitId();
  }

  /**
   * @inheritDoc
   */
  public function add(Unit $unit) {
    $model = $this->converter->domainToModel($unit);
    $model->exists = false;
    $model->save();
  }

  /**
   * @inheritDoc
   */
  public function update(Unit $unit): void {
    $model = $this->converter->domainToModel($unit);
    $model->exists = true;
    $model->save();
  }

  /**
   * @inheritDoc
   */
  public function remove(UnitId $unitId): void {
    $model = new Model();
    $model->exists = true;
    $model->id = (string)$unitId;
    $model->delete();
  }


  /**
   * @inheritDoc
   */
  public function findAll(): array {
    /** @var Model[] $models */
    $models = Model::orderBy('title', 'ASC')->get();
    if (empty($models)) {
      return [];
    }

    return $this->converter->modelsToDomains($models);
  }

  /**
   * @inheritDoc
   */
  public function findById(UnitId $unitId): ?Unit {
    $model = Model::find((string)$unitId);
    if (empty($model)) {
      return null;
    }

    return $this->converter->modelToDomain($model);
  }

  /**
   * @inheritDoc
   */
  public function findByTitle(Title $title): ?Unit {
    $model = Model::where('title', '=', (string)$title)->first();
    if (empty($model)) {
      return null;
    }

    return $this->converter->modelToDomain($model);
  }
}
