<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.04.2021
 * Time: 17:08
 */


namespace App\Application\Product\Listener;


use App\Application\Product\Event\MovedToCategory as AppProductMovedToCategoryEvent;
use App\Core\Analytics\IDataProvider;
use App\Core\Service\ICacheFlusher;
use Exception;

class MovedToCategory {

  public function __construct(protected readonly ICacheFlusher $cacheFlusher) {

  }

  /**
   * @param AppProductMovedToCategoryEvent $event
   *
   * @throws Exception
   */
  public function handle(AppProductMovedToCategoryEvent $event): void {
    $this->cacheFlusher->flush(IDataProvider::TOTALS);
  }
}
