<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 10:50
 */


namespace App\Application\Product;

use App\Core\Category\CategoryId;
use App\Core\Exception\NotFoundEntityException;
use App\Core\Product\IRepository;
use App\Core\Product\Product;
use App\Core\Product\ProductId;
use App\Core\Title;
use App\Models\Product as Model;
use DB;


class Repository implements IRepository {

  public function __construct(private readonly Converter $converter) {

  }


  /**
   * @inheritDoc
   */
  public function nextId(): ProductId {
    return new ProductId();
  }

  /**
   * @inheritDoc
   */
  public function add(Product $product): void {
    $model = $this->converter->domainToModel($product);
    $model->exists = false;

    $model->save();
  }

  /**
   * @inheritDoc
   */
  public function update(Product $product): void {
    $model = $this->converter->domainToModel($product);
    $model->exists = true;

    $model->save();
  }

  /**
   * @inheritDoc
   */
  public function updateCategory(ProductId $productId, CategoryId $newCategoryId): void {
    $model = Model::find((string)$productId);
    $model->category_id = (string)$newCategoryId;
    $model->save();
  }

  /**
   * @inheritDoc
   */
  public function remove(ProductId $productId): void {
    $model = new Model();
    $model->exists = true;
    $model->id = (string)$productId;
    $model->delete();
  }

  /**
   * @inheritDoc
   */
  public function findAll(): array {
    /** @var Model[] $models */
    $models = Model::orderBy('title', 'ASC')->get();
    if (empty($models)) {
      return [];
    }

    return $this->converter->modelsToDomains($models);
  }

  /**
   * @inheritDoc
   */
  public function get(ProductId $productId): Product {
    $model = Model::find((string)$productId);
    if (empty($model)) {
      throw new NotFoundEntityException("Товар с id \{$productId\} не найден.");
    }

    return $this->converter->modelToDomain($model);
  }

  /**
   * @inheritDoc
   */
  public function findById(ProductId $productId): ?Product {
    $model = Model::find((string)$productId);
    if (empty($model)) {
      return null;
    }

    return $this->converter->modelToDomain($model);
  }

  /**
   * @inheritDoc
   */
  public function findByTitle(Title $title): ?Product {
    $model = Model::where('title', '=', (string)$title)->first();
    if (empty($model)) {
      return null;
    }

    return $this->converter->modelToDomain($model);
  }

  /**
   * @inheritDoc
   */
  public function findByTitleAndCategory(Title $title, CategoryId $categoryId): ?Product {
    $model = Model::where([
      'title' => (string)$title,
      'category_id' => (string)$categoryId,
    ])->first();
    if (empty($model)) {
      return null;
    }

    return $this->converter->modelToDomain($model);
  }


}
