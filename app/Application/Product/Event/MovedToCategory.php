<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.04.2021
 * Time: 17:08
 */


namespace App\Application\Product\Event;


use App\Models\Category;
use App\Models\Product;
use Illuminate\Queue\SerializesModels;

class MovedToCategory {
  use SerializesModels;

  public function __construct(
    public readonly Product $product,
    public readonly Category $category,
    public readonly Category $oldCategory
  ) {

  }
}
