<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.08.2019
 * Time: 15:38
 */


namespace App\Application\Product;


use App\Core\Category\CategoryId;
use App\Core\Category\DTO\Item as CategoryItem;
use App\Core\Generic\DTO\Item;
use App\Core\Product\DTO\Item as ProductItem;
use App\Core\Product\Product as Entity;
use App\Core\Product\ProductId as EntityId;
use App\Core\Title;
use App\Models\Product as Model;
use Exception;
use stdClass;

class Converter {

  /**
   * @param Entity $entity
   *
   * @return Model
   */
  public function domainToModel(Entity $entity): Model {
    $model = new Model();
    $model->exists = true;
    $model->id = (string)$entity->id;
    $model->title = (string)$entity->title;
    $categoryId = $entity->categoryId;
    $model->category_id = $categoryId ? (string)$entity->categoryId : null;

    return $model;
  }

  /**
   * @param Model $model
   *
   * @return Entity
   * @throws Exception
   */
  public function modelToDomain(Model $model): Entity {
    $id = new EntityId($model->id);
    $title = new Title($model->title);
    $categoryId = $model->category_id ? new CategoryId($model->category_id) : null;

    return new Entity($id, $title, $categoryId);
  }

  /**
   * @param iterable $entities
   *
   * @return Model[]
   */
  public function domainsToModels(iterable $entities): array {
    $models = [];

    /** @var Entity $entity */
    foreach ($entities as $entity) {
      $id = (string)$entity->id;
      $models[$id] = $this->domainToModel($entity);
    }

    return $models;
  }

  /**
   * @param iterable $models
   *
   * @return Entity[]
   * @throws Exception
   */
  public function modelsToDomains(iterable $models): array {
    $entities = [];

    foreach ($models as $model) {
      $entities[$model->id] = $this->modelToDomain($model);
    }

    return $entities;
  }

  /**
   * @param stdClass $dbItem
   *
   * @return ProductItem
   * @throws Exception
   */
  public function dbItemToDto(stdClass $dbItem): ProductItem {
    $parentCategoryItem = null;
    if (!empty($dbItem->category_parent_id)) {
      $parentCategoryItem = new Item($dbItem->category_parent_id, $dbItem->category_parent_title);
    }
    $category = new CategoryItem($dbItem->category_id, $dbItem->category_title, $parentCategoryItem);
    return new ProductItem($dbItem->id, $dbItem->title, $category);
  }

  /**
   * @param iterable $db_items
   *
   * @return ProductItem[]
   * @throws Exception
   */
  public function dbItemsToDto(iterable $db_items): array {
    $items = [];
    foreach ($db_items as $db_item) {
      $items[] = $this->dbItemToDto($db_item);
    }

    return $items;
  }
}
