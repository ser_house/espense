<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 20.11.2019
 * Time: 3:55
 */


namespace App\Application\Product;

use App\Core\Category\CategoryId;
use App\Core\Product\DTO\Item as ProductItem;
use App\Core\Product\DTO\ManageItem as ManageProductItem;
use App\Core\Product\Exceptions\ProductNotFoundException;
use App\Core\Product\IDataProvider;
use App\Core\Product\ProductId;
use App\Core\Product\UseCase\Find\Input;
use DB;


class DataProvider implements IDataProvider {

  public function __construct(private readonly Converter $converter) {

  }

  /**
   * @inheritDoc
   */
  public function findById(ProductId $productId): ?ProductItem {
    $sql = 'SELECT 
      p.id AS id, 
      p.title AS title, 
      c.id AS category_id, 
      c.title AS category_title,
      cp.id AS category_parent_id,
      cp.title AS category_parent_title
		FROM product AS p
		INNER JOIN category AS c ON c.id = p.category_id
		LEFT JOIN category AS cp ON cp.id = c.parent_id
		WHERE p.id = :id';

    $data = DB::select($sql, ['id' => (string)$productId]);

    if (empty($data)) {
      return null;
    }

    $datum = reset($data);

    return $this->converter->dbItemToDto($datum);
  }

  /**
   * @inheritDoc
   */
  public function getById(ProductId $productId): ProductItem {
    $item = $this->findById($productId);
    if (null === $item) {
      throw new ProductNotFoundException();
    }
    return $item;
  }

  /**
   * @inheritDoc
   */
  public function search(Input $input): array {

    $query = DB::table('product')
      ->select('product.id',
        'product.title',
        'product.category_id',
        'category.title AS category_title',
        'category_parent.id AS category_parent_id',
        'category_parent.title AS category_parent_title'
      )
    ;

    if ($input->title) {
      $query = $query->where('product.title', (string)$input->title);
    }
    elseif ($input->like) {
      $query = $query->where('product.title', 'ILIKE', "%$input->like%");
    }

    if ($input->categoryTitle) {
      $query = $query->where('category.title', (string)$input->categoryTitle);
    }

    $query = $query->join('category', 'category.id', '=', 'product.category_id');
    $query = $query->leftJoin('category AS category_parent', 'category_parent.id', '=', 'category.parent_id');

    $db_items = $query->orderBy('title')->get();

    $items = [];
    foreach ($db_items as $db_item) {
      $item = [
        'id' => $db_item->id,
        'title' => $db_item->title,
        'category' => [
          'id' => $db_item->category_id,
          'title' => $db_item->category_title,
        ],
      ];
      if ($db_item->category_parent_id) {
        $item['category']['parent'] = [
          'id' => $db_item->category_parent_id,
          'title' => $db_item->category_parent_title,
        ];
      }
      $items[] = $item;
    }
    return $items;
  }

  /**
   * @inheritDoc
   */
  public function countByCategoryId(CategoryId $categoryId): int {
    $sql = 'SELECT COUNT(*) AS total
		FROM product
		WHERE category_id = :category_id';

    $data = DB::select($sql, ['category_id' => (string)$categoryId]);

    if (empty($data)) {
      return 0;
    }

    return reset($data)->total;
  }


  /**
   * @inheritDoc
   */
  public function allManageItems(): array {
    $sql = 'SELECT 
			product.id, 
			product.title, 
			product.category_id,
			COUNT(expense.id) AS expenses_count
		FROM product
		LEFT JOIN expense ON expense.product_id = product.id
		GROUP BY product.id
		ORDER BY product.title ASC';
    $db_items = DB::select($sql);

    $items = [];
    foreach ($db_items as $db_item) {
      $items[] = new ManageProductItem($db_item->id, $db_item->title, $db_item->expenses_count, $db_item->category_id);
    }

    return $items;
  }

  /**
   * @inheritDoc
   */
  public function findTitlesByCategoryId(CategoryId $categoryId): array {
    $sql = 'SELECT title
		FROM product
		WHERE category_id = :category_id
		ORDER BY title';

    $data = DB::select($sql, ['category_id' => (string)$categoryId]);

    if (empty($data)) {
      return [];
    }

    return array_column($data, 'title');
  }
}
