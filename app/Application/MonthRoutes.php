<?php

namespace App\Application;

use App\Core\Analytics\Calendar\Month\Month as CalendarMonth;

class MonthRoutes {

  public readonly string $prev;
  public readonly string $next;

  public function __construct(private readonly string $route, int $year, int $month) {
    $calendarMonth = new CalendarMonth($year, $month);
    $prevMonth = $calendarMonth->prevMonth();
    $nextMonth = $calendarMonth->nextMonth();

    if ($calendarMonth->canGoToPrev()) {
      $this->prev = route($this->route, ['month' => "$prevMonth->year-$prevMonth->month"]);
    }
    else {
      $this->prev = '';
    }

    if ($calendarMonth->canGoToNext()) {
      $this->next = route($this->route, ['month' => "$nextMonth->year-$nextMonth->month"]);
    }
    else {
      $this->next = '';
    }
  }
}
