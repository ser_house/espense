<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.08.2019
 * Time: 15:38
 */


namespace App\Application\Contractor;


use App\Core\Contractor\Contractor as Entity;
use App\Core\Contractor\ContractorId as EntityId;
use App\Core\Contractor\Url;
use App\Core\Generic\Note;
use App\Core\Title;
use App\Models\Contractor as Model;
use Exception;

/**
 * Class Converter
 *
 * @package App\Application\Contractor
 */
class Converter {

  /**
   * @param Entity $entity
   *
   * @return Model
   */
  public function domainToModel(Entity $entity): Model {
    $model = new Model();
    $model->exists = true;
    $model->id = (string)$entity->id;
    $model->title = (string)$entity->title;
    $model->url = $entity->url ? (string)$entity->url : null;
    $model->note = $entity->note ? (string)$entity->note : '';

    return $model;
  }

  /**
   * @param Model $model
   *
   * @return Entity
   * @throws Exception
   */
  public function modelToDomain(Model $model): Entity {
    $id = new EntityId($model->id);
    $title = new Title($model->title);
    $url = null;
    if ($model->url) {
      $url = new Url($model->url);
    }
    $note = null;
    if ($model->note) {
      $note = new Note($model->note);
    }

    return new Entity($id, $title, $url, $note);
  }

  /**
   * @param iterable $entities
   *
   * @return Model[]
   */
  public function domainsToModels(iterable $entities): array {
    $models = [];

    /** @var Entity $entity */
    foreach ($entities as $entity) {
      $id = (string)$entity->id;
      $models[$id] = $this->domainToModel($entity);
    }

    return $models;
  }

  /**
   * @param iterable $models
   *
   * @return Entity[]
   * @throws Exception
   */
  public function modelsToDomains(iterable $models): array {
    $entities = [];

    foreach ($models as $model) {
      $entities[$model->id] = $this->modelToDomain($model);
    }

    return $entities;
  }
}
