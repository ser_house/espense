<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 16.09.2019
 * Time: 23:31
 */


namespace App\Application\Contractor;


use App\Core\Contractor\Contractor;
use App\Core\Contractor\ContractorId;
use App\Core\Contractor\IRepository;
use App\Core\Exception\NotFoundEntityException;
use App\Core\Title;
use App\Models\Contractor as Model;

class Repository implements IRepository {

  public function __construct(private readonly Converter $converter) {

  }

  /**
   * @inheritDoc
   */
  public function nextId(): ContractorId {
    return new ContractorId();
  }

  /**
   * @inheritDoc
   */
  public function add(Contractor $contractor) {
    $model = $this->converter->domainToModel($contractor);
    $model->exists = false;
    $model->save();
  }

  /**
   * @inheritDoc
   */
  public function update(Contractor $contractor): void {
    $model = $this->converter->domainToModel($contractor);
    $model->exists = true;
    $model->save();
  }

  /**
   * @inheritDoc
   */
  public function remove(ContractorId $contractorId): void {
    $model = new Model();
    $model->exists = true;
    $model->id = (string)$contractorId;
    $model->delete();
  }


  /**
   * @inheritDoc
   */
  public function findAll(): array {
    /** @var Model[] $models */
    $models = Model::orderBy('title', 'ASC')->get();
    if (empty($models)) {
      return [];
    }

    return $this->converter->modelsToDomains($models);
  }

  /**
   * @inheritDoc
   */
  public function findById(ContractorId $contractorId): ?Contractor {
    $model = Model::find((string)$contractorId);
    if (empty($model)) {
      return null;
    }

    return $this->converter->modelToDomain($model);
  }

  /**
   * @inheritDoc
   */
  public function get(ContractorId $contractorId): Contractor {
    $contractor = $this->findById($contractorId);
    if (null === $contractor) {
      throw new NotFoundEntityException("Контрагент с id [$contractorId] не найден.");
    }
    return $contractor;
  }

  /**
   * @inheritDoc
   */
  public function findByTitle(Title $title): ?Contractor {
    $model = Model::where('title', '=', (string)$title)->first();
    if (empty($model)) {
      return null;
    }

    return $this->converter->modelToDomain($model);
  }
}
