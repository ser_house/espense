<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.12.2019
 * Time: 9:32
 */


namespace App\Application\Contractor;


use App\Core\Contractor\ContractorId;
use App\Core\Contractor\DTO\ManageItem;
use App\Core\Contractor\IDataProvider;
use App\Core\Contractor\UseCase\Show\Item;
use App\Core\Exception\NotFoundEntityException;
use DB;

class DataProvider implements IDataProvider {
  /**
   * @inheritDoc
   */
  public function findById(ContractorId $contractorId): ?Item {
    $sql = 'SELECT *
			FROM contractor AS c
      WHERE id = :id';

    $data = DB::select($sql, ['id' => (string)$contractorId]);

    if (empty($data)) {
      return null;
    }
    $datum = reset($data);

    return new Item($datum->id, $datum->title, (string)$datum->url, (string)$datum->note);
  }

  /**
   * @inheritDoc
   */
  public function manageItems(): array {
    $sql = 'SELECT 
				c.*,
				COUNT(e.id) AS expenses_count
			FROM contractor AS c
			LEFT JOIN expense AS e ON e.contractor_id = c.id
			GROUP BY c.id
			ORDER BY c.title';

    $data = DB::select($sql);

    $items = [];
    foreach ($data as $datum) {
      $items[] = new ManageItem($datum->id, $datum->title, $datum->url, $datum->note, $datum->expenses_count);
    }

    return $items;
  }

  /**
   * @inheritDoc
   */
  public function manageItemById(ContractorId $contractorId): ManageItem {
    $sql = 'SELECT 
				c.*,
				COUNT(e.id) AS expenses_count
			FROM contractor AS c
			LEFT JOIN expense AS e ON e.contractor_id = c.id
			WHERE c.id = :id
			GROUP BY c.id';

    $data = DB::select($sql, ['id' => $contractorId]);

    if (empty($data)) {
      throw new NotFoundEntityException("Контрагент с id [$contractorId] не найден.");
    }

    $datum = reset($data);

    return new ManageItem($datum->id, $datum->title, $datum->url, $datum->note, $datum->expenses_count);
  }

  /**
   * @inheritDoc
   */
  public function searchLikeTitle(string $query): array {
    $db_items = DB::table('contractor')
      ->select('*')
      ->where('title', 'ILIKE', "%$query%")
      ->orderBy('title')
      ->get();

    $items = [];
    foreach ($db_items as $db_item) {
      $items[] = new Item($db_item->id, $db_item->title, (string)$db_item->url, (string)$db_item->note);
    }

    return $items;
  }

}
