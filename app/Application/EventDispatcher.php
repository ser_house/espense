<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.11.2019
 * Time: 12:41
 */


namespace App\Application;


use App\Application\Category\Converter as CategoryConverter;
use App\Application\Category\Event\MovedToCategory as AppCategoryMovedToCategoryEvent;
use App\Application\Expense\Converter as ExpenseConverter;
use App\Application\Expense\Event\Added as AppExpenseAddedEvent;
use App\Application\Expense\Event\Removed as AppExpenseRemovedEvent;
use App\Application\Expense\Event\Updated as AppExpenseUpdatedEvent;
use App\Application\Product\Converter as ProductConverter;
use App\Application\Product\Event\MovedToCategory as AppProductMovedToCategoryEvent;
use App\Core\Category\UseCase\MoveToCategory\MovedToCategoryEvent as CoreCategoryMovedToCategoryEvent;
use App\Core\Expense\UseCase\Add\AddedEvent as CoreExpenseAddedEvent;
use App\Core\Expense\UseCase\Remove\RemovedEvent as CoreExpenseRemovedEvent;
use App\Core\Expense\UseCase\Update\UpdatedEvent as CoreExpenseUpdatedEvent;
use App\Core\IEvent;
use App\Core\IEventDispatcher;
use App\Core\Product\UseCase\MoveToCategory\MovedToCategoryEvent as CoreProductMovedToCategoryEvent;


/**
 * Class EventDispatcher.
 *
 * @package App\Application
 */
class EventDispatcher implements IEventDispatcher {

  public function __construct(
    private readonly ExpenseConverter $expenseConverter,
    private readonly ProductConverter $productConverter,
    private readonly CategoryConverter $categoryConverter,
  ) {

  }


  /**
   * @inheritDoc
   */
  public function dispatch(IEvent $event): void {
    switch (true) {
      case $event instanceof CoreExpenseAddedEvent:
        $expenseModel = $this->expenseConverter->domainToModel($event->expense);
        event(new AppExpenseAddedEvent($expenseModel, $event->prevFavoriteExpenseId));
        break;

      case $event instanceof CoreExpenseUpdatedEvent:
        $originalExpenseModel = $this->expenseConverter->domainToModel($event->originalExpense);
        $updatedExpenseModel = $this->expenseConverter->domainToModel($event->updatedExpense);
        event(new AppExpenseUpdatedEvent($originalExpenseModel, $updatedExpenseModel));
        break;

      case $event instanceof CoreExpenseRemovedEvent:
        $expenseModel = $this->expenseConverter->domainToModel($event->expense);
        event(new AppExpenseRemovedEvent($expenseModel));
        break;

      case $event instanceof CoreProductMovedToCategoryEvent:
        $productModel = $this->productConverter->domainToModel($event->product);
        $categoryModel = $this->categoryConverter->domainToModel($event->category);
        $oldCategoryModel = $this->categoryConverter->domainToModel($event->oldCategory);
        event(new AppProductMovedToCategoryEvent($productModel, $categoryModel, $oldCategoryModel));
        break;

      case $event instanceof CoreCategoryMovedToCategoryEvent:
        $categoryModel = $this->categoryConverter->domainToModel($event->category);
        $oldCategoryModel = null;
        if ($event->oldCategory) {
          $oldCategoryModel = $this->categoryConverter->domainToModel($event->oldCategory);
        }
        $newCategoryModel = null;
        if ($event->newCategory) {
          $newCategoryModel = $this->categoryConverter->domainToModel($event->newCategory);
        }

        event(new AppCategoryMovedToCategoryEvent($categoryModel, $oldCategoryModel, $newCategoryModel));
        break;
    }
  }
}
