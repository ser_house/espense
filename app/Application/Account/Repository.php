<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 10:50
 */


namespace App\Application\Account;

use App\Core\Account\Account;
use App\Core\Account\AccountId;
use App\Core\Account\IRepository;
use App\Core\Contractor\ContractorId;
use App\Core\Exception\NotFoundEntityException;
use App\Core\Title;
use App\Models\Account as Model;


class Repository implements IRepository {

  public function __construct(private readonly Converter $converter) {

  }


  /**
   * @inheritDoc
   */
  public function nextId(): AccountId {
    return new AccountId();
  }

  /**
   * @inheritDoc
   */
  public function add(Account $account): void {
    $model = $this->converter->domainToModel($account);
    $model->exists = false;
    $model->save();
  }

  /**
   * @inheritDoc
   */
  public function get(AccountId $accountId): Account {
    $account = $this->findById($accountId);
    if (null === $account) {
      throw new NotFoundEntityException("Счет с id [$accountId] не найден.");
    }

    return $account;
  }

  /**
   * @inheritDoc
   */
  public function update(Account $account): void {
    $model = $this->converter->domainToModel($account);
    $model->exists = true;
    $model->save();
  }

  /**
   * @inheritDoc
   */
  public function setContractorId(AccountId $accountId, ContractorId $contractorId): void {
    $account = $this->get($accountId);
    $model = $this->converter->domainToModel($account);
    $model->exists = true;
    $model->contractor_id = (string)$contractorId;
    $model->save();
  }

  /**
   * @inheritDoc
   */
  public function clearContractorId(AccountId $accountId): void {
    $account = $this->get($accountId);
    $model = $this->converter->domainToModel($account);
    $model->exists = true;
    $model->contractor_id = null;
    $model->save();
  }

  /**
   * @inheritDoc
   */
  public function remove(AccountId $accountId): void {
    $model = new Model();
    $model->exists = true;
    $model->id = (string)$accountId;
    $model->delete();
  }

  /**
   * @inheritDoc
   */
  public function all(): array {
    /** @var Model[] $models */
    $models = Model::orderBy('title', 'ASC')->get();
    if (empty($models)) {
      return [];
    }

    return $this->converter->modelsToDomains($models);
  }

  /**
   * @inheritDoc
   */
  public function findById(AccountId $accountId): ?Account {
    $model = Model::find((string)$accountId);
    if (empty($model)) {
      return null;
    }

    return $this->converter->modelToDomain($model);
  }

  /**
   * @inheritDoc
   */
  public function findByTitle(Title $title): ?Account {
    $model = Model::where('title', '=', (string)$title)->first();
    if (empty($model)) {
      return null;
    }

    return $this->converter->modelToDomain($model);
  }
}
