<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.08.2019
 * Time: 15:38
 */


namespace App\Application\Account;


use App\Core\Account\Account as Entity;
use App\Core\Account\AccountId as EntityId;
use App\Core\Contractor\ContractorId;
use App\Core\Title;
use App\Models\Account as Model;
use Exception;


/**
 * Class Converter
 *
 * @package App\Application\Account
 */
class Converter {

  /**
   * @param Entity $entity
   *
   * @return Model
   */
  public function domainToModel(Entity $entity): Model {
    $model = new Model();
    $model->exists = true;
    $model->id = (string)$entity->id;
    $model->title = (string)$entity->title;
    $contractorId = $entity->contractorId;
    $model->contractor_id = $contractorId ? (string)$contractorId : null;

    return $model;
  }

  /**
   * @param Model $model
   *
   * @return Entity
   * @throws Exception
   */
  public function modelToDomain(Model $model): Entity {
    $id = new EntityId($model->id);
    $title = new Title($model->title);
    $contractorId = $model->contractor_id ? new ContractorId($model->contractor_id) : null;

    return new Entity($id, $title, $contractorId);
  }

  /**
   * @param iterable $entities
   *
   * @return Model[]
   */
  public function domainsToModels(iterable $entities): array {
    $models = [];

    foreach ($entities as $entity) {
      $models[] = $this->domainToModel($entity);
    }

    return $models;
  }

  /**
   * @param iterable $models
   *
   * @return Entity[]
   * @throws Exception
   */
  public function modelsToDomains(iterable $models): array {
    $entities = [];

    foreach ($models as $model) {
      $entities[] = $this->modelToDomain($model);
    }

    return $entities;
  }
}
