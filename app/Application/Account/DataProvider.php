<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.12.2019
 * Time: 9:32
 */


namespace App\Application\Account;

use App\Core\Account\AccountId;
use App\Core\Account\AccountOperation;
use App\Core\Account\AccountItem;
use App\Core\Account\DefaultSrcAccount;
use App\Core\Account\IDataProvider;
use App\Core\Account\OpType;
use App\Core\Account\UseCase\Manage\ManageItem;
use App\Core\Amount;
use App\Core\Contractor\ContractorId;
use App\Core\Contractor\IDataProvider as ContractorDataProvider;
use App\Core\Account\UseCase\Find\Input as FindInput;
use App\Core\Exception\NotFoundEntityException;
use App\Core\Generic\DTO\Item;
use App\Core\Title;
use DateTimeImmutable;
use DB;


class DataProvider implements IDataProvider {
  public function __construct(
    private readonly ContractorDataProvider $contractorDataProvider,
  ) {
  }

  /**
   * @inheritDoc
   */
  public function findLikeTitle(string $like): array {
    $db_items = DB::table('account')
      ->select('id', 'title')
      ->where('title', 'ILIKE', "%$like%")
      ->orderBy('title')
      ->get()
    ;

    $items = [];
    foreach ($db_items as $db_item) {
      $items[] = new Item($db_item->id, $db_item->title);
    }

    return $items;
  }

  /**
   * @inheritDoc
   */
  public function findByTitle(string $title): array {
    $db_items = DB::table('account')
      ->select('id', 'title')
      ->where('title', $title)
      ->orderBy('title')
      ->get()
    ;

    $items = [];
    foreach ($db_items as $db_item) {
      $items[] = new Item($db_item->id, $db_item->title);
    }

    return $items;
  }

  /**
   * @inheritDoc
   */
  public function findById(AccountId $accountId): ?Item {
    $db_item = DB::table('account')
      ->select('id', 'title')
      ->where('id', (string)$accountId)
      ->first()
    ;

    if ($db_item) {
      return new Item($db_item->id, $db_item->title);
    }

    return null;
  }

  /**
   * @inheritDoc
   */
  public function get(AccountId $accountId): Item {
    $item = $this->findById($accountId);
    if (null === $item) {
      throw new NotFoundEntityException("Запись с id [$accountId] не найдена.");
    }

    return $item;
  }

  /**
   * @inheritDoc
   */
  public function items(): array {
    $sql = "SELECT id, title FROM account ORDER BY title";

    $data = DB::select($sql);
    $items = [];
    foreach ($data as $datum) {
      $items[] = new Item($datum->id, $datum->title);
    }
    return $items;
  }

  /**
   * @inheritDoc
   */
  public function itemsWithRemain(): array {
    $defaultAccount = new DefaultSrcAccount();

    $id = $defaultAccount->id;
    $sql = "
    SELECT
      a.id,
      a.title,
      a.contractor_id,
      c.title AS contractor_title,
      COALESCE(t_incoming.amount, 0) - COALESCE(t_outcoming.amount, 0) - COALESCE(expense.amount, 0) AS remain
    FROM account AS a
    LEFT JOIN (SELECT
                  t.account_target_id AS account_id,
                  SUM(t.amount) AS amount
                FROM transaction AS t
                WHERE t.account_target_id != '$id'
                GROUP BY t.account_target_id) AS t_incoming ON t_incoming.account_id = a.id
    LEFT JOIN (SELECT
                 t.account_src_id AS account_id,
                 SUM(t.amount) AS amount
               FROM transaction AS t
               WHERE t.account_src_id != '$id'
               GROUP BY t.account_src_id) AS t_outcoming ON t_outcoming.account_id = a.id
    LEFT JOIN (SELECT
                 e.account_id,
                 SUM(e.amount) AS amount
               FROM expense AS e
               WHERE e.account_id != '$id'
               GROUP BY e.account_id) AS expense ON expense.account_id = a.id
    LEFT JOIN contractor c ON c.id = a.contractor_id
    WHERE a.id != '$id' AND COALESCE(t_incoming.amount, 0) - COALESCE(t_outcoming.amount, 0) - COALESCE(expense.amount, 0) > 0
    ";

    $data = DB::select($sql);
    $items = [
      new AccountItem($defaultAccount->id, $defaultAccount->title, null, null),
    ];
    foreach ($data as $datum) {
      $contractor = null;
      if ($datum->contractor_id) {
        $contractor = new Item($datum->contractor_id, $datum->contractor_title);
      }
      $items[] = new AccountItem($datum->id, $datum->title, new Amount($datum->remain), $contractor);
    }
    return $items;
  }

  /**
   * @inheritDoc
   */
  public function itemsWithoutDefault(FindInput $input): array {
    $defaultAccount = new DefaultSrcAccount();

    $where = [
      'a.id != :id',
    ];
    $params = [
      ':id' => (string)$defaultAccount->id,
    ];
    if ($input->account) {
      $where = [
        'a.id = :id',
      ];
      $params[':id'] = (string)$input->account->id;
    }
    if ($input->with_remain_only) {
      $where[] = "(COALESCE(t_incoming.amount, 0) - COALESCE(t_outcoming.amount, 0) - COALESCE(expense.amount, 0)) > 0";
    }
    $where_str = implode(' AND ', $where);

    $sql = "
    SELECT
      a.id,
      a.title,
      a.contractor_id,
      c.title AS contractor_title,
      COALESCE(t_incoming.amount, 0) - COALESCE(t_outcoming.amount, 0) - COALESCE(expense.amount, 0) AS remain
    FROM account AS a
    INNER JOIN (SELECT
                  t.account_target_id AS account_id,
                  SUM(t.amount) AS amount
                FROM transaction AS t
                GROUP BY t.account_target_id) AS t_incoming ON t_incoming.account_id = a.id
    LEFT JOIN (SELECT
                 t.account_src_id AS account_id,
                 SUM(t.amount) AS amount
               FROM transaction AS t
               GROUP BY t.account_src_id) AS t_outcoming ON t_outcoming.account_id = a.id
    LEFT JOIN (SELECT
                 e.account_id,
                 SUM(e.amount) AS amount
               FROM expense AS e
               GROUP BY e.account_id) AS expense ON expense.account_id = a.id
    LEFT JOIN contractor c ON c.id = a.contractor_id
    WHERE $where_str
    ";

    $data = DB::select($sql, $params);
    $items = [];
    foreach ($data as $datum) {
      $contractor = null;
      if ($datum->contractor_id) {
        $contractor = new Item($datum->contractor_id, $datum->contractor_title);
      }
      $items[] = new AccountItem($datum->id, $datum->title, new Amount($datum->remain), $contractor);
    }
    return $items;
  }

  /**
   * @inheritDoc
   */
  public function accountRemain(AccountId $accountId): float {
    $sql = "
    SELECT
      COALESCE(t_incoming.amount, 0) - COALESCE(t_outcoming.amount, 0) - COALESCE(expense.amount, 0) AS remain
    FROM account AS a
    INNER JOIN (SELECT
                  t.account_target_id AS account_id,
                  SUM(t.amount) AS amount
                FROM transaction AS t
                WHERE t.account_target_id = '$accountId'
                GROUP BY t.account_target_id) AS t_incoming ON t_incoming.account_id = a.id
    LEFT JOIN (SELECT
                 t.account_src_id AS account_id,
                 SUM(t.amount) AS amount
               FROM transaction AS t
               WHERE t.account_src_id = '$accountId'
               GROUP BY t.account_src_id) AS t_outcoming ON t_outcoming.account_id = a.id
    LEFT JOIN (SELECT
                 e.account_id,
                 SUM(e.amount) AS amount
               FROM expense AS e
               WHERE e.account_id = '$accountId'
               GROUP BY e.account_id) AS expense ON expense.account_id = a.id
    ;";
    $data = DB::select($sql);
    if (empty($data)) {
      return 0.0;
    }
    $datum = reset($data);
    return (float)$datum->remain;
  }

  /**
   * @inheritDoc
   */
  public function manageItems(): array {
    $sql = 'SELECT
      a.*,
      c.title AS contractor_title,
      COUNT(e.id) AS expenses_count,
      COUNT(t.id) AS transactions_count
    FROM account AS a
    LEFT JOIN expense AS e ON e.account_id = a.id
    LEFT JOIN transaction AS t ON t.account_src_id = a.id OR t.account_target_id = a.id
    LEFT JOIN contractor c ON c.id = a.contractor_id
    GROUP BY a.id, a.title, c.title
    ORDER BY a.title';

    $data = DB::select($sql);

    $items = [];
    foreach ($data as $datum) {
      $contractor = null;
      if ($datum->contractor_id) {
        $contractor = new Item($datum->contractor_id, $datum->contractor_title);
      }
      $items[] = new ManageItem(
        $datum->id,
        $datum->title,
        $contractor,
        (int)$datum->expenses_count + (int)$datum->transactions_count
      );
    }
    return $items;
  }

  /**
   * @inheritDoc
   */
  public function manageItemById(AccountId $accountId): ManageItem {
    $sql = 'SELECT 
        a.*,
        c.title AS contractor_title,
        COUNT(e.id) AS expenses_count,
        COUNT(t.id) AS transactions_count
      FROM account AS a
      LEFT JOIN expense AS e ON e.account_id = a.id
      LEFT JOIN transaction AS t ON t.account_src_id = a.id OR t.account_target_id = a.id
      LEFT JOIN contractor c ON c.id = a.contractor_id
			WHERE a.id = :id
			GROUP BY a.id, c.title';

    $data = DB::select($sql, ['id' => $accountId]);

    if (empty($data)) {
      throw new NotFoundEntityException("Счет с id [$accountId] не найден.");
    }

    $datum = reset($data);
    $contractor = null;
    if ($datum->contractor_id) {
      $contractor = new Item($datum->contractor_id, $datum->contractor_title);
    }
    return new ManageItem(
      $datum->id,
      $datum->title,
      $contractor,
      (int)$datum->expenses_count + (int)$datum->transactions_count
    );
  }

  /**
   * @inheritDoc
   */
  public function operations(AccountId $accountId, bool $sort_date_asc = true): array {
    $sort = $sort_date_asc ? 'ASC' : 'DESC';
    $account_id_str = (string)$accountId;
    $sql = "
    SELECT
      t.account_target_id AS account_id,
      t.date,
      t.amount,
      t.note,
      a.title,
      0 AS type_index,
      'incoming' AS type
    FROM transaction AS t
    INNER JOIN account a ON a.id = t.account_src_id
    WHERE t.account_target_id = '$account_id_str'
    UNION
    SELECT
      t.account_src_id AS account_id,
      t.date,
      t.amount,
      t.note,
      a.title,
      1 AS type_index,
      'outcoming' AS type
    FROM transaction AS t
    INNER JOIN account a ON a.id = t.account_target_id
    WHERE t.account_src_id = '$account_id_str'
    UNION
    SELECT
      e.account_id,
      e.date,
      e.amount,
      e.note,
      p.title,
      2 AS type_index,
      'expense' AS type
    FROM expense AS e
    INNER JOIN product p ON e.product_id = p.id
    WHERE e.account_id = '$account_id_str'
    ORDER BY type_index, date $sort
    ";
    $data = DB::select($sql);

    $ops = [];
    foreach ($data as $datum) {
      $ops[] = new AccountOperation(
        new DateTimeImmutable($datum->date),
        new Title($datum->title),
        new Amount($datum->amount),
        OpType::from($datum->type_index),
        $datum->note ?? ''
      );
    }
    return $ops;
  }
}
