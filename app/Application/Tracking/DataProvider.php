<?php

namespace App\Application\Tracking;

use App\Core\Expense\Entity\ExpenseId;
use App\Core\Expense\IDataProvider as ExpenseDataProvider;
use App\Core\Tracking\DTO\ArchiveItem;
use App\Core\Tracking\DTO\CurrentItem;
use App\Core\Tracking\DTO\FinishedTrackingProduct;
use App\Core\Tracking\DTO\ToTrackingItem;
use App\Core\Tracking\IDataProvider;
use DateTimeImmutable;
use DB;

class DataProvider implements IDataProvider {
  public function __construct(private readonly ExpenseDataProvider $expenseDataProvider) {
  }

  /**
   * @inheritDoc
   */
  public function currentTrackedItems(): array {
    $data = DB::table('tracking')
      ->whereNull('finished_date')
      ->orderBy('started_date')
      ->get()
    ;
    $items = [];
    foreach ($data as $datum) {
      $expenseItem = $this->expenseDataProvider->itemById(new ExpenseId($datum->expense_id));
      $items[] = new CurrentItem(
        $datum->id,
        $expenseItem,
        $datum->quantity,
        new DateTimeImmutable($datum->started_date),
      );
    }

    return $items;
  }

  /**
   * @inheritDoc
   */
  public function finishedTracks(): array {
    $data = DB::table('tracking')
      ->whereNotNull('finished_date')
      ->orderBy('finished_date')
      ->get()
    ;
    $items = [];
    foreach ($data as $datum) {
      $expenseItem = $this->expenseDataProvider->itemById(new ExpenseId($datum->expense_id));
      $items[] = new ArchiveItem(
        $datum->id,
        $expenseItem,
        $datum->quantity,
        new DateTimeImmutable($datum->started_date),
        new DateTimeImmutable($datum->finished_date),
        $datum->days
      );
    }

    return $items;
  }

  /**
   * @inheritDoc
   */
  public function toTrackingExpenses(): array {
    // @todo: по идее, возможноть запустить новое отслеживание
    // должна проверяться не по id расхода, а по id товара.
    $sql = "SELECT
      e.id,
      e.date,
      CASE
        WHEN
        (SELECT id FROM tracking WHERE expense_id = e.id AND finished_date IS NULL LIMIT 1) IS NOT NULL
        THEN TRUE
        ELSE FALSE
        END
        AS is_tracking
    FROM expense AS e
    INNER JOIN to_tracking_expense AS tte ON tte.expense_id = e.id
    ORDER BY e.date, is_tracking, e.id ASC";
    $data = DB::select($sql);

    $items = [];
    foreach ($data as $datum) {
      $expenseItem = $this->expenseDataProvider->itemById(new ExpenseId($datum->id));
      $items[] = new ToTrackingItem($expenseItem, $datum->is_tracking);
    }
    return $items;
  }

  /**
   * @inheritDoc
   */
  public function finishedTotals(): array {
    $sql = "SELECT
      p.title,
      SUM(tracking.quantity) AS qty,
      u.title AS unit,
      SUM(tracking.days) AS days,
      ROUND(SUM(tracking.quantity) / SUM(tracking.days), 2) AS in_day
    FROM tracking
    INNER JOIN expense e ON e.id = tracking.expense_id
    INNER JOIN product p ON p.id = e.product_id
    INNER JOIN unit u ON u.id = e.unit_id
    WHERE tracking.finished_date IS NOT NULL
    GROUP BY p.title, u.title
    ORDER BY p.title
    ";
    $data = DB::select($sql);

    $items = [];
    foreach ($data as $datum) {
      $items[] = new FinishedTrackingProduct(
        $datum->title,
        $datum->qty,
        $datum->unit,
        $datum->days,
        $datum->in_day,
      );
    }
    return $items;
  }
}
