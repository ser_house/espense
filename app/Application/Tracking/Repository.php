<?php

namespace App\Application\Tracking;

use App\Core\Exception\NotFoundEntityException;
use App\Core\Expense\Entity\Expense;
use App\Core\Expense\Entity\ExpenseId;
use App\Core\Expense\Entity\Quantity;
use App\Core\ITransaction;
use App\Core\Tracking\IRepository;
use App\Core\Tracking\Track;
use App\Core\Tracking\TrackId;
use DateTimeImmutable;
use DB;
use Exception;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;
use Throwable;

final class Repository implements IRepository {
  public function __construct(private readonly ITransaction $transaction) {
  }


  /**
   * @inheritDoc
   */
  public function add(Track $track): void {
    DB::table('tracking')->insert([
      'id' => (string)$track->id,
      'expense_id' => (string)$track->expenseId,
      'quantity' => $track->quantity->value(),
      'started_date' => $track->startedDate->format('Y-m-d'),
    ]);
  }

  /**
   * @inheritDoc
   */
  public function get(TrackId $trackId): Track {
    $data = DB::table('tracking')
      ->where('id', (string)$trackId)
      ->first()
    ;
    if (null === $data) {
      throw new NotFoundEntityException("Запись учета срока использования с id \{$trackId\} не найдена.");
    }

    $refl = new ReflectionClass(Track::class);
    $track = $refl->newInstanceWithoutConstructor();

    foreach (['id', 'expenseId', 'quantity', 'startedDate'] as $prop_name) {
      $prop = new ReflectionProperty($track, $prop_name);

      $db_field_name = $this->propToDbFieldName($prop_name);
      if ($data->{$db_field_name}) {
        switch ($prop_name) {
          case 'startedDate':
            $value = new DateTimeImmutable($data->{$db_field_name});
            break;

          case 'id':
          case 'expenseId':
          case 'quantity':
            $value = $this->hydratePropValue($prop_name, $data->{$db_field_name});
            break;

          default:
            $value = $data->{$db_field_name};
            break;
        }
      }
      else {
        $value = null;
      }
      $prop->setValue($track, $value);
    }
    $track->setFinishedDate(new DateTimeImmutable());
    return $track;
  }

  /**
   * @inheritDoc
   */
  public function setFinishedDate(TrackId $trackId, DateTimeImmutable $finishedDate, int $days): void {
    DB::table('tracking')->where('id', (string)$trackId)->update([
      'finished_date' => $finishedDate->format('Y-m-d'),
      'days' => $days,
    ]);
  }

  /**
   * @inheritDoc
   */
  public function setQty(TrackId $trackId, Quantity $quantity): void {
    DB::table('tracking')->where('id', (string)$trackId)->update([
      'quantity' => $quantity->value(),
    ]);
  }


  /**
   * @inheritDoc
   */
  public function addExpenseToTracking(Expense $expense): void {
    $this->transaction->start();
    try {
      DB::table('to_tracking_expense')->insert([
        'expense_id' => (string)$expense->id,
      ]);
      DB::table('tracked_product')->insertOrIgnore([
        'product_id' => (string)$expense->productId,
      ]);
      $this->transaction->commit();
    }
    catch (Throwable $e) {
      $this->transaction->rollback();
      throw $e;
    }
  }

  /**
   * @inheritDoc
   */
  public function removeExpenseFromToTracking(Expense $expense): void {
    DB::table('to_tracking_expense')->where([
      'expense_id' => (string)$expense->id,
    ])->delete();
  }


  /**
   * @param string $prop
   *
   * @return string
   */
  private function propToDbFieldName(string $prop): string {
    return match ($prop) {
      'expenseId' => 'expense_id',
      'startedDate' => 'started_date',
      'finishedDate' => 'finished_date',
      default => $prop,
    };
  }

  /**
   * @param string $prop_name
   * @param mixed $val
   *
   * @return object
   * @throws ReflectionException
   */
  private function hydratePropValue(string $prop_name, mixed $val): object {
    $refl = match ($prop_name) {
      'id' => new ReflectionClass(TrackId::class),
      'expenseId' => new ReflectionClass(ExpenseId::class),
      'quantity' => new ReflectionClass(Quantity::class),
      default => throw new Exception("Unknown prop '$prop_name'"),
    };

    $prop = $refl->newInstanceWithoutConstructor();
    $prop_val_field = 'id';
    if ('quantity' === $prop_name) {
      $prop_val_field = 'value';
    }
    $propVal = new ReflectionProperty($prop, $prop_val_field);
    $propVal->setValue($prop, $val);

    return $prop;
  }
}
