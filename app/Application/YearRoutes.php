<?php

namespace App\Application;

use App\Core\Analytics\Calendar\Year\Year as CalendarYear;

class YearRoutes {
  public readonly string $prev;
  public readonly string $next;

  public function __construct(private readonly string $route, int $year) {
    $yearCalendar = new CalendarYear($year);

    if ($yearCalendar->canGoToPrev()) {
      $prevYear = $yearCalendar->prevYear();
      $this->prev = route($this->route, ['year' => $prevYear->year]);
    }
    else {
      $this->prev = '';
    }

    if ($yearCalendar->canGoToNext()) {
      $nextYear = $yearCalendar->nextYear();
      $this->next = route($this->route, ['year' => $nextYear->year]);
    }
    else {
      $this->next = '';
    }
  }
}
