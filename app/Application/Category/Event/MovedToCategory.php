<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.04.2021
 * Time: 17:07
 */


namespace App\Application\Category\Event;


use App\Models\Category;
use Illuminate\Queue\SerializesModels;

class MovedToCategory {
  use SerializesModels;

  public Category $category;
  public ?Category $oldCategory = null;
  public ?Category $newCategory = null;

  /**
   * MovedToCategory constructor.
   *
   * @param Category $category
   * @param Category|null $oldCategory
   * @param Category|null $newCategory
   */
  public function __construct(Category $category, ?Category $oldCategory, ?Category $newCategory) {
    $this->category = $category;
    $this->oldCategory = $oldCategory;
    $this->newCategory = $newCategory;
  }


}
