<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.08.2019
 * Time: 15:38
 */


namespace App\Application\Category;


use App\Core\Category\Category as Entity;
use App\Core\Category\CategoryId as EntityId;
use App\Core\Category\Converter as CoreConverter;
use App\Core\Title;
use App\Models\Category as Model;
use Exception;


/**
 * Class Converter
 *
 * @package App\Application\Category
 */
class Converter extends CoreConverter {

  /**
   * @param Entity $entity
   *
   * @return Model
   */
  public function domainToModel(Entity $entity): Model {
    $model = new Model();
    $model->exists = true;
    $model->id = (string)$entity->id;
    $model->title = (string)$entity->title;
    $model->parent_id = $entity->parentId ? (string)$entity->parentId : null;

    return $model;
  }

  /**
   * @param Model $model
   *
   * @return Entity
   * @throws Exception
   */
  public function modelToDomain(Model $model): Entity {
    $id = new EntityId($model->id);
    $title = new Title($model->title);
    $parentId = $model->parent_id ? new EntityId($model->parent_id) : null;
    return new Entity($id, $title, $parentId);
  }

  /**
   * @param iterable $entities
   *
   * @return Model[]
   */
  public function domainsToModels(iterable $entities): array {
    $models = [];

    foreach ($entities as $entity) {
      $models[] = $this->domainToModel($entity);
    }

    return $models;
  }

  /**
   * @param iterable $models
   *
   * @return Entity[]
   * @throws Exception
   */
  public function modelsToDomains(iterable $models): array {
    $entities = [];

    foreach ($models as $model) {
      $entities[] = $this->modelToDomain($model);
    }

    return $entities;
  }
}
