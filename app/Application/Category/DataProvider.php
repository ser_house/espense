<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.12.2019
 * Time: 9:32
 */


namespace App\Application\Category;

use App\Core\Category\CategoryId;
use App\Core\Category\IDataProvider;
use App\Core\Generic\DTO\Item;
use DB;
use Exception;
use stdClass;

class DataProvider implements IDataProvider {
  public function __construct(private \App\Core\Product\IDataProvider $productDataProvider) {
  }


  /**
   * @inheritDoc
   */
  public function findLikeTitle(string $like): array {
    $db_items = DB::table('category')
      ->select(
        'category.id',
        'category.title',
        'category_parent.id AS category_parent_id',
        'category_parent.title AS category_parent_title'
      )
      ->leftJoin('category AS category_parent', 'category_parent.id', '=', 'category.parent_id')
      ->where('category.title', 'ILIKE', "%$like%")
      ->orderBy('category.title')
      ->get()
    ;

    $items = [];
    foreach ($db_items as $db_item) {
      $item = [
        'id' => $db_item->id,
        'title' => $db_item->title,
      ];
      if ($db_item->category_parent_id) {
        $item['parent'] = [
          'id' => $db_item->category_parent_id,
          'title' => $db_item->category_parent_title,
        ];
      }
      $items[] = $item;
    }

    return $items;
  }

  /**
   * @inheritDoc
   */
  public function findByTitle(string $title): array {
    $db_items = DB::table('category')
      ->select(
        'category.id',
        'category.title',
        'category_parent.id AS category_parent_id',
        'category_parent.title AS category_parent_title'
      )
      ->leftJoin('category AS category_parent', 'category_parent.id', '=', 'category.parent_id')
      ->where('category.title', $title)
      ->orderBy('category.title')
      ->get()
    ;

    $items = [];
    foreach ($db_items as $db_item) {
      $item = [
        'id' => $db_item->id,
        'title' => $db_item->title,
      ];
      if ($db_item->category_parent_id) {
        $item['parent'] = [
          'id' => $db_item->category_parent_id,
          'title' => $db_item->category_parent_title,
        ];
      }
      $items[] = $item;
    }

    return $items;
  }

  /**
   * @inheritDoc
   */
  public function findById(CategoryId $categoryId): ?Item {
    $db_item = DB::table('category')
      ->select('id', 'title')
      ->where('id', (string)$categoryId)
      ->first()
    ;

    if ($db_item) {
      return new Item($db_item->id, $db_item->title);
    }

    return null;
  }

  /**
   * @inheritDoc
   */
  public function allAsTree(): array {
    $db_items = $this->allCategoriesDbItems();
    if (empty($db_items)) {
      return [];
    }

    return $this->buildtree($db_items);
  }

  /**
   * @inheritDoc
   */
  public function listMaterializedItems(string $delimiter = '/'): array {
    $db_items = $this->allCategoriesDbItems();
    if (empty($db_items)) {
      return [];
    }

    $tree = $this->buildTree($db_items);
    return $this->buildMaterializedList($tree, $delimiter);
  }

  /**
   * @inheritDoc
   */
  public function categoriesWithProduct(): array {
    $sql = "SELECT DISTINCT 
      c.id AS id, 
      c.title AS title, 
      parent.title AS parent_title
    FROM category AS c
    INNER JOIN product AS p ON p.category_id = c.id
    LEFT JOIN category AS parent ON parent.id = c.parent_id
    ORDER BY parent.title, c.title";
    $items = DB::select($sql);
    $result = [];
    foreach ($items as $item) {
      if (!empty($item->parent_title)) {
        $result[] = [
          'id' => $item->id,
          'title' => "$item->parent_title / $item->title",
        ];
      }
      else {
        $result[] = [
          'id' => $item->id,
          'title' => "/ $item->title",
        ];
      }
    }
    return $result;
  }

  /**
   * @inheritDoc
   */
  public function categories(): array {
    $sql = "SELECT DISTINCT 
      c.id AS id, 
      c.title AS title, 
      parent.title AS parent_title,
      CONCAT(nullif(parent.title,''), c.title)
    FROM category AS c
    LEFT JOIN category AS parent ON parent.id = c.parent_id
    ORDER BY CONCAT(nullif(parent.title,''), c.title)";

    $items = DB::select($sql);
    $result = [];
    foreach ($items as $item) {
      if (!empty($item->parent_title)) {
        $result[] = [
          'id' => $item->id,
          'title' => "$item->parent_title / $item->title",
        ];
      }
      else {
        $result[] = [
          'id' => $item->id,
          'title' => $item->title,
        ];
      }
    }
    return $result;
  }


  /**
   * @return array
   */
  private function allCategoriesDbItems(): array {
    $sql = 'WITH RECURSIVE a AS (
      SELECT id, parent_id, title
      FROM category
      WHERE parent_id IS NULL
      UNION ALL
      SELECT child.id, child.parent_id, child.title
      FROM category AS child
      JOIN a ON a.id = child.parent_id
      WHERE child.parent_id IS NOT NULL)
    SELECT * FROM a ORDER BY title';

    $db_items = DB::select($sql);
    if (empty($db_items)) {
      return [];
    }
    return $db_items;
  }

  /**
   * @param array $items
   * @param string|null $parent_id
   *
   * @return array
   */
  private function buildTree(array &$items, string $parent_id = null): array {
    $branch = [];

    foreach ($items as $element) {
      if ($element->parent_id === $parent_id) {
        $element->children = $this->buildTree($items, $element->id);
        $branch[$element->id] = $element;
        unset($items[$element->id]);
      }
    }

    return $branch;
  }

  /**
   * @param array $items
   * @param string $delimiter
   *
   * @return array
   * @throws Exception
   */
  private function buildMaterializedList(array $items, string $delimiter): array {
    $list = [];
    foreach ($items as $element) {
      $list = array_merge($list, $this->buildMaterializeItem($element, $delimiter));
    }
    return $list;
  }

  /**
   * @param stdClass $item
   * @param string $delimiter
   * @param string $parent_title
   *
   * @return array
   * @throws Exception
   */
  private function buildMaterializeItem(stdClass $item, string $delimiter, string $parent_title = ''): array {
    if (empty($parent_title)) {
      $parent_title = $item->title;
    }
    else {
      $parent_title .= " $delimiter $item->title";
    }

    $list = [];
    $product_titles = $this->productDataProvider->findTitlesByCategoryId(new CategoryId($item->id));
    if (!empty($product_titles)) {
      foreach ($product_titles as $product_title) {
        $list[] = "$parent_title $delimiter $product_title";
      }
    }
    foreach ($item->children as $child) {
      $list = array_merge($list, $this->buildMaterializeItem($child, $delimiter, $parent_title));
    }
    return $list;
  }
}
