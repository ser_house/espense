<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.04.2021
 * Time: 17:08
 */


namespace App\Application\Category\Listener;


use App\Application\Category\Event\MovedToCategory as AppCategoryMovedToCategoryEvent;
use App\Core\Analytics\IDataProvider;
use App\Core\Service\ICacheFlusher;
use Exception;

class MovedToCategory {
  protected ICacheFlusher $cacheFlusher;

  /**
   * MovedToCategory constructor.
   *
   * @param ICacheFlusher $cacheFlusher
   */
  public function __construct(ICacheFlusher $cacheFlusher) {
    $this->cacheFlusher = $cacheFlusher;
  }

  /**
   * @param AppCategoryMovedToCategoryEvent $event
   *
   * @throws Exception
   */
  public function handle(AppCategoryMovedToCategoryEvent $event): void {
    $this->cacheFlusher->flush(IDataProvider::TOTALS);
  }
}
