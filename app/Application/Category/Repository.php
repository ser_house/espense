<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.08.2019
 * Time: 10:50
 */


namespace App\Application\Category;

use App\Core\Category\Category;
use App\Core\Category\CategoryId;
use App\Core\Category\IRepository;
use App\Core\Title;
use App\Models\Category as Model;


class Repository implements IRepository {

  public function __construct(private readonly Converter $converter) {

  }


  /**
   * @inheritDoc
   */
  public function nextId(): CategoryId {
    return new CategoryId();
  }

  /**
   * @inheritDoc
   */
  public function add(Category $category): void {
    $model = $this->converter->domainToModel($category);
    $model->exists = false;
    $model->save();
  }

  /**
   * @inheritDoc
   */
  public function update(Category $category): void {
    $model = $this->converter->domainToModel($category);
    $model->exists = true;
    $model->save();
  }

  /**
   * @inheritDoc
   */
  public function remove(CategoryId $categoryId): void {
    $model = new Model();
    $model->exists = true;
    $model->id = (string)$categoryId;
    $model->delete();
  }

  /**
   * @inheritDoc
   */
  public function findAll(): array {
    /** @var Model[] $models */
    $models = Model::orderBy('title', 'ASC')->get();
    if (empty($models)) {
      return [];
    }

    return $this->converter->modelsToDomains($models);
  }

  /**
   * @inheritDoc
   */
  public function findById(CategoryId $categoryId): ?Category {
    $model = Model::find((string)$categoryId);
    if (empty($model)) {
      return null;
    }

    return $this->converter->modelToDomain($model);
  }

  /**
   * @inheritDoc
   */
  public function findByTitle(Title $title): ?Category {
    $model = Model::where('title', '=', (string)$title)->first();
    if (empty($model)) {
      return null;
    }

    return $this->converter->modelToDomain($model);
  }

  /**
   * @inheritDoc
   */
  public function findByParentAndTitle(Title $parent_title, Title $title): ?Category {
    $parentModel = Model::where('title', '=', (string)$parent_title)->first();
    if (empty($parentModel)) {
      return null;
    }

    $model = Model::where('title', '=', (string)$title)->where('parent_id', '=', $parentModel->id)->first();
    if (empty($model)) {
      return null;
    }

    return $this->converter->modelToDomain($model);
  }
}
