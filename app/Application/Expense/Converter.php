<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.08.2019
 * Time: 15:38
 */


namespace App\Application\Expense;


use App\Core\Account\AccountId;
use App\Core\Amount;
use App\Core\Category\DTO\Item as CategoryItem;
use App\Core\Category\IRepository as ICategoryRepository;
use App\Core\Contractor\ContractorId;
use App\Core\Contractor\IRepository as IContractorRepository;
use App\Core\Exception\NotFoundEntityException;
use App\Core\Expense\DTO\Item as ExpenseItem;
use App\Core\Expense\Entity\Expense;
use App\Core\Expense\Entity\Expense as Entity;
use App\Core\Expense\Entity\ExpenseId as EntityId;
use App\Core\Expense\Entity\Quantity;
use App\Core\Expense\IConverter;
use App\Core\Generic\DTO\Item;
use App\Core\Generic\Note;
use App\Core\Product\DTO\ProductItem as ExpenseProductItem;
use App\Core\Product\IRepository as IProductRepository;
use App\Core\Product\ProductId;
use App\Core\Unit\IDataProvider as UnitDataProvider;
use App\Core\Unit\UnitId;
use App\Core\Account\IRepository as IAccountRepository;
use App\Core\Account\IDataProvider as IDataProvider;
use App\Models\Expense as Model;
use DateTimeImmutable;
use Exception;
use stdClass;

/**
 * Class Converter
 *
 * @package App\Application\Expense
 */
class Converter implements IConverter {

  public function __construct(
    private readonly UnitDataProvider $unitDataProvider,
    private readonly IProductRepository $productRepository,
    private readonly IContractorRepository $contractorRepository,
    private readonly ICategoryRepository $categoryRepository,
    private readonly IDataProvider $accountDataProvider,
  ) {

  }


  /**
   * @param Entity $entity
   *
   * @return Model
   */
  public function domainToModel(Entity $entity): Model {
    $model = new Model();
    $model->exists = true;
    $model->id = (string)$entity->id;
    $model->date = $entity->date->format('Y-m-d H:i:s');
    $model->account_id = (string)$entity->accountId;
    $model->product_id = (string)$entity->productId;
    $model->unit_id = (string)$entity->unitId;
    $model->quantity = $entity->quantity->value();
    $model->amount = $entity->amount->value();
    $contractorId = $entity->contractorId;
    $model->contractor_id = $contractorId ? (string)$contractorId : null;
    $note = $entity->note;
    $model->note = $note ? (string)$note : null;

    return $model;
  }

  /**
   * @param Model $model
   *
   * @return Entity
   * @throws Exception
   */
  public function modelToDomain(Model $model): Entity {
    $id = new EntityId($model->id);
    $date = new DateTimeImmutable($model->date);
    $accountId = new AccountId($model->account_id);
    $productId = new ProductId($model->product_id);
    $unitId = new UnitId($model->unit_id);
    $quantity = new Quantity($model->quantity);
    $amount = new Amount($model->amount);
    $contractorId = $model->contractor_id ? new ContractorId($model->contractor_id) : null;
    $note = $model->note ? new Note($model->note) : null;

    return new Entity(
      $id,
      $date,
      $accountId,
      $productId,
      $unitId,
      $quantity,
      $amount,
      $contractorId,
      $note
    );
  }

  /**
   * @param iterable $entities
   *
   * @return Model[]
   */
  public function domainsToModels(iterable $entities): array {
    $models = [];

    foreach ($entities as $entity) {
      $models[] = $this->domainToModel($entity);
    }

    return $models;
  }

  /**
   * @param iterable $models
   *
   * @return Entity[]
   * @throws Exception
   */
  public function modelsToDomains(iterable $models): array {
    $entities = [];

    foreach ($models as $model) {
      $entities[] = $this->modelToDomain($model);
    }

    return $entities;
  }

  /**
   * @param stdClass $dbItem
   *
   * @return ExpenseItem
   * @throws Exception
   */
  public function dbItemToDto(stdClass $dbItem): ExpenseItem {
    $item = new ExpenseItem();
    $item->id = $dbItem->id;
    $item->date = new DateTimeImmutable($dbItem->date);

    $item->account = new Item($dbItem->account_id, $dbItem->account_title);

    $parentCategoryItem = null;
    if (!empty($dbItem->category_parent_id)) {
      $parentCategoryItem = new Item($dbItem->category_parent_id, $dbItem->category_parent_title);
    }
    $category = new CategoryItem($dbItem->category_id, $dbItem->category_title, $parentCategoryItem);

    $item->product = new ExpenseProductItem(
      $dbItem->product_id,
      $dbItem->product_title,
      $category,
    );

    $item->quantity = $dbItem->quantity;

    if ($dbItem->unit_id) {
      $item->unit = new Item($dbItem->unit_id, $dbItem->unit_title);
    }

    $item->amount = $dbItem->amount;
    if ($dbItem->contractor_id) {
      $item->contractor = new Item($dbItem->contractor_id, $dbItem->contractor_title);
    }
    else {
      $item->contractor = null;
    }
    $item->note = (string)$dbItem->note;
    $item->is_favorite = !empty($dbItem->is_favorite);
    $item->is_tracking = !empty($dbItem->is_tracking);
    return $item;
  }

  /**
   * @param iterable $db_items
   *
   * @return ExpenseItem[]
   * @throws Exception
   */
  public function dbItemsToDto(iterable $db_items): array {
    $items = [];
    foreach ($db_items as $db_item) {
      $items[] = $this->dbItemToDto($db_item);
    }

    return $items;
  }


  /**
   * @inheritDoc
   */
  public function domainToDto(Expense $expense): ExpenseItem {
    // @todo: когда идет конвертация в Converter::domainsToDto,
    // то у нас пачки запросов в цикле.
    // Страшного ничего нет (система не является высоконагруженной ни разу),
    // но всё-таки получше было бы собрать всё одним запросом и потом уже понасоздавать что надо.
    $item = new ExpenseItem();
    $item->id = (string)$expense->id;
    $item->date = $expense->date;
    $item->account = $this->accountDataProvider->get($expense->accountId);

    $product = $this->productRepository->findById($expense->productId);
    if (null === $product) {
      throw new NotFoundEntityException('Товар не найден.');
    }

    $categoryItem = null;
    if ($product->categoryId) {
      $category = $this->categoryRepository->findById($product->categoryId);
      if (null === $category) {
        throw new NotFoundEntityException('Категория не найдена.');
      }

      $category_title = (string)$category->title;

      $parentCategoryItem = null;
      if ($category->parentId) {
        $parentCategory = $this->categoryRepository->findById($category->parentId);
        if (null === $parentCategory) {
          throw new NotFoundEntityException("Родительская для '$category_title' категория не найдена.");
        }
        $parentCategoryItem = new Item((string)$parentCategory->id, (string)$parentCategory->title);
      }

      $categoryItem = new CategoryItem((string)$category->id, (string)$category->title, $parentCategoryItem);
    }

    $item->product = new ExpenseProductItem((string)$product->id, (string)$product->title, $categoryItem);

    $unit = $this->unitDataProvider->getById($expense->unitId);
    $item->unit = new Item($unit->id, $unit->title);
    $item->quantity = $expense->quantity->value();
    $item->amount = $expense->amount->value();

    $contractorId = $expense->contractorId;
    if ($contractorId) {
      $contractor = $this->contractorRepository->findById($contractorId);
      if (null === $contractor) {
        throw new NotFoundEntityException('Контрагент не найден.');
      }
      $item->contractor = new Item((string)$contractor->id, (string)$contractor->title);
    }
    else {
      $item->contractor = null;
    }

    $note = $expense->note;
    $item->note = $note ? (string)$note : '';

    $item->is_favorite = false;
    $item->is_tracking = false;

    return $item;
  }

  /**
   * @inheritDoc
   */
  public function domainsToDto(iterable $expenses): array {
    $items = [];

    foreach ($expenses as $expense) {
      $items[] = $this->domainToDto($expense);
    }

    return $items;
  }
}
