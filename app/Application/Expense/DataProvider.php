<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.09.2019
 * Time: 16:14
 */


namespace App\Application\Expense;

use App\Core\Analytics\Calendar\Month\Formatter;
use App\Core\Category\CategoryId;
use App\Core\Contractor\ContractorId;
use App\Core\Exception\NotFoundEntityException;
use App\Core\Expense\DTO\Item as ExpenseItem;
use App\Core\Expense\Entity\ExpenseId;
use App\Core\Expense\IDataProvider;
use App\Core\Expense\UseCase\Find\Input as FindInput;
use App\Core\Product\ProductId;
use App\Core\Unit\UnitId;
use DB;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\JoinClause;

class DataProvider implements IDataProvider {

  private Formatter $monthFormatter;
  private Converter $converter;

  /**
   * DataProvider constructor.
   *
   * @param Formatter $monthFormatter
   * @param Converter $converter
   */
  public function __construct(Formatter $monthFormatter, Converter $converter) {
    $this->monthFormatter = $monthFormatter;
    $this->converter = $converter;
  }

  /**
   * @inheritDoc
   */
  public function itemById(ExpenseId $expenseId): ExpenseItem {
    $query = $this->buildQuery();
    $query = $query->where('expense.id', '=', (string)$expenseId);
    $db_item = $query->first();

    if (null === $db_item) {
      throw new NotFoundEntityException("Запись с id [$expenseId] не найдена.");
    }

    return $this->converter->dbItemToDto($db_item);
  }

  /**
   * @inheritDoc
   */
  public function lastItem(): ?ExpenseItem {
    $query = $this->builderOrderedByExpenseDate();
    $db_item = $query->first();

    if (null === $db_item) {
      return null;
    }

    return $this->converter->dbItemToDto($db_item);
  }

  /**
   * @inheritDoc
   */
  public function todayItems(): array {
    $query = $this->builderOrderedByExpenseDate();
    // @todo: может, всё-таки, по дате расхода?
    $query = $query->where(DB::raw("TO_CHAR(created_at, 'yyyy-mm-dd')"), date('Y-m-d'));
    $db_items = $query->get();

    return $this->converter->dbItemsToDto($db_items);
  }

  /**
   * @inheritDoc
   */
  public function find(FindInput $input): array {
    $query = $this->buildQuery();

    $category = $input->category;
    if ($category) {
      // Расходы с товарами, у которых указанная категория или одна из её дочерних.
      $category_id_str = (string)$category->id;
      $data = DB::select("WITH RECURSIVE d AS (
        SELECT id FROM category WHERE id = '$category_id_str'
        UNION ALL
        SELECT c.id FROM d INNER JOIN category AS c ON c.parent_id = d.id
      )
      SELECT * FROM d");

      $query = $query->whereIn('product.category_id', array_column($data, 'id'));
    }

    $product = $input->product;
    if ($product) {
      $query = $query->where('expense.product_id', (string)$product->id);
    }

    $contractor = $input->contractor;
    if ($contractor) {
      $query = $query->where('expense.contractor_id', (string)$contractor->id);
    }

    if ($input->is_favorite) {
      /** @var JoinClause $join */
      foreach ($query->joins as $key => $join) {
        if ($join->table === 'favorite') {
          unset($query->joins[$key]);
        }
      }
      $query->join('favorite', 'favorite.expense_id', '=', 'expense.id');
    }

    $date = $input->date();
    if ($date) {
      $query = $query->where(DB::raw("TO_CHAR(expense.date, 'yyyy-mm-dd')"), $date->format('Y-m-d'));
    }
    else {
      $months = $input->months();
      foreach ($months as &$month) {
        $month = $this->monthFormatter->normalizedMonthNumber($month);
      }
      unset($month);

      $years = $input->years();

      if ($months && $years) {
        $in_months = [];
        foreach ($years as $year) {
          foreach ($months as $month) {
            $in_months[] = $this->monthFormatter->formattedYearMonthForSql($year, $month);
          }
        }

        $query = $query->whereIn(DB::raw("TO_CHAR(expense.date, 'yyyy-mm')"), $in_months);
      }
      else {
        if ($years) {
          $query = $query->whereIn(DB::raw("TO_CHAR(expense.date, 'yyyy')"), $years);
        }
        else {
          if ($months) {
            $query = $query->whereIn(DB::raw("TO_CHAR(expense.date, 'mm')"), $months);
          }
        }
      }
    }

    $query->orderBy('expense.date', $input->sort);

    $db_items = $query->get();

    return $this->converter->dbItemsToDto($db_items);
  }

  /**
   * @inheritDoc
   */
  public function countByProductId(ProductId $productId): int {
    $data = DB::table('expense')
      ->select(DB::raw('count(*) as total'))
      ->where('product_id', '=', (string)$productId)
      ->groupBy(['product_id'])
      ->first()
    ;

    return $data->total ?? 0;
  }

  /**
   * @inheritDoc
   */
  public function countByContractorId(ContractorId $contractorId): int {
    $data = DB::table('expense')
      ->select(DB::raw('count(*) as total'))
      ->where('contractor_id', '=', (string)$contractorId)
      ->groupBy(['contractor_id'])
      ->first()
    ;

    return $data->total ?? 0;
  }

  /**
   * @inheritDoc
   */
  public function countByUnitId(UnitId $unitId): int {
    $data = DB::table('expense')
      ->select(DB::raw('count(*) as total'))
      ->where('unit_id', '=', (string)$unitId)
      ->groupBy(['unit_id'])
      ->first()
    ;

    return $data->total ?? 0;
  }

  /**
   * @inheritDoc
   */
  public function setUnitByParams(UnitId $unitId, ?ExpenseId $expenseId, ?CategoryId $categoryId, ?ProductId $productId): int {
    $query = DB::table('expense');
    if ($expenseId) {
      $query->where('expense.id', '=', (string)$expenseId);
    }
    if ($productId) {
      $query->where('expense.product_id', '=', (string)$productId);
    }
    if ($categoryId) {
      $query->join('product', 'product.id', '=', 'expense.product_id');

      $category_id_str = (string)$categoryId;
      $data = DB::select("WITH RECURSIVE d AS (
        SELECT id FROM category WHERE id = '$category_id_str'
        UNION ALL
        SELECT c.id FROM d INNER JOIN category AS c ON c.parent_id = d.id
      )
      SELECT * FROM d");

      $query = $query->whereIn('product.category_id', array_column($data, 'id'));
    }

    return $query->update(['unit_id' => (string)$unitId]);
  }

  /**
   * @inheritDoc
   */
  public function getFavoriteItems(): array {
    $query = $this->buildQuery();
    /** @var JoinClause $join */
    foreach ($query->joins as $key => $join) {
      if ($join->table === 'favorite') {
        unset($query->joins[$key]);
      }
    }
    $query->join('favorite', 'favorite.expense_id', '=', 'expense.id');
    $query->orderBy('category_parent.title');
    $query->orderBy('category.title');
    $query->orderBy('product.title');
    $db_items = $query->get();

    return $this->converter->dbItemsToDto($db_items);
  }

  /**
   * @inheritDoc
   */
  public function getFavoriteExpenseId(ProductId $productId): ?ExpenseId {
    $sql = "SELECT e.id FROM expense AS e
    INNER JOIN favorite AS f ON f.expense_id = e.id
    WHERE e.product_id = :product_id";

    $data = DB::select($sql, [
      'product_id' => (string)$productId,
    ]);

    if (empty($data)) {
      return null;
    }
    $datum = reset($data);
    return new ExpenseId($datum->id);
  }

  /**
   * @inheritDoc
   */
  public function addToFavorites(ExpenseId $expenseId): void {
    DB::table('favorite')->insert(['expense_id' => (string)$expenseId]);
  }

  /**
   * @inheritDoc
   */
  public function removeFromFavorites(ExpenseId $expenseId): void {
    DB::table('favorite')->where(['expense_id' => (string)$expenseId])->delete();
  }

  /**
   * @return Builder
   */
  private function buildQuery(): Builder {
    return DB::table('expense')
      ->select('expense.id',
        'expense.date',
        'expense.quantity',
        'expense.amount',
        'expense.note',
        'expense.product_id',
        'account.id AS account_id',
        'account.title AS account_title',
        'unit.id AS unit_id',
        'unit.title AS unit_title',
        'product.title AS product_title',
        DB::raw('CASE
        WHEN
          (SELECT id FROM tracking WHERE expense_id = expense.id AND finished_date IS NULL LIMIT 1) IS NOT NULL
          THEN TRUE
        ELSE FALSE
        END
        AS is_tracking'),
        'favorite.expense_id AS is_favorite',
        'expense.contractor_id',
        'contractor.title AS contractor_title',
        'product.category_id',
        'category.title AS category_title',
        'category_parent.id AS category_parent_id',
        'category_parent.title AS category_parent_title',
      )
      ->leftJoin('unit', 'unit.id', '=', 'expense.unit_id')
      ->join('account', 'account.id', '=', 'expense.account_id')
      ->join('product', 'product.id', '=', 'expense.product_id')
      ->join('category', 'category.id', '=', 'product.category_id')
      ->leftJoin('category AS category_parent', 'category_parent.id', '=', 'category.parent_id')
      ->leftJoin('contractor', 'contractor.id', '=', 'expense.contractor_id')
      ->leftJoin('favorite', 'favorite.expense_id', '=', 'expense.id')
    ;
  }

  /**
   * @return Builder
   */
  private function builderOrderedByExpenseDate(): Builder {
    return $this->buildQuery()->orderBy('expense.date', 'desc');
  }
}
