<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.11.2019
 * Time: 12:26
 */


namespace App\Application\Expense\Listener;

use App\Application\Expense\Event\Removed as AppExpenseRemovedEvent;
use App\Core\Analytics\IDataProvider;
use App\Core\Service\ICacheFlusher;
use Exception;


class Removed {

  public function __construct(protected readonly ICacheFlusher $cacheFlusher) {

  }

  /**
   * @param AppExpenseRemovedEvent $event
   *
   * @throws Exception
   */
  public function handle(AppExpenseRemovedEvent $event): void {
    $this->cacheFlusher->flush(IDataProvider::TOTALS);
  }
}
