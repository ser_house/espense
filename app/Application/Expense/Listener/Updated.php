<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.11.2019
 * Time: 16:43
 */


namespace App\Application\Expense\Listener;


use App\Application\Expense\Converter;
use App\Application\Expense\Event\Updated as AppExpenseUpdatedEvent;
use App\Core\Analytics\IDataProvider;
use App\Core\Service\ICacheFlusher;
use Exception;


class Updated {

  public function __construct(protected readonly Converter $converter, protected readonly ICacheFlusher $cacheFlusher) {

  }

  /**
   * @param AppExpenseUpdatedEvent $event
   *
   * @throws Exception
   */
  public function handle(AppExpenseUpdatedEvent $event): void {
    $originalExpense = $this->converter->modelToDomain($event->originalExpense);
    $updatedExpense = $this->converter->modelToDomain($event->updatedExpense);

    if (!$originalExpense->amount->equals($updatedExpense->amount)) {
      // @todo: сброс надо делать для конкретной корневой категории.
      $this->cacheFlusher->flush(IDataProvider::TOTALS);
    }
  }
}
