<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.11.2019
 * Time: 12:26
 */


namespace App\Application\Expense\Listener;

use App\Application\Expense\Event\Added as AppExpenseAddedEvent;
use App\Core\Analytics\IDataProvider;
use App\Core\Service\ICacheFlusher;
use Exception;


class Added {

  public function __construct(protected readonly ICacheFlusher $cacheFlusher) {

  }

  /**
   * @param AppExpenseAddedEvent $event
   *
   * @throws Exception
   */
  public function handle(AppExpenseAddedEvent $event): void {
    $this->cacheFlusher->flush(IDataProvider::TOTALS);
  }
}
