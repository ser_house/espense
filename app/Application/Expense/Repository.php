<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 16.09.2019
 * Time: 23:31
 */


namespace App\Application\Expense;

use App\Core\Exception\FailedToSaveEntityException;
use App\Core\Exception\NotFoundEntityException;
use App\Core\Expense\Entity\Expense;
use App\Core\Expense\Entity\ExpenseId;
use App\Core\Expense\IRepository;
use App\Models\Expense as Model;
use Throwable;

class Repository implements IRepository {

  public function __construct(private readonly Converter $converter) {

  }

  /**
   * @inheritDoc
   */
  public function nextId(): ExpenseId {
    return new ExpenseId();
  }

  /**
   * @inheritDoc
   */
  public function get(ExpenseId $entityId): Expense {
    $model = Model::find((string)$entityId);
    if (empty($model)) {
      throw new NotFoundEntityException("Расход с id \{$entityId\} не найден.");
    }

    return $this->converter->modelToDomain($model);
  }

  /**
   * @inheritDoc
   */
  public function findById(ExpenseId $entityId): ?Expense {
    $model = Model::find((string)$entityId);
    if (empty($model)) {
      return null;
    }

    return $this->converter->modelToDomain($model);
  }

  /**
   * @inheritDoc
   */
  public function add(Expense $expense): void {
    $model = $this->converter->domainToModel($expense);
    $model->exists = false;
    try {
      $model->saveOrFail();
    }
    catch (Throwable $e) {
      throw new FailedToSaveEntityException($expense->id, $e);
    }
  }

  /**
   * @inheritDoc
   */
  public function update(Expense $expense): void {
    $model = $this->converter->domainToModel($expense);
    $model->exists = true;
    try {
      $model->saveOrFail();
    }
    catch (Throwable $e) {
      echo $e->getMessage(), PHP_EOL;
      throw new FailedToSaveEntityException($expense->id, $e);
    }
  }

  /**
   * @inheritDoc
   */
  public function remove(ExpenseId $expenseId) {
    $id = (string)$expenseId;
    Model::destroy($id);
  }
}
