<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.02.2020
 * Time: 17:19
 */


namespace App\Application\Expense\Event;


use App\Models\Expense;
use Illuminate\Queue\SerializesModels;

class Updated {
  use SerializesModels;

  public function __construct(public readonly Expense $originalExpense, public readonly Expense $updatedExpense) {

  }
}
