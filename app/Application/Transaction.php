<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.02.2020
 * Time: 11:22
 */


namespace App\Application;


use App\Core\ITransaction;
use DB;

class Transaction implements ITransaction {

  public function start(): void {
    DB::beginTransaction();
  }

  public function commit(): void {
    DB::commit();
  }

  public function rollback(): void {
    DB::rollBack();
  }
}
