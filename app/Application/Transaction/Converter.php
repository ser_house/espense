<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.08.2019
 * Time: 15:38
 */


namespace App\Application\Transaction;

use App\Core\Account\AccountId;
use App\Core\Account\IDataProvider;
use App\Core\Amount;
use App\Core\Generic\DTO\Item;
use App\Core\Generic\Note;
use App\Core\Transaction\DTO\Item as TransactionItem;
use App\Core\Transaction\IConverter;
use App\Core\Transaction\Transaction as Entity;
use App\Core\Transaction\TransactionId;
use App\Models\Transaction as Model;
use DateTimeImmutable;
use Exception;
use stdClass;


/**
 * Class Converter
 *
 * @package App\Application\Transaction
 */
class Converter implements IConverter {
  /**
   * @inheritDoc
   */
  public function __construct(private readonly IDataProvider $accountDataProvider) {
  }

  /**
   * @inheritDoc
   */
  public function domainToModel(Entity $entity): Model {
    $model = new Model();
    $model->exists = true;

    $model->id = (string)$entity->id;
    $model->date = $entity->date->format('Y-m-d H:i:s');
    $model->account_src_id = (string)$entity->accountSrcId;
    $model->account_target_id = (string)$entity->accountTargetId;
    $model->amount = $entity->amount->value();

    $note = $entity->note;
    $model->note = $note ? (string)$note : null;

    return $model;
  }

  /**
   * @inheritDoc
   */
  public function modelToDomain(Model $model): Entity {
    $id = new TransactionId($model->id);
    $date = new DateTimeImmutable($model->date);
    $accountSrcId = new AccountId($model->account_src_id);
    $accountTargetId = new AccountId($model->account_target_id);
    $amount = new Amount($model->amount);
    $note = $model->note ? new Note($model->note) : null;

    return new Entity(
      $id,
      $date,
      $accountSrcId,
      $accountTargetId,
      $amount,
      $note,
    );
  }

  /**
   * @inheritDoc
   */
  public function domainsToModels(iterable $entities): array {
    $models = [];

    foreach ($entities as $entity) {
      $models[] = $this->domainToModel($entity);
    }

    return $models;
  }

  /**
   * @inheritDoc
   */
  public function modelsToDomains(iterable $models): array {
    $entities = [];

    foreach ($models as $model) {
      $entities[] = $this->modelToDomain($model);
    }

    return $entities;
  }

  /**
   * @inheritDoc
   */
  public function dbItemToDto(stdClass $dbItem): TransactionItem {
    $item = new TransactionItem();
    $item->id = $dbItem->id;
    $item->date = new DateTimeImmutable($dbItem->date);


    $item->accountSrc = new Item(
      $dbItem->account_src_id,
      $dbItem->account_src_title,
    );
    $item->accountTarget = new Item(
      $dbItem->account_target_id,
      $dbItem->account_target_title,
    );


    $item->amount = $dbItem->amount;

    $item->note = (string)$dbItem->note;

    return $item;
  }

  /**
   * @inheritDoc
   */
  public function dbItemsToDto(iterable $db_items): array {
    $items = [];
    foreach ($db_items as $db_item) {
      $items[] = $this->dbItemToDto($db_item);
    }

    return $items;
  }


  /**
   * @inheritDoc
   */
  public function domainToDto(Entity $transaction): TransactionItem {
    // @todo: когда идет конвертация в Converter::domainsToDto,
    // то у нас пачки запросов в цикле.
    // Страшного ничего нет (система не является высоконагруженной ни разу),
    // но всё-таки получше было бы собрать всё одним запросом и потом уже понасоздавать что надо.
    $item = new TransactionItem();
    $item->id = (string)$transaction->id;
    $item->date = $transaction->date;

    $item->accountSrc = $this->accountDataProvider->get($transaction->accountSrcId);
    $item->accountTarget = $this->accountDataProvider->get($transaction->accountTargetId);

    $item->amount = $transaction->amount->value();

    $note = $transaction->note;
    $item->note = $note ? (string)$note : '';

    return $item;
  }

  /**
   * @inheritDoc
   */
  public function domainsToDto(iterable $transactions): array {
    $items = [];

    foreach ($transactions as $transaction) {
      $items[] = $this->domainToDto($transaction);
    }

    return $items;
  }
}
