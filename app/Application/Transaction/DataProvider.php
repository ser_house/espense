<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.09.2019
 * Time: 16:14
 */


namespace App\Application\Transaction;

use App\Core\Analytics\Calendar\Month\Formatter;
use App\Core\Exception\NotFoundEntityException;
use App\Core\Transaction\DTO\Item as TransactionItem;
use App\Core\Transaction\IDataProvider;
use App\Core\Transaction\TransactionId;
use App\Core\Transaction\UseCase\Find\Input as FindInput;
use DB;
use Illuminate\Database\Query\Builder;

class DataProvider implements IDataProvider {

  public function __construct(
    private readonly Formatter $monthFormatter,
    private readonly Converter $converter
  ) {
  }

  /**
   * @inheritDoc
   */
  public function all(): array {
    $query = $this->builderOrderedByTransactionDate();
    $db_items = $query->get();

    return $this->converter->dbItemsToDto($db_items);
  }

  /**
   * @inheritDoc
   */
  public function itemById(TransactionId $transactionId): TransactionItem {
    $query = $this->buildQuery();
    $query = $query->where('transaction.id', '=', (string)$transactionId);
    $db_item = $query->first();

    if (null === $db_item) {
      throw new NotFoundEntityException("Перевод с id [$transactionId] не найден.");
    }

    return $this->converter->dbItemToDto($db_item);
  }

  /**
   * @inheritDoc
   */
  public function lastItem(): ?TransactionItem {
    $query = $this->builderOrderedByTransactionDate();
    $db_item = $query->first();

    if (null === $db_item) {
      return null;
    }

    return $this->converter->dbItemToDto($db_item);
  }

  /**
   * @inheritDoc
   */
  public function todayItems(): array {
    $query = $this->builderOrderedByTransactionDate();
    // @todo: может, всё-таки, по дате перевода?
    $query = $query->where(DB::raw("TO_CHAR(transaction.created_at, 'yyyy-mm-dd')"), date('Y-m-d'));
    $db_items = $query->get();

    return $this->converter->dbItemsToDto($db_items);
  }

  /**
   * @inheritDoc
   */
  public function find(FindInput $input): array {
    $query = $this->buildQuery();

    $accountSrc = $input->accountSrc;
    if ($accountSrc) {
      $query = $query->where('transaction.account_src_id', (string)$accountSrc->id);
    }

    $accountTarget = $input->accountTarget;
    if ($accountTarget) {
      $query = $query->where('transaction.account_target_id', (string)$accountTarget->id);
    }

    $date = $input->date();
    if ($date) {
      $query = $query->where(DB::raw("TO_CHAR(transaction.date, 'yyyy-mm-dd')"), $date->format('Y-m-d'));
    }
    else {
      $months = $input->months();
      foreach ($months as &$month) {
        $month = $this->monthFormatter->normalizedMonthNumber($month);
      }
      unset($month);

      $years = $input->years();

      if ($months && $years) {
        $in_months = [];
        foreach ($years as $year) {
          foreach ($months as $month) {
            $in_months[] = $this->monthFormatter->formattedYearMonthForSql($year, $month);
          }
        }

        $query = $query->whereIn(DB::raw("TO_CHAR(transaction.date, 'yyyy-mm')"), $in_months);
      }
      else {
        if ($years) {
          $query = $query->whereIn(DB::raw("TO_CHAR(transaction.date, 'yyyy')"), $years);
        }
        else {
          if ($months) {
            $query = $query->whereIn(DB::raw("TO_CHAR(transaction.date, 'mm')"), $months);
          }
        }
      }
    }

    $query->orderBy('transaction.date', $input->sort);

    $db_items = $query->get();

    return $this->converter->dbItemsToDto($db_items);
  }

  /**
   * @return Builder
   */
  private function buildQuery(): Builder {
    return DB::table('transaction')
      ->select('transaction.*',
        'asrc.title AS account_src_title',
        'atarget.title AS account_target_title',
      )
      ->join('account AS asrc', 'asrc.id', '=', 'transaction.account_src_id')
      ->join('account AS atarget', 'atarget.id', '=', 'transaction.account_target_id')
    ;
  }

  /**
   * @return Builder
   */
  private function builderOrderedByTransactionDate(): Builder {
    return $this->buildQuery()->orderBy('transaction.date', 'desc');
  }
}
