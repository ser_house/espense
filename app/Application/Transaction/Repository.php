<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 16.09.2019
 * Time: 23:31
 */


namespace App\Application\Transaction;

use App\Core\Exception\FailedToSaveEntityException;
use App\Core\Exception\NotFoundEntityException;
use App\Core\Transaction\Transaction;
use App\Core\Transaction\TransactionId;
use App\Core\Transaction\IRepository;
use App\Models\Transaction as Model;
use Throwable;

class Repository implements IRepository {

  public function __construct(private readonly Converter $converter) {

  }

  /**
   * @inheritDoc
   */
  public function nextId(): TransactionId {
    return new TransactionId();
  }

  /**
   * @inheritDoc
   */
  public function get(TransactionId $entityId): Transaction {
    $model = Model::find((string)$entityId);
    if (empty($model)) {
      throw new NotFoundEntityException("Перевод с id \{$entityId\} не найден.");
    }

    return $this->converter->modelToDomain($model);
  }

  /**
   * @inheritDoc
   */
  public function findById(TransactionId $entityId): ?Transaction {
    $model = Model::find((string)$entityId);
    if (empty($model)) {
      return null;
    }

    return $this->converter->modelToDomain($model);
  }

  /**
   * @inheritDoc
   */
  public function add(Transaction $expense): void {
    $model = $this->converter->domainToModel($expense);
    $model->exists = false;
    try {
      $model->saveOrFail();
    }
    catch (Throwable $e) {
      throw new FailedToSaveEntityException($expense->id, $e);
    }
  }

  /**
   * @inheritDoc
   */
  public function update(Transaction $expense): void {
    $model = $this->converter->domainToModel($expense);
    $model->exists = true;
    try {
      $model->saveOrFail();
    }
    catch (Throwable $e) {
      echo $e->getMessage(), PHP_EOL;
      throw new FailedToSaveEntityException($expense->id, $e);
    }
  }

  /**
   * @inheritDoc
   */
  public function remove(TransactionId $expenseId) {
    $id = (string)$expenseId;
    Model::destroy($id);
  }
}
