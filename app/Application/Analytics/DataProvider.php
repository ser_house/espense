<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.09.2019
 * Time: 0:40
 */

namespace App\Application\Analytics;

use App\Core\Analytics\Calendar\Month;
use App\Core\Analytics\Calendar\Year\Input;
use App\Core\Analytics\IDataProvider;
use App\Core\Analytics\View\CategoryItem;
use App\Core\Analytics\View\ExpenseItem;
use App\Core\Analytics\View\CategoryWithProducts;
use App\Core\Analytics\View\ProductItem;
use App\Core\Category\CategoryId;
use App\Core\StaticCache;
use DB;

class DataProvider implements IDataProvider {

  /**
   * DataProvider constructor.
   *
   * @param Month\Formatter $monthFormatter
   */
  public function __construct(private Month\Formatter $monthFormatter) {

  }

  /**
   * @inheritDoc
   */
  public function journalYears(): array {
    $years = [];
    $data = DB::select("SELECT DISTINCT TO_CHAR(date, 'yyyy') AS year FROM expense ORDER BY year");
    foreach ($data as $datum) {
      $db_year = $datum->year;
      $years[$db_year] = $db_year;
    }

    return $years;
  }

  /**
   * @inheritDoc
   */
  public function totalByDatesAndCategories(int $year, int $month_num): array {
    $month = new Month\Month($year, $month_num);

    $month_str = $this->monthFormatter->formattedForSql($month);

    $sql = "SELECT 
				e.date,
				c.id AS category_id,
				c.title AS category, 
				SUM(e.amount) AS sum
			FROM expense AS e
			INNER JOIN product AS p ON p.id = e.product_id
			INNER JOIN category AS c ON c.id = p.category_id
			WHERE TO_CHAR(e.date, 'yyyy-mm') = '$month_str'
			GROUP BY c.id, e.date
			ORDER BY e.date, SUM(e.amount) DESC;";

    $categories_data = DB::select($sql);

    $products_data = $this->productsByDatesAndCategories($year, $month_num);

    $items = [];
    foreach ($categories_data as $datum) {
      $products = $products_data[$datum->date][$datum->category_id];
      $items[$datum->date][] = new CategoryWithProducts($datum->category, $products, $datum->sum);
    }

    return $items;
  }

  /**
   * @inheritDoc
   */
  public function productsByDatesAndCategories(int $year, int $month_num): array {
    $month = new Month\Month($year, $month_num);

    $month_str = $this->monthFormatter->formattedForSql($month);

    $sql = "SELECT
      e.date,
      p.category_id AS category,
      p.id,
			p.title AS product, 
			SUM(e.amount) AS sum
		FROM expense AS e
		INNER JOIN product AS p ON p.id = e.product_id
		WHERE TO_CHAR(e.date, 'yyyy-mm') = '$month_str'
		GROUP BY e.date, p.id
		ORDER BY SUM(e.amount) DESC;";

    $data = DB::select($sql);

    $items = [];
    foreach ($data as $datum) {
      $items[$datum->date][$datum->category][] = new ExpenseItem($datum->id, $datum->product, $datum->sum);
    }

    return $items;
  }

  /**
   * @inheritDoc
   */
  public function totalByMonthsAndCategories(Input $input): array {
    $query = DB::table('expense')
      ->select(
        DB::raw('EXTRACT(MONTH FROM expense.date) AS month'),
        'category.id AS category_id',
        'category.title AS category_title',
        DB::raw('SUM(expense.amount) AS sum')
      )
      ->join('product', 'product.id', '=', 'expense.product_id')
      ->join('category', 'category.id', '=', 'product.category_id')
    ;

    $category = $input->category;
    if ($category) {
      $query = $query->where('product.category_id', (string)$category->id);
    }

    $product = $input->product;
    if ($product) {
      $query = $query->where('expense.product_id', (string)$product->id);
    }

    $contractor = $input->contractor;
    if ($contractor) {
      $query = $query->where('expense.contractor_id', (string)$contractor->id);
    }

    $year = $input->year;
    if ($year) {
      $query = $query->where(DB::raw("TO_CHAR(expense.date, 'yyyy')"), $year);
    }

    $data = $query
      ->groupBy(DB::raw('category.id, EXTRACT(MONTH FROM expense.date)'))
      ->orderBy(DB::raw('EXTRACT(MONTH FROM expense.date), SUM(expense.amount)'), 'desc')
      ->get()
    ;

    $items = [];
    foreach ($data as $datum) {
      $items[$datum->month][] = new ExpenseItem($datum->category_id, $datum->category_title, $datum->sum);
    }

    return $items;
  }

  /**
   * @inheritDoc
   */
  public function productsByYearAndCategory(int $year, CategoryId $categoryId): array {
    $sql = "SELECT
      p.id,
			p.title, 
			SUM(e.amount) AS sum
		FROM expense AS e
		INNER JOIN product AS p ON p.id = e.product_id
		WHERE TO_CHAR(e.date, 'yyyy') = :year AND p.category_id = :category_id
		GROUP BY p.id
		ORDER BY SUM(e.amount) DESC;";

    $data = DB::select($sql, ['year' => $year, 'category_id' => (string)$categoryId]);

    $items = [];
    foreach ($data as $datum) {
      $items[] = new ExpenseItem($datum->id, $datum->title, $datum->sum);
    }

    return $items;
  }

  /**
   * @inheritDoc
   */
  public function totalByCategoriesForMonth(int $year, int $month_num): array {
    $month = new Month\Month($year, $month_num);

    $month_str = $this->monthFormatter->formattedForSql($month);

    $sql = "SELECT
      c.id, 
			c.title, 
			SUM(e.amount) AS sum
		FROM expense AS e
		INNER JOIN product AS p ON p.id = e.product_id
		INNER JOIN category AS c ON c.id = p.category_id
		WHERE TO_CHAR(e.date, 'yyyy-mm') = '$month_str'
		GROUP BY c.id
		ORDER BY SUM(e.amount) DESC;";

    $data = DB::select($sql);

    $items = [];
    foreach ($data as $datum) {
      $items[] = new ExpenseItem($datum->id, $datum->title, $datum->sum);
    }

    return $items;
  }

  /**
   * @inheritDoc
   */
  public function totalByCategoriesForYear(int $year): array {
    $sql = "SELECT
      c.id,
			c.title, 
			SUM(e.amount) AS sum
		FROM expense AS e
		INNER JOIN product AS p ON p.id = e.product_id
		INNER JOIN category AS c ON c.id = p.category_id
		WHERE TO_CHAR(e.date, 'yyyy') = :year
		GROUP BY c.id
		ORDER BY SUM(e.amount) DESC;";

    $data = DB::select($sql, ['year' => $year]);

    $items = [];
    foreach ($data as $datum) {
      $items[] = new ExpenseItem($datum->id, $datum->title, $datum->sum);
    }

    return $items;
  }

  /**
   * @inheritDoc
   */
  public function totalByMonths(Input $input): array {
    $query = DB::table('expense')
      ->select(
        DB::raw('EXTRACT(MONTH FROM expense.date) AS month'),
        DB::raw('SUM(expense.amount) AS sum')
      )
      ->join('product', 'product.id', '=', 'expense.product_id')
    ;

    $category = $input->category;
    $product = $input->product;

    if ($category) {
      $query = $query->join('category', 'category.id', '=', 'product.category_id');
      $query = $query->where('product.category_id', (string)$category->id);
    }

    if ($product) {
      $query = $query->where('expense.product_id', (string)$product->id);
    }

    $contractor = $input->contractor;
    if ($contractor) {
      $query = $query->where('expense.contractor_id', (string)$contractor->id);
    }

    $year = $input->year;
    if ($year) {
      $query = $query->where(DB::raw("TO_CHAR(expense.date, 'yyyy')"), $year);
    }

    $data = $query
      ->groupBy(DB::raw('EXTRACT(MONTH FROM expense.date)'))
      ->orderBy(DB::raw('EXTRACT(MONTH FROM expense.date)'), 'asc')
      ->get()
    ;

    $items = [];
    foreach ($data as $datum) {
      $month = (int)$datum->month;
      $items[] = new ExpenseItem('', $month, $datum->sum);
    }

    return $items;
  }

  /**
   * @inheritDoc
   */
  public function totalByCategories(): array {
    $result = [];
    $categories = DB::select('SELECT id, title FROM category WHERE parent_id IS NULL');
    foreach ($categories as $category_data) {
      $total = $this->totalByCategory(new CategoryId($category_data->id));
      if (null !== $total) {
        $result[] = new CategoryItem($category_data->id, $category_data->title, $total);
      }
    }
    $this->sortAmountDesc($result);
    return $result;
  }

  /**
   * @inheritDoc
   */
  public function totalByCategory(CategoryId $categoryId): ?float {
    $category_id = $categoryId->getId();
    $sql = "WITH RECURSIVE d AS (
          SELECT id, title FROM category WHERE id = '$category_id'
          UNION ALL
          SELECT c.id, c.title FROM d INNER JOIN category AS c ON c.parent_id = d.id
        )
        SELECT
          SUM(e.amount) AS total
        FROM d
          INNER JOIN product AS p ON p.category_id = d.id
          INNER JOIN expense AS e ON e.product_id = p.id
        ;";
    $data = DB::select($sql);

    if (empty($data[0])) {
      return null;
    }

    return (float)$data[0]->total;
  }

  private function sortAmountDesc(array &$items): void {
    usort($items, static function ($item1, $item2) {
      if ($item1->amount === $item2->amount) {
        return 0;
      }

      return $item1->amount > $item2->amount ? -1 : 1;
    });
  }

  /**
   * @inheritDoc
   */
  public function totalByCategoryChildren(CategoryId $categoryId): array {
    $category_id = $categoryId->getId();
    $result = [];
    $categories = DB::select("SELECT id, title FROM category WHERE parent_id = '$category_id'");
    if (empty($categories)) {
      $sql = "SELECT
        p.id,
        p.title,
        SUM(e.amount) AS amount
      FROM expense AS e
      INNER JOIN product AS p ON p.id = e.product_id
      WHERE p.category_id = '$category_id'
      GROUP BY (p.id, p.title)
      ORDER BY SUM(e.amount) DESC, p.title ASC";

      $data = DB::select($sql);
      foreach ($data as $datum) {
        $result[] = new ProductItem($datum->id, $datum->title, $datum->amount);
      }
    }
    else {
      foreach ($categories as $category_data) {
        $total = $this->totalByCategory(new CategoryId($category_data->id));
        if (null !== $total) {
          $result[] = new CategoryItem($category_data->id, $category_data->title, $total);
        }
      }
      $this->sortAmountDesc($result);
    }

    return $result;
  }

  /**
   * @inheritDoc
   */
  public function totalsByRoot(): array {
    // Надо сбрасывать, если:
    // добавилась/удалилась/изменилась корневая категория
    // добавился/удалился/изменился расход
    if (!StaticCache::has(self::TOTALS)) {
      $roots_data = DB::select('SELECT id, title FROM category WHERE parent_id IS NULL');
      $totals = [];
      foreach ($roots_data as $root_data) {
        $sql = "WITH RECURSIVE d AS (
          SELECT id, title FROM category WHERE id = '$root_data->id'
          UNION ALL
          SELECT c.id, c.title FROM d INNER JOIN category AS c ON c.parent_id = d.id
        )
        SELECT
          SUM(e.amount) AS total
        FROM expense AS e
        INNER JOIN product AS p ON p.id = e.product_id
        INNER JOIN d ON d.id = p.category_id;";
        $data = DB::select($sql);
        $totals[] = new ExpenseItem($root_data->id, $root_data->title, !empty($data[0]) ? (float)$data[0]->total : 0.0);
      }
      usort($totals, function (ExpenseItem $l, ExpenseItem $r) {
        return $l->amount <=> $r->amount;
      });
      StaticCache::set(self::TOTALS, $totals);
    }
    else {
      $totals = StaticCache::get(self::TOTALS);
    }

    return $totals;
  }
}
