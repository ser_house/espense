<?php
declare(strict_types = 1);

namespace App\Application\Analytics\ByTime;

use App\Core\Analytics\ByTime\IByTimeDataProvider;
use App\Core\Analytics\ByTime\Result;
use App\Core\Analytics\TimeLine\TimeLineBuilder;
use App\Core\Analytics\TimeLine\TimeLineKeyWithMonthNamesGenerator;
use App\Core\Category\CategoryId;
use App\Core\Service\DateGenerator;
use DB;

class GroupByMonthsSumByPeriodsDataProvider extends BaseDataProvider implements IByTimeDataProvider {
  public function __construct(protected CategoryId $categoryId, private array $years) {
    parent::__construct($this->categoryId);
  }

  /**
   * @inheritDoc
   */
  public function get(): Result {
    $years_str = implode(',', $this->years);
    $years_condition = '';
    if ($years_str !== 'any') {
      $years_condition = "AND EXTRACT(YEAR FROM e.date) IN ($years_str)";
    }

    $category_id = (string)$this->categoryId;
    $category_tree_sql = "WITH RECURSIVE d AS (
      SELECT id
      FROM category
      WHERE id = '$category_id'
      UNION
      SELECT c.id
      FROM category AS c
           INNER JOIN d ON c.parent_id = d.id
    )
    SELECT id FROM d";
    $sql = "SELECT
        EXTRACT(YEAR FROM e.date) AS year,
        EXTRACT(MONTH FROM e.date) AS month,
        SUM(e.amount) AS total
      FROM expense AS e
      INNER JOIN product AS p ON p.id = e.product_id
      WHERE p.category_id IN ($category_tree_sql) $years_condition
      GROUP BY EXTRACT(MONTH FROM e.date), EXTRACT(YEAR FROM e.date)
      ORDER BY EXTRACT(YEAR FROM e.date), EXTRACT(MONTH FROM e.date)";
    $db_data = DB::select($sql);

    $keyGenerator = new TimeLineKeyWithMonthNamesGenerator();
    $timeLineBuilder = new TimeLineBuilder($keyGenerator);
    $timeLineMonths = $timeLineBuilder->buildMonthsFromStdObjectsList($db_data);
    $data = $timeLineMonths->initialTimeLine();

    foreach ($db_data as $db_datum) {
      $key = $keyGenerator->generateByObject($db_datum);
      $data[$key] = $db_datum->total;
    }

    return new Result(array_values($data), array_keys($data));
  }
}
