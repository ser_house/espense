<?php

namespace App\Application\Analytics\ByTime;

use App\Core\Category\CategoryId;


abstract class BaseDataProvider {
  public function __construct(protected CategoryId $categoryId) {
  }

}
