<?php

namespace App\Application\Analytics\ByTime;

use App\Core\Analytics\ByTime\IByTimeDataProvider;
use App\Core\Analytics\ByTime\Result;
use App\Core\Product\ProductId;
use DB;

class GroupByYearSumByProductDataProvider implements IByTimeDataProvider {
  public function __construct(private ProductId $productId) {

  }

  /**
   * @inheritDoc
   */
  public function get(): Result {
    $product_id = (string)$this->productId;
    $sql = "SELECT
        EXTRACT(YEAR FROM e.date) AS year,
        SUM(e.amount) AS total
      FROM expense AS e
      INNER JOIN product AS p ON p.id = e.product_id
      WHERE e.product_id = '$product_id'
      GROUP BY EXTRACT(YEAR FROM e.date)
      ORDER BY EXTRACT(YEAR FROM e.date)";
    $db_data = DB::select($sql);

    $categories = [];
    foreach ($db_data as $db_datum) {
      $categories[$db_datum->year] = $db_datum->year;
    }
    $min_year = min($categories);
    $max_year = max($categories);
    $categories = range($min_year, $max_year);

    $data = [];
    foreach ($db_data as $db_datum) {
      $data[] = $db_datum->total;
    }

    return new Result($data, $categories);
  }
}
