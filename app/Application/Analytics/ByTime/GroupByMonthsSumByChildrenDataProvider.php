<?php
declare(strict_types = 1);

namespace App\Application\Analytics\ByTime;

use App\Core\Analytics\ByTime\IByTimeDataProvider;
use App\Core\Analytics\ByTime\Result;
use App\Core\Category\CategoryId;
use App\Core\Service\DateGenerator;
use DateTimeImmutable;
use DB;

class GroupByMonthsSumByChildrenDataProvider extends BaseDataProvider implements IByTimeDataProvider {
  public function __construct(protected CategoryId $categoryId, private array $years) {
    parent::__construct($this->categoryId);
  }

  /**
   * @inheritDoc
   */
  public function get(): Result {
    $years_str = implode(',', $this->years);
    $years_condition = '';
    if ($years_str !== 'any') {
      $years_condition = "AND EXTRACT(YEAR FROM e.date) IN ($years_str)";
    }

    $category_id = (string)$this->categoryId;
    $category_tree_sql = "WITH RECURSIVE d AS (
      SELECT id
      FROM category
      WHERE id = '$category_id'
      UNION
      SELECT c.id
      FROM category AS c
           INNER JOIN d ON c.parent_id = d.id
    )
    SELECT id FROM d";
    $sql = "SELECT
        p.title,
        EXTRACT(YEAR FROM e.date) AS year,
        EXTRACT(MONTH FROM e.date) AS month,
        SUM(e.amount) AS total
      FROM expense AS e
      INNER JOIN product AS p ON p.id = e.product_id
      WHERE p.category_id IN($category_tree_sql) $years_condition
      GROUP BY p.title, EXTRACT(MONTH FROM e.date), EXTRACT(YEAR FROM e.date)
      ORDER BY p.title, EXTRACT(YEAR FROM e.date), EXTRACT(MONTH FROM e.date)";
    $db_data = DB::select($sql);


    $data = [];

    $today = new DateTimeImmutable();
    $this_year = (int)$today->format('Y');
    $this_month = (int)$today->format('m');

    $by_year = [];
    foreach ($this->years as $year) {
      for ($month = 1; $month < 13; $month++) {
        if ((int)$year === $this_year && $month === $this_month) {
          break;
        }
        $by_year["$year-$month"] = null;
      }
    }

    foreach ($db_data as $db_datum) {
      $data[$db_datum->title] = $by_year;
    }

    foreach ($db_data as $db_datum) {
      $data[$db_datum->title]["$db_datum->year-$db_datum->month"] = $db_datum->total;
    }

    foreach ($data as &$values) {
      $values = array_values($values);
    }
    unset($values);

    return new Result($data, array_keys($by_year));


    $data = [];

    $month_names = DateGenerator::SHORT_MONTH_NAMES_RU;
//    $months = array_fill(1, 12, null);
//    foreach ($db_data as $db_datum) {
//      $data[$db_datum->title] = $months;
//    }

    foreach ($db_data as $db_datum) {
      $month = $month_names[$db_datum->month - 1];
      $data["$db_datum->year-$month"] = $db_datum->total;
    }

//    foreach ($data as &$values) {
//      $values = array_values($values);
//    }
//    unset($values);

    return new Result(array_values($data), array_keys($data));
  }

}
