<?php

namespace App\Application\Analytics\ByTime;

use App\Core\Analytics\ByTime\IByTimeDataProvider;
use App\Core\Analytics\ByTime\Result;
use DB;

class GroupByYearSumByPeriodsDataProvider extends BaseDataProvider implements IByTimeDataProvider {
  /**
   * @inheritDoc
   */
  public function get(): Result {
    $category_id = (string)$this->categoryId;
    $category_tree_sql = "WITH RECURSIVE d AS (
      SELECT id
      FROM category
      WHERE id = '$category_id'
      UNION
      SELECT c.id
      FROM category AS c
           INNER JOIN d ON c.parent_id = d.id
    )
    SELECT id FROM d";
    $sql = "SELECT
        EXTRACT(YEAR FROM e.date) AS year,
        SUM(e.amount) AS total
      FROM expense AS e
      INNER JOIN product AS p ON p.id = e.product_id
      WHERE p.category_id IN ($category_tree_sql)
      GROUP BY EXTRACT(YEAR FROM e.date)
      ORDER BY EXTRACT(YEAR FROM e.date)";
    $db_data = DB::select($sql);

    $categories = [];
    foreach ($db_data as $db_datum) {
      $categories[$db_datum->year] = $db_datum->year;
    }
    $min_year = min($categories);
    $max_year = max($categories);
    $categories = range($min_year, $max_year);

    $data = [];
    foreach ($db_data as $db_datum) {
      $data[] = $db_datum->total;
    }

    return new Result($data, $categories);
  }
}
