<?php

namespace App\Application\Analytics\ByTime;

use App\Core\Analytics\ByTime\IByTimeDataProvider;
use App\Core\Analytics\ByTime\Result;
use DB;

class GroupByYearSumByChildrenDataProvider extends BaseDataProvider implements IByTimeDataProvider {
  /**
   * @inheritDoc
   */
  public function get(): Result {
    $category_id = (string)$this->categoryId;
    $category_tree_sql = "WITH RECURSIVE d AS (
      SELECT id
      FROM category
      WHERE id = '$category_id'
      UNION
      SELECT c.id
      FROM category AS c
           INNER JOIN d ON c.parent_id = d.id
    )
    SELECT id FROM d";
    $sql = "SELECT
        p.title,
        EXTRACT(YEAR FROM e.date) AS year,
        SUM(e.amount) AS total
      FROM expense AS e
      INNER JOIN product AS p ON p.id = e.product_id
      WHERE p.category_id IN ($category_tree_sql)
      GROUP BY p.title, EXTRACT(YEAR FROM e.date)
      ORDER BY p.title, EXTRACT(YEAR FROM e.date)";
    $db_data = DB::select($sql);

    $titles = [];
    $years = [];
    foreach ($db_data as $db_datum) {
      $years[$db_datum->year] = $db_datum->year;
      $titles[$db_datum->title] = $db_datum->title;
    }
    $min_year = min($years);
    $max_year = max($years);
    $categories = range($min_year, $max_year);

    $data = [];
    foreach ($db_data as $db_datum) {
      if (!isset($data[$db_datum->year])) {
        $data[$db_datum->year] = [];
        foreach ($titles as $title) {
          $data[$db_datum->year][$title] = 0.0;
        }
      }
      $data[$db_datum->year][$db_datum->title] = (float)$db_datum->total;
    }
    ksort($data);

    $series = [];
    foreach ($data as $year => $items) {
      foreach ($items as $title => $value) {
        if (!isset($series[$title])) {
          $series[$title] = [];
        }
        $series[$title][] = $value;
      }
    }

    return new Result($series, $categories);
  }
}
