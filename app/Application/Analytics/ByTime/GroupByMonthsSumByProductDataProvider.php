<?php
declare(strict_types = 1);

namespace App\Application\Analytics\ByTime;

use App\Core\Analytics\ByTime\IByTimeDataProvider;
use App\Core\Analytics\ByTime\Result;
use App\Core\Analytics\TimeLine\TimeLineBuilder;
use App\Core\Analytics\TimeLine\TimeLineKeyWithMonthNamesGenerator;
use App\Core\Product\ProductId;
use DB;

class GroupByMonthsSumByProductDataProvider implements IByTimeDataProvider {
  public function __construct(private array $years, private ProductId $productId) {

  }

  /**
   * @inheritDoc
   */
  public function get(): Result {
    $years_str = implode(',', $this->years);
    $product_id = (string)$this->productId;

    $years_condition = '';
    if ($years_str !== 'any') {
      $years_condition = "AND EXTRACT(YEAR FROM e.date) IN ($years_str)";
    }
    $sql = "SELECT
        EXTRACT(YEAR FROM e.date) AS year,
        EXTRACT(MONTH FROM e.date) AS month,
        SUM(e.amount) AS total
      FROM expense AS e
      INNER JOIN product AS p ON p.id = e.product_id
      WHERE p.id = '$product_id' $years_condition
      GROUP BY EXTRACT(MONTH FROM e.date), EXTRACT(YEAR FROM e.date)
      ORDER BY EXTRACT(YEAR FROM e.date), EXTRACT(MONTH FROM e.date)";
    $db_data = DB::select($sql);

    $keyGenerator = new TimeLineKeyWithMonthNamesGenerator();
    $timeLineBuilder = new TimeLineBuilder($keyGenerator);
    $timeLineMonths = $timeLineBuilder->buildMonthsFromStdObjectsList($db_data);
    $data = $timeLineMonths->initialTimeLine();

    foreach ($db_data as $db_datum) {
      $key = $keyGenerator->generateByObject($db_datum);
      $data[$key] = $db_datum->total;
    }

    return new Result(array_values($data), array_keys($data));
  }
}
