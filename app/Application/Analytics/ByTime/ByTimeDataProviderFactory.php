<?php

namespace App\Application\Analytics\ByTime;

use App\Core\Analytics\ByTime\IByTimeDataProvider;
use App\Core\Analytics\ByTime\IByTimeDataProviderFactory;
use App\Core\Analytics\ByTime\Params;

class ByTimeDataProviderFactory implements IByTimeDataProviderFactory {

  /**
   * @inheritDoc
   */
  public function get(Params $params): IByTimeDataProvider {
    if ($params->isGroupByYear()) {
      if ($params->productId()) {
        return new GroupByYearSumByProductDataProvider($params->productId());
      }
      if ($params->isSumByPeriods()) {
        return new GroupByYearSumByPeriodsDataProvider($params->categoryId());
      }

      return new GroupByYearSumByChildrenDataProvider($params->categoryId());
    }
    if ($params->productId()) {
      return new GroupByMonthsSumByProductDataProvider($params->years(), $params->productId());
    }
    if ($params->isSumByPeriods()) {
      return new GroupByMonthsSumByPeriodsDataProvider($params->categoryId(), $params->years());
    }
    return new GroupByMonthsSumByChildrenDataProvider($params->categoryId(), $params->years());
  }
}
