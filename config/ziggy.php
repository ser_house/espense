<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.02.2020
 * Time: 9:40
 */

return [
  'whitelist' => ['api.*'],
  'blacklist' => ['debugbar.*', 'horizon.*', 'admin.*'],
];
