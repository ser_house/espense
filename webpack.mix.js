const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
  .js('resources/js/page/base.js', 'public/js/page')
  .js('resources/js/page/expense/main.js', 'public/js/page/expense')
  .js('resources/js/page/expense/journal_month.js', 'public/js/page/expense')
  .js('resources/js/page/expense/journal_ext.js', 'public/js/page/expense')
  .js('resources/js/page/analytics/by_categories.js', 'public/js/page/analytics')
  .js('resources/js/page/analytics/by_time.js', 'public/js/page/analytics')
  .js('resources/js/page/analytics/month.js', 'public/js/page/analytics')
  .js('resources/js/page/analytics/year.js', 'public/js/page/analytics')
  .js('resources/js/page/manage/contractors.js', 'public/js/page/manage')
  .js('resources/js/page/manage/accounts.js', 'public/js/page/manage')
  .js('resources/js/page/manage/units.js', 'public/js/page/manage')
  .js('resources/js/page/manage/products.js', 'public/js/page/manage')
  .js('resources/js/page/tracking/current.js', 'public/js/page/tracking')
  .js('resources/js/page/tracking/to_tracking.js', 'public/js/page/tracking')
;

mix.copy('resources/js/utils/tooltip.js', 'public/js/utils');
mix.copy('resources/js/utils/collapsible.js', 'public/js/utils');
mix.copy('resources/js/utils/menu.js', 'public/js/utils')
;

mix.vue();

mix.disableNotifications();

if (!mix.inProduction()) {
  mix.sourceMaps();
}

mix.sass('resources/sass/app.scss', 'public/css')
  .sass('resources/sass/manage.scss', 'public/css')
  .sass('resources/sass/calendar.scss', 'public/css')
;

mix.webpackConfig({
  stats: {
    children: true,
  },
});

mix.version();
