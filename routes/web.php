<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Expense;
use App\Http\Controllers\Settings;
use App\Http\Controllers\Manage;
use App\Http\Controllers\Analytics;
use App\Http\Controllers\Tracking;
use App\Http\Controllers\Transactions;
use App\Http\Controllers\Accounts;
use App\Http\Controllers;

Route::get('/', [Expense\Main::class, 'page'])->name('expense.page');

Route::get('/settings', [Settings\Main::class, 'page'])->name('settings.page');
Route::post('/settings-save', [Settings\Main::class, 'save'])->name('settings.save');

Route::get('/icons', Controllers\Icons::class)->name('icons');

Route::prefix('expenses')->group(function () {
  Route::post('/', function () {
    redirect('expense.page');
  });

  Route::match(['get', 'post'], '/journal-month', Expense\JournalMonth::class)->name('expense.journal_month');
  Route::match(['get', 'post'], '/journal-ext', Expense\JournalExt::class)->name('expense.journal_ext');
});


Route::prefix('manage')->group(function () {
  Route::get('/products', [Manage\Products::class, 'page'])->name('manage.products');
  Route::get('/categories-list', [Manage\CategoriesList::class, 'page'])->name('manage.categories_list');
  Route::get('/contractors', [Manage\Contractors::class, 'page'])->name('manage.contractors');
  Route::get('/units', [Manage\Units::class, 'page'])->name('manage.units');
  Route::get('/accounts', [Manage\Accounts::class, 'page'])->name('manage.accounts');
});

Route::prefix('analytics')->group(function () {
  Route::get('/', Analytics\ByCategories::class)->name('analytics');
  Route::get('/by-time', Analytics\ByTime::class)->name('analytics.by_time');
  Route::get('/month', Analytics\Month::class)->name('analytics.month');
  Route::get('/year', Analytics\Year::class)->name('analytics.year');
});

Route::prefix('tracking')->group(function () {
  Route::get('/', Tracking\Current::class)->name('tracking');
  Route::get('/finished', Tracking\Finished::class)->name('tracking.finished');
  Route::get('/to-tracking', Tracking\ToTrackingExpenses::class)->name('tracking.to_tracking');
});

Route::prefix('transactions')->group(function () {
  Route::get('/', Transactions\Main::class)->name('transactions');
});

Route::prefix('accounts')->group(function () {
  Route::get('/', Accounts\Main::class)->name('accounts');
});
