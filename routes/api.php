<?php


use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use App\Http\Api;

Route::middleware('json_unicode')->group(function () {

  Route::prefix('products')->group(function () {
    Route::post('/', [Api\Product::class, 'add'])->name('api.product.add');
    Route::get('/{id}', [Api\Product::class, 'show'])->name('api.product.show');
    Route::get('/', [Api\Product::class, 'find'])->name('api.products.find');
    Route::put('/{id}/rename', [Api\Product::class, 'rename'])->name('api.product.rename');
    Route::put('/{id}/move-to-category', [Api\Product::class, 'moveToCategory'])->name('api.product.move_to_category');
    Route::delete('/{id}', [Api\Product::class, 'remove'])->name('api.product.remove');
  });

  Route::prefix('tracking')->group(function () {
    Route::post('/', [Api\Tracking::class, 'add'])->name('api.tracking.add');
    Route::post('/{id}/finish', [Api\Tracking::class, 'finish'])->name('api.tracking.finish');
    Route::post('/{id}/update-qty', [Api\Tracking::class, 'updateQty'])->name('api.tracking.update_qty');
    Route::post('/add-expense-to-tracking/{id}', [Api\Tracking::class, 'addExpenseToTracking'])->name('api.tracking.add_expense_to_tracking');
    Route::delete('/remove-expense-from-to-tracking/{id}', [Api\Tracking::class, 'removeExpenseFromToTracking'])->name('api.tracking.remove_expense_from_to_tracking');
  });

  Route::prefix('categories')->group(function () {
    Route::post('/', [Api\Category::class, 'add'])->name('api.category.add');
    Route::get('/{id}', [Api\Category::class, 'show'])->name('api.category.show');
    Route::get('/', [Api\Category::class, 'find'])->name('api.categories.find');
    Route::put('/{id}/rename', [Api\Category::class, 'rename'])->name('api.category.rename');
    Route::put('/{id}/move-to-category', [Api\Category::class, 'moveToCategory'])->name('api.category.move_to_category');
    Route::delete('/{id}', [Api\Category::class, 'remove'])->name('api.category.remove');
  });

  Route::prefix('contractors')->group(function () {
    Route::post('/', [Api\Contractor::class, 'add'])->name('api.contractor.add');
    Route::get('/{id}', [Api\Contractor::class, 'show'])->name('api.contractor.show');
    Route::get('/', [Api\Contractor::class, 'find'])->name('api.contractors.find');
    Route::put('/{id}/change', [Api\Contractor::class, 'change'])->name('api.contractor.change');
    Route::delete('/{id}', [Api\Contractor::class, 'remove'])->name('api.contractor.remove');
  });

  Route::prefix('expenses')->group(function () {
    Route::post('/', [Api\Expense::class, 'add'])->name('api.expense.add');
    Route::get('/{id}', [Api\Expense::class, 'show'])->name('api.expense.show');
    Route::get('/', [Api\Expense::class, 'find'])->name('api.expense.find');
    Route::put('/{id}', [Api\Expense::class, 'update'])->name('api.expense.update');
    Route::post('/{id}/split', [Api\Expense::class, 'split'])->name('api.expense.split');
    Route::put('/{id}/add-to-favorites', [Api\Expense::class, 'addToFavorites'])->name('api.expense.add_to_favorites');
    Route::put('/{id}/remove-from-favorites', [Api\Expense::class, 'removeFromFavorites'])->name('api.expense.remove_from_favorites');
    Route::delete('/{id}', [Api\Expense::class, 'remove'])->name('api.expense.remove');
    Route::put('/set-unit/{unit_id}', [Api\Expense::class, 'setUnit'])->name('api.expense.set_unit');
  });

  Route::prefix('units')->group(function () {
    Route::post('/', [Api\Unit::class, 'add'])->name('api.unit.add');
    Route::get('/{id}', [Api\Unit::class, 'show'])->name('api.unit.show');
    Route::put('/{id}/rename', [Api\Unit::class, 'rename'])->name('api.unit.rename');
    Route::delete('/{id}', [Api\Unit::class, 'remove'])->name('api.unit.remove');
  });

  Route::prefix('analytics')->group(function () {
    Route::get('/by-category/{id}', Api\Analytics\ByCategory::class)->name('api.analytics.by_category');
    Route::get('/by-time', Api\Analytics\ByTime::class)->name('api.analytics.by_time');
  });

  Route::prefix('accounts')->group(function () {
    Route::post('/', [Api\Account::class, 'add'])->name('api.account.add');
    Route::get('/{id}', [Api\Account::class, 'show'])->name('api.account.show');
    Route::get('/', [Api\Account::class, 'find'])->name('api.account.find');
    Route::put('/{id}', [Api\Account::class, 'rename'])->name('api.account.rename');
    Route::put('/{id}/set-contractor', [Api\Account::class, 'setContractor'])->name('api.account.set_contractor');
    Route::put('/{id}/clear-contractor', [Api\Account::class, 'clearContractor'])->name('api.account.clear_contractor');
    Route::delete('/{id}', [Api\Account::class, 'remove'])->name('api.account.remove');
  });

  Route::prefix('transactions')->group(function () {
    Route::post('/', [Api\Transaction::class, 'add'])->name('api.transaction.add');
    Route::get('/{id}', [Api\Transaction::class, 'show'])->name('api.transaction.show');
    Route::get('/', [Api\Transaction::class, 'find'])->name('api.transaction.find');
    Route::put('/{id}', [Api\Transaction::class, 'update'])->name('api.transaction.update');
    Route::delete('/{id}', [Api\Transaction::class, 'remove'])->name('api.transaction.remove');
  });
});
