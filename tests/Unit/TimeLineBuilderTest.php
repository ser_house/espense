<?php

namespace Tests\Unit;

use App\Core\Analytics\TimeLine\TimeLineBuilder;
use App\Core\Analytics\TimeLine\TimeLineKeyWithMonthNamesGenerator;
use PHPUnit\Framework\TestCase;

class TimeLineBuilderTest extends TestCase {
  public function testMonths() {
    $first_year = 2000;
    $first_month = 5;
    $last_year = 2002;
    $last_month = 11;

    // Месяцев для полных лет между последним и первым годами, если такие есть.
    $full_year_months_count = 0;
    if ($last_year - $first_year > 1) {
      $full_year_months_count = (12 * ($last_year - $first_year - 1));
    }

    // Для первого года - оставшиеся до конца года месяцы, включая сам начальный месяц.
    $items_count = (12 - $first_month + 1) + $last_month + $full_year_months_count;

    $keyGenerator = new TimeLineKeyWithMonthNamesGenerator();
    $bulder = new TimeLineBuilder($keyGenerator);
    $result = $bulder->buildMonths($first_year, $first_month, $last_year, $last_month);

    $first_item = $result->first();
    $last_item = $result->last();

    self::assertEquals($items_count, count($result->items()));
    self::assertEquals($first_year, $first_item->year);
    self::assertEquals($first_month, $first_item->month);
    self::assertEquals($last_year, $last_item->year);
    self::assertEquals($last_month, $last_item->month);
  }

}
