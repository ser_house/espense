<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.02.2020
 * Time: 12:07
 */

namespace Tests\Feature;


use App\Core\Expense\Entity\ExpenseId;
use App\Core\Service\ICacheFlusher;
use App\Models\Expense;
use DateTimeImmutable;
use Illuminate\Http\Response;
use Database\Seeders\ProductSeeder;
use Database\Seeders\CategorySeeder;
use Database\Seeders\UnitSeeder;
use Tests\TestCaseDb;

class ExpenseApiTest extends TestCaseDb {

  private const ADD_ROUTE = 'api.expense.add';
  private const UPDATE_ROUTE = 'api.expense.update';
  private const SPLIT_ROUTE = 'api.expense.split';
  private const REMOVE_ROUTE = 'api.expense.remove';
  private const SET_UNIT_ROUTE = 'api.expense.set_unit';

  private ICacheFlusher $cacheFlusher;
  private Expense $baseExpenseModel;

  protected function setUp(): void {
    parent::setUp();

    $this->cacheFlusher = $this->app->make(ICacheFlusher::class);

    $expenseId = new ExpenseId();

    $today = new DateTimeImmutable();
    $this->baseExpenseModel = Expense::create([
      'id' => (string)$expenseId,
      'date' => $today->format('Y-m-d'),
      'product_id' => ProductSeeder::PRODUCT1['id'],
      'unit_id' => UnitSeeder::UNIT1['id'],
      'quantity' => '1',
      'amount' => 1000,
    ]);
  }

  protected function tearDown(): void {
    $this->cacheFlusher->clear();

    parent::tearDown();
  }


  public function testAdd(): void {
    $data = [
      'date' => date('Y-m-d'),
      'product_id' => ProductSeeder::PRODUCT1['id'],
      'quantity' => '1',
      'unit_id' => UnitSeeder::UNIT1['id'],
      'amount' => '1000,56',
    ];
    $response = $this->postJson(route(self::ADD_ROUTE), $data);
    $response->assertStatus(Response::HTTP_CREATED);
    $response->assertJson(['type' => 'success']);
  }

  public function testUpdate(): void {
    $data = [
      'date' => date('Y-m-d'),
      'product_id' => ProductSeeder::PRODUCT1['id'],
      'quantity' => '1',
      'amount' => '2000',
    ];
    $response = $this->putJson(route(self::UPDATE_ROUTE, ['id' => $this->baseExpenseModel->id]), $data);
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['type' => 'success']);
  }

  public function testSplit(): void {
    $data = [
      'expenses' => [
        [
          'product_id' => ProductSeeder::PRODUCT1['id'],
          'quantity' => 1,
          'amount' => 500,
        ],
        [
          'product_id' => ProductSeeder::PRODUCT2['id'],
          'quantity' => 1,
          'amount' => 500,
        ],
      ],
    ];
    $response = $this->postJson(route(self::SPLIT_ROUTE, ['id' => $this->baseExpenseModel->id]), $data);
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['type' => 'success']);
  }

  public function testSplitNotEqualsAmounts(): void {
    $data = [
      'expenses' => [
        [
          'product_id' => ProductSeeder::PRODUCT1['id'],
          'quantity' => 1,
          'amount' => 500,
        ],
        [
          'product_id' => ProductSeeder::PRODUCT2['id'],
          'quantity' => 1,
          'amount' => 300,
        ],
      ],
    ];

    $response = $this->postJson(route(self::SPLIT_ROUTE, ['id' => $this->baseExpenseModel->id]), $data);
    $response->assertStatus(Response::HTTP_BAD_REQUEST);
    $response->assertJson(['type' => 'error']);

    $data = [
      'expenses' => [
        [
          'product_id' => ProductSeeder::PRODUCT1['id'],
          'quantity' => 1,
          'amount' => -500,
        ],
        [
          'product_id' => ProductSeeder::PRODUCT2['id'],
          'quantity' => 1,
          'amount' => 1500,
        ],
      ],
    ];

    $response = $this->postJson(route(self::SPLIT_ROUTE, ['id' => $this->baseExpenseModel->id]), $data);
    $response->assertStatus(Response::HTTP_BAD_REQUEST);
    $response->assertJson(['type' => 'error']);
  }

  public function testRemove(): void {
    $response = $this->deleteJson(route(self::REMOVE_ROUTE, ['id' => $this->baseExpenseModel->id]));
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['type' => 'success']);
  }

  public function testSetUnitAll(): void {
    $unit_id = UnitSeeder::UNIT1['id'];

    $response = $this->putJson(route(self::SET_UNIT_ROUTE, ['unit_id' => $unit_id]));
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['type' => 'success']);

    $data = \DB::select("SELECT * FROM expense WHERE unit_id != '$unit_id'");
    self::assertEmpty($data);
  }

  public function testSetUnitByProduct(): void {
    $unit_id = UnitSeeder::UNIT1['id'];

    $product_id = ProductSeeder::PRODUCT3['id'];
    $response = $this->putJson(route(self::SET_UNIT_ROUTE, ['unit_id' => $unit_id, 'product_id' => $product_id]));
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['type' => 'success']);

    $data = \DB::select("SELECT * FROM expense WHERE unit_id != '$unit_id' AND product_id = '$product_id'");
    self::assertEmpty($data);
  }

  public function testSetUnitByCategory(): void {
    $unit_id = UnitSeeder::UNIT1['id'];

    $category_id = CategorySeeder::CATEGORY_CATEGORY['id'];
    $response = $this->putJson(route(self::SET_UNIT_ROUTE, ['unit_id' => $unit_id, 'category_id' => $category_id]));
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['type' => 'success']);

    $data = \DB::select("SELECT e.*
    FROM expense AS e
    INNER JOIN product AS p ON p.id = e.product_id
    WHERE e.unit_id != '$unit_id' AND p.category_id = '$category_id'");
    self::assertEmpty($data);
  }
}
