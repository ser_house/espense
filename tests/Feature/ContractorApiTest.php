<?php

namespace Tests\Feature;

use Database\Seeders\ContractorSeeder;
use Illuminate\Http\Response;
use Tests\TestCaseDb;

class ContractorApiTest extends TestCaseDb {
  private const SEARCH_ROUTE = 'api.contractors.find';
  private const ADD_ROUTE = 'api.contractor.add';
  private const SHOW_ROUTE = 'api.contractor.show';
  private const CHANGE_ROUTE = 'api.contractor.change';
  private const REMOVE_ROUTE = 'api.contractor.remove';

  private const TEST_BY_TITLE_DATA = [
    'ко' => [
      ContractorSeeder::CONTRACTOR1,
      ContractorSeeder::CONTRACTOR2,
      ContractorSeeder::CONTRACTOR5,
      ContractorSeeder::CONTRACTOR6,
    ],
  ];

  public function testSearchByQuery(): void {
    foreach (self::TEST_BY_TITLE_DATA as $search => $result) {
      $response = $this->getJson(route(self::SEARCH_ROUTE, ['like' => $search]));
      $response->assertStatus(Response::HTTP_OK);

      $this->sort($result);
      $response->assertJson($result);
    }
  }

  public function testMissingSearchString(): void {
    $response = $this->getJson(route(self::SEARCH_ROUTE, []));
    $response->assertStatus(Response::HTTP_BAD_REQUEST);
    $response->assertJson(['type' => 'error']);
  }

  public function testAddSuccess(): void {
    $data = [
      'title' => 'New contractor title',
    ];
    $response = $this->postJson(route(self::ADD_ROUTE), $data);
    $response->assertStatus(Response::HTTP_CREATED);
    $response->assertJson(['type' => 'success']);
  }

  public function testAddButExists(): void {
    $data = [
      'title' => ContractorSeeder::CONTRACTOR1['title'],
    ];
    $response = $this->postJson(route(self::ADD_ROUTE), $data);
    $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    $response->assertJson(['type' => 'exists']);
  }

  public function testShowSuccess(): void {
    $id = ContractorSeeder::CONTRACTOR1['id'];

    $response = $this->getJson(route(self::SHOW_ROUTE, ['id' => $id]));
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['type' => 'success']);
  }

  public function testChangeSuccess(): void {
    $id = ContractorSeeder::CONTRACTOR1['id'];

    $response = $this->getJson(route(self::SHOW_ROUTE, ['id' => $id]));
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson([
      'item' => [
        'title' => ContractorSeeder::CONTRACTOR1['title'],
      ],
    ]);

    $data = [
      'title' => 'Contractor',
    ];
    $response = $this->putJson(route(self::CHANGE_ROUTE, ['id' => $id]), $data);
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['type' => 'success']);

    $response = $this->getJson(route(self::SHOW_ROUTE, ['id' => $id]));
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson([
      'item' => [
        'title' => 'Contractor',
      ],
    ]);
  }

  public function testDeleteSuccess(): void {
    $id = ContractorSeeder::CONTRACTOR3['id'];

    $response = $this->deleteJson(route(self::REMOVE_ROUTE, ['id' => $id]));
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['type' => 'success']);
  }

  public function testDeleteFileContractorBusy(): void {
    $id = ContractorSeeder::CONTRACTOR2['id'];

    $response = $this->deleteJson(route(self::REMOVE_ROUTE, ['id' => $id]));
    $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    $response->assertJson(['type' => 'error']);
  }
}
