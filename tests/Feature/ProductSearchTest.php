<?php

namespace Tests\Feature;

use Database\Seeders\CategorySeeder;
use Illuminate\Http\Response;
use Database\Seeders\ProductSeeder;
use Tests\TestCaseDb;

class ProductSearchTest extends TestCaseDb {
  private const SEARCH_ROUTE = 'api.products.find';

  private const TEST_BY_PRODUCT_TITLE_DATA = [
    'ко' => [
      ProductSeeder::PRODUCT1,
      ProductSeeder::PRODUCT2,
      ProductSeeder::PRODUCT5,
      ProductSeeder::PRODUCT6,
    ],
  ];

  private const TEST_BY_CATEGORY_TITLE_DATA = [
    CategorySeeder::CATEGORY_CATEGORY['title'] => [
      ProductSeeder::PRODUCT1,
      ProductSeeder::PRODUCT2,
      ProductSeeder::PRODUCT3,
      ProductSeeder::PRODUCT4,
    ],
    CategorySeeder::CATEGORY_STRATEGY['title'] => [
      ProductSeeder::PRODUCT5,
      ProductSeeder::PRODUCT6,
      ProductSeeder::PRODUCT7,
      ProductSeeder::PRODUCT8,
    ],
  ];

  private const TEST_BY_CATEGORY_AND_PRODUCT_DATA = [
    [
      'like' => 'ко',
      'category_title' => CategorySeeder::CATEGORY_CATEGORY['title'],
      'products' => [
        ProductSeeder::PRODUCT1,
        ProductSeeder::PRODUCT2,
      ],
    ],
    [
      'like' => 'то',
      'category_title' => CategorySeeder::CATEGORY_STRATEGY['title'],
      'products' => [
        ProductSeeder::PRODUCT6,
        ProductSeeder::PRODUCT8,
      ],
    ],
    [
      'like' => 'ко',
      'category_title' => '',
      'products' => self::TEST_BY_PRODUCT_TITLE_DATA['ко'],
    ],
    [
      'like' => '',
      'category_title' => CategorySeeder::CATEGORY_CATEGORY['title'],
      'products' => [
        ProductSeeder::PRODUCT1,
        ProductSeeder::PRODUCT2,
        ProductSeeder::PRODUCT3,
        ProductSeeder::PRODUCT4,
      ],
    ],
  ];

  /**
   * Проверяем, что находятся правильно заполненные товары по поиску по части названия.
   */
  public function testSearchByQuery(): void {
    foreach (self::TEST_BY_PRODUCT_TITLE_DATA as $search => $result) {
      $response = $this->getJson(route(self::SEARCH_ROUTE, ['like' => $search]));
      $response->assertStatus(Response::HTTP_OK);

      $this->sort($result);
      $response->assertJson($result);
    }
  }

  /**
   * Проверяем, что находятся правильно заполненные товары по поиску по части названия категории.
   */
  public function testSearchByCategory(): void {
    foreach (self::TEST_BY_CATEGORY_TITLE_DATA as $search => $result) {
      $response = $this->getJson(route(self::SEARCH_ROUTE, ['category_title' => $search]));
      $response->assertStatus(Response::HTTP_OK);

      $this->sort($result);
      $response->assertJson($result);
    }
  }

  /**
   * Проверяем, что находятся правильно заполненные товары
   * по поиску по части названия товара и части названия категории.
   */
  public function testSearchByQueryAndCategory(): void {
    foreach (self::TEST_BY_CATEGORY_AND_PRODUCT_DATA as $data) {
      $result = $data['products'];
      unset($data['products']);

      $response = $this->getJson(route(self::SEARCH_ROUTE, $data));
      $response->assertStatus(Response::HTTP_OK);

      $this->sort($result);
      $response->assertJson($result);
    }
  }

  /**
   * Проверяем реакцию на отсутствующие данные запроса.
   */
  public function testMissingSearchString(): void {
    $response = $this->getJson(route(self::SEARCH_ROUTE));
    $response->assertStatus(Response::HTTP_BAD_REQUEST);
    $response->assertJson(['type' => 'error']);
  }
}
