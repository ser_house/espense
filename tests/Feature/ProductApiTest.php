<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.11.2019
 * Time: 20:43
 */

namespace Tests\Feature;

use Database\Seeders\CategorySeeder;
use Illuminate\Http\Response;
use Database\Seeders\ProductSeeder;
use Tests\TestCaseDb;

/**
 * Class ProductApiTest
 *
 * @package Tests\Feature
 */
class ProductApiTest extends TestCaseDb {

  private const ADD_ROUTE = 'api.product.add';
  private const SHOW_ROUTE = 'api.product.show';
  private const RENAME_ROUTE = 'api.product.rename';
  private const MOVE_TO_CATEGORY_ROUTE = 'api.product.move_to_category';
  private const REMOVE_ROUTE = 'api.product.remove';

  public function testShowSuccess(): void {
    $id = ProductSeeder::PRODUCT1['id'];

    $response = $this->getJson(route(self::SHOW_ROUTE, ['id' => $id]));
    $response->assertStatus(Response::HTTP_OK);
    $expected = [
      'type' => 'success',
      'item' => ProductSeeder::PRODUCT1,
    ];
    $response->assertJson($expected);
  }

  public function testAddButCategoryNotFound(): void {
    $data = [
      'title' => 'Product',
      'category_title' => 'Category',
    ];
    $response = $this->postJson(route(self::ADD_ROUTE), $data);
    $response->assertStatus(Response::HTTP_BAD_REQUEST);
    $response->assertJson(['type' => 'error']);
  }

  public function testByCategoryIdSuccess(): void {
    $data = [
      'title' => 'Product',
      'category_id' => CategorySeeder::CATEGORY_CATEGORY['id'],
    ];
    $response = $this->postJson(route(self::ADD_ROUTE), $data);
    $response->assertStatus(Response::HTTP_CREATED);
    $response->assertJson(['type' => 'success']);
  }

  public function testAddButExists(): void {
    $data = [
      'title' => ProductSeeder::PRODUCT1['title'],
      'category_id' => CategorySeeder::CATEGORY_CATEGORY['id'],
    ];
    $response = $this->postJson(route(self::ADD_ROUTE), $data);
    $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    $response->assertJson(['type' => 'exists']);
  }

  public function testRenameSuccess(): void {
    $id = ProductSeeder::PRODUCT1['id'];

    $response = $this->getJson(route(self::SHOW_ROUTE, ['id' => $id]));
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson([
      'item' => [
        'title' => ProductSeeder::PRODUCT1['title'],
      ],
    ]);

    $data = [
      'title' => 'Product',
    ];
    $response = $this->putJson(route(self::RENAME_ROUTE, ['id' => $id]), $data);
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['type' => 'success']);

    $response = $this->getJson(route(self::SHOW_ROUTE, ['id' => $id]));
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson([
      'item' => [
        'title' => 'Product',
      ],
    ]);
  }

  public function testMoveToCategorySuccess(): void {
    $id = ProductSeeder::PRODUCT1['id'];

    $data = [
      'category_id' => CategorySeeder::CATEGORY_STRATEGY['id'],
    ];
    $response = $this->putJson(route(self::MOVE_TO_CATEGORY_ROUTE, ['id' => $id]), $data);
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['type' => 'success']);

    $response = $this->get(route(self::SHOW_ROUTE, ['id' => $id]));

    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson([
      'item' => [
        'category' => [
          'id' => CategorySeeder::CATEGORY_STRATEGY['id'],
        ],
      ],
    ]);
  }

  public function testDeleteSuccess(): void {
    $id = ProductSeeder::PRODUCT8['id'];

    $response = $this->deleteJson(route(self::REMOVE_ROUTE, ['id' => $id]));
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['type' => 'success']);
  }

  public function testDeleteFailProductBusy(): void {
    $id = ProductSeeder::PRODUCT1['id'];

    $response = $this->deleteJson(route(self::REMOVE_ROUTE, ['id' => $id]));
    $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    $response->assertJson(['type' => 'error']);
  }
}
