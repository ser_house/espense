<?php

namespace Tests\Feature;

use Database\Seeders\CategorySeeder;
use Illuminate\Http\Response;
use Tests\TestCaseDb;

class CategoryApiTest extends TestCaseDb {
  private const SEARCH_ROUTE = 'api.categories.find';
  private const ADD_ROUTE = 'api.category.add';
  private const SHOW_ROUTE = 'api.category.show';
  private const RENAME_ROUTE = 'api.category.rename';
  private const REMOVE_ROUTE = 'api.category.remove';

  private const TEST_BY_TITLE_DATA = [
    'те' => [
      CategorySeeder::CATEGORY_CATEGORY,
      CategorySeeder::CATEGORY_STRATEGY,
    ],
  ];

  public function testSearchByQuery(): void {
    foreach (self::TEST_BY_TITLE_DATA as $search => $result) {
      $response = $this->getJson(route(self::SEARCH_ROUTE, ['like' => $search]));
      $response->assertStatus(Response::HTTP_OK);

      $this->sort($result);
      $response->assertJson($result);
    }

    $title = CategorySeeder::CATEGORY_CATEGORY['title'];
    $response = $this->getJson(route(self::SEARCH_ROUTE, ['title' => $title]));
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson([CategorySeeder::CATEGORY_CATEGORY]);

    $title = 'some custom title';
    $response = $this->getJson(route(self::SEARCH_ROUTE, ['title' => $title]));
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson([]);
  }

  public function testMissingSearchString(): void {
    $response = $this->getJson(route(self::SEARCH_ROUTE, []));
    $response->assertStatus(Response::HTTP_BAD_REQUEST);
    $response->assertJson(['type' => 'error']);
  }

  public function testAddSuccess(): void {
    $data = [
      'title' => 'New category title',
    ];
    $response = $this->postJson(route(self::ADD_ROUTE), $data);
    $response->assertStatus(Response::HTTP_CREATED);
    $response->assertJson(['type' => 'success']);
  }

  public function testShowSuccess(): void {
    $id = CategorySeeder::CATEGORY_CATEGORY['id'];

    $response = $this->getJson(route(self::SHOW_ROUTE, ['id' => $id]));
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['type' => 'success']);
    $response->assertJson([
      'item' => [
        'title' => CategorySeeder::CATEGORY_CATEGORY['title'],
      ],
    ]);
  }

  public function testAddButExists(): void {
    $data = [
      'title' => CategorySeeder::CATEGORY_CATEGORY['title'],
    ];
    $response = $this->postJson(route(self::ADD_ROUTE), $data);
    $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    $response->assertJson(['type' => 'exists']);
  }

  public function testRenameSuccess(): void {
    $id = CategorySeeder::CATEGORY_CATEGORY['id'];

    $data = [
      'title' => 'Category',
    ];
    $response = $this->putJson(route(self::RENAME_ROUTE, ['id' => $id]), $data);
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['type' => 'success']);

    $response = $this->getJson(route(self::SHOW_ROUTE, ['id' => $id]));
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson([
      'item' => [
        'title' => 'Category',
      ],
    ]);
  }

  public function testDeleteSuccess(): void {
    $data = [
      'title' => 'New category title',
    ];
    $response = $this->postJson(route(self::ADD_ROUTE), $data);
    $result = json_decode($response->content());

    $id = $result->category->id;

    $response = $this->deleteJson(route(self::REMOVE_ROUTE, ['id' => $id]));
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['type' => 'success']);
  }

  public function testTryToDeleteWithProductsFail(): void {
    $id = CategorySeeder::CATEGORY_CATEGORY['id'];
    $response = $this->deleteJson(route(self::REMOVE_ROUTE, ['id' => $id]));
    $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    $response->assertJson(['type' => 'error']);
  }
}
