<?php

namespace Tests\Feature;

use Database\Seeders\UnitSeeder;
use Illuminate\Http\Response;
use Tests\TestCaseDb;

class UnitApiTest extends TestCaseDb {
  private const ADD_ROUTE = 'api.unit.add';
  private const SHOW_ROUTE = 'api.unit.show';
  private const RENAME_ROUTE = 'api.unit.rename';
  private const REMOVE_ROUTE = 'api.unit.remove';


  public function testAddSuccess(): void {
    $data = [
      'title' => 'New unit title',
    ];
    $response = $this->postJson(route(self::ADD_ROUTE), $data);
    $response->assertStatus(Response::HTTP_CREATED);
    $response->assertJson(['type' => 'success']);
  }

  public function testAddButExists(): void {
    $data = [
      'title' => UnitSeeder::UNIT1['title'],
    ];
    $response = $this->postJson(route(self::ADD_ROUTE), $data);
    $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    $response->assertJson(['type' => 'exists']);
  }

  public function testShowSuccess(): void {
    $id = UnitSeeder::UNIT1['id'];

    $response = $this->getJson(route(self::SHOW_ROUTE, ['id' => $id]));
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['type' => 'success']);
  }

  public function testRenameSuccess(): void {
    $id = UnitSeeder::UNIT1['id'];

    $data = [
      'title' => 'New unit title',
    ];
    $response = $this->putJson(route(self::RENAME_ROUTE, ['id' => $id]), $data);
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['type' => 'success']);

    $response = $this->getJson(route(self::SHOW_ROUTE, ['id' => $id]));
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson([
      'item' => [
        'title' => 'New unit title',
      ],
    ]);
  }

  public function testDeleteSuccess(): void {
    $id = UnitSeeder::UNIT3['id'];

    $response = $this->deleteJson(route(self::REMOVE_ROUTE, ['id' => $id]));
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['type' => 'success']);
  }

  public function testDeleteFailBusy(): void {
    $id = UnitSeeder::UNIT1['id'];

    $response = $this->deleteJson(route(self::REMOVE_ROUTE, ['id' => $id]));
    $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    $response->assertJson(['type' => 'error']);
  }
}
