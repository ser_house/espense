<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.11.2019
 * Time: 11:34
 */


namespace Tests;


use App\Core\IEventDispatcher;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Tests\Feature\CustomTestResponse;


class TestCaseDb2 extends BaseTestCase {
  use CreateApplication;
  use DatabaseMigrations;
  use RefreshDatabase;

  /** @var IEventDispatcher */
  protected $eventDispatcher;

  protected function setUp(): void {
    parent::setUp();

    $this->eventDispatcher = $this->app->make(IEventDispatcher::class);

    $this->artisan('db:seed');
  }

  // Для случая, когда мы хотим посмотреть тестовую базу после тестов.
//	use RefreshDatabase; не использовать
//	/**
//	 * Define hooks to migrate the database before and after each test.
//	 *
//	 * @return void
//	 */
//	public function runDatabaseMigrations() {
//
//		$this->artisan('migrate:fresh');
//	}

  /**
   * Сортирует результат аналогично серверной сортировке результатов поиска.
   *
   * @param array $items
   */
  protected function sort(array &$items): void {

    usort($items, static function (array $left, array $right) {
      return $left['title'] <=> $right['title'];
    });
  }

  protected function createTestResponse($response) {
    return CustomTestResponse::fromBaseResponse($response);
  }
}
