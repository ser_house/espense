<?php

namespace Tests;


use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Feature\CustomTestResponse;


class TestCaseDb extends TestCase {
  use CreateApplication;
  use DatabaseMigrations;

//  use RefreshDatabase;

  protected function setUp(): void {
    parent::setUp();

//    $this->artisan('db:seed');
    $this->runDatabaseMigrations();
  }

  // Для случая, когда мы хотим посмотреть тестовую базу после тестов.
//	use RefreshDatabase; не использовать
  /**
   * Define hooks to migrate the database before and after each test.
   *
   * @return void
   */
  public function runDatabaseMigrations() {

    $this->artisan('migrate:fresh --seed');

    $this->app[Kernel::class]->setArtisan(null);

//    $this->beforeApplicationDestroyed(function () {
//      $this->artisan('migrate:rollback');
//
//      RefreshDatabaseState::$migrated = false;
//    });
  }

  /**
   * Сортирует результат аналогично серверной сортировке результатов поиска.
   *
   * @param array $items
   */
  protected function sort(array &$items): void {

    usort($items, static function (array $left, array $right) {
      return $left['title'] <=> $right['title'];
    });
  }

  protected function createTestResponse($response) {
    return CustomTestResponse::fromBaseResponse($response);
  }
}
