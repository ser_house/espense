export class Errors {
	errors = Array;
	uniqueErrors = Object;

	addError(index, field, error_text) {
		if ('undefined' == typeof this.errors[index]) {
			this.errors[index] = {};
		}

		this.errors[index][field] = error_text;

		if ('undefined' == typeof this.uniqueErrors[field]) {
			this.uniqueErrors[field] = error_text;
		}
	}
	hasFieldErrors(index, field) {
		return 'undefined' != typeof this.errors[index]
			&& 'undefined' != typeof this.errors[index][field]
			&& this.errors[index][field].length > 0;
	}
	fieldError(index, field) {
		if (!this.hasFieldErrors(index, field)) {
			return '';
		}

		return this.errors[index][field];
	}
	hasErrors() {
		return this.errors.length > 0;
	}
	reset() {
		this.errors = [];
		this.uniqueErrors = {};
	}
	resetFieldErrors(index, field) {
		if (!this.hasFieldErrors(index, field)) {
			return;
		}
		this.errors[index][field] = '';
	}
	listUniqueErrors() {
		if (!this.hasErrors()) {
			return {};
		}
		return Object.values(this.uniqueErrors);
	}
}
