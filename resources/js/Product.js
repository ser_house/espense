import {Item} from './Item';

export class Product extends Item {
	constructor(itemFromServer = null) {
		super(itemFromServer);

		this.category = itemFromServer ? {
			id: itemFromServer.category.id,
			title: itemFromServer.category.title,
			parent: itemFromServer.category.parent,
		} : null;
	}

	hasCategoryTitle() {
		return this.category ? this.category.title.length : false;
	}

	categoryTitle() {
		return this.category ? this.category.title : '';
	}
}
