/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue';

window.Vue = Vue;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));
Vue.config.productionTip = false;

Vue.component('totals', require('./components/Totals').default);

// https://github.com/shakee93/vue-toasted
import Toasted from 'vue-toasted';
Vue.use(Toasted, {
	duration: 5000,
	keepOnHover: true
});

Vue.config.keyCodes = {
	"d": 68,
	"s": 83,
};

function trimEmptyTextNodes (el) {
	for (let node of el.childNodes) {
		if (node.nodeType === Node.TEXT_NODE && node.data.trim() === '') {
			node.remove();
		}
		else {
			trimEmptyTextNodes(node);
		}
	}
}

Vue.directive('trim-whitespace', {
	inserted: trimEmptyTextNodes,
	componentUpdated: trimEmptyTextNodes
});
