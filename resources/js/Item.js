export class Item {
	constructor(itemFromServer = null) {
		this.id = itemFromServer ? itemFromServer.id : null;
		this.title = itemFromServer ? itemFromServer.title : '';
	}

	hasTitle() {
		return this.title.length;
	}

	hasId() {
		return null !== this.id;
	}
}
