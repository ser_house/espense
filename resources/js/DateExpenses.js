export class DateExpenses {
  date = '';
  expenses = [];

  constructor(date) {
    this.date = date;
  }

  add(expense) {
    this.expenses.push(expense);
  }

  update(expense) {
    for (let i in this.expenses) {
      if (this.expenses[i].id === expense.id) {
        this.expenses[i] = expense;
        return;
      }
    }
  }

  clearFavoriteFlag(expense_id) {
    for (let i in this.expenses) {
      if (this.expenses[i].id === expense_id) {
        this.expenses[i].is_favorite = false;
        return;
      }
    }
  }

  setTrackingFlag(expense_id) {
    for (let i in this.expenses) {
      if (this.expenses[i].id === expense_id) {
        this.expenses[i].is_tracking = true;
        return;
      }
    }
  }

  setUnit(unit) {
    for (let i in this.expenses) {
      this.expenses[i].unit = unit;
    }
  }

  remove(expense) {
    for (let i in this.expenses) {
      if (this.expenses[i].id === expense.id) {
        this.expenses.splice(i, 1);
        return;
      }
    }
  }

  total() {
    let total = 0.0;
    for (let expense of this.expenses) {
      total += parseFloat(expense.amount);
    }

    return total;
  }

  countExpenses() {
    return this.expenses.length;
  }
}
