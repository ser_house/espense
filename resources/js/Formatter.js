import {Product} from './Product';
import {Item} from './Item';

export const CATEGORY_PRODUCT_DELIMITER = ' / ';

export class Formatter {

  isStrHasDelimiterLast(str) {
    return str.lastIndexOf(CATEGORY_PRODUCT_DELIMITER) === str.length - 1;
  }

  /**
   * @param {Product} product
   *
   * @returns {string}
   */
  product(product) {
    if (!product || !product.title) {
      return '';
    }

    return this.productTitle(product);
  }

  /**
   * @param {Product} product
   *
   * @returns {string}
   */
  _productTitleLevel1(product) {
    if (product.category) {
      return product.category.title + CATEGORY_PRODUCT_DELIMITER + product.title;
    }
    return product.title;
  }

  /**
   * @param {Product} product
   *
   * @returns {string}
   */
  productTitle(product) {
    if (product.category) {
      if (product.category.parent) {
        return product.category.parent.title + CATEGORY_PRODUCT_DELIMITER + this._productTitleLevel1(product);
      }
      else {
        return this._productTitleLevel1(product);
      }
    }
    return product.title;
  }

  /**
   * @param {Product} product
   *
   * @returns {string}
   */
  productCategory(product) {
    if (product.category.parent) {
      return product.category.parent.title + CATEGORY_PRODUCT_DELIMITER + product.category.title;
    }
    else {
      return product.category.title;
    }
  }

  /**
   * @param {string} str
   * @param {string} search_str
   *
   * @returns {string}
   */
  foundStrHighlighted(str, search_str) {
    let re = new RegExp(search_str + '(?!([^<]+)?<)', 'gi');
    return str.replace(re, '<span class="found-highlight">$&</span>');
  }

  /**
   * @param {Product} foundProduct
   * @param {Product} searchData
   *
   * @returns {string}
   */
  foundProductFormatted(foundProduct, searchData) {
    if (!foundProduct || (!foundProduct.hasTitle() && !foundProduct.hasCategoryTitle()) || !searchData) {
      return '';
    }

    let product = JSON.parse(JSON.stringify(foundProduct));
    product.title = this.foundStrHighlighted(product.title, searchData.title);

    if (product.category) {
      if (searchData.category) {
        product.category.title = this.foundStrHighlighted(product.category.title, searchData.category.title);
      }

      return this.productTitle(product);
    }

    return product.title;
  }

  /**
   * @param {Item} foundContractor
   * @param {Item} searchData
   *
   * @returns {string}
   */
  foundContractorFormatted(foundContractor, searchData) {
    if (!foundContractor || !foundContractor.hasTitle() || !searchData) {
      return '';
    }

    let contractor = JSON.parse(JSON.stringify(foundContractor));
    contractor.title = this.foundStrHighlighted(contractor.title, searchData.title);

    return contractor.title;
  }

  /**
   * @param {Item} contractor
   *
   * @returns {string}
   */
  contractor(contractor) {
    if (!contractor || !contractor.title) {
      return '';
    }

    return contractor.title;
  }

  /**
   * @param {string} title
   * @param {string} search
   * @param {number} min_search_len
   *
   * @returns {string}
   */
  searchableTitle(title, search, min_search_len = 2) {
    if (search.length < min_search_len) {
      return title;
    }
    return this.foundStrHighlighted(title, search);
  }
}
