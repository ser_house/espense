import axios from 'axios';

export default {
  add(title) {
    return axios.post(route('api.unit.add'), {
      title: title,
    });
  },

  rename(unit_id, new_title) {
    return axios.put(route('api.unit.rename', {id: unit_id}), {
      title: new_title,
    });
  },

  remove(unit_id) {
    return axios.delete(route('api.unit.remove', {id: unit_id}));
  },
};
