import axios from 'axios';

export default {

  add(expense_id, start_date, qty) {
    return axios.post(route('api.tracking.add'), {
      expense_id: expense_id,
      start_date: start_date,
      quantity: qty,
    });
  },

  finish(tracking_id, finished_date) {
    return axios.post(route('api.tracking.finish', {id: tracking_id}), {
      finished_date: finished_date,
    });
  },

  updateQty(tracking_id, new_qty) {
    return axios.post(route('api.tracking.update_qty', {id: tracking_id}), {
      new_qty: new_qty,
    });
  },

  removeExpense(expense_id) {
    return axios.delete(route('api.tracking.remove_expense_from_to_tracking', {id: expense_id}));
  },
};
