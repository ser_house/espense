import axios from 'axios';

export default {

  getByCategory(category_id) {
    return axios.get(route('api.analytics.by_category', {id: category_id}));
  },

  getByTime(params) {
    return axios.get(route('api.analytics.by_time', params));
  },
};
