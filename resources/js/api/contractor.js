import axios from 'axios';

export default {
  add(title) {
    return axios.post(route('api.contractor.add'), {
      title: title,
    });
  },

  find(str) {
    return axios.get(route('api.contractors.find', {like: str}));
  },

  change(contractor) {
    return axios.put(route('api.contractor.change', {id: contractor.id}), {
      title: contractor.title,
      url: contractor.url,
      note: contractor.note,
    });
  },

  remove(id) {
    return axios.delete(route('api.contractor.remove', {id: id}));
  },
};
