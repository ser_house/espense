import axios from 'axios';

export default {

  add(category_id, title) {
    return axios.post(route("api.product.add"), {
      category_id: category_id,
      title: title
    });
  },

  rename(product_id, new_title) {
    return axios.put(route('api.product.rename', {id: product_id}), {
      title: new_title,
    });
  },

  remove(product_id) {
    return axios.delete(route('api.product.remove', {id: product_id}));
  },

  moveToCategory(product_id, category_id) {
    return axios.put(route('api.product.move_to_category', {id: product_id}), {
      category_id: category_id,
    });
  },

  find(params) {
    return axios.get(route('api.products.find'), {
      params: params,
    });
  },
};
