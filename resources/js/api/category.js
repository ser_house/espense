import axios from 'axios';

export default {
  add(title, parent_id) {
    return axios.post(route('api.category.add'), {
      title: title,
      parent_id: parent_id,
    });
  },

  rename(category_id, new_title) {
    return axios.put(route('api.category.rename', {id: category_id}), {
      title: new_title,
    });
  },

  remove(category_id) {
    return axios.delete(route('api.category.remove', {id: category_id}));
  },

  moveToCategory(category_id, target_id = null) {
    if (null === target_id) {
      return axios.put(route('api.category.move_to_category', {id: category_id}));
    }
    else {
      return axios.put(route('api.category.move_to_category', {id: category_id}), {
        target_category_id: target_id,
      });
    }
  },

  findByTitle(title) {
    return axios.get(route('api.categories.find', {title: title}));
  },
};
