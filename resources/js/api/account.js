import axios from 'axios';

export default {
  add(account) {
    return axios.post(route('api.account.add'), {
      title: account.title,
      contractor_id: account.contractor.id,
    });
  },

  rename(account) {
    return axios.put(route('api.account.rename', {id: account.id}), {
      title: account.title,
    });
  },

  setContractorId(account) {
    return axios.put(route('api.account.set_contractor', {id: account.id}), {
      contractor_id: account.contractor.id,
    });
  },

  clearContractorId(account) {
    return axios.put(route('api.account.clear_contractor', {id: account.id}));
  },

  remove(account_id) {
    return axios.delete(route('api.account.remove', {id: account_id}));
  },

  findByTitle(title) {
    return axios.get(route('api.account.find', {title: title}));
  },
};
