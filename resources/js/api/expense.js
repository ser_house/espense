import axios from 'axios';

export default {
  add(data) {
    return axios.post(route('api.expense.add'), data);
  },

  update(expense_id, data) {
    return axios.put(route('api.expense.update', {id: expense_id}), data);
  },

  setUnit(data) {
    return axios.put(route('api.expense.set_unit', data));
  },

  split(expense_id, new_expenses) {
    return axios.post(route('api.expense.split', {id: expense_id}), {
      expenses: new_expenses,
    });
  },

  remove(expense_id) {
    return axios.delete(route('api.expense.remove', {id: expense_id}));
  },

  add_to_tracking(expense_id) {
    return axios.post(route('api.tracking.add_expense_to_tracking', {id: expense_id}));
  },

  add_to_favorites(expense_id) {
    return axios.put(route('api.expense.add_to_favorites', {id: expense_id}));
  },

  remove_from_favorites(expense_id) {
    return axios.put(route('api.expense.remove_from_favorites', {id: expense_id}));
  },

  find(params) {
    return axios.get(route('api.expense.find'), {
      params: params,
    });
  },
};
