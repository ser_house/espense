import axios from 'axios';

export default {
  add(data) {
    return axios.post(route('api.transaction.add'), data);
  },

  update(transaction_id, data) {
    return axios.put(route('api.transaction.update', {id: transaction_id}), data);
  },

  remove(transaction_id) {
    return axios.delete(route('api.transaction.remove', {id: transaction_id}));
  },

  find(params) {
    return axios.get(route('api.transaction.find'), {
      params: params,
    });
  },
};
