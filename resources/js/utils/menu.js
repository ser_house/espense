window.addEventListener('DOMContentLoaded', function() {

  document.querySelectorAll('.dropdown-toggle').forEach(function(el) {
    el.addEventListener('click', (event) => {
      el.parentElement.classList.toggle('opened');
    });
  });

  document.addEventListener('click', (event) => {
    if (!event.target.closest('.dropdown')) {
      document.querySelectorAll('.dropdown').forEach((el) => {
        el.classList.remove('opened');
      });
    }
  });
});
