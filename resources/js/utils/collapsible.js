window.addEventListener('DOMContentLoaded', function() {

  document.querySelectorAll('.collapsible > .toggle-link').forEach(function(el) {
    el.addEventListener('click', (event) => {
      let content = el.parentElement.getElementsByClassName('toggle-content')[0];
      content.classList.toggle('collapsed');
      el.classList.toggle('collapsed');

      let icon_right = el.getElementsByClassName('icon-expand');

      if (icon_right.length) {
        icon_right[0].classList.add('icon-collapse');
        icon_right[0].classList.remove('icon-expand');
      }
      else {
        let icon_down = el.getElementsByClassName('icon-collapse');
        if (icon_down.length) {
          icon_down[0].classList.add('icon-expand');
          icon_down[0].classList.remove('icon-collapse');
        }
      }
    });
  });
});
