export let expenseValidator = {
  methods: {
    /**
     * @param {Object} expense
     * @param {string} expense.date
     * @param {Object} expense.product
     * @param {string} expense.product.id
     * @param {string} expense.product.title
     * @param {Object} expense.product.category
     * @param {Object} expense.product.category.id
     * @param {Object} expense.product.category.title
     * @param {number} expense.quantity
     * @param {number} expense.amount
     *
     * @returns {Map} errors
     */
    validate(expense) {
      let map = new Map();

      if (!expense.date.length) {
        map.set('date', 'Дата обязательна.');
      }
      if (!expense.product || !expense.product.id || !expense.product.id.length) {
        map.set('product', 'Товар обязателен.');
      }
      if (!expense.quantity.toString().length) {
        map.set('quantity', 'Кол-во обязательно.');
      }
      if (null === expense.amount || !expense.amount.toString().length) {
        map.set('amount', 'Сумма обязательна.');
      }

      return map;
    },
  },
};
