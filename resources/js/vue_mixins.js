export let msgToUser = {
  methods: {
    showErrors: function (errors) {
      for (let error of errors) {
        this.$toasted.error(error);
      }
    },
    showError: function (error) {
      this.$toasted.error(error);
    },
    showInfo: function (text) {
      this.$toasted.info(text, {
        duration: 10000,
        fullWidth: false,
        action: {
          text: 'закрыть',
          onClick: (e, toastObject) => {
            toastObject.goAway(0);
          },
        },
      });
    },
    processResponse: function (response) {
      if ('undefined' === typeof response.data.type) {
        this.showErrors([response.data.msg]);
      }

      switch (response.data.type) {
        case 'success':
          this.processSuccessInResponse(response);
          break;

        case 'error':
          this.processErrorsInResponse(response);
          break;

        default:
          this.showErrors([response.data.msg]);
          break;
      }
    },
    processErrorsInResponse: function (response) {
      this.$toasted.error(response.data.msg);
      if ('undefined' !== typeof response.data.errors) {
        this.showErrors(response.data.errors);
      }
    },
    processSuccessInResponse: function (response) {
      this.$toasted.success(response.data.msg);
      if ('undefined' !== typeof response.data.warning) {
        this.$toasted.info(response.data.warning);
      }
    },
    processCatch(response) {
      this.$toasted.error(response);
    },
  },
};

export let autocomplete = {
  methods: {
    onArrowDown() {
      if (this.selectedIndex < this.results.length) {
        this.selectedIndex = this.selectedIndex + 1;
      }
    },
    onArrowUp() {
      if (this.selectedIndex > 0) {
        this.selectedIndex = this.selectedIndex - 1;
      }
    },
    onEsc() {
      this.clearResult();
      this.$emit('clear');
    },
    onTab() {
      if (-1 !== this.selectedIndex) {
        this.setResult(this.results[this.selectedIndex]);
        this.selectedIndex = -1;
      }
    },
    handleClickOutside(event) {
      if (!this.$el.contains(event.target)) {
        this.clearResult();
        this.selectedIndex = -1;
      }
    },
    resetFindTimeout(timer) {
      clearTimeout(timer);
    },
    clearResult() {
      this.results = [];
      this.isOpen = false;
    },
  },
  mounted() {
    document.addEventListener('click', this.handleClickOutside);
  },
  destroyed() {
    document.removeEventListener('click', this.handleClickOutside);
  },
};

export let routeAdapter = {
  methods: {
    route: function (route_name) {
      return route(route_name);
    },
  },
};

import {money_format, quantity_format} from './functions';

export let filters = {
  filters: {
    amount: function (value) {
      return money_format(value) + ' р.';
    },
    quantity(quantity) {
      return quantity_format(quantity);
    },
    /**
     * @param {Item} item
     *
     * @returns {string}
     */
    item(item) {
      if (null === item) {
        return '';
      }
      return 'string' === typeof item ? item : item.title;
    },
    nl2br(str) {
      return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1<br>$2');
    },
  },
};
