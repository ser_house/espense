require('../../app');

Vue.component('products-manage', require('../../components/manage/ProductsManage').default);

// https://github.com/cameronhimself/vue-drag-drop
import VueDragDrop from 'vue-drag-drop';
Vue.use(VueDragDrop);

const app = new Vue({
  el: '#app',
});
