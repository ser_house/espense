require('../../app');

Vue.component('read-only-autocomplete', require('../../components/ReadOnlyAutocomplete').default);
Vue.component('year', require('../../components/analytics/calendar/Year').default);

const app = new Vue({
	el: '#app',
});
