require('../../app');

Vue.component('journal', require('../../components/expense/Journal/Journal').default);
Vue.component('journal-filter-ext', require('../../components/expense/Journal/FilterExt').default);
Vue.component('journal-set-units', require('../../components/expense/Journal/SetUnits').default);

const app = new Vue({
  el: '#app',
});
