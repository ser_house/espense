require('../../app');

Vue.component('journal', require('../../components/expense/Journal/Journal').default);
Vue.component('journal-filter-month', require('../../components/expense/Journal/FilterMonth.vue').default);
Vue.component('journal-set-units', require('../../components/expense/Journal/SetUnits').default);

const app = new Vue({
  el: '#app',
});
