<?php

use App\Core\Formatter;
use App\Core\Account\AccountItem;

/**
 * @var AccountItem[] $accounts
 */
?>
<div class="home-link"><a href="{{ url('/') }}">{{ config('app.name', 'Laravel') }}@if(config('app.debug'))
      dev
    @endif</a></div>
@if($accounts)
  <div class="totals">
    @foreach($accounts as $account)
      <div class="totals-item">
        <span class="title">{{$account->title}}</span>
        <span class="amount">{!! Formatter::formatAmount($account->remain) !!}</span>
      </div>
  @endforeach
@endif
