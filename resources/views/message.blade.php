<?php
/**
 * @var App\Core\Generic\View\Message $message
 */
?>
@extends('layouts.app')
@section('page_title', $message->page_title ?? 'Сообщение')

@section('content')
	<h1>{!! $message->title !!}</h1>
	<div class="alert alert-{!! $message->type !!}">{!! $message->text !!}</div>
@endsection
