<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.10.2019
 * Time: 10:28
 *
 * @var array $timezones
 * @var string $current_tz
 * @var array $icon_classes
 */
?>

@extends('layouts.app')
@section('page_title', 'Иконки')

@push('styles')
  <style>
    ol .icon {
      margin-right: .5em;
    }
  </style>
@endpush
@push('scripts')
  <script src="{{ mix('js/page/base.js') }}"></script>
@endpush
@section('content')
  <h1>Иконки</h1>
  <ol>
    @foreach($icon_classes as $icon_class)
      <li><span class="icon {!! $icon_class !!}"></span>{!! $icon_class !!}</li>
    @endforeach
  </ol>
@endsection
