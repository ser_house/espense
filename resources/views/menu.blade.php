<ul class="menu">
  @component('menu_link', ['route' => 'expense.page'])
    Новая операция
  @endcomponent
  @component('menu_link', ['route' => 'expense.journal_month', 'active' =>  request()->routeIs(['expense.journal_month', 'expense.journal_ext', 'accounts', 'transactions'])])
    Журнал
  @endcomponent
  <li>
    Расходование
    <ul>
      @component('menu_link', ['route' => 'tracking.to_tracking'])
        Расходы
      @endcomponent
      @component('menu_link', ['route' => 'tracking'])
        Текущее
      @endcomponent
      @component('menu_link', ['route' => 'tracking.finished'])
        Завершенные
      @endcomponent
    </ul>
  </li>
  <li>
    Аналитика
    <ul>
      @component('menu_link', ['route' => 'analytics'])
        По категориям
      @endcomponent
      @component('menu_link', ['route' => 'analytics.by_time'])
        По времени
      @endcomponent
      @component('menu_link', ['route' => 'analytics.year'])
        За год
      @endcomponent
      @component('menu_link', ['route' => 'analytics.month'])
        За месяц
      @endcomponent
    </ul>
  </li>
  <li>
    Управление
    <ul>
      @component('menu_link', ['route' => 'manage.accounts'])
        Счета
      @endcomponent
      @component('menu_link', ['route' => 'manage.products'])
        Категории и товары
      @endcomponent
      @component('menu_link', ['route' => 'manage.categories_list'])
        Категории и товары списком
      @endcomponent
      @component('menu_link', ['route' => 'manage.contractors'])
        Контрагенты
      @endcomponent
      @component('menu_link', ['route' => 'manage.units'])
        Единицы измерения
      @endcomponent
    </ul>
  </li>
  @component('menu_link', ['route' => 'settings.page'])
    Настройки
  @endcomponent
  @component('menu_link', ['route' => 'icons'])
    Иконки
  @endcomponent
</ul>
