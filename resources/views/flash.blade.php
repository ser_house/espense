@if (\Session::has('dbg'))
	<pre>{!! \Session::get('dbg') !!}</pre>
@endif

@foreach(['success', 'error', 'warning'] as $type)
	@if (\Session::has($type))
		<div class="alert alert-{!! $css_class !!}">
			<div>{!! \Session::get($type) !!}</div>
		</div>
	@endif
@endforeach
