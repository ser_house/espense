<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.10.2019
 * Time: 10:28
 *
 * @var array $timezones
 * @var string $current_tz
 * @var array $icon_classes
 */
?>

@extends('layouts.app')
@section('page_title', 'Настройки')
@push('scripts')
  <script src="{{ mix('js/page/base.js') }}"></script>
@endpush
@section('content')
  <h1>Настройки</h1>
  <form id="settings-form" method="post" action="{{route('settings.save')}}">
    {{csrf_field()}}
    <div class="form-group">
      <label for="tz">Временная зона</label>
      <select class="form-control" id="tz" name="tz">
        @foreach($timezones as $key => $title)
          <option value="{!! $key !!}" @if($current_tz == $key)selected @endif>{!! $title !!}</option>
        @endforeach
      </select>
    </div>
    <div class="form-group">
      <button type="submit" class="btn btn-primary btn-sm">Сохранить</button>
    </div>
  </form>
@endsection
