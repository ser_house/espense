<?php
  $title = 'Расходы для отслеживания';
?>
@extends('layouts.app')
@section('page_title', $title)
@push('scripts')
  <script src="{{ mix('js/page/tracking/to_tracking.js') }}"></script>
@endpush
@section('content')
  <h1>{!! $title !!}</h1>
  <to-tracking :init-items='@json($items)'></to-tracking>
@endsection
