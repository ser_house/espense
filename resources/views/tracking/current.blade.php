<?php
?>
@extends('layouts.app')
@section('page_title', 'Текущее расходование')
@push('scripts')
  <script src="{{ mix('js/page/tracking/current.js') }}"></script>
@endpush
@section('content_class', 'tracking-current')
@section('content')
  <h1>Текущее расходование</h1>
  <current :init-items='@json($items)'></current>
@endsection
