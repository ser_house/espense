<?php
/**
 * @var $items \App\Core\Tracking\Views\ViewArchive[]
 * @var $finished_products \App\Core\Tracking\DTO\FinishedTrackingProduct[]
 */
?>
@extends('layouts.app')
@section('page_title', 'Завершенные')
@section('content_class', 'finished-tracking-content')
@section('content')
  <h1>Завершенные расходы</h1>
  @if ($finished_products)
    <table class="table table-sm table-striped">
      <caption>Расход в день</caption>
      <thead>
      <tr>
        <th>Товар</th>
        <th>Кол-во</th>
        <th>Дней</th>
        <th>В день</th>
      </tr>
      </thead>
      <tbody>
      @foreach($finished_products as $finished_product)
        <tr>
          <td>{{$finished_product->title}}</td>
          <td>{{$finished_product->qty}} ({{$finished_product->unit}})</td>
          <td>{{$finished_product->days}}</td>
          <td>{{$finished_product->in_day}}</td>
        </tr>
      @endforeach
      </tbody>
    </table>
  @endif
  @if ($items)
    <table class="table table-sm table-striped">
      <caption>Отдельные расходы</caption>
      <thead>
      <tr>
        <th>Товар</th>
        <th>Кол-во</th>
        <th>Дата начала</th>
        <th>Дата завершения</th>
        <th>Дней</th>
      </tr>
      </thead>
      <tbody>
      @foreach($items as $item)
        <tr>
          <td>{{ $item->expense->product->title }}</td>
          <td>{{ $item->quantity }} ({{ $item->expense->unit->title }})</td>
          <td>{{ $item->started_date }}</td>
          <td>{{ $item->finished_date }}</td>
          <td>{{ $item->days }}</td>
        </tr>
      @endforeach
      </tbody>
    </table>
  @endif
@endsection
