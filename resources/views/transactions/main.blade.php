<?php
/**
 * @var App\Core\Transaction\View[] $items
 */
?>
@extends('layouts.app')
@section('page_title', 'Переводы')

@section('content')
  <h1>Переводы</h1>
  <div class="tabs">
    <ul class="tabs-links">
      <li>
        <a href="{{route('expense.journal_month')}}" class="tab-link">За месяц</a>
      </li>
      <li>
        <a href="{{route('expense.journal_ext')}}" class="tab-link">Расширенный</a>
      </li>
      <li>
        <a href="{{route('accounts')}}" class="tab-link">Счета</a>
      </li>
      <li class="active">
        <a href="{{route('transactions')}}" class="tab-link">Переводы</a>
      </li>
    </ul>
    <div class="tab-content">
      @if(empty($items))
        Нет переводов.
      @else
        <table class="table table-sm table-bordered table-striped table-hover">
          <thead>
          <tr>
            <th scope="col">Дата</th>
            <th scope="col">Счёт-источник</th>
            <th scope="col">Счёт назначения</th>
            <th scope="col" class="expense-amount">Сумма</th>
            <th scope="col">Примечание</th>
          </tr>
          </thead>
          <tbody>
          @foreach($items as $item)
            <tr>
              <td>{!! $item->date !!}</td>
              <td>{!! $item->accountSrc->title !!}</td>
              <td>{!! $item->accountTarget->title !!}</td>
              <td>{!! $item->amount !!}</td>
              <td>{!! $item->note !!}</td>
            </tr>
          @endforeach
          </tbody>
        </table>
      @endif
    </div>
  </div>
@endsection
