<?php

use App\Core\Formatter;
use App\Core\Account\OpType;

/**
 * @var App\Core\Generic\DTO\Item[] $items
 * @var \App\Core\Account\AccountOperation[] $ops
 */
?>
@extends('layouts.app')
@section('page_title', 'Счета')

@section('content')
  <h1>Счета</h1>
  <div class="tabs">
    <ul class="tabs-links">
      <li>
        <a href="{{route('expense.journal_month')}}" class="tab-link">За месяц</a>
      </li>
      <li>
        <a href="{{route('expense.journal_ext')}}" class="tab-link">Расширенный</a>
      </li>
      <li class="active">
        <a href="{{route('accounts')}}" class="tab-link">Счета</a>
      </li>
      <li>
        <a href="{{route('transactions')}}" class="tab-link">Переводы</a>
      </li>
    </ul>
    <div class="tab-content">
      @if(empty($items))
        Нет счетов.
      @else
        <ul class="accounts-history">
          @foreach($items as $item)
            <li class="account-title">{!! $item->title !!}<span class="account-remain @if(!$item->remain->isEmpty()) has @endif">(остаток: {!! Formatter::formatAmount($item->remain) !!})</span></li>
            @if (!empty($ops[$item->id]))
              <table class="table table-sm table-bordered table-striped">
                <tbody>
                @foreach($ops[$item->id] as $op)
                  <tr @if(OpType::Incoming !== $op->type)class="out"@endif>
                    <td class="date">{!! $op->date->format('d.m.Y') !!}</td>
                    <td>{!! $op->type->label() !!}</td>
                    <td class="title">{!! $op->title !!}</td>
                    <td class="expense-amount">{!! OpType::Incoming === $op->type ? '' : '-' !!}{!! Formatter::formatAmount($op->amount) !!}</td>
                    <td class="note">{!! $op->note !!}</td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            @endif
          @endforeach
        </ul>
      @endif
    </div>
  </div>
@endsection
