<?php
/**
 * @var string $input
 */
?>
@extends('layouts.app')
@section('page_title', 'Импорт расходов')

@section('content')
	@spaceless
	<h1>Импорт расходов</h1>
	<form id="import-form" method="POST" action="{{route('expenses_import')}}">
		@csrf
		<div class="form-group">
			<label for="input">Вставьте данные</label>
			<textarea class="form-control" id="input" name="input" rows="15" required>{{$input}}</textarea>
		</div>
		<div class="form-group form-actions">
			<input class="btn btn-primary" type="submit" value="Импортировать">
		</div>
	</form>
	@endspaceless
@endsection
