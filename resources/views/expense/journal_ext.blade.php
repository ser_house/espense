<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.09.2019
 * Time: 15:57
 *
 * @var string $category
 * @var string $product
 * @var string $contractor
 * @var bool $is_favorite
 * @var string $selected_sort_mode
 * @var array $selected_years
 * @var array $years
 * @var array $selected_months
 * @var array $months
 * @var array $any_item
 * @var App\Core\Expense\DTO\Item[] $items
 * @var array $units [id => title]
 */
?>
@extends('layouts.app')
@section('page_title', 'Журнал')

@push('scripts')
  <script src="{{ mix('js/page/expense/journal_ext.js') }}"></script>
@endpush
@section('content_class', 'journal-page journal-page-ext')
@section('content')
  <h1>Журнал расширенный</h1>
  <div class="tabs">
    <ul class="tabs-links">
      <li>
        <a href="{{route('expense.journal_month')}}" class="tab-link">За месяц</a>
      </li>
      <li class="active">
        <a href="{{route('expense.journal_ext')}}" class="tab-link">Расширенный</a>
      </li>
      <li>
        <a href="{{route('accounts')}}" class="tab-link">Счета</a>
      </li>
      <li>
        <a href="{{route('transactions')}}" class="tab-link">Переводы</a>
      </li>
    </ul>
    <div class="tab-content">
      <journal-filter-ext
        :years='@json($years)'
        :selected-years='@json($selected_years)'
        :months='@json($months)'
        :selected-months='@json($selected_months)'
        :any-item='@json($any_item)'
        selected-date="{!! $date ?? '' !!}"
        category="{!! $category ?? '' !!}"
        :selected-product='@json($selected_product)'
        contractor="{!! $contractor ?? '' !!}"
        :is-favorite='@json($is_favorite)'
        selected-sort-mode="{!! $selected_sort_mode !!}"
        csrf='@csrf'
      ></journal-filter-ext>
      @if(request()->has('product'))
        <journal-set-units :init-units='@json($units)'></journal-set-units>
      @endif
      <journal :items='@json($items)' :init-units='@json($units)'></journal>
    </div>
  </div>
@endsection
