<?php
/**
 * @var App\Core\Expense\Main\View $expense
 * @var App\Core\Transaction\View $transaction
 * @var array $units [id => title]
 * @var App\Core\Expense\Main\View[] $items
 * @var App\Core\Expense\Main\View $last_expense_item
 */
$defaultAccount = new \App\Core\Account\DefaultSrcAccount();
?>
@extends('layouts.app')
@section('page_title', 'Новая операция')

@push('scripts')
  <script src="{{ mix('js/page/expense/main.js') }}"></script>
@endpush
@section('content')
  <h1>Новая операция</h1>
  <main-page
    :expense='@json($expense)'
    :transaction='@json($transaction)'
    :accounts='@json($accounts)'
    :init-units='@json($units)'
    :today-items='@json($items)'
    :init-favorite-items='@json($favorite_items)'
    :last-expense='@json($last_expense_item)'
    default-account-id="{!! $defaultAccount->id !!}"
  ></main-page>
@endsection
