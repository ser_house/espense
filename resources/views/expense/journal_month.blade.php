<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.09.2019
 * Time: 15:57
 *
 * @var string $category
 * @var string $product
 * @var string $contractor
 * @var bool $is_favorite
 * @var string $selected_sort_mode
 * @var array $selected_years
 * @var array $years
 * @var array $selected_months
 * @var array $months
 * @var array $any_item
 * @var App\Core\Expense\DTO\Item[] $items
 * @var array $units [id => title]
 */
?>
@extends('layouts.app')
@section('page_title', 'Журнал')

@push('scripts')
  <script src="{{ mix('js/page/expense/journal_month.js') }}"></script>
@endpush
@section('content_class', 'journal-page journal-page-month')
@section('content')
  <h1>Журнал</h1>
  <div class="tabs">
    <ul class="tabs-links">
      <li class="active">
        <a href="{{route('expense.journal_month')}}" class="tab-link">За месяц</a>
      </li>
      <li>
        <a href="{{route('expense.journal_ext')}}" class="tab-link">Расширенный</a>
      </li>
      <li>
        <a href="{{route('accounts')}}" class="tab-link">Счета</a>
      </li>
      <li>
        <a href="{{route('transactions')}}" class="tab-link">Переводы</a>
      </li>
    </ul>
    <div class="tab-content">
      <journal-filter-month
        category="{!! $category ?? '' !!}"
        :init-product='@json($product)'
        contractor="{!! $contractor ?? '' !!}"
        sort-mode="{!! $sort_mode !!}"
        :month='@json($month)'
        month-title="{!! $month_title !!}"
        year="{!! $year !!}"
        :any-item='@json($any_item)'
        prev-route="{!! $prev_route !!}"
        next-route="{!! $next_route !!}"
        csrf='@csrf'
      ></journal-filter-month>
      <journal :items='@json($items)' :init-units='@json($units)'></journal>
    </div>
  </div>
@endsection
