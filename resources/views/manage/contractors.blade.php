<?php

use App\Core\Contractor\DTO\ManageItem as Item;

/**
 * @var Item[] $items
 */
?>
@extends('layouts.app')
@section('page_title', 'Контрагенты')

@push('styles')
	<link href="{{ mix('css/manage.css') }}" rel="stylesheet">
@endpush
@push('scripts')
	<script src="{{ mix('js/page/manage/contractors.js') }}"></script>
@endpush
@section('content')
	<h1>Контрагенты</h1>
	<contractors-manage :init-contractors='@json($items)'></contractors-manage>
@endsection
