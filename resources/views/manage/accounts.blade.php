<?php

use App\Core\Contractor\DTO\ManageItem as Item;

/**
 * @var Item[] $items
 */
?>
@extends('layouts.app')
@section('page_title', 'Счета')

@push('styles')
  <link href="{{ mix('css/manage.css') }}" rel="stylesheet">
@endpush
@push('scripts')
  <script src="{{ mix('js/page/manage/accounts.js') }}"></script>
@endpush
@section('content')
  <h1>Счета</h1>
  <accounts-manage :init-accounts='@json($items)'></accounts-manage>
@endsection
