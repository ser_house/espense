<?php

use App\Core\Unit\DTO\ManageItem as Item;

/**
 * @var Item[] $items
 */
?>
@extends('layouts.app')
@section('page_title', 'Единицы измерения')

@push('styles')
	<link href="{{ mix('css/manage.css') }}" rel="stylesheet">
@endpush
@push('scripts')
	<script src="{{ mix('js/page/manage/units.js') }}"></script>
@endpush
@section('content')
	<h1>Единицы измерения</h1>
	<units-manage :init-units='@json($items)'></units-manage>
@endsection
