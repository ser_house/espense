<?php

use App\Core\Category\DTO\ManageItem as CategoryItem;


/**
 * @var CategoryItem[] $category_items
 */
?>
@extends('layouts.app')
@section('page_title', 'Категории и товары')

@push('styles')
	<link href="{{ mix('css/manage.css') }}" rel="stylesheet">
@endpush
@push('scripts')
	<script src="{{ mix('js/page/manage/products.js') }}"></script>
@endpush
@section('content')
	<h1>Категории и товары</h1>
	<products-manage :categories='@json($category_items)'></products-manage>
@endsection
