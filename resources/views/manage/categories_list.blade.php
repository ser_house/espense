<?php
/**
 * @var string[] $categories
 */
?>
@extends('layouts.app')
@section('page_title', 'Категории и товары списком')

@push('styles')
	<link href="{{ mix('css/manage.css') }}" rel="stylesheet">
@endpush
@push('scripts')
	<script src="{{ mix('js/page/base.js') }}"></script>
@endpush
@section('content')
	<h1>Категории и товары списком</h1>
	<ul>
		@foreach($categories as $category)
			<li>{{$category}}</li>
		@endforeach
	</ul>
@endsection
