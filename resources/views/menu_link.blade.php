<?php
/**
 * @var $active bool
 * @var $route string
 */
$active = $active ?? request()->routeIs($route);
?>
<li @if($active)class="active" @endif><a href="{{ route($route) }}">{{ $slot }}</a></li>
