<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>@yield('page_title', config('app.name', 'Laravel'))</title>

	<!-- Styles -->
	<link href="{{ mix('css/app.css') }}" rel="stylesheet">
	@stack('styles')
</head>
<body>
<div id="app" class="content-wrapper">
	@spaceless
	<header>
		@component('header')@endcomponent
	</header>
	<aside>@component('menu')@endcomponent</aside>
	<section class="content @yield('content_class', '')">
		@component('flash')@endcomponent
		@yield('content')
	</section>
	@endspaceless
</div>
@routes

@push('scripts')
	<script src="{{ mix('js/utils/tooltip.js') }}"></script>
	<script src="{{ mix('js/utils/collapsible.js') }}"></script>
	<script src="{{ mix('js/utils/menu.js') }}"></script>
@endpush

@stack('scripts')
</body>
</html>
