<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.09.2019
 * Time: 17:30
 *
 * @var array $categories
 * @var array $years
 * @var array $group_by_options
 * @var array $sum_by_options
 */
?>
@extends('layouts.app')
@section('page_title', 'По времени')

@push('scripts')
	<script src="{{ mix('js/page/analytics/by_time.js') }}"></script>
@endpush
@section('content')
	<h1>По времени</h1>
	<div class="analytics-by-time">
		<by-time
			:categories='@json($categories)'
			:years='@json($years)'
			:group-by-options='@json($group_by_options)'
			:sum-by-options='@json($sum_by_options)'
		></by-time>
@endsection
