<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 26.09.2019
 * Time: 18:55
 *
 * @var App\Core\Analytics\Calendar\Month\Item $month
 * @var array $by_categories
 * @var array $months
 * @var array $weekday_names
 * @var string $prev_route
 * @var string $next_route
 */
?>
@extends('layouts.app')
@section('page_title', 'За месяц')

@push('styles')
	<link href="{{ mix('css/calendar.css') }}" rel="stylesheet">
@endpush

@push('scripts')
	<script src="{{ mix('js/page/analytics/month.js') }}"></script>
@endpush

@section('content')
	<h1>За месяц</h1>
		<month
			:weekday-names='@json($weekday_names)'
			:data='@json($month)'
			:by-categories='@json($by_categories)'
			:months='@json($months)'
			prev-route="{!! $prev_route !!}"
			next-route="{!! $next_route !!}"
		></month>
@endsection
