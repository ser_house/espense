<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 26.09.2019
 * Time: 18:55
 *
 * @var App\Core\Analytics\Calendar\Year\Item $yearData
 * @var array $grouped_main
 * @var App\Core\Expense\DTO\Item[] $by_months
 * @var array $month_names
 * @var string $prev_route
 * @var string $next_route
 * @var string|null $category
 */
?>
@extends('layouts.app')
@section('page_title', 'За год')

@push('styles')
	<link href="{{ mix('css/calendar.css') }}" rel="stylesheet">
@endpush

@push('scripts')
	<script src="{{ mix('js/page/analytics/year.js') }}"></script>
@endpush

@section('content')
	<h1>За год</h1>
	@spaceless
	<form class="filter-form flexed" method="GET">
		<read-only-autocomplete id="category" name="category"
			query-route='{{ route('api.categories.find') }}' value="{{ $category ?? '' }}">
			<template v-slot:label><label for="category">Категория</label></template>
		</read-only-autocomplete>
		<read-only-autocomplete id="product" name="product"
			query-route='{{ route('api.products.find') }}' value="{{ $product ?? '' }}">
			<template v-slot:label><label for="product">Товар</label></template>
		</read-only-autocomplete>
		<div class="form-group">
			<label for="year">Год</label>
			<select class="form-control" id="year" name="year" >
				@foreach($years as $key => $title)
					<option value="{!! $key !!}" @if($year == $key)selected @endif>{!! $title !!}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group form-actions">
			<button class="btn btn-sm btn-primary" type="submit">Применить</button>
		</div>
		@csrf
	</form>
	@endspaceless
	<year
		:month-names='@json($month_names)'
		:data='@json($yearData)'
		:grouped-main='@json($grouped_main)'
		:by-months='@json($by_months)'
		year="{!! $year !!}"
		prev-route="{!! $prev_route !!}"
		next-route="{!! $next_route !!}"
	></year>
@endsection
