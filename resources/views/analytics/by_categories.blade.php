<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.09.2019
 * Time: 17:30
 *
 * @var array $items
 */
?>
@extends('layouts.app')
@section('page_title', 'По категориям')

@push('scripts')
	<script src="{{ mix('js/page/analytics/by_categories.js') }}"></script>
@endpush
@section('content')
	<h1>По категориям</h1>
	@if(!empty($items))
		<div class="analytics-by-categories">
			<by-categories :init-items='@json($items)'></by-categories>
		</div>
	@else
		Нет записей о расходах.
	@endif

@endsection
